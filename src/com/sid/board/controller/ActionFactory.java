package com.sid.board.controller;

import com.sid.board.controller.action.Action;
import com.sid.board.controller.action.BoardCheckPassAction;
import com.sid.board.controller.action.BoardCheckPassFormAction;
import com.sid.board.controller.action.BoardDeleteAction;
import com.sid.board.controller.action.BoardListAction;
import com.sid.board.controller.action.BoardUpdateAction;
import com.sid.board.controller.action.BoardUpdate2Action;
import com.sid.board.controller.action.BoardViewAction;
import com.sid.board.controller.action.BoardWriteAction;
import com.sid.board.controller.action.ListFAQAction;

public class ActionFactory {
	private static ActionFactory instance = new ActionFactory();

	private ActionFactory() {
		super();
	}

	public static ActionFactory getInstance() {
		return instance;
	}

	public Action getAction(String command) {
		Action action = null;

		if (command.equals("board_list")) {
			action = new BoardListAction();
			//공지사항 글쓰기
		} else if (command.equals("board_write")) {
			action = new BoardWriteAction();
			//공지사항 보기
		} else if (command.equals("board_view")) {
			action = new BoardViewAction();
		} else if (command.equals("board_check_pass_form")) {
			action = new BoardCheckPassFormAction();
		} else if (command.equals("board_check_pass")) {
			action = new BoardCheckPassAction();
			
			//공지사항 수정된거 다시 등록
		} else if (command.equals("board_update2")) {
			action = new BoardUpdate2Action();
			//공지사항 수정하기
		} else if (command.equals("board_update")) {
			action = new BoardUpdateAction();
			//공지사항 삭제
		} else if (command.equals("board_delete")) {
			action = new BoardDeleteAction();
			// 자주하는 질문 불러오기
		} else if (command.equals("list_faq")) {
			action = new ListFAQAction();
		}
		return action;
	}
}
