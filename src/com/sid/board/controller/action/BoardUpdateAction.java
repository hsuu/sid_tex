package com.sid.board.controller.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sid.dao.BoardDAO;
import com.sid.dto.BoardVO;


public class BoardUpdateAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String url="board/boardUpdate.jsp";
		
		BoardVO bVo = new BoardVO();
		BoardDAO bDao=BoardDAO.getInstance();
		
		int contentNum=Integer.parseInt(request.getParameter("num"));
		
		bVo=bDao.selectBoard(contentNum);
		
		request.setAttribute("vo", bVo);
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
		
	}
}
