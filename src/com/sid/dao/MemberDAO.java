package com.sid.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.sid.dto.CompanyVO;
import com.sid.dto.UserVO;
import com.sid.util.DBManager;

public class MemberDAO {
	private MemberDAO() {

	}

	private static MemberDAO instance = new MemberDAO() {
	};

	public static MemberDAO getInstance() {
		return instance;
	}

	// 일반 회원 가입
	public int insertUser(UserVO vo) {
		int result = -1;
		String sql = "insert into user(email,pwd,name,nation,phone,address1,zipnum1,admin,salt) values(?,?,?,?,?,?,?,?,?)";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, vo.getEmail());
			pstmt.setString(2, vo.getPwd());
			pstmt.setString(3, vo.getName());
			pstmt.setInt(4, vo.getNation());
			pstmt.setString(5, vo.getPhone());
			pstmt.setString(6, vo.getAddress1());
			pstmt.setString(7, vo.getZipnum1());
			pstmt.setInt(8, vo.getAdmin());
			pstmt.setString(9, vo.getSalt());

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	// 기업회원 가입
	public int insertUserB(UserVO vo) {
		int result = -1;

		String sql = "insert into user(email,pwd,name,nation,phone,address1,zipnum1,admin,salt) values(?,?,?,?,?,?,?,?,?)";
		String sql2 = "insert into user_b(rep_name,rep_num,rep_phone,company,email,fullCompany,companyAddress,companyZipnum,rep_num_flag,"
				+ "delivery_way,returnZipnum,returnRoadAddrPart1,returnAddrDetail,deliveryCompany_quick,delivery_period,delivery,"
				+ "delivery_cost,delivery_cost2,re_delivery_cost1,delivery_company) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, vo.getEmail());
			pstmt.setString(2, vo.getPwd());
			pstmt.setString(3, vo.getName());
			pstmt.setInt(4, vo.getNation());
			pstmt.setString(5, vo.getPhone());
			pstmt.setString(6, vo.getAddress1());
			pstmt.setString(7, vo.getZipnum1());
			pstmt.setInt(8, vo.getAdmin());
			pstmt.setString(9, vo.getSalt());
			
		

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, pstmt);
		}
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql2);

			pstmt.setString(1, vo.getRep_name());
			pstmt.setString(2, vo.getRep_num());
			pstmt.setString(3, vo.getRep_phone());
			pstmt.setString(4, vo.getCompany());
			pstmt.setString(5, vo.getEmail());
			pstmt.setString(6, vo.getFullCompany());
			pstmt.setString(7, vo.getCompanyAddress());
			pstmt.setString(8, vo.getCompanyZipnum());
			pstmt.setInt(9,vo.getRep_num_flag());
			pstmt.setInt(10, vo.getDelivery_way());
			pstmt.setString(11, vo.getReturnZipnum());
			pstmt.setString(12, vo.getReturnRoadAddrPart1());
			pstmt.setString(13, vo.getReturnAddrDetail());
			pstmt.setString(14, vo.getDeliveryCompany_quick());
			pstmt.setInt(15, vo.getDelivery_period());
			pstmt.setInt(16,vo.getDelivery());
			pstmt.setInt(17, vo.getDelivery_cost());
			pstmt.setInt(18, vo.getDelivery_cost2());
			pstmt.setInt(19, vo.getRe_delivery_cost1());
			pstmt.setString(20, vo.getDeliveryCompany());
			
			
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, pstmt);
		}

		return result;
	}

	// 회원 이메일 인증 후 권한 주기
	public int selectAuth(String email, String auth) {
		int result = -1;
		String sql = "select emailchk from user where email=?";

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, email);

			rs = pstmt.executeQuery();
			while (rs.next()) {
				if (rs.getString("emailchk").equals(auth)) {
					result = 1;
				}
			}

		} catch (Exception e) {
			System.out.println("select auth error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	// 회원 이메일 인증 후 권한 주기
	public int updateAuth(String email) {
		int result = -1;
		String sql = "update user set emailchk=1 where email=?";

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, email);

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("update auth error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	// 이메일로 임시 비밀번호 보내고 임시 비밀번호로 바꾸기
	public int updateTempPassword(String email, String pwd) {
		int result = -1;
		String sql = "update user set pwd=? where email=?";

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, pwd);
			pstmt.setString(2, email);

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("update to temp password error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	// 이메일 중복체크
	public int confirmID(String email) {
		int result = -1;
		String sql = "select * from user where email=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				result = 1;
			} else {
				result = -1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, pstmt, rs);
		}
		return result;
	}
	// 판매자명 중복검사
		public int confirmCompany(String company) {
			int result = -1;
			String sql = "select * from user_b where company=?";
			Connection conn = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			try {
				conn = DBManager.getConnection();
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, company);
				rs = pstmt.executeQuery();
				if (rs.next()) {
					result = 1;
				} else {
					result = -1;
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				DBManager.close(conn, pstmt, rs);
			}
			return result;
		}

	public UserVO userCheck(String email, String pwd) {
		int result = -1;
		String sql = "select pwd,admin,emailchk from user where email=?";
		String sql2 = "select company,rep_num_flag,rep_name,rep_num,fullCompany,rep_phone,companyAddress from user_b where email=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		UserVO vo = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();
			vo = new UserVO();
			if (rs.next()) {
				if (rs.getString("pwd") != null // 비밀번호 맞으면
						&& rs.getString("pwd").equals(pwd)) {
					vo.setAdmin(rs.getInt("admin"));
					vo.setEmailchk(rs.getInt("emailchk"));

					// 권한을 가져옴 (2 or 1 or 0)
				} else { // 비밀 번호 틀리면

					vo.setAdmin(-1);
				}
			} else {// 결과가 없으면
				vo.setAdmin(-1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, pstmt, rs);
		}
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql2);
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();
			if (rs.next()){
				vo.setCompany(rs.getString("company"));
				vo.setRep_num_flag(rs.getInt("rep_num_flag"));
				vo.setRep_name(rs.getString("rep_name"));
				vo.setRep_num(rs.getString("rep_num"));
				vo.setFullCompany(rs.getString("fullCompany"));
				vo.setRep_phone(rs.getString("rep_phone"));
				vo.setCompanyAddress(rs.getString("companyAddress"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, pstmt, rs);
		}
		return vo;
	}

	// salt 가져오기
	public String getUserSalt(String email) {
		String salt = "";
		String sql = "select salt from user where email=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				salt = rs.getString("salt");
			}

		} catch (Exception e) {
			System.out.println("get user salt error" + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return salt;
	}

	// 패스워드 확인
	public int passwordCheck(String email, String pwd) {
		int result = -1;
		String sql = "select pwd from user where email=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				if (rs.getString("pwd") != null // 비밀번호 맞으면
						&& rs.getString("pwd").equals(pwd)) {
					result = 1;
				}
			}

		} catch (Exception e) {
			System.out.println("password check error" + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	// 인증번호 넣기
	public void setAuthNum(String email, String auth) {
		String sql = "update user set emailchk=? where email=?";
		int result = 0;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, auth);
			pstmt.setString(2, email);

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("update auth num password error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return;
	}

	public UserVO getUserAddress(String email) {
		UserVO vo = null;
		String sql = "select * from user where email=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				vo = new UserVO();
				vo.setAddress1(rs.getString("address1"));
				vo.setAddress2(rs.getString("address2"));
				vo.setAddress3(rs.getString("address3"));
				vo.setAddress4(rs.getString("address4"));
				vo.setZipnum1(rs.getString("zipnum1"));
				vo.setZipnum2(rs.getString("zipnum2"));
				vo.setZipnum3(rs.getString("zipnum3"));
				vo.setZipnum4(rs.getString("zipnum4"));
			}
		} catch (Exception e) {
			System.out.println("get user address error" + e);
		} finally {
			DBManager.close(conn, pstmt, rs);
		}

		return vo;
	}

	public UserVO getMember(String email) {
		String sql = "select * from user where email=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		UserVO mVo = null;

		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				mVo = new UserVO();
				mVo.setEmail(email);
				mVo.setName(rs.getString("name"));
				mVo.setNation(rs.getInt("nation"));
				mVo.setPhone(rs.getString("phone"));
				mVo.setAddress1(rs.getString("address1"));
				mVo.setZipnum1(rs.getString("zipnum1"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, pstmt, rs);
		}

		return mVo;
	}

	public UserVO getRepMember(String email) {
		String sql2 = "select * from user_b where email=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		UserVO mVo = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql2);
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				mVo = new UserVO();
				mVo.setRep_name(rs.getString("rep_name"));
				mVo.setRep_num(rs.getString("rep_num"));
				mVo.setRep_phone(rs.getString("rep_phone"));
				mVo.setCompany(rs.getString("company"));
				mVo.setFullCompany(rs.getString("fullCompany"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, pstmt);
		}
		return mVo;
	}

	//배송 정보 수정
	public int updateDelivery(UserVO mVo) {
		int result = -1;
		String sql = "update user set address1=?,address2=?,address3=?,address4=?,zipnum1=?,"
				+ "zipnum2=?,zipnum3=?,zipnum4=? where email=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, mVo.getAddress1());
			pstmt.setString(2, mVo.getAddress2());
			pstmt.setString(3, mVo.getAddress3());
			pstmt.setString(4, mVo.getAddress4());
			pstmt.setString(5, mVo.getZipnum1());
			pstmt.setString(6, mVo.getZipnum2());
			pstmt.setString(7, mVo.getZipnum3());
			pstmt.setString(8, mVo.getZipnum4());
			pstmt.setString(9, mVo.getEmail());

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("updateDelivery error : "+e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}
	
	// 기본 정보 수정
	public int updateUser(UserVO mVo) {
		int result = -1;
		String sql = "update user set name=?,phone=? where email=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, mVo.getName());
			pstmt.setString(2, mVo.getPhone());
			pstmt.setString(3, mVo.getEmail());

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("update user error");
			e.printStackTrace();
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}
	public int updateUserB(UserVO mVo) {
		int result = -1;
		String sql = "update user_b set rep_phone=? where email=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, mVo.getRep_phone());
			pstmt.setString(2, mVo.getEmail());

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("update user B error");
			e.printStackTrace();
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	// password 수정
	public int updatePassword(UserVO mVo) {
		int result = -1;
		String sql = "update user set pwd=? where email=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, mVo.getPwd());
			pstmt.setString(2, mVo.getEmail());
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("update user password error");
			e.printStackTrace();
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	public int insertConsult(String email, String content) {
		int result = -1;
		String sql = "insert into consult(email,content) values(?,?)";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, email);
			pstmt.setString(2, content);

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("insert consult error : " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	//회원 탈퇴 - 일반회원
	public void deleteMember(String email) {
		String sql = "delete from user where email=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, email);
			pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("deleteMember error : "+e);
		} finally {
			DBManager.close(conn, pstmt);
		}
	}
	
	//회원 탈퇴 - 사업자회원
	public void deleteMember_b(String email) {
		String sql = "delete from user_b where email=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, email);
			pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("deleteMember_b error : "+e);
		} finally {
			DBManager.close(conn, pstmt);
		}
	}
	
	//회원 탈퇴 신청
		public int insertUserLeave(String email,String reason) {
			int result=-1;
			String sql = "insert into user_leave(email,reason) values(?,?)";
			Connection conn = null;
			PreparedStatement pstmt = null;
			try {
				conn = DBManager.getConnection();
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, email);
				pstmt.setString(2, reason);
				result=pstmt.executeUpdate();
			} catch (Exception e) {
				System.out.println("insertuser leave error : "+e);
			} finally {
				DBManager.close(conn, pstmt);
			}
			return result;
		}
		
		//회원 탈퇴 
		public int leaveMember(String email){
			int result = -1;
			String sql = "update user set admin=9 where email=?";
			Connection conn = null;
			PreparedStatement pstmt = null;
			try {
				conn = DBManager.getConnection();
				pstmt = conn.prepareStatement(sql);

				pstmt.setString(1, email);
				result = pstmt.executeUpdate();
			} catch (Exception e) {
				System.out.println("leave member error");
				e.printStackTrace();
			} finally {
				DBManager.close(conn, pstmt);
			}
			return result;
		}
		
		//회원 탈퇴시 상품삭제(delyn=1)
		public void updateProductDelyn(String email){
			String sql="UPDATE user a LEFT JOIN product b ON (a.email=b.email) SET b.delyn=1 WHERE a.admin=9 and a.email=?";
			Connection conn = null;
			PreparedStatement pstmt = null;
			try {
				conn = DBManager.getConnection();
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, email);
				pstmt.executeUpdate();
			} catch (Exception e) {
				System.out.println("updateProductDelyn error "+e);
				e.printStackTrace();
			} finally {
				DBManager.close(conn, pstmt);
			}
		}
		
		//회원 탈퇴할때 장바구니 db만 삭제
		public void deleteBasket(String email){
			String sql = "DELETE a FROM basket a LEFT JOIN product b ON a.productId=b.productId  WHERE b.email=?";
			Connection conn = null;
			PreparedStatement pstmt = null;
			try {
				conn = DBManager.getConnection();
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, email);
				pstmt.executeUpdate();
			} catch (Exception e) {
				System.out.println("deleteBasket error : "+e);
			} finally {
				DBManager.close(conn, pstmt);
			}
		}
		
	public UserVO readEmail(int productId) {
		String sql = "select email from product where productId=?";
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		UserVO vo = null;

		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, productId);
			rst = stmt.executeQuery();

			if (rst.next()) {
				vo = new UserVO();
				vo.setEmail(rst.getString("email"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return vo;
		}
	
	public UserVO getRepMember(UserVO vo) {
		String sql2="select * from user_b where email=?";
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		UserVO vo2 = null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql2);
			stmt.setString(1, vo.getEmail());
			rst = stmt.executeQuery();

			if (rst.next()) {
				vo2 = new UserVO();
				vo2.setRep_name(rst.getString("rep_name"));
				vo2.setRep_num(rst.getString("rep_num"));
				vo2.setRep_phone(rst.getString("rep_phone"));
				vo2.setCompany(rst.getString("company"));
				vo2.setFullCompany(rst.getString("fullCompany"));
				vo2.setCompanyAddress(rst.getString("companyAddress"));
				vo2.setCompanyZipnum(rst.getString("companyZipnum"));
				vo2.setEmail(rst.getString("email"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt);
		}
		return vo2;
	}
	
	//판매자 페이지 -배송 정보 입력
	public int insertDelivery(UserVO mVo,String email) {
		int result = -1;
		String sql = "update user_b set delivery=?,delivery_way=?,delivery_costway=?,delivery_cost=?,"
				+ "re_delivery_cost1=?,deliveryCompany=?,delivery_cost2=?"
				+ ",deliveryCompany_quick=?, delivery_period=?, returnZipnum=?, returnRoadAddrPart1=?,returnAddrDetail=? where email=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setInt(1, mVo.getDelivery());
			pstmt.setInt(2, mVo.getDelivery_way());
			pstmt.setString(3, mVo.getDelivery_costway());
			pstmt.setInt(4, mVo.getDelivery_cost());
			pstmt.setInt(5, mVo.getRe_delivery_cost1());
			
			pstmt.setString(6, mVo.getDeliveryCompany());
			pstmt.setInt(7, mVo.getDelivery_cost2());
			pstmt.setString(8, mVo.getDeliveryCompany_quick());
			pstmt.setInt(9, mVo.getDelivery_period());
			pstmt.setString(10, mVo.getReturnZipnum());
			pstmt.setString(11, mVo.getReturnRoadAddrPart1());
			pstmt.setString(12, mVo.getReturnAddrDetail());
			pstmt.setString(13, email);
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("insertDelivery error : "+e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}
	
	//판매자 페이지 -배송 정보 불러오기
	public UserVO getDelivery(String email) {
		String sql2="select * from user_b where email=?";
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		UserVO vo2 = null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql2);
			stmt.setString(1, email);
			rst = stmt.executeQuery();

			if (rst.next()) {
				vo2 = new UserVO();
				vo2.setCompany(rst.getString("company"));
				vo2.setDelivery(rst.getInt("delivery"));
				vo2.setDelivery_way(rst.getInt("delivery_way"));
				vo2.setDelivery_costway(rst.getString("delivery_costway"));
				vo2.setDelivery_cost(rst.getInt("delivery_cost"));
				vo2.setDelivery_cost2(rst.getInt("delivery_cost2"));
				vo2.setRe_delivery_cost1(rst.getInt("re_delivery_cost1"));
				vo2.setDeliveryCompany(rst.getString("deliveryCompany"));
				vo2.setDeliveryCompany_quick(rst.getString("deliveryCompany_quick"));
				vo2.setDelivery_period(rst.getInt("delivery_period"));
				vo2.setReturnZipnum(rst.getString("returnZipnum"));
				vo2.setReturnRoadAddrPart1(rst.getString("returnRoadAddrPart1"));
				vo2.setReturnAddrDetail(rst.getString("returnAddrDetail"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt);
		}
		return vo2;
	}
	//payment에서 회사정보 가져오기
	public CompanyVO getCompanyInfo(String email) {
		String sql2="select * from user_b where email=?";
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		CompanyVO vo=null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql2);
			stmt.setString(1, email);
			rst = stmt.executeQuery();

			if (rst.next()) {
				vo = new CompanyVO();
				vo.setEmail(email);
				vo.setCompany(rst.getString("company"));
				vo.setDelivery(rst.getInt("delivery"));
				vo.setDelivery_way(rst.getInt("delivery_way"));
				vo.setDelivery_costway(rst.getString("delivery_costway"));
				vo.setDelivery_cost(rst.getInt("delivery_cost"));
				vo.setDelivery_cost2(rst.getInt("delivery_cost2"));
				vo.setRe_delivery_cost1(rst.getInt("re_delivery_cost1"));
				vo.setDeliveryCompany(rst.getString("deliveryCompany"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt);
		}
		return vo;
	}

	/*
	 * public int updateAdmin(String email) { int result = -1; String sql =
	 * "update user set admin=1 where email=?";
	 * 
	 * Connection conn = null; PreparedStatement pstmt = null; try { conn =
	 * DBManager.getConnection(); pstmt = conn.prepareStatement(sql);
	 * 
	 * pstmt.setString(1, email);
	 * 
	 * result = pstmt.executeUpdate(); } catch (Exception e) {
	 * e.printStackTrace(); } finally { DBManager.close(conn, pstmt); } return
	 * result; }
	 * 
	 * public int confirmID(String email) { int result = -1; String sql =
	 * "select * from user_b a LEFT JOIN user_p b where email=?"; Connection
	 * conn = null; PreparedStatement pstmt = null; ResultSet rs = null; try {
	 * conn = DBManager.getConnection(); pstmt = conn.prepareStatement(sql);
	 * pstmt.setString(1, email); rs = pstmt.executeQuery(); if (rs.next()) {
	 * result = 1; } else { result = -1; } } catch (Exception e) {
	 * e.printStackTrace(); } finally { DBManager.close(conn, pstmt, rs); }
	 * return result; }
	 * 
	 * public int userCheck(String email, String pwd) { int result = -1; String
	 * sql = "select pwd,admin from user where email=?"; Connection conn = null;
	 * PreparedStatement pstmt = null; ResultSet rs = null; try { conn =
	 * DBManager.getConnection(); pstmt = conn.prepareStatement(sql);
	 * pstmt.setString(1, email); rs = pstmt.executeQuery(); if (rs.next()) { if
	 * (rs.getString("pwd") != null // 비밀번호 맞으면 &&
	 * rs.getString("pwd").equals(pwd)) { result=rs.getInt("admin"); //권한을 가져옴
	 * (2 or 1 or 0) } else { //비밀 번호 틀리면
	 * 
	 * result = -1; } } else {// 결과가 없으면 result = -1; }
	 * 
	 * } catch (Exception e) { e.printStackTrace(); } finally { try { if (rs !=
	 * null) rs.close(); if (pstmt != null) pstmt.close(); if (conn != null)
	 * conn.close(); } catch (Exception e) { e.printStackTrace(); } } return
	 * result; }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * public ArrayList<MemberVO> listMember(String email) { ArrayList<MemberVO>
	 * memberList = new ArrayList<MemberVO>(); String sql =
	 * "select * from user where email like '%'||?||'%' " +
	 * "  order by indate desc"; Connection conn = null; PreparedStatement pstmt
	 * = null; ResultSet rs = null; try { conn = DBManager.getConnection();
	 * pstmt = conn.prepareStatement(sql); if (email == "") { pstmt.setString(1,
	 * "%"); } else { pstmt.setString(1, email); } rs = pstmt.executeQuery();
	 * while (rs.next()) { MemberVO memberVO = new MemberVO();
	 * memberVO.setEmail(rs.getString("email"));
	 * memberVO.setPwd(rs.getString("pwd"));
	 * memberVO.setNickname(rs.getString("nickname"));
	 * memberVO.setEmail(rs.getString("email"));
	 * memberVO.setZipNum(rs.getString("zip_num"));
	 * memberVO.setAddress(rs.getString("address"));
	 * memberVO.setPhone(rs.getString("phone"));
	 * memberVO.setUseyn(rs.getString("useyn"));
	 * memberVO.setIndate(rs.getTimestamp("indate")); memberList.add(memberVO);
	 * } } catch (Exception e) { e.printStackTrace(); } finally {
	 * DBManager.close(conn, pstmt, rs); } return memberList; }
	 * 
	 * public MemberVO getAddress(String email) { MemberVO mVo = null; String
	 * sql = "select zipNum, address, phone from user where email=?"; Connection
	 * conn = null; PreparedStatement pstmt = null; ResultSet rs = null; try {
	 * conn = DBManager.getConnection(); pstmt = conn.prepareStatement(sql);
	 * pstmt.setString(1, email); rs = pstmt.executeQuery();
	 * 
	 * if (rs.next()) { mVo = new MemberVO();
	 * mVo.setZipNum(rs.getString("zipNum"));
	 * mVo.setAddress(rs.getString("address"));
	 * mVo.setPhone(rs.getString("phone")); } } catch (Exception e) {
	 * e.printStackTrace(); } finally { DBManager.close(conn, pstmt, rs); }
	 * 
	 * return mVo; }
	 */
}