package com.sid.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.sid.dto.AlarmVO;
import com.sid.dto.ConsultVO;
import com.sid.dto.ProductVO;
import com.sid.dto.UserVO;
import com.sid.util.DBManager;

public class AlarmDAO {
	private AlarmDAO() {

	}

	private static AlarmDAO instance = new AlarmDAO() {
	};

	public static AlarmDAO getInstance() {
		return instance;
	}

	// 보내기
	public int sendAlarm(AlarmVO vo) {
		int result = -1;
		String sql = "insert into alarm(sort,message,receiver,sender,reason,state) values(?,?,?,?,?,?)";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, vo.getSort());
			pstmt.setString(2, vo.getMessage());
			pstmt.setString(3, vo.getReceiver());
			pstmt.setString(4, vo.getSender());
			pstmt.setString(5, vo.getReason());
			pstmt.setInt(6, vo.getState());

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("send alarm error ");
			e.printStackTrace();
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	// 안읽은 쪽지함 읽기
	public ArrayList<AlarmVO> listUnreadAlarm(String email) {
		String sql = "select * from alarm where receiver=? and state=0 order by date desc";

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<AlarmVO> list = new ArrayList<>();
		AlarmVO vo = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, email);

			rs = pstmt.executeQuery();
			while (rs.next()) {
				vo = new AlarmVO();
				vo.setAlarmId(rs.getInt("alarmId"));
				vo.setSort(rs.getString("sort"));
				vo.setMessage(rs.getString("message"));
				vo.setReceiver(rs.getString("receiver"));
				vo.setSender(rs.getString("sender"));
				vo.setDate(rs.getDate("date"));
				vo.setState(rs.getInt("state"));
				vo.setReason(rs.getString("reason"));
				list.add(vo);
			}

		} catch (Exception e) {
			System.out.println("list all alarm error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return list;
	}
	// 안읽은 쪽지함 읽기
	public ArrayList<AlarmVO> listReadAlarm(String email) {
		String sql = "select * from alarm where receiver=? and state=1 order by date desc";

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<AlarmVO> list = new ArrayList<>();
		AlarmVO vo = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, email);

			rs = pstmt.executeQuery();
			while (rs.next()) {
				vo = new AlarmVO();
				vo.setAlarmId(rs.getInt("alarmId"));
				vo.setSort(rs.getString("sort"));
				vo.setMessage(rs.getString("message"));
				vo.setReceiver(rs.getString("receiver"));
				vo.setSender(rs.getString("sender"));
				vo.setDate(rs.getDate("date"));
				vo.setState(rs.getInt("state"));
				vo.setReason(rs.getString("reason"));
				list.add(vo);
			}

		} catch (Exception e) {
			System.out.println("list all alarm error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return list;
	}

	// 알람 상태 변경 (0:초기/1:읽음/2:삭제)
	public int updateAlarmState(int id, int state) {
		int result = -1;
		String sql = "update alarm set state=" + state + " where alarmId=?";

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setInt(1, id);

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("update alarm state error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	// 새로운 알람

	// 알람 상태 변경 (0:초기/1:읽음/2:삭제)
	public int checkNewAlarm(String email) {
		int result = -1;
		String sql = "select count(*) from alarm where receiver=? and state=0";

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, email);

			rs = pstmt.executeQuery();
			while (rs.next()) {
				result = rs.getInt(1);
			}
		} catch (Exception e) {
			System.out.println("update alarm state error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

}