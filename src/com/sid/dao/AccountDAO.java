package com.sid.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.ArrayList;

import com.sid.dto.CalculateVO;
import com.sid.dto.UserVO;
import com.sid.util.DBManager;

public class AccountDAO {
	private static AccountDAO instance=new AccountDAO(){};

	public static AccountDAO getInstance() {
		return instance;
	}

	// 정산관리 - 계좌관리 입력
	public int insertAccount(UserVO vo, String email) {
		int result = -1;
		String sql = "update user_b set bank_name=?, account_number=?,account_holder=? where email=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, vo.getBank_name());
			pstmt.setString(2, vo.getAccount_number());
			pstmt.setString(3, vo.getAccount_holder());
			pstmt.setString(4, email);

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("insertAccount error " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	// 정산관리 - 계좌관리 보기
	public UserVO getAccount(String email) {
		String sql="select * from user_b where email=?";
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		UserVO vo = null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, email);
			rst = stmt.executeQuery();

			if (rst.next()) {
				vo = new UserVO();
				vo.setBank_name(rst.getString("bank_name"));
				vo.setAccount_number(rst.getString("account_number"));
				vo.setAccount_holder(rst.getString("account_holder"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return vo;
	}
	
	//정산관리 입력
	public int insertCalculate(CalculateVO vo,String email) {
		int result = -1;
		String sql = "INSERT INTO calculate(email,purchaseConfirm_date,calculate_date,number_of_orders,"
				+ "calculate_amount,payment_amount,surtax,fee,brokerage_fee,purchaseId) values(?,?,?,?,?,?,?,?,?,?)";

		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1,email);
			stmt.setString(2, vo.getPurchaseConfirm_date());
			stmt.setString(3, vo.getCalculate_date());
			stmt.setInt(4, vo.getNumber_of_orders());
			stmt.setDouble(5, vo.getCalculate_amount());
			stmt.setDouble(6, vo.getPayment_amount());
			stmt.setDouble(7, vo.getSurtax());
			stmt.setDouble(8, vo.getFee());
			stmt.setDouble(9, vo.getBrokerage_fee());
			stmt.setString(10, vo.getPurchaseId());
			result = stmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt);
		}
		return result;
	}
	
	
	//건별 정산
	public ArrayList<CalculateVO> getCalculateByCase(String email){
		String sql="SELECT * FROM calculate where email=?";
		ArrayList<CalculateVO> list=new ArrayList<>();
		CalculateVO vo=null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, email);
			rst = stmt.executeQuery();

			while (rst.next()) {
			vo=new CalculateVO();
			vo.setPurchaseConfirm_date(rst.getString("purchaseConfirm_date"));
			vo.setCalculate_date(rst.getString("calculate_date"));
			vo.setPurchaseId(rst.getString("purchaseId"));
			
			vo.setCalculate_amount2((int)Math.ceil(rst.getDouble("calculate_amount")));
			vo.setPayment_amount2((int)Math.ceil(rst.getDouble("payment_amount")));
			vo.setSurtax2((int)Math.ceil(rst.getDouble("surtax")));
			vo.setFee2((int)Math.ceil(rst.getDouble("fee")));
			vo.setBrokerage_fee2((int)Math.ceil(rst.getDouble("brokerage_fee")));
			list.add(vo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return list;
	
	}
	
	//일별 정산 - 정산완료일 빼고
	public ArrayList<CalculateVO> getCalculateByDay(String email,String first,String end){
		String sql="SELECT email,purchaseConfirm_date,calculate_date,sum(number_of_orders) as Tnumber_of_orders, sum(calculate_amount) as Tcalculate_amount,"
				+ " sum(payment_amount)as Tpayment_amount, sum(surtax) as Tsurtax, sum(fee) as Tfee, "
				+ "sum(brokerage_fee) as Tbrokerage_fee FROM sid_tex.calculate"
				+ " where email=? and purchaseConfirm_date between ? and ? group by purchaseConfirm_date" ;
		ArrayList<CalculateVO> list=new ArrayList<>();
		CalculateVO vo=null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, email);
			stmt.setString(2, first);
			stmt.setString(3, end);
			rst = stmt.executeQuery();

			while (rst.next()) {
			vo=new CalculateVO();
			vo.setPurchaseConfirm_date(rst.getString("purchaseConfirm_date"));
			vo.setCalculate_date(rst.getString("calculate_date"));
			vo.setNumber_of_orders(rst.getInt("Tnumber_of_orders"));

			vo.setCalculate_amount2((int)Math.ceil(rst.getDouble("Tcalculate_amount")));
			vo.setPayment_amount2((int)Math.ceil(rst.getDouble("Tpayment_amount")));
			vo.setSurtax2((int)Math.ceil(rst.getDouble("Tsurtax")));
			vo.setFee2((int)Math.ceil(rst.getDouble("Tfee")));
			vo.setBrokerage_fee2((int)Math.ceil(rst.getDouble("Tbrokerage_fee")));
			list.add(vo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return list;
	}
	
}
