package com.sid.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.sid.dto.CancelOrderVO;
import com.sid.dto.PayInfoVO;
import com.sid.util.DBManager;

public class PayInfoDAO {
	public PayInfoDAO(){
		
	}
	public static PayInfoDAO instance =new PayInfoDAO();
	
	public static PayInfoDAO getInstance(){
		return instance;
	}
	
	public PayInfoVO getAccountInfo(String oid){
		String sql = "select * from payinfo where oid=?";

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		PayInfoVO vo = null;

		try {

			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, oid);
			rst = stmt.executeQuery();

			if (rst.next()) {
				vo = new PayInfoVO();
				vo.setTid(rst.getString("tid"));
				vo.setPrice(rst.getInt("price"));
				vo.setMoid(rst.getString("moid"));
				vo.setApplDate(rst.getString("applDate"));
				vo.setApplTime(rst.getString("applTime"));
				vo.setPayMethod(rst.getString("payMethod"));
				
				vo.setVact_num(rst.getString("vact_num"));
				vo.setVact_bankCode(rst.getString("vact_bankCode"));
				vo.setVact_bankName(rst.getString("vact_bankName"));
				vo.setVact_name(rst.getString("vact_name"));
				vo.setVact_inputName(rst.getString("vact_inputName"));
				vo.setVact_date(rst.getString("vact_date"));
				vo.setVact_time(rst.getString("vact_time"));
				
				vo.setAcct_bankCode(rst.getString("acct_bankCode"));
				vo.setCshr_resultCode(rst.getString("cshr_resultCode"));
				vo.setCshr_type(rst.getString("cshr_type"));
				
				vo.setKwpy_cellPhone(rst.getString("kwpy_cellPhone"));
				vo.setKwpy_salesAmount(rst.getString("kwpy_salesAmount"));
				vo.setKwpy_amount(rst.getString("kwpy_amount"));
				vo.setKwpy_tax(rst.getString("kwpy_tax"));
				vo.setKwpy_serviceFee(rst.getString("kwpy_serviceFee"));
				vo.setKwpy_payMethod(rst.getString("kwpy_payMethod"));
				vo.setKwpy_resultCode(rst.getString("kwpy_resultCode"));
				vo.setKwpy_resultMsg(rst.getString("kwpy_resultMsg"));
				vo.setKwpy_tid(rst.getString("kwpy_tid"));
				vo.setKwpy_moid(rst.getString("kwpy_moid"));
				vo.setKwpy_price(rst.getString("kwpy_price"));
				vo.setKwpy_applDate(rst.getString("kwpy_applDate"));
				vo.setKwpy_applTime(rst.getString("kwpy_applTime"));
				
				vo.setEventCode(rst.getString("eventCode"));
				vo.setCard_num(rst.getString("card_num"));
				vo.setCard_applNum(rst.getString("card_applNum"));
				vo.setCard_quota(rst.getString("card_quota"));
				vo.setCard_interest(rst.getString("card_interest"));
				vo.setCard_point(rst.getString("card_point"));
				vo.setCard_code(rst.getString("card_code"));
				vo.setCard_bankCode(rst.getString("card_bankCode"));
				
				vo.setOcb_num(rst.getString("ocb_num"));
				vo.setOcb_saveApplNum(rst.getString("ocb_saveApplNum"));
				vo.setOcb_payPrice(rst.getString("ocb_payPrice"));
				
				vo.setGspt_num(rst.getString("gspt_num"));
				vo.setGspt_remains(rst.getString("gspt_remains"));
				vo.setGspt_applPrice(rst.getString("gspt_applPrice"));
				
				vo.setUnpt_cardNum(rst.getString("unpt_cardNum"));
				vo.setUnpt_usablePoint(rst.getString("unpt_usablePoint"));
				vo.setUnpt_payPrice(rst.getString("unpt_payPrice"));
				
				vo.setOid(rst.getString("oid"));
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return vo;
	}
	
	public int insertPayInfo(PayInfoVO vo) {
		int result = -1;
		String sql = "INSERT INTO payinfo(tid, price, moid, applDate, applTime, payMethod,"
				+ "vact_num, vact_bankCode, vact_bankName, vact_name, vact_inputName, vact_date, vact_time,"
				+ "acct_bankCode, cshr_resultCode, cshr_type,"
				+ "kwpy_cellPhone, kwpy_salesAmount, kwpy_amount, kwpy_tax, kwpy_serviceFee, kwpy_payMethod,"
				+ "kwpy_resultCode, kwpy_resultMsg, kwpy_tid, kwpy_moid, kwpy_price, kwpy_applDate, kwpy_applTime,"
				+ "eventCode, card_num, card_applNum, card_quota, card_interest, card_point, card_code, card_bankCode,"
				+ "ocb_num, ocb_saveApplNum, ocb_payPrice, gspt_num, gspt_remains, gspt_applPrice,"
				+ "unpt_cardNum, unpt_usablePoint, unpt_payPrice, oid,email) values(?,?,?,?,?,?,"
				+ "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		

		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);

			stmt.setString(1, vo.getTid());
			stmt.setInt(2,vo.getPrice());
			stmt.setString(3, vo.getMoid());
			stmt.setString(4, vo.getApplDate());
			stmt.setString(5, vo.getApplTime());
			stmt.setString(6,vo.getPayMethod());
			stmt.setString(7, vo.getVact_num());
			stmt.setString(8, vo.getVact_bankCode());
			stmt.setString(9, vo.getVact_bankName());
			stmt.setString(10, vo.getVact_name());
			stmt.setString(11, vo.getVact_inputName());
			stmt.setString(12, vo.getVact_date());
			stmt.setString(13, vo.getVact_time());
			stmt.setString(14, vo.getAcct_bankCode());
			stmt.setString(15, vo.getCshr_resultCode());
			stmt.setString(16,vo.getCshr_type());
			stmt.setString(17, vo.getKwpy_cellPhone());
			stmt.setString(18, vo.getKwpy_salesAmount());
			stmt.setString(19,vo.getKwpy_amount());
			stmt.setString(20, vo.getKwpy_tax());
			stmt.setString(21, vo.getKwpy_serviceFee());
			stmt.setString(22, vo.getKwpy_payMethod());
			stmt.setString(23, vo.getKwpy_resultCode());
			stmt.setString(24, vo.getKwpy_resultMsg());
			stmt.setString(25, vo.getKwpy_tid());
			stmt.setString(26, vo.getKwpy_moid());
			stmt.setString(27, vo.getKwpy_price());
			stmt.setString(28, vo.getKwpy_applDate());
			stmt.setString(29, vo.getKwpy_applTime());
			stmt.setString(30, vo.getEventCode());
			stmt.setString(31, vo.getCard_num());
			stmt.setString(32, vo.getCard_applNum());
			stmt.setString(33, vo.getCard_quota());
			stmt.setString(34, vo.getCard_interest());
			stmt.setString(35, vo.getCard_point());
			stmt.setString(36, vo.getCard_code());
			stmt.setString(37, vo.getCard_bankCode());
			stmt.setString(38, vo.getOcb_num());
			stmt.setString(39, vo.getOcb_saveApplNum());
			stmt.setString(40, vo.getOcb_payPrice());
			stmt.setString(41, vo.getGspt_num());
			stmt.setString(42, vo.getGspt_remains());
			stmt.setString(43, vo.getGspt_applPrice());
			stmt.setString(44, vo.getUnpt_cardNum());
			stmt.setString(45, vo.getUnpt_usablePoint());
			stmt.setString(46, vo.getUnpt_payPrice());
			stmt.setString(47, vo.getOid());
			stmt.setString(48, vo.getEmail());
			result = stmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt);
		}
		return result;
	}
	
	public PayInfoVO getPayMethod(String oid){
		String sql = "select payMethod from payinfo where oid=?";

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		PayInfoVO vo = null;

		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, oid);
			rst = stmt.executeQuery();

			if (rst.next()) {
				vo = new PayInfoVO();
				vo.setPayMethod(rst.getString("payMethod"));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return vo;
	}

}
