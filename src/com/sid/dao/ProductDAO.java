package com.sid.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.sid.dto.ImageVO;
import com.sid.dto.ProductVO;
import com.sid.dto.RelateProductVO;
import com.sid.dto.UserVO;
import com.sid.util.DBManager;

public class ProductDAO {
	private ProductDAO() {

	}

	private static ProductDAO instance = new ProductDAO();

	public static ProductDAO getInstance() {
		return instance;
	}

	public int insertProduct(ImageVO iVo, ProductVO pVo, String email, String company) {
		int productId = -1;
		String sql = "INSERT INTO product(email,mainImg,itemName, expl, retail,"
				+ "retail_only,wholesale,standard,stock,colorName,state,swatch,manufacturer,manuDate,origin,"
				+ "sortName,pattern,width,thick,weight,"
				+ "thickUnit,weave,thickWp,thickWt,lengthWp,lengthWt,twistWp,twistWt,"
				+ "gagong,dye,texture,through,gloss,elastic,pliability,company,subImg5,density_weight,density_volume,widthUnit,stock_unknown,mainyn,"
				+ "delyn,color)"
				+ " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;

		try {
			conn = DBManager.getConnection();

			stmt = conn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);

			stmt.setString(1, email);
			stmt.setString(2, iVo.getMainImg());
			stmt.setString(3, pVo.getItemName());
			stmt.setString(4, pVo.getExpl());
			stmt.setInt(5, pVo.getRetail());
			stmt.setInt(6, pVo.getRetail_only());
			stmt.setInt(7, pVo.getWholesale());
			stmt.setInt(8, pVo.getStandard());
			stmt.setInt(9, iVo.getStock());
			stmt.setString(10, iVo.getColorName());
			stmt.setString(11, pVo.getState());
			stmt.setString(12, pVo.getSwatch());
			stmt.setString(13, pVo.getManufacturer());
			stmt.setString(14, pVo.getManuDate());
			stmt.setString(15, pVo.getOrigin());
			stmt.setString(16, pVo.getSortName());
			stmt.setString(17, pVo.getPattern());
			stmt.setInt(18, pVo.getWidth());
			stmt.setString(19, pVo.getThick());
			stmt.setInt(20, pVo.getWeight());
			stmt.setString(21, pVo.getThickUnit());
			stmt.setString(22, pVo.getWeave());
			stmt.setInt(23, pVo.getThickWp());
			stmt.setInt(24, pVo.getThickWt());
			stmt.setString(25, pVo.getLengthWp());
			stmt.setString(26, pVo.getLengthWt());
			stmt.setString(27, pVo.getTwistWp());
			stmt.setString(28, pVo.getTwistWt());
			stmt.setString(29, pVo.getGagong());
			stmt.setString(30, pVo.getDye());
			stmt.setString(31, pVo.getTexture());
			stmt.setString(32, pVo.getThrough());
			stmt.setString(33, pVo.getGloss());
			stmt.setString(34, pVo.getElastic());
			stmt.setString(35, pVo.getPliability());
			stmt.setString(36, company);
			stmt.setString(37, iVo.getSubImg5());
			stmt.setInt(38, pVo.getDensity_weight());
			stmt.setInt(39, pVo.getDensity_volume());
			stmt.setString(40, pVo.getWidthUnit());
			stmt.setInt(41, pVo.getStock_unknown());
			stmt.setInt(42, iVo.getMainyn());
			stmt.setInt(43, pVo.getDelyn());
			stmt.setString(44, pVo.getColor());

			stmt.executeUpdate();
			rst = stmt.getGeneratedKeys();
			String autoInsertedKey = (rst.next()) ? rst.getString(1) : rst.getString(1);

			productId = Integer.parseInt(autoInsertedKey);
		} catch (SQLException e) {
			System.out.println("ProductDAO insertProduct error : " + e);
		} finally {
			DBManager.close(conn, stmt);
		}
		return productId;
	}

	// 용도 보기
	public ArrayList<String> readUses(int productId) {
		String sql = "select * from uses where productId=?";
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		ArrayList<String> uses = null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, productId);
			rst = stmt.executeQuery();
			uses = new ArrayList<>();

			while (rst.next()) {
				uses.add((String) rst.getString("uses"));
			}

		} catch (Exception e) {
			System.out.println("ProductDAO readUses Error : " + e);
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return uses;
	}

	// 용도 입력
	public int insertUses(String uses, int productId) {
		int result = -1;
		String sql = "INSERT INTO uses(uses,productId) values(?,?)";
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);

			stmt.setString(1, uses);
			stmt.setInt(2, productId);
			result = stmt.executeUpdate();

		} catch (Exception e) {
			System.out.println("ProductDAO insertUses Error : " + e);
		} finally {
			DBManager.close(conn, stmt);
		}
		return result;
	}
	
	//용도 삭제
	public int deleteUses(int productId) {
		String sql = "delete from uses where productId=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		int result = -1;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, productId);
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("deleteUses error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}

		return result;
	}

	// 색상 보기
	public ArrayList<String> readColor(int productId) {
		String sql = "select * from color where productId=?";
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		ArrayList<String> color = null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, productId);
			rst = stmt.executeQuery();
			color = new ArrayList<>();

			while (rst.next()) {
				color.add((String) rst.getString("color"));
			}

		} catch (Exception e) {
			System.out.println("ProductDAO readColor Error : " + e);
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return color;
	}

	// 색상 입력
	public int insertColor(String color, int productId) {
		int result = -1;
		String sql = "INSERT INTO color(color,productId) values(?,?)";
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, color);
			stmt.setInt(2, productId);
			result = stmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("ProductDAO insertColor Error : " + e);
		} finally {
			DBManager.close(conn, stmt);
		}
		return result;
	}
	
	//색상 삭제
	public int deleteColor(int productId) {
		String sql = "delete from color where productId=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		int result = -1;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, productId);
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("deleteColor error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}

		return result;
	}

	// 섬유 입력
	public int insertFiber(String fiber, int productId) {
		int result = -1;
		String sql = "INSERT INTO fiber(fiber,productId) values(?,?)";
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);

			stmt.setString(1, fiber);
			stmt.setInt(2, productId);
			result = stmt.executeUpdate();

		} catch (Exception e) {
			System.out.println("ProductDAO insertFiber Error : " + e);
		} finally {
			DBManager.close(conn, stmt);
		}
		return result;
	}

	// 섬유 보기
	public ArrayList<String> readFiber(int productId) {
		String sql = "select * from fiber where productId=?";
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		ArrayList<String> fiber = null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, productId);
			rst = stmt.executeQuery();
			fiber = new ArrayList<>();

			while (rst.next()) {
				fiber.add((String) rst.getString("fiber"));
			}

		} catch (Exception e) {
			System.out.println("ProductDAO readFiber Error : " + e);
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return fiber;
	}
	
	//섬유 삭제
		public int deleteFiber(int productId) {
			String sql = "delete from fiber where productId=? and delyn=0";
			Connection conn = null;
			PreparedStatement pstmt = null;
			int result = -1;
			try {
				conn = DBManager.getConnection();
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, productId);
				result = pstmt.executeUpdate();
			} catch (Exception e) {
				System.out.println("deleteFiber error :  " + e);
			} finally {
				DBManager.close(conn, pstmt);
			}

			return result;
		}

	// 연관 상품 입력
	public int updateRelateProduct(String relate, int productId) {
		int result = -1;
		String sql = "update product set relate=? where productId=? and delyn='0'";

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, relate);
			pstmt.setInt(2, productId);

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("update relate Product error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}
	

	// 연관 상품 입력
	public int updateRelateNullProduct(int productId) {
		int result = -1;
		String sql = "update product set relate=null where productId=? and delyn='0'";

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, productId);
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("update relate null Product error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	public RelateProductVO readRelateProduct(int productId) {
		String sql = "select * from product where productId=? and delyn='0'";
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		RelateProductVO vo = null;

		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, productId);
			rst = stmt.executeQuery();

			if (rst.next()) {
				vo = new RelateProductVO();
				vo.setProductId(productId);
				vo.setMainImg(rst.getString("mainImg"));
				vo.setColorName(rst.getString("colorName"));
				vo.setStock(rst.getString("stock"));
				vo.setState(rst.getString("state"));

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}

		return vo;
	}
	
	//재고량 가져오기
	public int selectProductStock(int productId) {
		String sql = "select stock from product where productId=? and delyn='0'";
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		int stock=0;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, productId);
			rst = stmt.executeQuery();

			if (rst.next()) {
				
				stock=rst.getInt("stock");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}

		return stock;
	}

	// 특정 상품 보기
	public ProductVO readProduct(int productId) {
		String sql = "select * from product where productId=? and delyn='0'";
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		ProductVO vo = null;

		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, productId);
			rst = stmt.executeQuery();

			if (rst.next()) {
				vo = new ProductVO();
				vo.setProductID(rst.getInt("productID"));
				vo.setMainImg(rst.getString("mainImg"));
				vo.setItemName(rst.getString("itemName"));
				vo.setExpl(rst.getString("expl"));
				vo.setRetail(rst.getInt("retail"));
				vo.setWholesale(rst.getInt("wholesale"));
				vo.setStandard(rst.getInt("standard"));
				vo.setStock(rst.getInt("stock"));
				vo.setState(rst.getString("state"));
				vo.setColorName(rst.getString("colorName"));
				vo.setSwatch(rst.getString("swatch"));
				vo.setManufacturer(rst.getString("manufacturer"));
				vo.setManuDate(rst.getString("manuDate"));
				vo.setOrigin(rst.getString("origin"));
				vo.setSortName(rst.getString("sortName"));
				vo.setPattern(rst.getString("pattern"));
				vo.setWidth(rst.getInt("width"));
				vo.setThick(rst.getString("thick"));
				vo.setWeight(rst.getInt("weight"));
				vo.setWeave(rst.getString("weave"));
				vo.setThickWp(rst.getInt("thickWp"));
				vo.setThickWt(rst.getInt("thickWt"));
				vo.setLengthWp(rst.getString("lengthWp"));
				vo.setLengthWt(rst.getString("lengthWt"));
				vo.setTwistWp(rst.getString("twistWp"));
				vo.setTwistWt(rst.getString("twistWt"));
				vo.setGagong(rst.getString("gagong"));
				vo.setDye(rst.getString("dye"));
				vo.setTexture(rst.getString("texture"));
				vo.setThrough(rst.getString("through"));
				vo.setGloss(rst.getString("gloss"));
				vo.setElastic(rst.getString("elastic"));
				vo.setPliability(rst.getString("pliability"));
				vo.setPostDate(rst.getString("postDate"));
				vo.setCompany(rst.getString("company"));
				vo.setDensity_volume(rst.getInt("density_volume"));
				vo.setDensity_weight(rst.getInt("density_weight"));
				vo.setRetail_only(rst.getInt("retail_only"));
				vo.setThickUnit(rst.getString("thickUnit"));
				vo.setWidthUnit(rst.getString("widthUnit"));
				vo.setRelate(rst.getString("relate"));
				vo.setStock_unknown(rst.getInt("stock_unknown"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}

		return vo;
	}

	public int countList(){
		String sql = "SELECT COUNT(*) FROM product where delyn=0 and state='판매중'";
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		int result=0;
		
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			rst = stmt.executeQuery();
			while (rst.next()) {
				result=rst.getInt(1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return result;
	}
	// 모든 상품 보기
	public ArrayList<ProductVO> listAll_Product(int start,int size) {
		ArrayList<ProductVO> list = new ArrayList<ProductVO>();
		String sql = "select * from product where delyn=0 and state='판매중' order by postDate desc limit "+start+","+size;

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		ProductVO vo = null;

		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			rst = stmt.executeQuery();
			while (rst.next()) {
				vo = new ProductVO();
				vo.setProductID(rst.getInt("productId"));
				vo.setMainImg(rst.getString("mainImg"));
				vo.setItemName(rst.getString("itemName"));
				vo.setExpl(rst.getString("expl"));
				vo.setRetail(rst.getInt("retail"));
				vo.setWholesale(rst.getInt("wholesale"));
				vo.setStandard(rst.getInt("standard"));
				vo.setStock(rst.getInt("stock"));
				vo.setState(rst.getString("state"));
				vo.setColorName(rst.getString("colorName"));
				vo.setSwatch(rst.getString("swatch"));
				vo.setManufacturer(rst.getString("manufacturer"));
				vo.setManuDate(rst.getString("manuDate"));
				vo.setOrigin(rst.getString("origin"));
				vo.setSortName(rst.getString("sortName"));
				vo.setPattern(rst.getString("pattern"));
				vo.setWidth(rst.getInt("width"));
				vo.setThick(rst.getString("thick"));
				vo.setWeight(rst.getInt("weight"));
				vo.setWeave(rst.getString("weave"));
				vo.setThickWp(rst.getInt("thickWp"));
				vo.setThickWt(rst.getInt("thickWt"));
				vo.setLengthWp(rst.getString("lengthWp"));
				vo.setLengthWt(rst.getString("lengthWt"));
				vo.setTwistWp(rst.getString("twistWp"));
				vo.setTwistWt(rst.getString("twistWt"));
				vo.setGagong(rst.getString("gagong"));
				vo.setDye(rst.getString("dye"));
				vo.setTexture(rst.getString("texture"));
				vo.setThrough(rst.getString("through"));
				vo.setGloss(rst.getString("gloss"));
				vo.setElastic(rst.getString("elastic"));
				vo.setPliability(rst.getString("pliability"));
				vo.setPostDate(rst.getString("postDate"));
				vo.setCompany(rst.getString("company"));
				vo.setDensity_volume(rst.getInt("density_volume"));
				vo.setDensity_weight(rst.getInt("density_weight"));
				vo.setRetail_only(rst.getInt("retail_only"));
				vo.setThickUnit(rst.getString("thickUnit"));
				vo.setWidthUnit(rst.getString("widthUnit"));
				vo.setStock_unknown(rst.getInt("stock_unknown"));
				vo.setColor(rst.getString("color"));
				list.add(vo);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return list;
	}

	// 판매자 상품관리 리스트 가져오기
	public ArrayList<ProductVO> listAll(String email) {

		ArrayList<ProductVO> list = new ArrayList<ProductVO>();
		String sql = "select * from product where email=? and delyn='0'";

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		ProductVO vo = null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, email);
			rst = stmt.executeQuery();
			while (rst.next()) {
				vo = new ProductVO();
				vo.setProductID(rst.getInt("productId"));
				vo.setMainImg(rst.getString("mainImg"));
				vo.setItemName(rst.getString("itemName"));
				vo.setExpl(rst.getString("expl"));
				vo.setRetail(rst.getInt("retail"));
				vo.setWholesale(rst.getInt("wholesale"));
				vo.setStandard(rst.getInt("standard"));
				vo.setStock(rst.getInt("stock"));
				vo.setState(rst.getString("state"));
				vo.setColorName(rst.getString("colorName"));
				vo.setSwatch(rst.getString("swatch"));
				vo.setManufacturer(rst.getString("manufacturer"));
				vo.setManuDate(rst.getString("manuDate"));
				vo.setOrigin(rst.getString("origin"));
				vo.setSortName(rst.getString("sortName"));
				vo.setPattern(rst.getString("pattern"));
				vo.setWidth(rst.getInt("width"));
				vo.setThick(rst.getString("thick"));
				vo.setWeight(rst.getInt("weight"));
				vo.setWeave(rst.getString("weave"));
				vo.setThickWp(rst.getInt("thickWp"));
				vo.setThickWt(rst.getInt("thickWt"));
				vo.setLengthWp(rst.getString("lengthWp"));
				vo.setLengthWt(rst.getString("lengthWt"));
				vo.setTwistWp(rst.getString("twistWp"));
				vo.setTwistWt(rst.getString("twistWt"));
				vo.setGagong(rst.getString("gagong"));
				vo.setDye(rst.getString("dye"));
				vo.setTexture(rst.getString("texture"));
				vo.setThrough(rst.getString("through"));
				vo.setGloss(rst.getString("gloss"));
				vo.setElastic(rst.getString("elastic"));
				vo.setPliability(rst.getString("pliability"));
				vo.setPostDate(rst.getString("postDate"));
				vo.setCompany(rst.getString("company"));
				vo.setDensity_volume(rst.getInt("density_volume"));
				vo.setDensity_weight(rst.getInt("density_weight"));
				vo.setRetail_only(rst.getInt("retail_only"));
				vo.setThickUnit(rst.getString("thickUnit"));
				vo.setWidthUnit(rst.getString("widthUnit"));
				vo.setStock_unknown(rst.getInt("stock_unknown"));
				vo.setRelate(rst.getString("relate"));
				vo.setMainyn(rst.getInt("mainyn"));
				vo.setDelyn(rst.getInt("delyn"));
				list.add(vo);
			}
		} catch (SQLException e) {
			System.out.println("list error : " + e);
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return list;
	}

	public int updateProduct(String input, int productId, String column) {
		int result = -1;
		String sql = "update product set " + column + "='" + input + "' where productId=? and delyn='0'";

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setInt(1, productId);

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("update Product error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}
	
	
	public int reduceStockProduct(int productId, int amount) {
		int result = -1;
		String sql = "update product set stock=stock-" + amount + " where productId=? and delyn='0'";

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setInt(1, productId);

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("reduce stock Product error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}
	
	public int updateDelyn(int num) {
		String sql = "update product set delyn='1' where productId=? and delyn='0'";
		Connection conn = null;
		PreparedStatement pstmt = null;
		int result = -1;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, num);
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("updateDelyn error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}

		return result;
	}
	
	public int updateState(int num) {
		String sql = "update product set state='판매중' where productId=? and delyn='0'";
		Connection conn = null;
		PreparedStatement pstmt = null;
		int result = -1;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, num);
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("updateState error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}

		return result;
	}
	
	public int updateState2(int num) {
		String sql = "update product set state='판매중지' where productId=? and delyn='0'";
		Connection conn = null;
		PreparedStatement pstmt = null;
		int result = -1;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, num);
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("updateState error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}

		return result;
	}


	// 슈퍼관리자- 상품삭제
	public int deleteProduct(int num) {
		String sql = "delete from product where productId=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		int result = -1;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, num);
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("delete product error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}

		return result;
	}

	// 슈퍼관리자 -상품삭제시 연관상품 선택
	public String selectRelateProduct(int num) {
		String sql = "select relate from product where productId=?";
		String result = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, num);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				result = rs.getString("relate");
			}

		} catch (Exception e) {
			System.out.println("selectRelateProduct error : " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	public int addToBasket(int productId, String email) {
		int result = -1;
		String sql = "INSERT INTO basket(productId,email) VALUES(?,?)";
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, productId);
			pstmt.setString(2, email);

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("addToBasket error : " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	public int addToBasketWithQuantity(int productId, String email, int quantity) {
		int result = -1;
		String sql = "INSERT INTO basket(productId,email,quantity) VALUES(?,?,?)";
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, productId);
			pstmt.setString(2, email);
			pstmt.setInt(3, quantity);

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("addToBasketWithQuantity error : " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	public int updateBasketQuantity(int productId, String email, int quantity) {
		String sql = "update basket set quantity=quantity+" + quantity + " where productId=? and email=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		int result = -1;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setInt(1, productId);
			pstmt.setString(2, email);
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("update Basket error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	public int deleteBasketQuantity(int productId, String email) {
		String sql = "update basket set quantity=0 where productId=? and email=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		int result = -1;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setInt(1, productId);
			pstmt.setString(2, email);
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("delete Basket quantity error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	public int checkBasket(int productId, String email) {
		int result = -1;
		String sql = "select * from basket where productId=? and email=?";
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, productId);
			stmt.setString(2, email);
			rst = stmt.executeQuery();
			if (rst.next()) {
				result = 1;

			} else {
				result = 0;
			}
		} catch (SQLException e) {
			System.out.println("checkBasket error : " + e);
		} finally {
			DBManager.close(conn, stmt, rst);
		}

		/*
		 * if (result == 1) { sql =
		 * "update basket set quantity=quantity+1 where productId=? and email=?"
		 * ; try { conn = DBManager.getConnection(); stmt =
		 * conn.prepareStatement(sql);
		 * 
		 * stmt.setInt(1, productId); stmt.setString(2, email);
		 * stmt.executeUpdate(); } catch (Exception e) {
		 * System.out.println("update Basket error :  " + e); } finally {
		 * DBManager.close(conn, stmt); } } return result;
		 */

		return result;
	}

	// 상품 찾기
	public ArrayList<ProductVO> searchProduct(String search,String product) {
		ArrayList<ProductVO> list = new ArrayList<ProductVO>();
		String sql="";
		int flag=0;
		if(search.equals("company")){
			sql= "select * from product where company and delyn=0 LIKE ? order by postDate desc";
		}else if(search.equals("itemName")){
			sql= "select * from product where itemName and delyn=0 LIKE ? order by postDate desc";
		}else if(search.equals("colorName")){
			sql= "select * from product where colorName and delyn=0 LIKE ? order by postDate desc";
		}else{
			flag=1;
			sql= "select * from product where company LIKE ? or itemName LIKE ? or colorName LIKE ? and delyn=0 order by postDate desc";
		}
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		ProductVO vo = null;
		try {
			
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			if(flag==0){
				stmt.setString(1, "%" + product + "%");
				
			}else{
				stmt.setString(1, "%" + product + "%");
				stmt.setString(2, "%" + product + "%");
				stmt.setString(3, "%" + product + "%");
			}
			
			rst = stmt.executeQuery();

			while (rst.next()) {
				vo = new ProductVO();
				vo.setProductID(rst.getInt("productId"));
				vo.setMainImg(rst.getString("mainImg"));
				vo.setItemName(rst.getString("itemName"));
				vo.setExpl(rst.getString("expl"));
				vo.setRetail(rst.getInt("retail"));
				vo.setWholesale(rst.getInt("wholesale"));
				vo.setStandard(rst.getInt("standard"));
				vo.setStock(rst.getInt("stock"));
				vo.setState(rst.getString("state"));
				vo.setColorName(rst.getString("colorName"));
				vo.setSwatch(rst.getString("swatch"));
				vo.setManufacturer(rst.getString("manufacturer"));
				vo.setManuDate(rst.getString("manuDate"));
				vo.setOrigin(rst.getString("origin"));
				vo.setSortName(rst.getString("sortName"));
				vo.setPattern(rst.getString("pattern"));
				vo.setWidth(rst.getInt("width"));
				vo.setThick(rst.getString("thick"));
				vo.setWeight(rst.getInt("weight"));
				vo.setWeave(rst.getString("weave"));
				vo.setThickWp(rst.getInt("thickWp"));
				vo.setThickWt(rst.getInt("thickWt"));
				vo.setLengthWp(rst.getString("lengthWp"));
				vo.setLengthWt(rst.getString("lengthWt"));
				vo.setTwistWp(rst.getString("twistWp"));
				vo.setTwistWt(rst.getString("twistWt"));
				vo.setGagong(rst.getString("gagong"));
				vo.setDye(rst.getString("dye"));
				vo.setTexture(rst.getString("texture"));
				vo.setThrough(rst.getString("through"));
				vo.setGloss(rst.getString("gloss"));
				vo.setElastic(rst.getString("elastic"));
				vo.setPliability(rst.getString("pliability"));
				vo.setPostDate(rst.getString("postDate"));
				vo.setCompany(rst.getString("company"));
				vo.setDensity_volume(rst.getInt("density_volume"));
				vo.setDensity_weight(rst.getInt("density_weight"));
				vo.setRetail_only(rst.getInt("retail_only"));
				vo.setThickUnit(rst.getString("thickUnit"));
				vo.setWidthUnit(rst.getString("widthUnit"));
				vo.setStock_unknown(rst.getInt("stock_unknown"));
				list.add(vo);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return list;
	}
	
	//상품 수정-하나씩
	public int updateAllProduct(ProductVO pVo,int productId,String email, String company) {
		int result = -1;
		String sql = "update product set email=?, expl=?, "
				+ "stock=?,colorName=?,state=?,manufacturer=?,manuDate=?,origin=?,"
				+ "sortName=?,pattern=?,weight=?,"
				+ "thickUnit=?,weave=?,thickWp=?,thickWt=?,lengthWp=?,lengthWt=?,twistWp=?,twistWt=?,"
				+ "gagong=?,dye=?,texture=?,through=?,gloss=?,elastic=?,pliability=?,company=?,density_weight=?,density_volume=?,"
				+ "stock_unknown=? where productId=? and delyn=0";

		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			
			stmt.setString(1, email);
			stmt.setString(2, pVo.getExpl());
			stmt.setInt(3, pVo.getStock());
			stmt.setString(4, pVo.getColorName());
			stmt.setString(5, pVo.getState());
			stmt.setString(6, pVo.getManufacturer());
			stmt.setString(7, pVo.getManuDate());
			stmt.setString(8, pVo.getOrigin());
			stmt.setString(9, pVo.getSortName());
			stmt.setString(10, pVo.getPattern());
			stmt.setInt(11, pVo.getWeight());
			stmt.setString(12, pVo.getThickUnit());
			stmt.setString(13, pVo.getWeave());
			stmt.setInt(14, pVo.getThickWp());
			stmt.setInt(15, pVo.getThickWt());
			stmt.setString(16, pVo.getLengthWp());
			stmt.setString(17, pVo.getLengthWt());
			stmt.setString(18, pVo.getTwistWp());
			stmt.setString(19, pVo.getTwistWt());
			stmt.setString(20, pVo.getGagong());
			stmt.setString(21, pVo.getDye());
			stmt.setString(22, pVo.getTexture());
			stmt.setString(23, pVo.getThrough());
			stmt.setString(24, pVo.getGloss());
			stmt.setString(25, pVo.getElastic());
			stmt.setString(26, pVo.getPliability());
			stmt.setString(27, company);
			stmt.setInt(28, pVo.getDensity_weight());
			stmt.setInt(29, pVo.getDensity_volume());
			stmt.setInt(30, pVo.getStock_unknown());
			stmt.setInt(31,productId);
			result = stmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("update all Product error :  " + e);
		} finally {
			DBManager.close(conn, stmt);
		}
		return result;
	}
	
	//모든 상품 수정
	public int updateAllImageProduct(int productId, String imgName,String imgType) {
		int result = -1;
		String sql = "update product set "+imgType+"=? where productId=? and delyn=0";
		

		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			
			stmt.setString(1, imgName);
			stmt.setInt(2, productId);
			
			result = stmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("update all image Product error :  " + e);
		} finally {
			DBManager.close(conn, stmt);
		}
		return result;
	}
	



}
