package com.sid.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.simple.JSONObject;

import com.sid.dto.AlarmVO;
import com.sid.dto.CalculateVO;
import com.sid.dto.CancelOrderVO;
import com.sid.dto.PayInfoVO;
import com.sid.dto.PurchaseVO;
import com.sid.util.DBManager;

public class PurchaseDAO {
	public PurchaseDAO() {
	}

	public static PurchaseDAO instance = new PurchaseDAO();

	public static PurchaseDAO getInstance() {
		return instance;
	}

	public CancelOrderVO getCancelReason(String id) {
		String sql = "select * from order_cancel where purchaseId=?";

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		CancelOrderVO vo = null;

		try {

			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, id);
			rst = stmt.executeQuery();

			if (rst.next()) {
				vo = new CancelOrderVO();
				vo.setPurchaseId(rst.getString("purchaseId"));
				vo.setReason(rst.getString("reason"));
				vo.setDate(rst.getDate("date"));
				vo.setCanceller(rst.getString("canceller"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return vo;
	}

	public int insertOrder(PurchaseVO vo) {
		int result = -1;
		String sql = "INSERT INTO purchase(productId,quantity,buyerName,buyerEmail,buyerPhone,"
				+ "recipientName,recipientPhone,recipientAddress,cost,"
				+ "seller,company,purchaseId,payFlag,state,oid,delivery_cost,purchase_detail,delivery_message) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);

			stmt.setInt(1, vo.getProductId());
			stmt.setInt(2, vo.getQuantity());
			stmt.setString(3, vo.getBuyerName());
			stmt.setString(4, vo.getBuyerEmail());
			stmt.setString(5, vo.getBuyerPhone());
			stmt.setString(6, vo.getRecipientName());
			stmt.setString(7, vo.getRecipientPhone());
			stmt.setString(8, vo.getRecipientAddress());
			stmt.setInt(9, vo.getCost());
			stmt.setString(10, vo.getSeller());
			stmt.setString(11, vo.getCompany());
			stmt.setString(12, vo.getPurchaseId());
			stmt.setInt(13, 0);
			stmt.setInt(14, -1);
			stmt.setString(15, vo.getOid());
			stmt.setInt(16, vo.getDelivery_cost());
			stmt.setString(17, vo.getPurchase_detail());
			stmt.setString(18, vo.getDelivery_message());
		
			result = stmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt);
		}
		return result;
	}

	// 미완성
	public PurchaseVO orderChange(int state) {
		String sql = "select * from purchase where state=?";

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		PurchaseVO vo = null;

		try {

			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, state);
			rst = stmt.executeQuery();

			if (rst.next()) {
				vo = new PurchaseVO();
				vo.setPurchaseId(rst.getString("purchaseId"));
				vo.setProductId(rst.getInt("productId"));
				vo.setQuantity(rst.getInt("quantity"));
				vo.setBuyerName(rst.getString("buyerName"));
				vo.setBuyerEmail(rst.getString("buyerEmail"));
				vo.setBuyerPhone(rst.getString("buyerPhone"));
				vo.setRecipientName(rst.getString("recipientName"));
				vo.setRecipientPhone(rst.getString("recipientPhone"));
				vo.setRecipientAddress(rst.getString("recipientAddress"));
				vo.setCost(rst.getInt("cost"));
				vo.setPayOption(rst.getString("payOption"));
				vo.setCoupon(rst.getInt("coupon"));
				vo.setState(rst.getInt("state"));
				vo.setExpress(rst.getInt("express"));
				vo.setBuyOption(rst.getInt("buyOption"));

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return vo;
	}

	// 주문취소 사유 넣기
	public int insertOrderCancel(String id, String reason,String canceller) {
		int result = -1;
		String sql = "insert into order_cancel(purchaseId,reason,canceller) values(?,?,?)";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, id);
			pstmt.setString(2, reason);
			pstmt.setString(3, canceller);
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("insert order cancel error " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	// 판매자 주문관리 리스트 가져오기
	public ArrayList<PurchaseVO> listAll(String seller) {
		ArrayList<PurchaseVO> list = new ArrayList<PurchaseVO>();

		String sql = "select * from purchase a LEFT JOIN product b ON (a.productId=b.productId) where seller=? order by orderDate desc";
		// (purchaseId,purchase.productId,quantity,buyerName,buyerEmail,buyerPhone
		// ,recipientName,"
		// +
		// "recipientPhone,recipientAddress,cost,payOption,coupon,state,orederDate,express,"
		// + "buyOption,itemName)
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		PurchaseVO vo = null;
		String date=null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, seller);
			rst = stmt.executeQuery();

			while (rst.next()) {
				vo = new PurchaseVO();
				vo.setPurchaseId(rst.getString("purchaseId"));
				vo.setProductId(rst.getInt("productId"));
				vo.setQuantity(rst.getInt("quantity"));
				vo.setBuyerName(rst.getString("buyerName"));
				vo.setBuyerEmail(rst.getString("buyerEmail"));
				vo.setBuyerPhone(rst.getString("buyerPhone"));
				vo.setRecipientName(rst.getString("recipientName"));
				vo.setRecipientPhone(rst.getString("recipientPhone"));
				vo.setRecipientAddress(rst.getString("recipientAddress"));
				vo.setCost(rst.getInt("cost"));
				vo.setPayOption(rst.getString("payOption"));
				vo.setCoupon(rst.getInt("coupon"));
				vo.setColorName(rst.getString("colorName"));
				vo.setState(rst.getInt("a.state"));
				vo.setExpress(rst.getInt("express"));
				vo.setBuyOption(rst.getInt("buyOption"));
				vo.setItemName(rst.getString("itemName"));
				vo.setInvoiceNum(rst.getString("invoiceNum"));	
				date=rst.getString("orderDate");
				vo.setOrderDate(date.substring(0, date.length()-5));
				vo.setCompany(rst.getString("company"));
				vo.setRelate(rst.getString("relate"));
				vo.setSeller(rst.getString("seller"));
				vo.setPayFlag(rst.getInt("payFlag"));
				vo.setOid(rst.getString("oid"));
				list.add(vo);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return list;
	}

	public ArrayList<PayInfoVO> listAllOid(String email,int state) {
		ArrayList<PayInfoVO> list = new ArrayList<PayInfoVO>();
		String sql=null;
		if(state==0){
			sql = "select distinct(moid),price from payinfo a LEFT JOIN purchase b ON (a.moid=b.oid) where email=? and b.state between -1 and 3 order by moid desc";
		}else if(state==4){
			sql = "select distinct(moid),price from payinfo a LEFT JOIN purchase b ON (a.moid=b.oid) where email=? and b.state=7 order by moid desc";
		}else if(state==5){
			sql = "select distinct(moid),price from payinfo a LEFT JOIN purchase b ON (a.moid=b.oid) where email=? order by moid desc";
		}
		// (purchaseId,purchase.productId,quantity,buyerName,buyerEmail,buyerPhone
		// ,recipientName,"
		// +
		// "recipientPhone,recipientAddress,cost,payOption,coupon,state,orederDate,express,"
		// + "buyOption,itemName)
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		PayInfoVO vo = null;

		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, email);
			rst = stmt.executeQuery();

			while (rst.next()) {
				vo = new PayInfoVO();
				vo.setMoid(rst.getString("moid"));
				vo.setPrice(rst.getInt("price"));
				list.add(vo);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return list;
	}
	
	public ArrayList<PayInfoVO> listAllOidBySeller(String email,int state) {
		ArrayList<PayInfoVO> list = new ArrayList<PayInfoVO>();
		
		String sql=null;
		if(state==4){
			sql = "select distinct(moid) from payinfo a LEFT JOIN purchase b ON (a.moid=b.oid) where seller=? and state=? or state=7 order by moid desc";
		}else{
			sql = "select distinct(moid) from payinfo a LEFT JOIN purchase b ON (a.moid=b.oid) where seller=? and state=? order by moid desc";
		}
		// (purchaseId,purchase.productId,quantity,buyerName,buyerEmail,buyerPhone
		// ,recipientName,"
		// +
		// "recipientPhone,recipientAddress,cost,payOption,coupon,state,orederDate,express,"
		// + "buyOption,itemName)
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		PayInfoVO vo = null;

		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, email);
			stmt.setInt(2, state);
			rst = stmt.executeQuery();

			while (rst.next()) {
				vo = new PayInfoVO();
				vo.setMoid(rst.getString("moid"));
				list.add(vo);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return list;
	}

	public HashMap<String, Integer> countState(String email) {

		String sql = "select state, count(state) as count from purchase where seller=? group by state;";
		HashMap<String, Integer> map=new HashMap<>();
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		map.put("state0", 0);
		map.put("state1", 0);
		map.put("state2", 0);
		map.put("state3", 0);
		map.put("state4", 0);
		map.put("state5", 0);
		int count=0;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, email);
			rst = stmt.executeQuery();
			
			while (rst.next()) {
				switch(rst.getInt("state")){
				case 0:
					map.put("state0", rst.getInt("count"));
					break;
				case 1:
					map.put("state1", rst.getInt("count"));
					break;
				case 2:
					map.put("state2", rst.getInt("count"));
					break;
				case 3:
					map.put("state3", rst.getInt("count"));
					break;
				case 4:
					count+=rst.getInt("count");
					break;
				case 5:
					map.put("state5", rst.getInt("count"));
					break;
				case 7:
					count+=rst.getInt("count");
					break;
				default:
					break;
					
				}
				map.put("state4",count);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return map;
	}
	// 구매자 결제내역 리스트 가져오기
	public ArrayList<PurchaseVO> listAllForBuyer(String seller) {
		ArrayList<PurchaseVO> list = new ArrayList<PurchaseVO>();

		String sql = "select * from purchase a LEFT JOIN product b ON (a.productId=b.productId) where buyerEmail=? order by orderDate desc";
		// (purchaseId,purchase.productId,quantity,buyerName,buyerEmail,buyerPhone
		// ,recipientName,"
		// +
		// "recipientPhone,recipientAddress,cost,payOption,coupon,state,orederDate,express,"
		// + "buyOption,itemName)
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		PurchaseVO vo = null;
		String date=null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, seller);
			rst = stmt.executeQuery();

			while (rst.next()) {
				vo = new PurchaseVO();
				vo.setPurchaseId(rst.getString("purchaseId"));
				vo.setProductId(rst.getInt("productId"));
				vo.setQuantity(rst.getInt("quantity"));
				vo.setBuyerName(rst.getString("buyerName"));
				vo.setBuyerEmail(rst.getString("buyerEmail"));
				vo.setBuyerPhone(rst.getString("buyerPhone"));
				vo.setRecipientName(rst.getString("recipientName"));
				vo.setRecipientPhone(rst.getString("recipientPhone"));
				vo.setRecipientAddress(rst.getString("recipientAddress"));
				vo.setCost(rst.getInt("cost"));
				vo.setColorName(rst.getString("colorName"));
				vo.setPayOption(rst.getString("payOption"));
				vo.setCoupon(rst.getInt("coupon"));
				vo.setState(rst.getInt("a.state"));
				vo.setExpress(rst.getInt("express"));
				vo.setBuyOption(rst.getInt("buyOption"));
				vo.setItemName(rst.getString("itemName"));
				vo.setInvoiceNum(rst.getString("invoiceNum"));	
				date=rst.getString("orderDate");
				vo.setOrderDate(date.substring(0, date.length()-5));
				vo.setItemName(rst.getString("itemName"));
				vo.setMainImg(rst.getString("mainImg"));
				vo.setCompany(rst.getString("company"));
				vo.setRelate(rst.getString("relate"));
				vo.setSeller(rst.getString("seller"));
				vo.setPayFlag(rst.getInt("payFlag"));
				vo.setOid(rst.getString("oid"));
				vo.setPurchase_detail(rst.getString("purchase_detail"));
				list.add(vo);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return list;
	}

	public JSONObject listCompanyByOid(String oid) {
		String sql = "SELECT company, count(*) as COUNT FROM purchase where oid=? GROUP BY company order by company asc;";

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		JSONObject obj = new JSONObject();
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, oid);
			rst = stmt.executeQuery();

			while (rst.next()) {
				obj.put(rst.getString("company"), rst.getString("COUNT"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return obj;
	}
	
	public ArrayList<PurchaseVO> listAllByOidSeller(String oid,String email) {
		ArrayList<PurchaseVO> list = new ArrayList<PurchaseVO>();

		String sql = "select *, (SELECT sum(cost) FROM purchase where oid=? and seller=?) as totalcost from purchase a LEFT JOIN product b ON (a.productId=b.productId) where oid=? and seller=? order by orderDate desc";
		// (purchaseId,purchase.productId,quantity,buyerName,buyerEmail,buyerPhone
		// ,recipientName,"
		// +
		// "recipientPhone,recipientAddress,cost,payOption,coupon,state,orederDate,express,"
		// + "buyOption,itemName)
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		PurchaseVO vo = null;
		String date=null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, oid);
			stmt.setString(2, email);
			stmt.setString(3, oid);
			stmt.setString(4, email);
			rst = stmt.executeQuery();

			while (rst.next()) {
				vo = new PurchaseVO();
				vo.setPurchaseId(rst.getString("purchaseId"));
				vo.setProductId(rst.getInt("productId"));
				vo.setQuantity(rst.getInt("quantity"));
				vo.setBuyerName(rst.getString("buyerName"));
				vo.setBuyerEmail(rst.getString("buyerEmail"));
				vo.setBuyerPhone(rst.getString("buyerPhone"));
				vo.setRecipientName(rst.getString("recipientName"));
				vo.setRecipientPhone(rst.getString("recipientPhone"));
				vo.setRecipientAddress(rst.getString("recipientAddress"));
				vo.setCost(rst.getInt("cost"));
				vo.setColorName(rst.getString("colorName"));
				vo.setPayOption(rst.getString("payOption"));
				vo.setCoupon(rst.getInt("coupon"));
				vo.setState(rst.getInt("a.state"));
				vo.setExpress(rst.getInt("express"));
				vo.setBuyOption(rst.getInt("buyOption"));
				vo.setItemName(rst.getString("itemName"));
				vo.setInvoiceNum(rst.getString("invoiceNum"));
				date=rst.getString("orderDate");
				vo.setOrderDate(date.substring(0, date.length()-5));
				vo.setItemName(rst.getString("itemName"));
				vo.setMainImg(rst.getString("mainImg"));
				vo.setCompany(rst.getString("company"));
				vo.setRelate(rst.getString("relate"));
				vo.setSeller(rst.getString("seller"));
				vo.setPayFlag(rst.getInt("payFlag"));
				vo.setOid(rst.getString("oid"));
				vo.setPurchase_detail(rst.getString("purchase_detail"));
				vo.setDelivery_cost(rst.getInt("delivery_cost"));
				vo.setTotalcost(rst.getInt("totalCost"));
				vo.setDelivery_message(rst.getString("delivery_message"));
				list.add(vo);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return list;
	}
	
	public ArrayList<PurchaseVO> listAllByOid(String oid) {
		ArrayList<PurchaseVO> list = new ArrayList<PurchaseVO>();

		String sql = "select * from purchase a LEFT JOIN product b ON (a.productId=b.productId) where oid=? order by a.company desc";
		// (purchaseId,purchase.productId,quantity,buyerName,buyerEmail,buyerPhone
		// ,recipientName,"
		// +
		// "recipientPhone,recipientAddress,cost,payOption,coupon,state,orederDate,express,"
		// + "buyOption,itemName)
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		PurchaseVO vo = null;
		String date=null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, oid);
			rst = stmt.executeQuery();

			while (rst.next()) {
				vo = new PurchaseVO();
				vo.setPurchaseId(rst.getString("purchaseId"));
				vo.setProductId(rst.getInt("productId"));
				vo.setQuantity(rst.getInt("quantity"));
				vo.setBuyerName(rst.getString("buyerName"));
				vo.setBuyerEmail(rst.getString("buyerEmail"));
				vo.setBuyerPhone(rst.getString("buyerPhone"));
				vo.setRecipientName(rst.getString("recipientName"));
				vo.setRecipientPhone(rst.getString("recipientPhone"));
				vo.setRecipientAddress(rst.getString("recipientAddress"));
				vo.setCost(rst.getInt("cost"));
				vo.setColorName(rst.getString("colorName"));
				vo.setPayOption(rst.getString("payOption"));
				vo.setCoupon(rst.getInt("coupon"));
				vo.setState(rst.getInt("a.state"));
				vo.setExpress(rst.getInt("express"));
				vo.setBuyOption(rst.getInt("buyOption"));
				vo.setItemName(rst.getString("itemName"));
				vo.setInvoiceNum(rst.getString("invoiceNum"));
				date=rst.getString("orderDate");
				vo.setOrderDate(date.substring(0, date.length()-5));
				vo.setItemName(rst.getString("itemName"));
				vo.setMainImg(rst.getString("mainImg"));
				vo.setCompany(rst.getString("company"));
				vo.setRelate(rst.getString("relate"));
				vo.setSeller(rst.getString("seller"));
				vo.setPayFlag(rst.getInt("payFlag"));
				vo.setOid(rst.getString("oid"));
				vo.setPurchase_detail(rst.getString("purchase_detail"));
				vo.setDelivery_cost(rst.getInt("delivery_cost"));
				vo.setDelivery_message(rst.getString("delivery_message"));
				list.add(vo);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return list;
	}

	public ArrayList<PurchaseVO> listCancelReady(String email) {
		ArrayList<PurchaseVO> list = new ArrayList<PurchaseVO>();

		String sql = "select * from purchase a LEFT JOIN product b ON (a.productId=b.productId) where buyerEmail=? and a.state=6 order by a.oid desc";
		// (purchaseId,purchase.productId,quantity,buyerName,buyerEmail,buyerPhone
		// ,recipientName,"
		// +
		// "recipientPhone,recipientAddress,cost,payOption,coupon,state,orederDate,express,"
		// + "buyOption,itemName)
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		PurchaseVO vo = null;
		String date=null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, email);
			rst = stmt.executeQuery();

			while (rst.next()) {
				vo = new PurchaseVO();
				vo.setPurchaseId(rst.getString("purchaseId"));
				vo.setProductId(rst.getInt("productId"));
				vo.setQuantity(rst.getInt("quantity"));
				vo.setBuyerName(rst.getString("buyerName"));
				vo.setBuyerEmail(rst.getString("buyerEmail"));
				vo.setBuyerPhone(rst.getString("buyerPhone"));
				vo.setRecipientName(rst.getString("recipientName"));
				vo.setRecipientPhone(rst.getString("recipientPhone"));
				vo.setRecipientAddress(rst.getString("recipientAddress"));
				vo.setCost(rst.getInt("cost"));
				vo.setColorName(rst.getString("colorName"));
				vo.setPayOption(rst.getString("payOption"));
				vo.setCoupon(rst.getInt("coupon"));
				vo.setState(rst.getInt("a.state"));
				vo.setExpress(rst.getInt("express"));
				vo.setBuyOption(rst.getInt("buyOption"));
				vo.setItemName(rst.getString("itemName"));
				vo.setInvoiceNum(rst.getString("invoiceNum"));
				date=rst.getString("orderDate");
				vo.setOrderDate(date.substring(0, date.length()-5));
				vo.setItemName(rst.getString("itemName"));
				vo.setMainImg(rst.getString("mainImg"));
				vo.setCompany(rst.getString("company"));
				vo.setRelate(rst.getString("relate"));
				vo.setSeller(rst.getString("seller"));
				vo.setPayFlag(rst.getInt("payFlag"));
				vo.setOid(rst.getString("oid"));
				vo.setPurchase_detail(rst.getString("purchase_detail"));
				vo.setDelivery_cost(rst.getInt("delivery_cost"));
				list.add(vo);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return list;
	}

	public ArrayList<PurchaseVO> listCancel(String email) {
		ArrayList<PurchaseVO> list = new ArrayList<PurchaseVO>();

		String sql = "select * from purchase a LEFT JOIN product b ON (a.productId=b.productId) where buyerEmail=? and a.state=5 order by a.oid desc";
		// (purchaseId,purchase.productId,quantity,buyerName,buyerEmail,buyerPhone
		// ,recipientName,"
		// +
		// "recipientPhone,recipientAddress,cost,payOption,coupon,state,orederDate,express,"
		// + "buyOption,itemName)
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		PurchaseVO vo = null;
		String date=null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, email);
			rst = stmt.executeQuery();

			while (rst.next()) {
				vo = new PurchaseVO();
				vo.setPurchaseId(rst.getString("purchaseId"));
				vo.setProductId(rst.getInt("productId"));
				vo.setQuantity(rst.getInt("quantity"));
				vo.setBuyerName(rst.getString("buyerName"));
				vo.setBuyerEmail(rst.getString("buyerEmail"));
				vo.setBuyerPhone(rst.getString("buyerPhone"));
				vo.setRecipientName(rst.getString("recipientName"));
				vo.setRecipientPhone(rst.getString("recipientPhone"));
				vo.setRecipientAddress(rst.getString("recipientAddress"));
				vo.setCost(rst.getInt("cost"));
				vo.setColorName(rst.getString("colorName"));
				vo.setPayOption(rst.getString("payOption"));
				vo.setCoupon(rst.getInt("coupon"));
				vo.setState(rst.getInt("a.state"));
				vo.setExpress(rst.getInt("express"));
				vo.setBuyOption(rst.getInt("buyOption"));
				vo.setItemName(rst.getString("itemName"));
				vo.setInvoiceNum(rst.getString("invoiceNum"));
				date=rst.getString("orderDate");
				vo.setOrderDate(date.substring(0, date.length()-5));
				vo.setItemName(rst.getString("itemName"));
				vo.setMainImg(rst.getString("mainImg"));
				vo.setCompany(rst.getString("company"));
				vo.setRelate(rst.getString("relate"));
				vo.setSeller(rst.getString("seller"));
				vo.setPayFlag(rst.getInt("payFlag"));
				vo.setOid(rst.getString("oid"));
				vo.setPurchase_detail(rst.getString("purchase_detail"));
				vo.setDelivery_cost(rst.getInt("delivery_cost"));
				list.add(vo);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return list;
	}

	public ArrayList<PurchaseVO> listComplete(String email) {
		ArrayList<PurchaseVO> list = new ArrayList<PurchaseVO>();

		String sql = "select * from purchase a LEFT JOIN product b ON (a.productId=b.productId) where buyerEmail=? and a.state=7 order by a.oid desc";
		// (purchaseId,purchase.productId,quantity,buyerName,buyerEmail,buyerPhone
		// ,recipientName,"
		// +
		// "recipientPhone,recipientAddress,cost,payOption,coupon,state,orederDate,express,"
		// + "buyOption,itemName)
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		PurchaseVO vo = null;
		String date=null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, email);
			rst = stmt.executeQuery();

			while (rst.next()) {
				vo = new PurchaseVO();
				vo.setPurchaseId(rst.getString("purchaseId"));
				vo.setProductId(rst.getInt("productId"));
				vo.setQuantity(rst.getInt("quantity"));
				vo.setBuyerName(rst.getString("buyerName"));
				vo.setBuyerEmail(rst.getString("buyerEmail"));
				vo.setBuyerPhone(rst.getString("buyerPhone"));
				vo.setRecipientName(rst.getString("recipientName"));
				vo.setRecipientPhone(rst.getString("recipientPhone"));
				vo.setRecipientAddress(rst.getString("recipientAddress"));
				vo.setCost(rst.getInt("cost"));
				vo.setColorName(rst.getString("colorName"));
				vo.setPayOption(rst.getString("payOption"));
				vo.setCoupon(rst.getInt("coupon"));
				vo.setState(rst.getInt("a.state"));
				vo.setExpress(rst.getInt("express"));
				vo.setBuyOption(rst.getInt("buyOption"));
				vo.setItemName(rst.getString("itemName"));
				vo.setInvoiceNum(rst.getString("invoiceNum"));
				date=rst.getString("orderDate");
				vo.setOrderDate(date.substring(0, date.length()-5));
				vo.setItemName(rst.getString("itemName"));
				vo.setMainImg(rst.getString("mainImg"));
				vo.setCompany(rst.getString("company"));
				vo.setRelate(rst.getString("relate"));
				vo.setSeller(rst.getString("seller"));
				vo.setPayFlag(rst.getInt("payFlag"));
				vo.setOid(rst.getString("oid"));
				vo.setPurchase_detail(rst.getString("purchase_detail"));
				vo.setDelivery_cost(rst.getInt("delivery_cost"));
				vo.setDelivery_message(rst.getString("delivery_message"));
				list.add(vo);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return list;
	}

	// 관리자 페이지 - 구매자 전체 결제내역 리스트 가져오기
	public ArrayList<PurchaseVO> listAllPurchase_admin() {
		ArrayList<PurchaseVO> list = new ArrayList<PurchaseVO>();

		String sql = "select * from purchase a LEFT JOIN product b ON (a.productId=b.productId) order by orderDate desc";
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		PurchaseVO vo = null;

		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			rst = stmt.executeQuery();

			while (rst.next()) {
				vo = new PurchaseVO();
				vo.setPurchaseId(rst.getString("purchaseId"));
				vo.setProductId(rst.getInt("productId"));
				vo.setQuantity(rst.getInt("quantity"));
				vo.setBuyerName(rst.getString("buyerName"));
				vo.setBuyerEmail(rst.getString("buyerEmail"));
				vo.setBuyerPhone(rst.getString("buyerPhone"));
				vo.setRecipientName(rst.getString("recipientName"));
				vo.setRecipientPhone(rst.getString("recipientPhone"));
				vo.setRecipientAddress(rst.getString("recipientAddress"));
				vo.setCost(rst.getInt("cost"));
				vo.setColorName(rst.getString("colorName"));
				vo.setPayOption(rst.getString("payOption"));
				vo.setCoupon(rst.getInt("coupon"));
				vo.setState(rst.getInt("a.state"));
				vo.setExpress(rst.getInt("express"));
				vo.setBuyOption(rst.getInt("buyOption"));
				vo.setItemName(rst.getString("itemName"));
				vo.setInvoiceNum(rst.getString("invoiceNum"));
				vo.setOrderDate(rst.getString("orderDate"));
				vo.setCompany(rst.getString("company"));
				vo.setRelate(rst.getString("relate"));
				vo.setSeller(rst.getString("seller"));
				list.add(vo);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return list;
	}

	// 결제성공시 payFlag 1로
	public int updatePayFlag(int payFlag, String oid,String payOption) {
		int result = -1;
		int state = 0;

		if (payFlag == 2) {
			state = 0;
		} else {
			state = -1;
		}
		String sql = "update purchase set payFlag=1, state=" + state + ", payOption='"+payOption+"' where oid=?";

		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, oid);
			result = stmt.executeUpdate();

		} catch (Exception e) {
			System.out.println("updatePayFlag method error :  " + e);
		} finally {
			DBManager.close(conn, stmt);
		}

		return result;
	}

	// 주문 삭제
	public int deletePayFlag(String oid) {
		int result = -1;
		String sql = "delete from purchase where oid=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, oid);
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("deletePayFlag error : " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	// 결제 실패시 payFlag를 불러오기 - 입력된 상품 제거하기 위해 필요(payFlag=0인)
	public String readPayFlag(String email) {
		String oid = "";
		String sql = "select oid from purchase where payFlag=0 and buyerEmail=?";
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;

		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, email);
			rst = stmt.executeQuery();

			if (rst.next()) {
				oid = rst.getString("oid");
				System.out.println("oid불러오기 성공");
			}

		} catch (Exception e) {
			System.out.println("readPayFlag error : " + e);
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return oid;
	}
	// 구매자 주문관리 상태변경
		public int updateStateForBuyer(String pid,int state) {
			int result = -1;
			String sql = "update purchase set state=IF(state>=-1 AND state <=4,"+state+",state) where purchaseId=?";
			Connection conn = null;
			PreparedStatement stmt = null;

			try {
				conn = DBManager.getConnection();
				stmt = conn.prepareStatement(sql);
				stmt.setString(1, pid);
				result = stmt.executeUpdate();

			} catch (Exception e) {
				System.out.println("update Basket error :  " + e);
			} finally {
				DBManager.close(conn, stmt);
			}

			return result;
		}
	// 판매자 주문관리 상태변경
	public int updateState(String pid,int state) {
		int result = -1;
		String sql = "update purchase set state=IF(state>=-1 AND state <=4,"+state+",state) where purchaseId=?";
		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, pid);
			result = stmt.executeUpdate();

		} catch (Exception e) {
			System.out.println("update Basket error :  " + e);
		} finally {
			DBManager.close(conn, stmt);
		}

		return result;
	}

	// 주문관리 송장 번호 변경
	public int updateInvoiceNum(String oid,String input) {
		int result = -1;
		String sql = "update purchase set invoiceNum=? where oid=?";

		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, input);
			stmt.setString(2, oid);
			result = stmt.executeUpdate();

		} catch (Exception e) {
			System.out.println("update invoice error :  " + e);
		} finally {
			DBManager.close(conn, stmt);
		}

		return result;
	}

	// 배송 시작
	public int startDelivery(String oid, String input,String email) {
		int result = -1;
		String sql = "update purchase set invoiceNum=?, state=IF(state>=-1 AND state<=4,2,state) where oid=? and seller=?";

		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, input);
			stmt.setString(2, oid);
			stmt.setString(3, email);
			result = stmt.executeUpdate();

		} catch (Exception e) {
			System.out.println("update invoice error :  " + e);
		} finally {
			DBManager.close(conn, stmt);
		}

		return result;
	}

	// 정산시 결제금액 건별 가져오기
	public PurchaseVO getPayment_amount(String seller,String oid) {
		String sql = "select sum(cost) as totalcost, oid, delivery_cost, seller, company, purchaseId"
				+ " from purchase where seller=? and oid=? and state!=5 group by oid";
		PurchaseVO vo = new PurchaseVO();
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
	
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, seller);
			stmt.setString(2, oid);
			rst = stmt.executeQuery();

			if (rst.next()) {
				vo.setTotalcost(rst.getInt("totalcost"));
				vo.setOid(rst.getString("oid"));
				vo.setDelivery_cost(rst.getInt("delivery_cost"));
				vo.setSeller(rst.getString("seller"));
				vo.setCompany(rst.getString("company"));
				vo.setPurchaseId(rst.getString("purchaseId"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return vo;
	}

	public Boolean checkState(String oid, String seller) {
		Boolean result = false;
		String sql = "select distinct(state) from purchase where oid=? and seller=? and state!=5";
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		int size = -1;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, oid);
			stmt.setString(2, seller);
			rst = stmt.executeQuery();

			rst.last();
			size = rst.getRow();
			
			if(size==1){
				result=true;
			}else{
				result=false;
				System.out.println("관리자에게 문의하세요");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return result;
	}
	public Boolean checkStateByOid(String oid) {
		Boolean result = false;
		String sql = "select distinct(state) from purchase where oid=? and state!=5";
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		int size = -1;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, oid);
			rst = stmt.executeQuery();

			rst.last();
			size = rst.getRow();
			
			if(size==1&&rst.getInt("state")==4){
				result=true;
			}else{
				result=false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return result;
	}
	// 모든 아이템들이 구매확정이면  완료된거래로 변경
	public int updateStateToComplete(String oid) {
		int result = -1;
		String sql = "update purchase set state=IF(state>=-1 AND state <=4,7,state) where oid=?";

		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, oid);
			result = stmt.executeUpdate();

		} catch (Exception e) {
			System.out.println("update state to complete error :  " + e);
		} finally {
			DBManager.close(conn, stmt);
		}

		return result;
	}

	public PurchaseVO getSellerAndOid(String purchaseId) {
		String sql = "select seller,oid from purchase where purchaseId=?";
		PurchaseVO vo = new PurchaseVO();
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;

		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, purchaseId);
			rst = stmt.executeQuery();

			if (rst.next()) {
				vo.setPurchaseId(purchaseId);
				vo.setSeller(rst.getString("seller"));
				vo.setOid(rst.getString("oid"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return vo;
	}
	

}
