package com.sid.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

import com.sid.dto.UserVO;
import com.sid.util.DBManager;

public class IpAddressDAO {
	private static IpAddressDAO instance =new IpAddressDAO();
	
	public static IpAddressDAO getInstance(){
		return instance;
	}
	
	public int insertIP(UserVO vo){
		int result=-1;
		String sql="insert into ipaddress(ipAddress,email) values(?,?)";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, vo.getIpAddress());
			pstmt.setString(2, vo.getEmail());

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}
	
}
