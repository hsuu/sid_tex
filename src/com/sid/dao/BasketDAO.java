package com.sid.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.sid.dto.BasketVO;
import com.sid.dto.ProductVO;
import com.sid.dto.PurchaseVO;
import com.sid.util.DBManager;

public class BasketDAO {
	private BasketDAO() {

	}

	private static BasketDAO instance = new BasketDAO();

	public static BasketDAO getInstance() {
		return instance;
	}

	/*
	 * public int insertImage(DWriteVO dVo) { int result = -1; String sql =
	 * "INSERT INTO dwrite(imageUrl, standardDate, endDate, cost, condi, point, expl,imageName,userEmail) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)"
	 * ; Connection conn = null; PreparedStatement pstmt = null;
	 * 
	 * try { conn = JDBCUtil.getConnection(); pstmt =
	 * conn.prepareStatement(sql);
	 * 
	 * pstmt.setString(1, dVo.getImageUrl()); pstmt.setString(2,
	 * dVo.getStandardDate()); pstmt.setString(3, dVo.getEndDate());
	 * pstmt.setInt(4, dVo.getCost()); pstmt.setInt(5, dVo.getCondition());
	 * pstmt.setString(6, dVo.getPoint()); pstmt.setString(7, dVo.getExpl());
	 * pstmt.setString(8, dVo.getImageName()); pstmt.setString(9,
	 * dVo.getUserEmail());
	 * 
	 * result = pstmt.executeUpdate(); } catch (Exception e) {
	 * System.out.println("insert image error " + e); } finally {
	 * JDBCUtil.close(pstmt, conn); } return result; }
	 */
	/*
	 * public DWriteVO readItem(int num) { int result = -1; String sql =
	 * "select * from dwrite where dWriteId=?"; Connection conn = null;
	 * PreparedStatement stmt = null; ResultSet rst = null; DWriteVO vo = new
	 * DWriteVO(); System.out.println("readItem" + num);
	 * 
	 * try { conn = JDBCUtil.getConnection(); stmt = conn.prepareStatement(sql);
	 * stmt.setInt(1, num); rst = stmt.executeQuery();
	 * 
	 * if (rst.next()) { vo.setdWriteId(rst.getInt("dWriteId"));
	 * vo.setImageUrl(rst.getString("imageUrl"));
	 * vo.setStandardDate(rst.getString("standardDate"));
	 * vo.setEndDate(rst.getString("endDate")); vo.setCost(rst.getInt("cost"));
	 * vo.setCondition(rst.getInt("condi"));
	 * vo.setPoint(rst.getString("point")); vo.setExpl(rst.getString("expl")); }
	 * 
	 * } catch (SQLException e) { System.out.println("read item error : " + e);
	 * } finally { JDBCUtil.close(rst, stmt, conn); } return vo; }
	 */

	// 모든 장바구니 리스트 가져오기
	public ArrayList<BasketVO> listAllBasket(String email) {

		ArrayList<BasketVO> list = new ArrayList<BasketVO>();

		// product table , basket table 조인
		String sql = "select * from basket a LEFT JOIN product b ON(a.productId=b.productId) left JOIN user_b c ON(b.email = c.email) where a.email=? and delyn=0";

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		BasketVO vo = null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, email);
			rst = stmt.executeQuery();
			while (rst.next()) {
				vo = new BasketVO();
				vo.setBasketId(rst.getInt("basketId"));
				vo.setProductId(rst.getInt("productId"));
				vo.setItemName(rst.getString("itemName"));
				vo.setMainImg(rst.getString("mainImg"));
				vo.setCompany(rst.getString("company"));
				vo.setQuantity(rst.getInt("quantity"));
				vo.setRetail(rst.getInt("retail"));
				vo.setWholesale(rst.getInt("wholesale"));
				vo.setDelivery(rst.getInt("c.delivery"));
				vo.setStandard(rst.getInt("standard"));
				vo.setSwatch(rst.getString("swatch"));
				vo.setCost(rst.getInt("cost"));
				vo.setRelate(rst.getString("relate"));
				vo.setStock(rst.getString("stock"));
				vo.setColorName(rst.getString("colorName"));
				vo.setState(rst.getString("b.state"));

				list.add(vo);
			}
		} catch (SQLException e) {
			System.out.println("list all basket error : " + e);
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return list;
	}

	// 원단 결제페이지로 넘어갈때 선택 가져오기
	public BasketVO listSelectOrder(String email, String id) {

		String sql = "select * from basket a LEFT JOIN product b ON(a.productId=b.productId) where a.email=? and a.productId=?";

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		BasketVO vo = null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, email);
			stmt.setInt(2, Integer.parseInt(id));
			rst = stmt.executeQuery();

			if (rst.next()) {
				vo = new BasketVO();
				vo.setBasketId(rst.getInt("basketId"));
				vo.setProductId(rst.getInt("productId"));
				vo.setItemName(rst.getString("itemName"));
				vo.setMainImg(rst.getString("mainImg"));
				vo.setCompany(rst.getString("company"));
				vo.setQuantity(rst.getInt("quantity"));
				vo.setRetail(rst.getInt("retail"));
				vo.setWholesale(rst.getInt("wholesale"));
				vo.setStandard(rst.getInt("standard"));
				vo.setSwatch(rst.getString("swatch"));
				vo.setDelivery(rst.getInt("express"));
				vo.setCost(rst.getInt("cost"));
				vo.setRelate(rst.getString("relate"));
				vo.setStock(rst.getString("stock"));
				vo.setColorName(rst.getString("colorName"));
				vo.setSeller(rst.getString("b.email"));
			}

		} catch (SQLException e) {
			System.out.println("list select order basket error : " + e);
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return vo;
	}
	// 스와치 결제페이지로 넘어갈때 선가져오기
		public BasketVO listSelectSwatchOrder(String email, String id) {

			String sql = "select * from basket a LEFT JOIN product b ON(a.productId=b.productId) where a.email=? and a.productId=?";

			Connection conn = null;
			PreparedStatement stmt = null;
			ResultSet rst = null;
			BasketVO vo = null;
			try {
				conn = DBManager.getConnection();
				stmt = conn.prepareStatement(sql);
				stmt.setString(1, email);
				stmt.setInt(2, Integer.parseInt(id));
				rst = stmt.executeQuery();

				if (rst.next()) {
					vo = new BasketVO();
					vo.setBasketId(rst.getInt("basketId"));
					vo.setProductId(rst.getInt("productId"));
					vo.setItemName(rst.getString("itemName"));
					vo.setMainImg(rst.getString("mainImg"));
					vo.setCompany(rst.getString("company"));
					vo.setQuantity(rst.getInt("quantity"));
					vo.setRetail(rst.getInt("retail"));
					vo.setWholesale(rst.getInt("wholesale"));
					vo.setStandard(rst.getInt("standard"));
					vo.setSwatch(rst.getString("swatch"));
					vo.setDelivery(rst.getInt("express"));
					vo.setCost(200);
					vo.setRelate(rst.getString("relate"));
					vo.setStock(rst.getString("stock"));
					vo.setColorName(rst.getString("colorName"));
					vo.setSeller(rst.getString("email"));
				}

			} catch (SQLException e) {
				System.out.println("list select swatch order basket error : " + e);
			} finally {
				DBManager.close(conn, stmt, rst);
			}
			return vo;
		}
	public PurchaseVO listByBasketId(String id) {

		String sql = "select * from basket a LEFT JOIN product b ON(a.productId=b.productId) where a.basketId=?";

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rst = null;
		PurchaseVO vo = null;
		
		try {
			conn = DBManager.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, Integer.parseInt(id));
			rst = stmt.executeQuery();

			if (rst.next()) {
				vo = new PurchaseVO();
				vo.setItemName(rst.getString("b.itemName"));
				vo.setBasketId(rst.getInt("basketId"));
				vo.setProductId(rst.getInt("productId"));
				vo.setQuantity(rst.getInt("quantity"));
				vo.setColorName(rst.getString("colorName"));
				vo.setSeller(rst.getString("b.email"));
				vo.setCompany(rst.getString("company"));
			}

		} catch (SQLException e) {
			System.out.println("list by baksetid error : " + e);
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return vo;
	}

	public int updateProduct(String input, int productId, String column) {
		int result = -1;
		String sql = "update product set " + column + "='" + input + "' where productId=?";

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setInt(1, productId);

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("update Product error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	public int updateBasketQuantity(String id[], String q[], String c[]) {
		int result = 0;
		String sql = "update basket set quantity=?,cost=? where basketId=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			for (int i = 0; i < id.length; i++) {
				pstmt.setInt(1, Integer.parseInt(q[i]));
				pstmt.setInt(2, Integer.parseInt(c[i]));
				pstmt.setInt(3, Integer.parseInt(id[i]));
				pstmt.addBatch();
			}

			pstmt.executeBatch();
		} catch (Exception e) {
			System.out.println("update basket quantity error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	public void updateColorOption(String[] basketId,String[] quantity) {
		
		String sql = "update basket set quantity=? where basketId=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			
			for (int i = 0; i < basketId.length; i++) {
				pstmt.setInt(1, Integer.parseInt(quantity[i]));
				pstmt.setInt(2, Integer.parseInt(basketId[i]));
				pstmt.addBatch();
			}
			
			pstmt.executeBatch();
			
		} catch (Exception e) {
			System.out.println("update color option error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		
	}

	public int deleteBasket(int num) {
		String sql = "delete from basket where basketId=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		int result = -1;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, num);
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("delete basket error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	public int deleteRelateBasket(int num, String email) {
		String sql = "delete from basket where productId=? and email=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		int result = -1;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, num);
			pstmt.setString(2, email);
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("delete basket error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	// 삭제한 상품 장바구니에서 지우기
	public int deleteBasketByProductId(int num) {
		String sql = "delete from basket where productId=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		int result = -1;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, num);
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("delete product error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}

		return result;
	}

}
