package com.sid.dao;

import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.naming.spi.DirStateFactory.Result;

import com.sid.dto.BoardVO;
import com.sid.dto.FAQVO;
import com.sid.util.DBManager;

public class BoardDAO {
	private BoardDAO() {
	}

	private static BoardDAO instance = new BoardDAO();

	public static BoardDAO getInstance() {
		return instance;
	}

	public ArrayList<FAQVO> selectAllFAQ() {
		String sql = "select * from faq order by faqId desc";
		ArrayList<FAQVO> list = new ArrayList<>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rst = null;
		FAQVO vo = null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.createStatement();
			rst = stmt.executeQuery(sql);
			while (rst.next()) {
				vo = new FAQVO();
				vo.setSort(rst.getString("sort"));
				vo.setQuestion(rst.getString("question"));
				vo.setAnswer(rst.getString("answer"));
				list.add(vo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rst);
		}
		return list;
	}

	public List<BoardVO> selectAllBoards() {
		String sql = "select * from board order by contentNum desc";
		List<BoardVO> list = new ArrayList<BoardVO>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = DBManager.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				BoardVO bVo = new BoardVO();
				bVo.setContentNum(rs.getInt("contentNum"));
				bVo.setTitle(rs.getString("title"));
				bVo.setDate(rs.getDate("date"));
				bVo.setSort(rs.getString("sort"));
				list.add(bVo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, stmt, rs);
		}
		return list;
	}

	public void insertBoard(BoardVO bVo) {
		String sql = "insert into board(" + "title,content,sort) " + "values(?,?,?)";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, bVo.getTitle());
			pstmt.setString(2, bVo.getContent());
			pstmt.setString(3, bVo.getSort());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, pstmt);
		}

	}

	public BoardVO selectOneBoardByNum(String contentNum) {
		String sql = "select * from board where contentNum = ?";
		BoardVO bVo = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, contentNum);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				bVo = new BoardVO();
				bVo.setContentNum(rs.getInt("contentNum"));
				bVo.setTitle(rs.getString("title"));
				bVo.setContent(rs.getString("content"));
				bVo.setDate(rs.getDate("date"));
				bVo.setSort(rs.getString("sort"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, pstmt, rs);
		}
		return bVo;
	}

	public void deleteBoard(String num) {
		String sql = "delete from board where contentNum=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, num);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public BoardVO selectBoard(int contentNum) {
		String sql = "select * from board where contentNum=?";
		BoardVO vo = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, contentNum);
			rst = pstmt.executeQuery();
			while (rst.next()) {
				vo = new BoardVO();
				vo.setContentNum(rst.getInt("contentNum"));
				vo.setContent(rst.getString("content"));
				vo.setTitle(rst.getString("title"));
				vo.setDate(rst.getDate("date"));
				vo.setSort(rst.getString("sort"));
			}
		} catch (Exception e) {
			System.out.println("selectBoard error : " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return vo;
	}

	public int updateBoard(String title, String content, String sort,int contentNum) {
		int result = -1;
		String sql = "update board set title=?, content=?, sort=? where contentNum=?";

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, title);
			pstmt.setString(2, content);
			pstmt.setString(3, sort);
			pstmt.setInt(4,contentNum);

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("updateBoard error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

}