package com.sid.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.sid.dto.AdminCalculateVO;
import com.sid.dto.CancelBalanceVO;
import com.sid.dto.ConsultVO;
import com.sid.dto.UserVO;
import com.sid.util.DBManager;

public class AdminDAO {
	private AdminDAO() {

	}

	private static AdminDAO instance=new AdminDAO(){};

	public static AdminDAO getInstance() {
		return instance;
	}

	//모든 유저 리스트 가져오기
		public ArrayList<UserVO> listAllUser() {
			ArrayList<UserVO> list = new ArrayList<UserVO>();
			String sql = "select * from user a LEFT JOIN user_b b ON (a.email=b.email) order by admin asc";
			Connection conn = null;
			PreparedStatement stmt = null;
			ResultSet rst = null;
			UserVO vo = null;
			try {
				conn = DBManager.getConnection();
				stmt = conn.prepareStatement(sql);
				rst = stmt.executeQuery();
				while (rst.next()) {
					vo = new UserVO();
					vo.setEmail(rst.getString("a.email"));
					vo.setName(rst.getString("name"));
					vo.setNation(rst.getInt("nation"));
					vo.setPhone(rst.getString("phone"));
					vo.setAddress1(rst.getString("address1"));
					vo.setZipnum1(rst.getString("zipnum1"));
					vo.setAdmin(rst.getInt("admin"));
					list.add(vo);
				}
			} catch (SQLException e) {
				System.out.println("list all user error : " + e);
			} finally {
				DBManager.close(conn, stmt, rst);
			}
			return list;
		}
		//사업자 회원 리스트 가져오기
				public ArrayList<UserVO> listAllUser_b() {
					ArrayList<UserVO> list = new ArrayList<UserVO>();
					String sql = "select * from user_b order by email asc";
					Connection conn = null;
					PreparedStatement stmt = null;
					ResultSet rst = null;
					UserVO vo = null;
					try {
						conn = DBManager.getConnection();
						stmt = conn.prepareStatement(sql);
						rst = stmt.executeQuery();
						while (rst.next()) {
							vo = new UserVO();
							vo.setEmail(rst.getString("email"));
							vo.setBank_name(rst.getString("bank_name"));
							vo.setAccount_number(rst.getString("account_number"));
							vo.setAccount_holder(rst.getString("account_holder"));
							list.add(vo);
						}
					} catch (SQLException e) {
						System.out.println("listAllUser_b error : " + e);
					} finally {
						DBManager.close(conn, stmt, rst);
					}
					return list;
				}
		
		
		
		//사업자 등록 대기중 회원 가져오기
				public ArrayList<UserVO> listRepFlag() {
					ArrayList<UserVO> list = new ArrayList<UserVO>();
					String sql = "select * from user a LEFT JOIN user_b b ON (a.email=b.email) where rep_num_flag=0 and admin=2 order by admin asc";
					Connection conn = null;
					PreparedStatement stmt = null;
					ResultSet rst = null;
					UserVO vo = null;
					try {
						conn = DBManager.getConnection();
						stmt = conn.prepareStatement(sql);
						rst = stmt.executeQuery();
						while (rst.next()) {
							vo = new UserVO();
							vo.setEmail(rst.getString("email"));
							vo.setName(rst.getString("name"));
							vo.setNation(rst.getInt("nation"));
							vo.setPhone(rst.getString("phone"));
							vo.setAddress1(rst.getString("address1"));
							vo.setZipnum1(rst.getString("zipnum1"));
							vo.setAdmin(rst.getInt("admin"));
							vo.setCompany(rst.getString("company"));
							vo.setRep_name(rst.getString("rep_name"));
							vo.setRep_num(rst.getString("rep_num"));
							vo.setRep_phone(rst.getString("rep_phone"));
							vo.setRep_num_flag(rst.getInt("rep_num_flag"));
							list.add(vo);
						}
					} catch (SQLException e) {
						System.out.println("list all user error : " + e);
					} finally {
						DBManager.close(conn, stmt, rst);
					}
					return list;
				}
				
				// 사업자 승인
				public int updateRepNumFlag(String email) {
					int result = -1;
					String sql = "update user_b set rep_num_flag=1 where email=?";

					Connection conn = null;
					PreparedStatement pstmt = null;
					try {
						conn = DBManager.getConnection();
						pstmt = conn.prepareStatement(sql);

						pstmt.setString(1, email);

						result = pstmt.executeUpdate();
					} catch (Exception e) {
						System.out.println("update rep Num flag error :  " + e);
					} finally {
						DBManager.close(conn, pstmt);
					}
					return result;
				}

				
	//모든 문의 가져오기
		
		public ArrayList<ConsultVO> listAllConsult() {

			ArrayList<ConsultVO> list = new ArrayList<ConsultVO>();
			String sql = "select * from consult order by postdate desc";
			
			Connection conn = null;
			PreparedStatement stmt = null;
			ResultSet rst = null;
			ConsultVO vo = null;
			try {
				conn = DBManager.getConnection();
				stmt = conn.prepareStatement(sql);
				rst = stmt.executeQuery();
				while (rst.next()) {
					vo = new ConsultVO();
					vo.setEmail(rst.getString("email"));
					vo.setConsultNum(rst.getInt("consultNum"));
					vo.setContent(rst.getString("content"));
					vo.setPostdate(rst.getString("postdate"));
					vo.setReply(rst.getInt("reply"));
					list.add(vo);
				}
			} catch (SQLException e) {
				System.out.println("list all consult error : " + e);
			} finally {
				DBManager.close(conn, stmt, rst);
			}
			return list;
		}
		
		public int getTotalCostByCompany(String company,String oid){
			int cost=0;
			String sql = "select sum(cost) as totalcost from purchase where oid=? and company=?";
			
			Connection conn = null;
			PreparedStatement stmt = null;
			ResultSet rst = null;
			
			try {
				conn = DBManager.getConnection();
				stmt = conn.prepareStatement(sql);
				stmt.setString(1, oid);
				stmt.setString(2, company);
				rst = stmt.executeQuery();
				
				if (rst.next()) {
					cost=rst.getInt("totalcost");
				}
			} catch (SQLException e) {
				System.out.println("list all consult error : " + e);
			} finally {
				DBManager.close(conn, stmt, rst);
			}
			return cost;
			
		}
		
		
		//취소정산 불러오기 (수정중)
		public ArrayList<CancelBalanceVO> listCancelBalance(){
			ArrayList<CancelBalanceVO> list = new ArrayList<CancelBalanceVO>();
			String sql = "select a.state,c.appldate,a.date,b.company,b.buyerEmail,sum(b.cost) as totalCost "
					+ ",c.price,c.oid,a.canceller,b.delivery_cost from order_cancel a LEFT JOIN purchase b "
					+ "ON (a.purchaseId=b.purchaseId) LEFT JOIN payinfo c ON(b.oid=c.oid) "
					+ "group by c.oid order by a.date asc;";
			
			Connection conn = null;
			PreparedStatement stmt = null;
			ResultSet rst = null;
			CancelBalanceVO vo = null;
			try {
				conn = DBManager.getConnection();
				stmt = conn.prepareStatement(sql);
				rst = stmt.executeQuery();
				while (rst.next()) {
					vo = new CancelBalanceVO();
					vo.setState(rst.getInt("a.state"));
					vo.setAppldate(rst.getDate("c.appldate"));
					vo.setDate(rst.getDate("a.date"));
					vo.setCompany(rst.getString("b.company"));
					vo.setBuyerEmail(rst.getString("b.buyerEmail"));
					vo.setCost(rst.getInt("totalCost"));
					vo.setPrice(rst.getInt("c.price"));
					vo.setOid(rst.getString("c.oid"));
					vo.setCanceller(rst.getString("a.canceller"));
					vo.setDelivery_cost(rst.getInt("b.delivery_cost"));
					if(rst.getInt("c.price")>(rst.getInt("b.delivery_cost")+rst.getInt("totalCost"))){
						vo.setCancelWay("부분취소");
					}else{
						vo.setCancelWay("전체취소");
					}
						
					list.add(vo);
				}
			} catch (SQLException e) {
				System.out.println("list all consult error : " + e);
			} finally {
				DBManager.close(conn, stmt, rst);
			}
			return list;
		}
		// 답장 완료
		public int updateReply(int num) {
			int result = -1;
			String sql = "update consult set reply=1 where consultNum=?";

			Connection conn = null;
			PreparedStatement pstmt = null;
			try {
				conn = DBManager.getConnection();
				pstmt = conn.prepareStatement(sql);

				pstmt.setInt(1, num);

				result = pstmt.executeUpdate();
			} catch (Exception e) {
				System.out.println("update reply error :  " + e);
			} finally {
				DBManager.close(conn, pstmt);
			}
			return result;
		}

		//자주하는 질문 등록
		public int insertFAQ(String sort,String question,String answer) {
			int result = -1;
			String sql = "insert into faq(sort,question,answer) values(?,?,?)";
			Connection conn = null;
			PreparedStatement pstmt = null;
			try {
				conn = DBManager.getConnection();
				pstmt = conn.prepareStatement(sql);

				pstmt.setString(1, sort);
				pstmt.setString(2, question);
				pstmt.setString(3, answer);

				result = pstmt.executeUpdate();
			} catch (Exception e) {
				System.out.println("insert faq error "+e);
			} finally {
				DBManager.close(conn, pstmt);
			}
			return result;
		}
		
		
		//정산하기
		public ArrayList<AdminCalculateVO> getAllCalculate(String first,String end){
			String sql="SELECT state,a.email,purchaseConfirm_date,"
					+ "sum(number_of_orders) as Tnum_of_orders,sum(calculate_amount) as Tcalculate_amount,"
					+ "sum(payment_amount) as Tpayment_amount,sum(surtax) as Tsurtax,sum(fee) as Tfee,"
					+ "sum(brokerage_fee) as Tbrokerage_fee, company, fullCompany, bank_name, account_holder,"
					+ "account_number FROM calculate a LEFT JOIN user_b b ON(a.email=b.email)"
					+ " where state='0' and purchaseConfirm_date between ? and ? group by a.email,purchaseConfirm_date";
			ArrayList<AdminCalculateVO> list=new ArrayList<>();
			AdminCalculateVO vo=null;
			Connection conn = null;
			PreparedStatement stmt = null;
			ResultSet rst = null;
			try {
				conn = DBManager.getConnection();
				stmt = conn.prepareStatement(sql);
				stmt.setString(1, first);
				stmt.setString(2, end);
				rst = stmt.executeQuery();

				while (rst.next()) {
				vo=new AdminCalculateVO();
				
				vo.setEmail(rst.getString("a.email"));
				vo.setState(rst.getInt("state"));
				vo.setPurchaseConfirm_date(rst.getString("purchaseConfirm_date"));
				vo.setCompany(rst.getString("company"));
				vo.setFullCompany(rst.getString("fullCompany"));
				vo.setCalculate_amount((int)(Math.ceil(rst.getDouble("Tcalculate_amount"))));
				vo.setBank_name(rst.getString("bank_name"));
				vo.setAccount_number(rst.getString("account_number"));
				vo.setAccount_holder(rst.getString("account_holder"));
				
				list.add(vo);
				
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("getAllCalculate error : "+e);
			} finally {
				DBManager.close(conn, stmt, rst);
			}
			return list;
		
		}
		
		//정산완료로 상태변경
		public int updateState(String purchaseConfirm_date, String email) {
			int result = -1;
			String sql = "update calculate set state=1,calculate_date=? where purchaseConfirm_date=? and email=?";
			Connection conn = null;
			PreparedStatement pstmt = null;
			try {
				conn = DBManager.getConnection();
				pstmt = conn.prepareStatement(sql);
				
				pstmt.setString(1, getDate() );
				pstmt.setString(2, purchaseConfirm_date);
				pstmt.setString(3, email);

				result = pstmt.executeUpdate();
			} catch (Exception e) {
				System.out.println("updateState error " + e);
			} finally {
				DBManager.close(conn, pstmt);
			}
			return result;
		}
		//취소 정산 정산완료로 상태변경
				public int updateCancelState(String oid) {
					int result = -1;
					String sql = "update order_cancel a LEFT JOIN purchase b ON(a.purchaseId=b.purchaseId) set a.state=1 where b.oid=?";
					Connection conn = null;
					PreparedStatement pstmt = null;
					try {
						conn = DBManager.getConnection();
						pstmt = conn.prepareStatement(sql);
						
						pstmt.setString(1, oid);
						result = pstmt.executeUpdate();
					} catch (Exception e) {
						System.out.println("updateCancelState error " + e);
					} finally {
						DBManager.close(conn, pstmt);
					}
					return result;
				}
		//날짜 포맷 바꾸기
				public String getDate() {

					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

					Calendar c1 = Calendar.getInstance();

					return sdf.format(c1.getTime());


				}
		
		//정산하기
				public ArrayList<AdminCalculateVO> getAllCalculateComplete(String first,String end){
					String sql="SELECT state,a.email,purchaseConfirm_date,"
							+ "sum(number_of_orders) as Tnum_of_orders,sum(calculate_amount) as Tcalculate_amount,"
							+ "sum(payment_amount) as Tpayment_amount,sum(surtax) as Tsurtax,sum(fee) as Tfee,"
							+ "sum(brokerage_fee) as Tbrokerage_fee, company, fullCompany, bank_name, account_holder,"
							+ "account_number FROM calculate a LEFT JOIN user_b b ON(a.email=b.email)"
							+ " where state='1' and purchaseConfirm_date between ? and ? group by a.email,purchaseConfirm_date";
					ArrayList<AdminCalculateVO> list=new ArrayList<>();
					AdminCalculateVO vo=null;
					Connection conn = null;
					PreparedStatement stmt = null;
					ResultSet rst = null;
					try {
						conn = DBManager.getConnection();
						stmt = conn.prepareStatement(sql);
						stmt.setString(1, first);
						stmt.setString(2, end);
						rst = stmt.executeQuery();

						while (rst.next()) {
						vo=new AdminCalculateVO();
						
						vo.setEmail(rst.getString("a.email"));
						vo.setState(rst.getInt("state"));
						vo.setPurchaseConfirm_date(rst.getString("purchaseConfirm_date"));
						vo.setCompany(rst.getString("company"));
						vo.setFullCompany(rst.getString("fullCompany"));
						vo.setCalculate_amount((int)(Math.ceil(rst.getDouble("Tcalculate_amount"))));
						vo.setBank_name(rst.getString("bank_name"));
						vo.setAccount_number(rst.getString("account_number"));
						vo.setAccount_holder(rst.getString("account_holder"));
						
						list.add(vo);
						
						}
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println("getAllCalculate error : "+e);
					} finally {
						DBManager.close(conn, stmt, rst);
					}
					return list;
				
				}
		
	/*
	// 일반 회원 가입
	public int insertUser(UserVO vo) {
		int result = -1;
		String sql = "insert into user(email,pwd,name,nation,phone,address1,zipnum1,admin) values(?,?,?,?,?,?,?,?)";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, vo.getEmail());
			pstmt.setString(2, vo.getPwd());
			pstmt.setString(3, vo.getName());
			pstmt.setInt(4, vo.getNation());
			pstmt.setString(5, vo.getPhone());
			pstmt.setString(6, vo.getAddress1());
			pstmt.setString(7, vo.getZipnum1());
			pstmt.setInt(8, vo.getAdmin());

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	// 기업회원 가입
	public int insertUserB(UserVO vo) {
		int result = -1;

		String sql = "insert into user(email,pwd,name,nation,phone,address1,zipnum1,admin) values(?,?,?,?,?,?,?,?)";
		String sql2 = "insert into user_b(rep_name,rep_num,rep_phone,company,email) values(?,?,?,?,?)";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, vo.getEmail());
			pstmt.setString(2, vo.getPwd());
			pstmt.setString(3, vo.getName());
			pstmt.setInt(4, vo.getNation());
			pstmt.setString(5, vo.getPhone());
			pstmt.setString(6, vo.getAddress1());
			pstmt.setString(7, vo.getZipnum1());
			pstmt.setInt(8, vo.getAdmin());

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, pstmt);
		}
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql2);

			pstmt.setString(1, vo.getRep_name());
			pstmt.setString(2, vo.getRep_num());
			pstmt.setString(3, vo.getRep_phone());
			pstmt.setString(4, vo.getCompany());
			pstmt.setString(5, vo.getEmail());

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, pstmt);
		}

		return result;
	}

	// 회원 이메일 인증 후 권한 주기
	public int selectAuth(String email,String auth) {
		int result = -1;
		String sql = "select emailchk from user where email=?";

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs=null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, email);	
	
			rs = pstmt.executeQuery();
			while(rs.next()){
				if(rs.getString("emailchk").equals(auth)){
					result=1;
				}
			}
			
		} catch (Exception e) {
			System.out.println("select auth error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	// 회원 이메일 인증 후 권한 주기
	public int updateAuth(String email) {
		int result = -1;
		String sql = "update user set emailchk=1 where email=?";

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, email);

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("update auth error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	// 이메일로 임시 비밀번호 보내고 임시 비밀번호로 바꾸기
	public int updateTempPassword(String email, String pwd) {
		int result = -1;
		String sql = "update user set pwd=? where email=?";

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, pwd);
			pstmt.setString(2, email);

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("update to temp password error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return result;
	}

	// 이메일 중복체크
	public int confirmID(String email) {
		int result = -1;
		String sql = "select * from user where email=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				result = 1;
			} else {
				result = -1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, pstmt, rs);
		}
		return result;
	}

	public UserVO userCheck(String email, String pwd) {
		int result = -1;
		String sql = "select pwd,admin,emailchk from user where email=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		UserVO vo = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();
			vo = new UserVO();
			if (rs.next()) {
				if (rs.getString("pwd") != null // 비밀번호 맞으면
						&& rs.getString("pwd").equals(pwd)) {
					vo.setAdmin(rs.getInt("admin"));
					vo.setEmailchk(rs.getInt("emailchk"));

					// 권한을 가져옴 (2 or 1 or 0)
				} else { // 비밀 번호 틀리면

					vo.setAdmin(-1);
				}
			} else {// 결과가 없으면
				vo.setAdmin(-1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return vo;
	}
	
	//인증번호 넣기
	public void setAuthNum(String email,String auth){
		String sql = "update user set emailchk=? where email=?";
		int result=0;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, auth);
			pstmt.setString(2, email);

			result = pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("update auth num password error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return;
	}

	public UserVO getUserAddress(String email) {
		UserVO vo = null;
		String sql = "select * from user where email=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				vo = new UserVO();
				vo.setAddress1(rs.getString("address1"));
				vo.setAddress2(rs.getString("address2"));
				vo.setAddress3(rs.getString("address3"));
				vo.setAddress4(rs.getString("address4"));
				vo.setZipnum1(rs.getString("zipnum1"));
				vo.setZipnum2(rs.getString("zipnum2"));
				vo.setZipnum3(rs.getString("zipnum3"));
				vo.setZipnum4(rs.getString("zipnum4"));
			}
		} catch (Exception e) {
			System.out.println("get user address error"+e);
		} finally {
			DBManager.close(conn, pstmt, rs);
		}

		return vo;
	}

	public UserVO getMember(String email) {
		String sql = "select * from user where email=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		UserVO mVo = null;

		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				mVo = new UserVO();
				mVo.setEmail(email);
				mVo.setName(rs.getString("name"));
				mVo.setNation(rs.getInt("nation"));
				mVo.setPhone(rs.getString("phone"));
				mVo.setAddress1(rs.getString("address1"));
				mVo.setZipnum1(rs.getString("zipnum1"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, pstmt, rs);
		}
		
		return mVo;
	}
	
	public UserVO getRepMember(String email){
		String sql2="select * from user_b where email=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		UserVO mVo=null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql2);
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();

			if(rs.next()){
				mVo = new UserVO();
				mVo.setRep_name(rs.getString("rep_name"));
				mVo.setRep_num(rs.getString("rep_num"));
				mVo.setRep_phone(rs.getString("rep_phone"));
				mVo.setCompany(rs.getString("company"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.close(conn, pstmt);
		}
		return mVo;
	}*/
	
	

}