package com.sid.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.sid.dto.ConsultVO;
import com.sid.dto.CustomerVO;
import com.sid.dto.ProductVO;
import com.sid.dto.UserVO;
import com.sid.util.DBManager;

public class CustomerDAO {
	private CustomerDAO() {

	}

	private static CustomerDAO instance=new CustomerDAO(){};

	public static CustomerDAO getInstance() {
		return instance;
	}

	public ArrayList<CustomerVO> ListCustomer(String email) {
		String sql = "SELECT count(*), sum(cost), buyerEmail, buyerPhone, buyerName FROM (SELECT buyerEmail,buyerPhone,buyerName,cost FROM purchase where seller=? group by purchaseId)PID GROUP BY buyerEmail ORDER BY count(*) desc";
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rst=null;
		ArrayList<CustomerVO> list=new ArrayList<>();
		CustomerVO vo=null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, email);	
	
			rst = pstmt.executeQuery();
			while(rst.next()){
				vo=new CustomerVO();
				vo.setTransaction_count(rst.getInt(1));
				vo.setTransaction_price(rst.getInt(2));
				vo.setBuyerEmail(rst.getString("buyerEmail"));
				vo.setBuyerPhone(rst.getString("buyerPhone"));
				vo.setBuyerName(rst.getString("buyerName"));
				list.add(vo);
			}
			
		} catch (Exception e) {
			System.out.println("select list customer error :  " + e);
		} finally {
			DBManager.close(conn, pstmt);
		}
		return list;
	}

		
}