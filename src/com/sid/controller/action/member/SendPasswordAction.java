package com.sid.controller.action.member;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sid.controller.Action;
import com.sid.dao.MemberDAO;

//일반 회원 가입
public class SendPasswordAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String url = "member/joinResult.jsp";

		MemberDAO memberDAO = MemberDAO.getInstance();

		String email = request.getParameter("email");
		
		int emailCheck=memberDAO.confirmID(email);
		if(emailCheck>0){

			String tempPwd = "";
			EmailAuth auth = new EmailAuth();

			tempPwd = auth.RandomPassword();//임시 비밀번호 생성

			auth.sendTempPassword(email, tempPwd);//임시 비밀번호 이메일 전송
			
			String salt = memberDAO.getUserSalt(email);
			String newPassword = SHA256Util.getEncrypt(tempPwd, salt);

			int result = memberDAO.updateTempPassword(email, newPassword);

			if (result > 0) {
				request.setAttribute("result", "1");
			} else {
				request.setAttribute("result", "0");
			}
			
		}else{
			request.setAttribute("result", "-1");
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
