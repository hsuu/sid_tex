package com.sid.controller.action.member;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sid.controller.Action;
import com.sid.dao.MemberDAO;
import com.sid.dto.UserVO;

//일반 회원 가입
public class JoinBAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String url = "member/joinResult.jsp";

		UserVO vo = new UserVO();
		String email = request.getParameter("email");
		vo.setEmail(email);
		// 비밀번호 암호화
		String salt = SHA256Util.generateSalt();
		String newPassword = SHA256Util.getEncrypt(request.getParameter("pwd"), salt);
		vo.setSalt(salt);
		vo.setPwd(newPassword);
		vo.setName(request.getParameter("lastname") + "/" + request.getParameter("firstname"));
		vo.setPhone(request.getParameter("phone"));
		vo.setCompanyAddress(request.getParameter("companyroadAddrPart1") + "/" + request.getParameter("companyaddrDetail"));
		vo.setCompanyZipnum(request.getParameter("companyzipnum"));
		vo.setAddress1(request.getParameter("roadAddrPart1") + "/" + request.getParameter("addrDetail"));
		vo.setZipnum1(request.getParameter("zipnum"));
		vo.setAdmin(Integer.parseInt(request.getParameter("admin")));
		vo.setRep_name(request.getParameter("rep_name"));
		vo.setRep_num(request.getParameter("rep_num"));
		vo.setRep_phone(request.getParameter("rep_phone"));
		vo.setFullCompany(request.getParameter("fullCompany"));
		if(request.getParameter("company")!=null){
			vo.setCompany(request.getParameter("company"));
		}
		vo.setRep_num_flag(1);//사업자 등록번호 검토
		
		//-----배송정보
				int exp2 = 0;
				String[] delivery_way = request.getParameterValues("delivery_way");
				if (delivery_way != null) {
					for (int i = 0; i < delivery_way.length; i++) {
						exp2 += Integer.parseInt(delivery_way[i]);
					}
				}
				vo.setDelivery_way(exp2);
				vo.setDelivery_costway(request.getParameter("delivery_detail"));
				
				//교환반품지 주소
				vo.setReturnZipnum(request.getParameter("returnZipnum"));
				vo.setReturnRoadAddrPart1(request.getParameter("returnRoadAddrPart1"));
				vo.setReturnAddrDetail(request.getParameter("returnAddrDetail"));
				
				//판매자 지정 퀵
				vo.setDeliveryCompany_quick(request.getParameter("deliveryCompany_quick"));
				
				//예상되는 배송기간
				if(request.getParameter("delivery_period")!=null){
					vo.setDelivery_period(Integer.parseInt(request.getParameter("delivery_period")));
				}else{
					vo.setDelivery_period(7);
				}

				int exp = 0;
				String[] express = request.getParameterValues("delivery");
				if (express != null) {
					for (int i = 0; i < express.length; i++) {
						exp += Integer.parseInt(express[i]);
					}
				}
				
				//비용
				vo.setDelivery(exp);
				vo.setDelivery_cost(0);
				if (request.getParameter("delivery_detail").equals("무료")) {

				} else if (request.getParameter("delivery_detail").equals("유료")) {
					if (request.getParameter("delivery_cost") != null) {
						vo.setDelivery_cost(Integer.parseInt(request.getParameter("delivery_cost")));
					}
				} else if (request.getParameter("delivery_detail").equals("조건부무료")) {
					if (request.getParameter("delivery_cost2") != null) {
						vo.setDelivery_cost(Integer.parseInt(request.getParameter("delivery_cost2")));
						vo.setDelivery_cost2(Integer.parseInt(request.getParameter("delivery_cost3")));
					}
				}
				
				//왕복택배비
				if (request.getParameter("re_delivery_cost1") != null) {
					vo.setRe_delivery_cost1(Integer.parseInt(request.getParameter("re_delivery_cost1")));

				} else {
					vo.setRe_delivery_cost1(0);
				}
				//판매자 지정택배사
					vo.setDeliveryCompany(request.getParameter("deliveryCompany"));

					
		
		MemberDAO memberDAO = MemberDAO.getInstance();
		int result = memberDAO.insertUserB(vo);
		String authNum = "";
		EmailAuth auth = new EmailAuth();
		if (result > 0) {
			System.out.println("join success");

			authNum = auth.RandomNum();// 인증번호 생성

			auth.sendEmail(email, authNum);// 이메일 전송

			memberDAO.setAuthNum(email, authNum);// 인증번호 세팅
			request.setAttribute("result", "1");
		} else {
			System.out.println("join fail");
			request.setAttribute("result", "0");
		}

		
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
