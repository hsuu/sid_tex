package com.sid.controller.action.member;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.MemberDAO;
import com.sid.dto.UserVO;


//일반 회원 가입
public class CompanyCheckAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String url = "member/joinResult.jsp";
		
		String company=request.getParameter("company");
		MemberDAO memberDAO = MemberDAO.getInstance();
		//복호화
		int result=memberDAO.confirmCompany(company);
		
		if(result>0){
			request.setAttribute("result", 0);
		}else{
			request.setAttribute("result", 1);
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
