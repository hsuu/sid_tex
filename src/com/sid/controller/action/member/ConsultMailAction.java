package com.sid.controller.action.member;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sid.controller.Action;
import com.sid.dao.MemberDAO;

//일반 회원 가입
public class ConsultMailAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String url = "member/joinResult.jsp";

		MemberDAO memberDAO = MemberDAO.getInstance();

		String email = request.getParameter("email");
		String content = request.getParameter("content");

		int result = memberDAO.insertConsult(email, content);
		if (result > 0) {

			request.setAttribute("result", "1");
		} else {
			request.setAttribute("result", "0");
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
