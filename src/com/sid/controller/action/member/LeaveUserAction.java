package com.sid.controller.action.member;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.MemberDAO;
import com.sid.dto.UserVO;

public class LeaveUserAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "member/joinResult.jsp";

		HttpSession session = request.getSession();
		MemberDAO dao=MemberDAO.getInstance();
		
		String email = (String)session.getAttribute("email");
		
		
		String reason=request.getParameter("reason");
		int result=dao.insertUserLeave(email, reason);
		int result2=0;
		
		if(result>0){
			result2=dao.leaveMember(email);
			dao.updateProductDelyn(email);
			dao.deleteBasket(email);
			session.invalidate();
		}

		request.setAttribute("result",result2);
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
