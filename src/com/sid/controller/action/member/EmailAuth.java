package com.sid.controller.action.member;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailAuth {

	private String host="mw-002.cafe24.com";//smtp서버
	private String fromName="sid관리자";
	private String from="admin@sid-tex.com";//보내는 메일
	
	
	//인증번호 생성
	public String RandomNum(){
		StringBuffer buffer=new StringBuffer();
		for(int i=0;i<7;i++){
			int n=(int)(Math.random()*10);
			buffer.append(n);
		}
		return buffer.toString();
	}
	
	//임시 비밀번호 생성
	public String RandomPassword(){
		StringBuffer buffer=new StringBuffer();
		for(int i=0;i<7;i++){
			if((int)(Math.random()*10)>4){
				char c = (char) (Math.random()*26 + 'a');
				buffer.append(c);
			}else{
				int n=(int)(Math.random()*10);
				buffer.append(n);
			}
		}
		return buffer.toString();
	}
	
	// 인증 번호 보내기
	public void sendEmail(String email,String authNum){
		String subject="Sid_tex 인증 메일";//제목
		String to=email;//보낼 이메일
		String content="인증번호 ["+authNum+"]";//내용
		
		//-----

		Properties props = new Properties();
		props.put("mail.smtp.host", host); //호스트 설정
		props.put("mail.smtp.user", fromName); //보내는 사람 설정
		props.put("mail.smtp.auth","true"); //권한

		SmtpAuthenticator auth=new SmtpAuthenticator();//smtp 권한 얻기
		Session sess = Session.getInstance(props, auth);

		try {
		        Message msg = new MimeMessage(sess);
		        msg.setFrom(new InternetAddress(from));
		        InternetAddress[] address = {new InternetAddress(to)};
		        msg.setRecipients(Message.RecipientType.TO, address);
		        msg.setSubject(subject);
		        msg.setSentDate(new Date());
		        msg.setText(content);

		        Transport.send(msg);
		        System.out.println(email+"로 이메일 전송");
		        
		} catch (MessagingException mex) {
		        System.out.println(mex.getMessage());
		        System.out.println(email+"로 전송 실패");
		}


		
	}
	
	//임시비밀번호 보내기
	public void sendTempPassword(String email,String pwd){
		String subject="Sid_tex 임시 비밀번호";//제목
		String to=email;
		String content="임시 비밀번호 ["+pwd+"] <br> 내 정보에서 비밀번호를 바꿔주세요.";
		
		//-----

		Properties props = new Properties();
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.user", fromName);
		props.put("mail.smtp.auth","true");

		SmtpAuthenticator auth=new SmtpAuthenticator();
		Session sess = Session.getInstance(props, auth);

		try {
		        Message msg = new MimeMessage(sess);
		        msg.setFrom(new InternetAddress(from));
		        InternetAddress[] address = {new InternetAddress(to)};
		        msg.setRecipients(Message.RecipientType.TO, address);
		        msg.setSubject(subject);
		        msg.setSentDate(new Date());
		        msg.setText(content);

		        Transport.send(msg);
		        System.out.println(email+"로 이메일 전송");
		        
		} catch (MessagingException mex) {
		        System.out.println(mex.getMessage());
		        System.out.println(email+"로 전송 실패");
		}


		
	}
	//문의 답장 보내기
		public int sendConsult(String email,String content){
			String subject="Sid_tex 메일 문의 답변입니다";//제목
			String to=email;
			int result=0;
			//-----

			Properties props = new Properties();
			props.put("mail.smtp.host", host);
			props.put("mail.smtp.user", fromName);
			props.put("mail.smtp.auth","true");

			SmtpAuthenticator auth=new SmtpAuthenticator();
			Session sess = Session.getInstance(props, auth);

			try {
			        Message msg = new MimeMessage(sess);
			        msg.setFrom(new InternetAddress(from));
			        InternetAddress[] address = {new InternetAddress(to)};
			        msg.setRecipients(Message.RecipientType.TO, address);
			        msg.setSubject(subject);
			        msg.setSentDate(new Date());
			        msg.setText(content);

			        Transport.send(msg);
			        System.out.println(email+"로 이메일 전송");
			        result=1;
			        
			} catch (MessagingException mex) {
			        System.out.println(mex.getMessage());
			        System.out.println(email+"로 전송 실패");
			        result=-1;
			}

			return result;
		}
	
}
