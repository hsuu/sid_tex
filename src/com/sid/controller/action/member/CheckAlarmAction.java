package com.sid.controller.action.member;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.AlarmDAO;
import com.sid.dao.MemberDAO;
import com.sid.dto.UserVO;


//일반 회원 가입
public class CheckAlarmAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String url = "member/joinResult.jsp";
		
		HttpSession session=request.getSession();
		String email=(String)session.getAttribute("email");
		
		if(email!=null){
			//알람 체크
			AlarmDAO adao=AlarmDAO.getInstance();
			int alarm=adao.checkNewAlarm(email);
			System.out.println("alarm : "+alarm);
			session.setAttribute("alarm", alarm);
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
