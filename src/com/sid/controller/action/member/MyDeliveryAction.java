package com.sid.controller.action.member;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.MemberDAO;
import com.sid.dto.UserVO;

public class MyDeliveryAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "mypage/editDelivery.jsp";

		HttpSession session = request.getSession();
		String email = (String) session.getAttribute("email");
		MemberDAO mDao = MemberDAO.getInstance();
		UserVO vo = mDao.getUserAddress(email);
		String[] arr = null;
		String[] arr2 = null;
		String[] arr3 = null;
		String[] arr4=null;
		try {
			if (vo.getAddress1() != null) {
				arr = vo.getAddress1().split("/");
				if (arr.length > 1) {
					request.setAttribute("address1", arr[0]);
					request.setAttribute("address_detail1", arr[1]);
				}
			}
			if (vo.getAddress2() != null) {
				arr2 = vo.getAddress2().split("/");
				if (arr2.length > 1) {
					request.setAttribute("address2", arr2[0]);
					request.setAttribute("address_detail2", arr2[1]);
				}
			}
			if (vo.getAddress3() != null) {
				arr3 = vo.getAddress3().split("/");
				if (arr3.length > 1) {
					request.setAttribute("address3", arr3[0]);
					request.setAttribute("address_detail3", arr3[1]);
				}
			}
			if (vo.getAddress4() != null) {
				arr4 = vo.getAddress4().split("/");
				if (arr4.length > 1) {
					request.setAttribute("address4", arr4[0]);
					request.setAttribute("address_detail4", arr4[1]);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("MyDeliveryAction error: " + e);
		}
		request.setAttribute("list", vo);
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
