package com.sid.controller.action.member;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.authentication.Sha256PasswordPlugin;
import com.sid.controller.Action;
import com.sid.dao.MemberDAO;
import com.sid.dto.UserVO;

//일반 회원 가입
public class JoinAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String url = "member/joinResult.jsp";

		UserVO vo = new UserVO();
		String email = request.getParameter("email");
		vo.setEmail(email);
		
		// 비밀번호 암호화
		String salt = SHA256Util.generateSalt();
		String newPassword = SHA256Util.getEncrypt(request.getParameter("pwd"), salt);
		vo.setSalt(salt);
		vo.setPwd(newPassword);
		vo.setName(request.getParameter("lastname") + request.getParameter("firstname"));
		vo.setPhone(request.getParameter("phone"));
		vo.setAddress1(request.getParameter("roadAddrPart1") + "/" + request.getParameter("addrDetail"));
		vo.setZipnum1(request.getParameter("zipNum"));
		vo.setAdmin(4);

		MemberDAO memberDAO = MemberDAO.getInstance();
		int result = memberDAO.insertUser(vo);
		String authNum = "";
		EmailAuth auth = new EmailAuth();

		if (result > 0) {
			System.out.println("join success");

			authNum = auth.RandomNum();// 인증번호 생성

			auth.sendEmail(email, authNum);// 이메일 전송

			memberDAO.setAuthNum(email, authNum);// 인증번호 세팅
			request.setAttribute("result", "1");
		} else {
			System.out.println("join fail");
			request.setAttribute("result", "0");
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
