package com.sid.controller.action.member;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.MemberDAO;
import com.sid.dto.UserVO;


//암호 확인
public class PasswordCheckAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String url = "member/joinResult.jsp";
		
		HttpSession session = request.getSession();
		String email=(String)session.getAttribute("email");
		String pwd=request.getParameter("pwd");
		MemberDAO memberDAO = MemberDAO.getInstance();
		//복호화
		String salt=memberDAO.getUserSalt(email);
		String newPassword=SHA256Util.getEncrypt(pwd, salt);
		int result=memberDAO.passwordCheck(email, newPassword);
		
		System.out.println("result : "+result);
		if(result>0){
			request.setAttribute("result", "1");
		}else{
			request.setAttribute("result", "0");
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
