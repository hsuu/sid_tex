package com.sid.controller.action.member;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.MemberDAO;
import com.sid.dto.UserVO;


//일반 회원 가입
public class UpdateAuthAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String url = "member/joinResult.jsp";
		
		System.out.println("u a action");
		
		HttpSession session = request.getSession();
		
		String email=(String)session.getAttribute("email");
		
		//인증번호
		String chk=request.getParameter("chk");
		
		MemberDAO memberDAO = MemberDAO.getInstance();
		//인증 번호 확인
		int result=memberDAO.selectAuth(email, chk);
		//인증번호 확인 후 처리
		if(result>0){
			result=memberDAO.updateAuth(email);
		}
		
		if(result>0){
			request.setAttribute("result", "1");
			session.setAttribute("emailChk", 0);
		}else{
			request.setAttribute("result", "0");
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
