package com.sid.controller.action.member;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.MemberDAO;
import com.sid.dto.UserVO;

public class MyInfoBAction implements Action{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="mypage/editBInfo.jsp";
		HttpSession session=request.getSession();
		
		MemberDAO mDao=MemberDAO.getInstance();
		UserVO vo=mDao.getMember((String)session.getAttribute("email"));
		UserVO vo2=mDao.getRepMember((String)session.getAttribute("email"));
		
//		String[] arr=null;
//		arr=vo.getAddress1().split("/");
//		try{
//			request.setAttribute("address", arr[0]);
//			request.setAttribute("address_detail", arr[1]);
//		}catch(Exception e){
//			e.printStackTrace();
//		}
		request.setAttribute("list", vo);
		request.setAttribute("list2", vo2);
		RequestDispatcher dispatcher=request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
