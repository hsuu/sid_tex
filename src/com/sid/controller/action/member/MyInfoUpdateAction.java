package com.sid.controller.action.member;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.MemberDAO;
import com.sid.dto.UserVO;

public class MyInfoUpdateAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "member/joinResult.jsp";
		
		HttpSession session = request.getSession();
		UserVO vo = new UserVO();
		String email=request.getParameter("email");
		vo.setEmail(email);
		vo.setPwd(request.getParameter("pwd"));
		vo.setName(request.getParameter("name"));
		vo.setNation(Integer.parseInt(request.getParameter("nation")));
		vo.setPhone(request.getParameter("phone"));
		vo.setAddress1(request.getParameter("roadAddrPart1")+"/"+request.getParameter("addrDetail"));
		vo.setZipnum1(request.getParameter("zipNum"));
		
		
		MemberDAO memberDAO = MemberDAO.getInstance();
		int result=memberDAO.updateUser(vo);
		String authNum="";
		EmailAuth auth=new EmailAuth();
		
		if(result>0){
			
			authNum=auth.RandomNum();//인증번호 생성
			
			auth.sendEmail(email, authNum);//이메일 전송
			
			request.setAttribute("result", authNum);
			
			session.setAttribute("email", request.getParameter("email"));
		}else{
			request.setAttribute("result", "0");
		}
	

		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
