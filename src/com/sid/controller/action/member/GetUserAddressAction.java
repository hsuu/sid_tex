package com.sid.controller.action.member;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.MemberDAO;
import com.sid.dto.UserVO;


//일반 회원 가입
public class GetUserAddressAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String url = "mypage/getUserAddressResult.jsp";
		
		HttpSession session = request.getSession();
		String email=(String)session.getAttribute("email");
	
		MemberDAO memberDAO = MemberDAO.getInstance();
		UserVO vo=memberDAO.getUserAddress(email);
				
		request.setAttribute("result", vo);
	
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
