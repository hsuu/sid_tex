package com.sid.controller.action.member;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.MemberDAO;
import com.sid.dto.UserVO;

public class MyDeliveryUpdateAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "member/joinResult.jsp";

		UserVO vo = new UserVO();
		MemberDAO dao = MemberDAO.getInstance();
		HttpSession session = request.getSession();
		String email = (String) session.getAttribute("email");
		vo.setEmail(email);

		if (request.getParameter("roadAddrPart1") != null && request.getParameter("addrDetail") != null) {
			vo.setAddress1(request.getParameter("roadAddrPart1") + "/" + request.getParameter("addrDetail"));
		}
		if (request.getParameter("zipNum") != null) {
			vo.setZipnum1(request.getParameter("zipNum"));
		}
		if (request.getParameter("roadAddrPart2") != null && request.getParameter("addrDetail2") != null) {
			vo.setAddress2(request.getParameter("roadAddrPart2") + "/" + request.getParameter("addrDetail2"));
		}
		if (request.getParameter("zipNum2") != null) {
			vo.setZipnum2(request.getParameter("zipNum2"));
		}
		if (request.getParameter("roadAddrPart3") != null && request.getParameter("addrDetail3") != null) {
			vo.setAddress3(request.getParameter("roadAddrPart3") + "/" + request.getParameter("addrDetail3"));
		}
		if (request.getParameter("zipNum3") != null) {
			vo.setZipnum3(request.getParameter("zipNum3"));
		}
		if (request.getParameter("roadAddrPart4") != null && request.getParameter("addrDetail4") != null) {
			vo.setAddress4(request.getParameter("roadAddrPart4") + "/" + request.getParameter("addrDetail4"));
		}
		if (request.getParameter("zipNum4") != null) {
			vo.setZipnum4(request.getParameter("zipNum4"));
		}
		int result = 0;
		result = dao.updateDelivery(vo);

		if (result > 0) {
			request.setAttribute("result", "1");
		} else {
			request.setAttribute("result", "0");
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
