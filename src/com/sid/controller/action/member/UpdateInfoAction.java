package com.sid.controller.action.member;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.authentication.Sha256PasswordPlugin;
import com.sid.controller.Action;
import com.sid.dao.MemberDAO;
import com.sid.dto.UserVO;

//일반 회원 가입
public class UpdateInfoAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String url = "member/joinResult.jsp";

		UserVO vo = new UserVO();
		
		String email = request.getParameter("email");
		String state=request.getParameter("state");
		MemberDAO memberDAO = MemberDAO.getInstance();
		
		vo.setEmail(email);
		
		int result=0;
		if(state.equals("standard")){//개인 회원 정보 수정
			vo.setName(request.getParameter("name"));
//			vo.setNation(Integer.parseInt(request.getParameter("nation")));
			vo.setPhone(request.getParameter("phone"));
//			vo.setAddress1(request.getParameter("roadAddrPart1") + "/" + request.getParameter("addrDetail"));
//			vo.setZipnum1(request.getParameter("zipNum"));
			result = memberDAO.updateUser(vo);
		}else if(state.equals("pwd")){//비밀번호 수정
			// 비밀번호 암호화
			String salt = memberDAO.getUserSalt(email);
			String newPassword = SHA256Util.getEncrypt(request.getParameter("pwd"), salt);
			vo.setPwd(newPassword);
			result = memberDAO.updatePassword(vo);
		}else{ //기업 회원 정보 수정
			vo.setRep_phone(request.getParameter("rep_phone"));
			vo.setName(request.getParameter("name"));
			vo.setPhone(request.getParameter("phone"));
//			vo.setNation(Integer.parseInt(request.getParameter("nation")));
//			vo.setAddress1(request.getParameter("roadAddrPart1") + "/" + request.getParameter("addrDetail"));
//			vo.setZipnum1(request.getParameter("zipNum"));
			result = memberDAO.updateUser(vo);
			if(result>0){
				result=memberDAO.updateUserB(vo);
			}
		}

		if (result > 0) {
			request.setAttribute("result", "1");
		} else {
			request.setAttribute("result", "0");
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
