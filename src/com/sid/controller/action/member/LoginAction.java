package com.sid.controller.action.member;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import com.sid.controller.Action;
import com.sid.dao.AlarmDAO;
import com.sid.dao.IpAddressDAO;
import com.sid.dao.MemberDAO;
import com.sid.dto.UserVO;

public class LoginAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "";

		HttpSession session = request.getSession();
		
		String email = request.getParameter("email");
		String pwd = request.getParameter("pwd");
		String id_chk = request.getParameter("remember");
		
		// 로그인
		MemberDAO memberDAO = MemberDAO.getInstance();
		
		//IP주소
//		IpAddressDAO ip=IpAddressDAO.getInstance();
//		UserVO vo2 = new UserVO();
		
//		vo2.setEmail(email);
//		vo2.setIpAddress((String)request.getRemoteAddr());
		
		//복호화
		String salt=memberDAO.getUserSalt(email);
		String newPassword=SHA256Util.getEncrypt(pwd, salt);
		UserVO vo = memberDAO.userCheck(email, newPassword);
		
		
		

		//로그인 실패시
		if (vo.getAdmin() == -1) {
			request.setAttribute("result", "0");
			url = "member/login.jsp";
		}else if(vo.getAdmin()==9){
			request.setAttribute("result", "1");
			url = "member/login.jsp";
		}else { //로그인 성공시
				
//				ip.insertIP(vo2);
				session.setAttribute("email", email);
				session.setAttribute("admin", vo.getAdmin());
				session.setAttribute("company", vo.getCompany());
				session.setAttribute("rep_phone", vo.getRep_phone());
				session.setAttribute("rep_name", vo.getRep_name());
				session.setAttribute("rep_num", vo.getRep_num());
				session.setAttribute("repflag", vo.getRep_num_flag());
				session.setAttribute("fullCompany", vo.getFullCompany());
				session.setAttribute("companyAddress", vo.getCompanyAddress());
				url = "SidServlet?command=main";
				if (vo.getEmailchk() != 1) { //email 인증을 안했을 경우
					session.setAttribute("emailChk", 1);
					url="member/emailAuthentication.jsp";
				}
				// 쿠키 설정
				if (id_chk != null && id_chk.trim().equals("on")) {
					Cookie cookie = new Cookie("SaveEmail", email);
					cookie.setMaxAge(60 * 60 * 24 * 365);// 1년
					cookie.setPath("/");
					response.addCookie(cookie);

				} else {
					Cookie cookie = new Cookie("SaveEmail", email);
					cookie.setMaxAge(0);
					cookie.setPath("/");
					response.addCookie(cookie);
				}
			
			
			
		}
		AlarmDAO adao=AlarmDAO.getInstance();
		int alarm=adao.checkNewAlarm(email);
		session.setAttribute("alarm", alarm);
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
	
}

