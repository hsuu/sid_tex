package com.sid.controller.action.product;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sid.controller.Action;
import com.sid.dao.ProductDAO;
import com.sid.dto.ProductVO;

public class ProductBeforeModifyAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="product/product_modify.jsp";
	 	ProductDAO pDao=ProductDAO.getInstance();
	 	ProductVO pVo=new ProductVO();
	 	int productId=Integer.parseInt(request.getParameter("productId"));
		System.out.println("ProductBeforeModifyAction : "+productId);
			
		pVo=pDao.readProduct(productId);
		
		
		ArrayList<String> uses=new ArrayList<>();
		uses=pDao.readUses(productId);
		
		ArrayList<String> color=new ArrayList<>();
		color=pDao.readColor(productId);
		
		ArrayList<String> fiber=new ArrayList<>();
		fiber=pDao.readFiber(productId);
		
		request.setAttribute("product", pVo);
		request.setAttribute("uses", uses.toString());
		request.setAttribute("color", color.toString());
		request.setAttribute("fiber", fiber.toString());
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
