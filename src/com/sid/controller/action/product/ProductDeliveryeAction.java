package com.sid.controller.action.product;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.MemberDAO;
import com.sid.dto.UserVO;

public class ProductDeliveryeAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="product/product_manage_delivery.jsp";
		
		HttpSession session = request.getSession();
		UserVO vo=new UserVO();
		MemberDAO dao=MemberDAO.getInstance();
		vo=dao.getDelivery((String)session.getAttribute("email"));
		
		request.setAttribute("list", vo);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
