package com.sid.controller.action.product;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sid.controller.Action;
import com.sid.dao.MemberDAO;
import com.sid.dao.ProductDAO;
import com.sid.dto.ProductVO;
import com.sid.dto.RelateProductVO;
import com.sid.dto.UserVO;

public class ProductReadAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "home/item.jsp";
		int productId=Integer.parseInt(request.getParameter("productId"));
		UserVO uVo=new UserVO();
		UserVO uVo2=new UserVO();
		UserVO uVo3=new UserVO();
		MemberDAO mDao=MemberDAO.getInstance();
		uVo=mDao.readEmail(productId);
		uVo2=mDao.getRepMember(uVo);
		uVo3=mDao.getDelivery(uVo.getEmail());
		
		ProductDAO pDao=ProductDAO.getInstance();
		ProductVO pVo=new ProductVO();
		pVo=pDao.readProduct(productId);
		
		ArrayList<String> uses=new ArrayList<>();
		uses=pDao.readUses(productId);
		
		ArrayList<String> color=new ArrayList<>();
		color=pDao.readColor(productId);
		
		ArrayList<String> fiber=new ArrayList<>();
		fiber=pDao.readFiber(productId);
		
		int relateCount=1;
		//관련상품 가져오기
		
		String relatestr=null;
		relatestr=pVo.getRelate();
		String[] relatearr=null;
		ArrayList<RelateProductVO> relatelist=new ArrayList<>();
		RelateProductVO rVo=null;
		
		if(relatestr!=null){
			
			relatearr=relatestr.split(",");
		
			relateCount=relatearr.length;
	
			for(int i=0;i<relateCount;i++){
				rVo=new RelateProductVO();
				rVo=pDao.readRelateProduct(Integer.parseInt(relatearr[i]));
				if(rVo!=null){
					rVo.setColor(pDao.readColor(Integer.parseInt(relatearr[i])).toString());
					relatelist.add(rVo);
				}
			}
		}else{
			rVo=new RelateProductVO();
			rVo.setColor(color.toString());
			rVo.setColorName(pVo.getColorName());
			rVo.setMainImg(pVo.getMainImg());
			rVo.setProductId(pVo.getProductID());
			relatelist.add(rVo);
		}
		
		request.setAttribute("relate", relatelist);
		request.setAttribute("list", uVo3);
		request.setAttribute("rep", uVo2);
		request.setAttribute("vo", uVo);
		request.setAttribute("relateCount", relateCount);
		request.setAttribute("product", pVo);
		request.setAttribute("uses", uses.toString());
		request.setAttribute("color", color.toString());
		request.setAttribute("fiber", fiber.toString());
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
	
}
