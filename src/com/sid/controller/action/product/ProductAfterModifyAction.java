package com.sid.controller.action.product;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;
import com.sid.controller.Action;
import com.sid.dao.ProductDAO;
import com.sid.dto.ProductVO;

public class ProductAfterModifyAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "product/close.jsp";
		
		HttpSession session = request.getSession();
		ProductDAO dao = ProductDAO.getInstance();
		ProductVO vo = new ProductVO();
		int sizeLimit = 20 * 1024 * 1024;
		String savePath = "C:/Users/sid/git/sid_tex/WebContent/img";// 아이맥
		//String savePath = "C:/Users/hs/sid_tex/sid_tex/WebContent/img";//놋북
		//String savePath = "./tomcat/webapps/ROOT/img";//서버
		// String savePath="C:/Users/kj/git/sid_tex/WebContent/img";//재택-장

		
		
		MultipartRequest multi = new MultipartRequest(request, savePath, sizeLimit, "UTF-8",
				new DefaultFileRenamePolicy());

		String tempName = null;
		int productId = Integer.parseInt(multi.getParameter("productId"));
		try {

			tempName = multi.getFilesystemName("mainImg_1");
			if (tempName != null)
				vo.setMainImg("../img/" + tempName);
			

//			vo.setItemName(multi.getParameter("itemName"));
			vo.setExpl(multi.getParameter("expl"));
//			vo.setRetail(Integer.parseInt(multi.getParameter("retail")));
//			vo.setWholesale(Integer.parseInt(multi.getParameter("wholesale")));
//			vo.setStandard(Integer.parseInt(multi.getParameter("standard")));
			vo.setState("판매중");
			vo.setColorName(multi.getParameter("colorName1"));
			vo.setManufacturer(multi.getParameter("manufacturer"));
			vo.setManuDate(multi.getParameter("manuDate"));
			vo.setOrigin(multi.getParameter("origin"));
			vo.setSortName(multi.getParameter("sortName"));
			vo.setPattern(multi.getParameter("pattern"));
			vo.setThick(multi.getParameter("thick"));
			vo.setWeave(multi.getParameter("weave"));
			vo.setCompany((String) session.getAttribute("company"));
//			if (multi.getParameter("widthUnit").equals("inch")) {
//				vo.setWidth((int)(2.54 * Integer.parseInt(multi.getParameter("width"))));
//			} else {
//				vo.setWidth(Integer.parseInt(multi.getParameter("width")));
//			}
//			vo.setWidthUnit(multi.getParameter("widthUnit"));
			vo.setLengthWp(multi.getParameter("lengthWp"));
			vo.setLengthWt(multi.getParameter("lengthWt"));
			vo.setTwistWp(multi.getParameter("twistWp"));
			vo.setTwistWt(multi.getParameter("twistWt"));
			vo.setGagong(multi.getParameter("gagong"));
			vo.setDye(multi.getParameter("dye"));
			vo.setTexture(multi.getParameter("texture"));
			vo.setThrough(multi.getParameter("through"));
			vo.setGloss(multi.getParameter("gloss"));
			vo.setElastic(multi.getParameter("elastic"));
			vo.setPliability(multi.getParameter("pliability"));
			vo.setThickUnit(multi.getParameter("thickUnit"));
//			vo.setSwatch(multi.getParameter("swatch"));

//			// 배송 입력
//			int exp = 0;
//			String[] express = multi.getParameterValues("express");
//			if (express != null) {
//				for (int i = 0; i < express.length; i++) {
//					exp += Integer.parseInt(express[i]);
//				}
//			}
//			vo.setExpress(exp);

			if (multi.getParameter("stock_unknown") == null) {
				vo.setStock_unknown(0);
				vo.setStock(Integer.parseInt(multi.getParameter("stock1")));
			} else {
				vo.setStock_unknown(Integer.parseInt(multi.getParameter("stock_unknown")));
				vo.setStock(-1);
			}
			if (!multi.getParameter("weight").equals("")) {
				vo.setWeight(Integer.parseInt(multi.getParameter("weight")));
			} else {
				vo.setWeight(0);
			}
			if (!multi.getParameter("thickWp").equals("")) {
				vo.setThickWp(Integer.parseInt(multi.getParameter("thickWp")));
			} else {
				vo.setThickWp(0);
			}
			if (!multi.getParameter("thickWt").equals("")) {
				vo.setThickWt(Integer.parseInt(multi.getParameter("thickWt")));
			} else {
				vo.setThickWt(0);
			}
			if (!multi.getParameter("density_volume").equals("")) {
				vo.setDensity_volume(Integer.parseInt(multi.getParameter("density_volume")));
			} else {
				vo.setDensity_volume(0);
			}
			if (!multi.getParameter("density_weight").equals("")) {
				vo.setDensity_weight(Integer.parseInt(multi.getParameter("density_weight")));
			} else {
				vo.setDensity_weight(0);
			}

//			if (multi.getParameter("retail_only") == null) {
//				vo.setRetail_only(0);
//			} else {
//				vo.setRetail_only(Integer.parseInt(multi.getParameter("retail_only")));
//			}


		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("ProductAfterModifyAction Error" + e);
		}
		// vo.setAddress(multi.getParameter("address"));

		try {

			dao.updateAllProduct(vo, productId, (String) session.getAttribute("email"),
					(String) session.getAttribute("company"));

			// 용도 업데이트
			if (multi.getParameterValues("uses") != null) {
				String[] uses = multi.getParameterValues("uses");
				dao.deleteUses(productId);
				for (int i = 0; i < uses.length; i++) {
					dao.insertUses(uses[i], productId);
				}
			}
			// 색상 업데이트
			if (multi.getParameterValues("color1") != null) {
				String color = multi.getParameter("color1");
				dao.deleteColor(productId);
				dao.insertColor(color, productId);
			}
			// 섬유 업데이트
			if (multi.getParameterValues("fiber") != null) {
				String[] fiber = multi.getParameterValues("fiber");
				dao.deleteFiber(productId);
				for (int i = 0; i < fiber.length; i++) {
					 dao.insertFiber(fiber[i], productId);
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		if (vo.getMainImg() != null) {
			System.out.println("main not null");
			dao.updateAllImageProduct(productId, vo.getMainImg(), "mainImg");
		}
		if (vo.getSubImg() != null) {

			System.out.println("sub1 not null");
			dao.updateAllImageProduct(productId, vo.getSubImg(), "subImg");
		}
		if (vo.getSubImg2() != null) {

			System.out.println("sub2 not null");
			dao.updateAllImageProduct(productId, vo.getSubImg2(), "subImg2");
		}
		if (vo.getSubImg3() != null) {

			System.out.println("sub3 not null");
			dao.updateAllImageProduct(productId, vo.getSubImg3(), "subImg3");
		}
		if (vo.getSubImg4() != null) {

			System.out.println("sub4 not null");
			dao.updateAllImageProduct(productId, vo.getSubImg4(), "subImg4");
		}
		if (vo.getSubImg5() != null) {
			dao.updateAllImageProduct(productId, vo.getSubImg5(), "subImg5");
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}