package com.sid.controller.action.product;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.ProductDAO;
import com.sid.dto.ProductVO;


public class ProductListAction implements Action{
	 @Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		 	String url="product/product_manage_stock.jsp";
		 	
		 	HttpSession session=request.getSession();
		 	ProductDAO dao=ProductDAO.getInstance();
			ArrayList<ProductVO> list = dao.listAll((String)session.getAttribute("email"));

			request.setAttribute("list", list);
			
			RequestDispatcher dispatcher = request.getRequestDispatcher(url);
			dispatcher.forward(request, response);
	}
}
