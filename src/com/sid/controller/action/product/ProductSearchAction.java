package com.sid.controller.action.product;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sid.controller.Action;
import com.sid.dao.ProductDAO;
import com.sid.dto.ProductVO;

public class ProductSearchAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "home.jsp";
		String search = request.getParameter("search_param");
		
		System.out.println("검색 : "+request.getParameter("value"));
			ProductDAO pDao = ProductDAO.getInstance();
			ArrayList<ProductVO> plist = pDao.searchProduct(search, request.getParameter("value"));
			request.setAttribute("plist", plist);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}

}
