package com.sid.controller.action.product;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;
import com.sid.controller.Action;
import com.sid.dao.ProductDAO;
import com.sid.dto.ImageVO;
import com.sid.dto.ProductVO;

public class ProductSubmitAction implements Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "product/productUpload_success.jsp";

		HttpSession session = request.getSession();
		ProductDAO dao = ProductDAO.getInstance();
		ProductVO vo = new ProductVO();

		int sizeLimit = 20 * 1024 * 1024;
		String savePath = "C:/Users/sid/git/sid_tex/WebContent/img";//아이맥
		//String savePath = "C:/Users/hs/sid_tex/sid_tex/WebContent/img";//놋북
		//String savePath = "./tomcat/webapps/ROOT/img";//서버
		//String savePath="C:/Users/kj/git/sid_tex/WebContent/img";//재택-장

		MultipartRequest multi = new MultipartRequest(request, savePath, sizeLimit, "UTF-8",
				new DefaultFileRenamePolicy());
		int count = 0;

		ArrayList<ImageVO> imagelist = null;

		try {
			count = Integer.parseInt(multi.getParameter("add_count"));

			imagelist = new ArrayList<>();

			ImageVO ivo = null;
			String tempName = null;
			String colorName = null;
			for (int i = 1; i < count + 1; i++) {
				ivo = new ImageVO();
				tempName = multi.getFilesystemName("mainImg_" + i);
				ivo.setMainImg("../img/" + tempName);
//				tempName = multi.getFilesystemName("subImg_" + 1);
//				ivo.setSubImg("../img/" + tempName);
//				tempName = multi.getFilesystemName("subImg2_" + 1);
//				ivo.setSubImg2("../img/" + tempName);
//				tempName = multi.getFilesystemName("subImg3_" + 1);
//				ivo.setSubImg3("../img/" + tempName);
//				tempName = multi.getFilesystemName("subImg4_" + 1);
//				ivo.setSubImg4("../img/" + tempName);
//				tempName = multi.getFilesystemName("subImg5_" + 1);
//				ivo.setSubImg5("../img/" + tempName);
				colorName = multi.getParameter("colorName" + i);
				if (i == 1) {
					ivo.setMainyn(1);
				} else {
					ivo.setMainyn(0);
				}
				ivo.setColorName(colorName);

				if (multi.getParameter("stock" + i) == null) {
					ivo.setStock(-1);
				} else {
					ivo.setStock(Integer.parseInt(multi.getParameter("stock" + i)));
				}

				imagelist.add(ivo);
			}

			vo.setItemName(multi.getParameter("itemName"));
			vo.setExpl(multi.getParameter("expl"));
			vo.setRetail(Integer.parseInt(multi.getParameter("retail")));
			vo.setWholesale(Integer.parseInt(multi.getParameter("wholesale")));
			vo.setStandard(Integer.parseInt(multi.getParameter("standard")));
			vo.setState("판매중");
			vo.setColorName(multi.getParameter("colorName"));

			vo.setManufacturer(multi.getParameter("manufacturer"));
			vo.setManuDate(multi.getParameter("manuDate"));
			vo.setOrigin(multi.getParameter("origin"));
			vo.setSortName(multi.getParameter("sortName"));
			vo.setPattern(multi.getParameter("pattern"));
			vo.setThick(multi.getParameter("thick"));
			vo.setWeave(multi.getParameter("weave"));
			vo.setCompany((String) session.getAttribute("company"));
			if (multi.getParameter("widthUnit").equals("inch")) {
				vo.setWidth((int)(2.54 * Integer.parseInt(multi.getParameter("width"))));
			} else {
				vo.setWidth(Integer.parseInt(multi.getParameter("width")));
			}
			vo.setWidthUnit(multi.getParameter("widthUnit"));
			vo.setLengthWp(multi.getParameter("lengthWp"));
			vo.setLengthWt(multi.getParameter("lengthWt"));
			vo.setTwistWp(multi.getParameter("twistWp"));
			vo.setTwistWt(multi.getParameter("twistWt"));
			vo.setGagong(multi.getParameter("gagong"));
			vo.setDye(multi.getParameter("dye"));
			vo.setTexture(multi.getParameter("texture"));
			vo.setThrough(multi.getParameter("through"));
			vo.setGloss(multi.getParameter("gloss"));
			vo.setElastic(multi.getParameter("elastic"));
			vo.setPliability(multi.getParameter("pliability"));
			vo.setThickUnit(multi.getParameter("thickUnit"));
			vo.setSwatch(multi.getParameter("swatch"));
			vo.setDelyn(0);

//			// 배송 입력
//			int exp = 0;
//			String[] express = multi.getParameterValues("express");
//			if (express != null) {
//				for (int i = 0; i < express.length; i++) {
//					exp += Integer.parseInt(express[i]);
//				}
//			}
//			vo.setExpress(exp);

			if (multi.getParameter("stock_unknown") == null) {
				vo.setStock_unknown(0);
			} else {
				vo.setStock_unknown(Integer.parseInt(multi.getParameter("stock_unknown")));
			}
			if (!multi.getParameter("weight").equals("")) {
				vo.setWeight(Integer.parseInt(multi.getParameter("weight")));
			} else {
				vo.setWeight(0);
			}
			if (!multi.getParameter("thickWp").equals("")) {
				vo.setThickWp(Integer.parseInt(multi.getParameter("thickWp")));
			} else {
				vo.setThickWp(0);
			}
			if (!multi.getParameter("thickWt").equals("")) {
				vo.setThickWt(Integer.parseInt(multi.getParameter("thickWt")));
			} else {
				vo.setThickWt(0);
			}
			if (!multi.getParameter("density_volume").equals("")) {
				vo.setDensity_volume(Integer.parseInt(multi.getParameter("density_volume")));
			} else {
				vo.setDensity_volume(0);
			}
			if (!multi.getParameter("density_weight").equals("")) {
				vo.setDensity_weight(Integer.parseInt(multi.getParameter("density_weight")));
			} else {
				vo.setDensity_weight(0);
			}

			if (multi.getParameter("retail_only") == null) {
				vo.setRetail_only(0);
			} else {
				vo.setRetail_only(Integer.parseInt(multi.getParameter("retail_only")));
			}


		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("ProductSubmitAction Error" + e);
		}
		// vo.setAddress(multi.getParameter("address"));

		try {
			// 연관 상품 리스트
			ArrayList<Integer> relatelist = null;
			relatelist = new ArrayList<>();
			ArrayList<String> colorlist=new ArrayList<>();
			for(int k=0;k<count;k++){
				if (multi.getParameter("color" + (k + 1)) != null) {
					colorlist.add(multi.getParameter("color" + (k + 1)));
				}
			}
			vo.setColor(colorlist.toString());
			// 상품 등록
			for (int j = 0; j < count; j++) {
				
				int productId = dao.insertProduct(imagelist.get(j), vo, (String) session.getAttribute("email"),
						(String) session.getAttribute("company"));
				relatelist.add(productId);

				// 용도 입력
				if (multi.getParameterValues("uses") != null) {

					String[] uses = multi.getParameterValues("uses");
					for (int i = 0; i < uses.length; i++) {
						dao.insertUses(uses[i], productId);
					}
				}
				// 색상 입력
				if (multi.getParameterValues("color" + (j + 1)) != null) {
					String[] color = multi.getParameterValues("color" + (j + 1));
					for (int i = 0; i < color.length; i++) {
						dao.insertColor(color[i], productId);
					}
				}
				// 섬유 입력
				if (multi.getParameterValues("fiber") != null) {
					String[] fiber = multi.getParameterValues("fiber");
					for (int i = 0; i < fiber.length; i++) {
						dao.insertFiber(fiber[i], productId);
					}
				}
			}

			String relateStr = relatelist.toString();
			relateStr = relateStr.replaceAll("\\p{Z}", "");
			relateStr = relateStr.substring(1, relateStr.length() - 1);

			// 연관상품 등록
			if (relatelist.size() != 1) {
				for (int i = 0; i < relatelist.size(); i++) {
					dao.updateRelateProduct(relateStr, relatelist.get(i));
				}

			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}