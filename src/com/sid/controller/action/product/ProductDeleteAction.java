package com.sid.controller.action.product;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sid.controller.Action;
import com.sid.dao.ProductDAO;


public class ProductDeleteAction implements Action{
	 @Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		 	String url="product/productResult.jsp";
		 	
		 	ProductDAO dao=ProductDAO.getInstance();
			int result=0;
			
			int pid=Integer.parseInt(request.getParameter("productId"));
			result=dao.updateDelyn(pid);
			
			if(result>0){
				
				request.setAttribute("result", "1");
				
			}else{
				request.setAttribute("result", "0");
			}
			
			String rel = dao.selectRelateProduct(pid);

		      if (rel != null) {

		         String[] relate = rel.split(",");

		         System.out.println(relate);
		         request.getParameter("productId");

		         ArrayList<Integer> relateUpdate = new ArrayList<>();

		         if(relate.length!=2){
		         
		            for (int i = 0; i < relate.length; i++) {
		               if (Integer.parseInt(relate[i]) != pid) {
		                  relateUpdate.add(Integer.parseInt(relate[i]));
		               }
		            }
		   
		            String relateStr = relateUpdate.toString();
		            relateStr = relateStr.replaceAll("\\p{Z}", "");
		            relateStr = relateStr.substring(1, relateStr.length() - 1);
		   
		            // 연관상품 등록
		            if (relateUpdate.size() != 1) {
		               for (int i = 0; i < relateUpdate.size(); i++) {
		                  dao.updateRelateProduct(relateStr, relateUpdate.get(i));
		               }
		            }
		         }else{
		            System.out.println("relate length :"+relate.length);
		            for (int i = 0; i < relate.length; i++) {
		                dao.updateRelateNullProduct(Integer.parseInt(relate[i]));
		             }
		         }
		         
		      }
			
			RequestDispatcher dispatcher = request.getRequestDispatcher(url);
			dispatcher.forward(request, response);
	}
}
