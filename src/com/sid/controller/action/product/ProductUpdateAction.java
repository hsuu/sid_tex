	package com.sid.controller.action.product;

import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sid.controller.Action;
import com.sid.dao.ProductDAO;


public class ProductUpdateAction implements Action{
	 @Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		 	String url="product/productResult.jsp";
		 	
		 	ProductDAO dao=ProductDAO.getInstance();
			int result=0;
			String[] relate={};
			String rel="";
			
			rel=dao.selectRelateProduct(Integer.parseInt(URLDecoder.decode(request.getParameter("productId"),"utf-8")));
			if(rel!=null){
				relate=rel.split(",");
				//재고관리에서 메인상품 상품명,소매가,도매가,스와치,폭,두께 변경시 다른상품들도 변경
				if(request.getParameter("column").equals("retail")||request.getParameter("column").equals("wholesale")
						||request.getParameter("column").equals("width")||request.getParameter("column").equals("thick")
						||request.getParameter("column").equals("itemName")||request.getParameter("column").equals("swatch")){
					for(int i=0; i<relate.length; i++){
						result=dao.updateProduct(request.getParameter("input"),
								Integer.parseInt(relate[i]), URLDecoder.decode(request.getParameter("column"),"utf-8"));
					}
				}else{
					result=dao.updateProduct(request.getParameter("input"),
							Integer.parseInt(URLDecoder.decode(request.getParameter("productId"),"utf-8")), URLDecoder.decode(request.getParameter("column"),"utf-8"));
				}
			}else{
				result=dao.updateProduct(request.getParameter("input"),
						Integer.parseInt(URLDecoder.decode(request.getParameter("productId"),"utf-8")), URLDecoder.decode(request.getParameter("column"),"utf-8"));
			}
			
			
			
			if(result>0){
				
				request.setAttribute("result", "1");
				
			}else{
				request.setAttribute("result", "0");
			}
			
			RequestDispatcher dispatcher = request.getRequestDispatcher(url);
			dispatcher.forward(request, response);
	}
}
