package com.sid.controller.action.product;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sid.controller.Action;
import com.sid.dao.ProductDAO;

public class ProductStateUpdateAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="product/productResult.jsp";
	 	
	 	ProductDAO dao=ProductDAO.getInstance();
		int result=0;
		int result2=1;
		String[] parr=request.getParameterValues("productId");
		
		if(request.getParameter("state").equals("on")){
			for(int i=0;i<parr.length;i++){
				result=dao.updateState(Integer.parseInt(parr[i]));
				if(result<0){
					result2=-1;
					break;
				}
			}
			
		}else if(request.getParameter("state").equals("stop")){
			for(int i=0;i<parr.length;i++){
				result=dao.updateState2(Integer.parseInt(parr[i]));
				if(result<0){
					result2=-1;
					break;
				}
			}
		}
		
		request.setAttribute("result", result2);
			
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
