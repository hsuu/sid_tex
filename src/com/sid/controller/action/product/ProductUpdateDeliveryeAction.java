package com.sid.controller.action.product;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.MemberDAO;
import com.sid.dto.UserVO;

public class ProductUpdateDeliveryeAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "product/productResult.jsp";
		int result = 0;
		HttpSession session = request.getSession();
		UserVO vo = new UserVO();
		MemberDAO mDao = MemberDAO.getInstance();

		int exp2 = 0;
		String[] delivery_way = request.getParameterValues("delivery_way");
		if (delivery_way != null) {
			for (int i = 0; i < delivery_way.length; i++) {
				exp2 += Integer.parseInt(delivery_way[i]);
			}
		}
		vo.setDelivery_way(exp2);
		vo.setDelivery_costway(request.getParameter("delivery_detail"));
		
		//교환반품지 주소
		vo.setReturnZipnum(request.getParameter("returnZipnum"));
		vo.setReturnRoadAddrPart1(request.getParameter("returnRoadAddrPart1"));
		vo.setReturnAddrDetail(request.getParameter("returnAddrDetail"));
		//판매자 지정 퀵
		vo.setDeliveryCompany_quick(request.getParameter("deliveryCompany_quick"));
		//예상되는 배송기간
		if(request.getParameter("delivery_period")!=null){
			vo.setDelivery_period(Integer.parseInt(request.getParameter("delivery_period")));
		}else{
			vo.setDelivery_period(7);
		}

		int exp = 0;
		String[] express = request.getParameterValues("delivery");
		if (express != null) {
			for (int i = 0; i < express.length; i++) {
				exp += Integer.parseInt(express[i]);
			}
		}
		
		//비용
		vo.setDelivery(exp);
		vo.setDelivery_cost(0);
		if (request.getParameter("delivery_detail").equals("무료")) {

		} else if (request.getParameter("delivery_detail").equals("유료")) {
			if (request.getParameter("delivery_cost") != null) {
				vo.setDelivery_cost(Integer.parseInt(request.getParameter("delivery_cost")));
			}
		} else if (request.getParameter("delivery_detail").equals("조건부무료")) {
			if (request.getParameter("delivery_cost2") != null) {
				vo.setDelivery_cost(Integer.parseInt(request.getParameter("delivery_cost2")));
				vo.setDelivery_cost2(Integer.parseInt(request.getParameter("delivery_cost3")));
			}
		}
		
		//왕복택배비
		if (request.getParameter("re_delivery_cost1") != null) {
			vo.setRe_delivery_cost1(Integer.parseInt(request.getParameter("re_delivery_cost1")));

		} else {
			vo.setRe_delivery_cost1(0);
		}
		//판매자 지정택배사
			vo.setDeliveryCompany(request.getParameter("deliveryCompany"));

		
		// 배송정보 입력
		result = mDao.insertDelivery(vo, (String) session.getAttribute("email"));
		if (result > 0) {

			request.setAttribute("result", 1);
		} else {
			request.setAttribute("result", 0);
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
