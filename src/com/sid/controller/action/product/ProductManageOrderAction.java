package com.sid.controller.action.product;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sid.controller.Action;
import com.sid.dao.PurchaseDAO;
import com.sid.dto.PurchaseVO;

public class ProductManageOrderAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="";
		
		PurchaseDAO pDao=PurchaseDAO.getInstance();
		PurchaseVO pVo=new PurchaseVO();
		pVo.setProductId(Integer.parseInt(request.getParameter("productId")));
		pVo.setQuantity(Integer.parseInt(request.getParameter("quantity")));
		pVo.setBuyerName(request.getParameter("buyerName"));
		pVo.setBuyerEmail(request.getParameter("buyerEmail"));
		pVo.setBuyerPhone(request.getParameter("buyerPhone"));
		pVo.setRecipientName(request.getParameter("recipientName"));
		pVo.setRecipientPhone(request.getParameter("recipientPhone"));
		pVo.setRecipientAddress(request.getParameter("recipientAddress"));
		
		
		
//		pDao.insertOrder();
		
		RequestDispatcher dispatcher=request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
