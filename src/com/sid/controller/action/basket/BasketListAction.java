package com.sid.controller.action.basket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.BasketDAO;
import com.sid.dto.BasketVO;

public class BasketListAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "mypage/basket.jsp";

		HttpSession session = request.getSession();
		BasketDAO dao = BasketDAO.getInstance();
		ArrayList<BasketVO> list = dao.listAllBasket((String) session.getAttribute("email"));
		// a : 연관상품이 있는 상품
		ArrayList<BasketVO> a = new ArrayList<>();
		// b : 연관상품이 없는 상품
		ArrayList<BasketVO> b = new ArrayList<>();
		

		ArrayList<Integer> relateNum=new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getRelate() !=null) {
				a.add(list.get(i));
			} else {
				b.add(list.get(i));
			}
		}

		if (a.size() != 0) {
			
			int j = 0;
			String relate = "";

			while (true) {
				System.out.println("j = "+j+" a.size() = "+a.size());
				if (j == a.size())
					break;

				relate = a.get(j).getRelate();
				System.out.println(relate.toString());
				String[] arr = relate.split(",");
				relateNum.add(arr.length);
				j+=arr.length;

			}
			System.out.println(relateNum.toString());
		}
		
		
		
		
		request.setAttribute("relate", a);
		request.setAttribute("norelate", b);
		request.setAttribute("relateNum", relateNum);

		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
