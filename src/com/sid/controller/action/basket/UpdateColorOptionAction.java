package com.sid.controller.action.basket;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.BasketDAO;
import com.sid.dao.ProductDAO;

public class UpdateColorOptionAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "product/ItemResult.jsp";

		BasketDAO dao = BasketDAO.getInstance();
		String basketIdStr = request.getParameter("basketId");
		String quantityStr = request.getParameter("quantity");
		System.out.println("basket id = "+basketIdStr+" quantity = "+quantityStr);
		
		String[] bArr=basketIdStr.split(",");
		String[] qArr=quantityStr.split(",");
		
		
		
		dao.updateColorOption(bArr,qArr);

		request.setAttribute("result", 1);
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
