package com.sid.controller.action.basket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.BasketDAO;
import com.sid.dao.ProductDAO;
import com.sid.dto.BasketVO;

public class DeleteColorOptionAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "product/ItemResult.jsp";

		HttpSession session = request.getSession();
		ProductDAO dao = ProductDAO.getInstance();
		System.out.println("delete");
		int productId=Integer.parseInt(request.getParameter("productId"));
		String email=(String)session.getAttribute("email");
		
		int result=dao.deleteBasketQuantity(productId, email);

		if(result>0)
			request.setAttribute("result", 1);
		else
			request.setAttribute("result", 0);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
