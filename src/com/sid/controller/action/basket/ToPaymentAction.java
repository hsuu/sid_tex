package com.sid.controller.action.basket;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.BasketDAO;
import com.sid.dao.MemberDAO;
import com.sid.dao.ProductDAO;
import com.sid.dto.BasketVO;
import com.sid.dto.CompanyVO;
import com.sid.dto.ProductVO;
import com.sid.dto.UserVO;

public class ToPaymentAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="mypage/payment.jsp";
		String way=request.getParameter("delivery_way");
		
		if(way.equals("quick")){
			url = "mypage/paymentquick.jsp";
		}else{
			url = "mypage/payment.jsp";
		}
		

		HttpSession session = request.getSession();
		BasketDAO dao = BasketDAO.getInstance();
		MemberDAO mdao = MemberDAO.getInstance();
		String email = (String) session.getAttribute("email");
		UserVO vo = mdao.getMember(email);

		String idlist = request.getParameter("idList");

		String[] idArr = null;
		idArr = idlist.split(",");
		ArrayList<BasketVO> list = new ArrayList<BasketVO>();
		for (int i = 0; i < idArr.length; i++) {
			list.add(dao.listSelectOrder(email, idArr[i]));
		}

		int lastidx = -1;
		int flag = 0;
		
		
		//회사명 추출 ----
		ArrayList<String> companyList = new ArrayList<>();

		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getQuantity() == 0)
				continue;

			if (lastidx == -1) { // companyList에 아무것도 없을경우
				companyList.add(list.get(i).getSeller());
				lastidx++;
				continue;
			}
			flag = 0;
			for (int j = 0; j < companyList.size(); j++) {
				
				if (list.get(i).getSeller().equals(companyList.get(j).toString())) {
					flag = 1;
				}

			}
			if (flag == 0) {
				companyList.add(list.get(i).getSeller());
				lastidx++;
			}
		}
		//-------
	
		// a : 연관상품이 있는 상품
		ArrayList<BasketVO> a = new ArrayList<>();
		// b : 연관상품이 없는 상품
		ArrayList<BasketVO> b = new ArrayList<>();

		ArrayList<Integer> relateNum = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getRelate() != null) {
				a.add(list.get(i));
			} else {
				b.add(list.get(i));
			}
		}

		if (a.size() != 0) {

			int j = 0;
			String relate = "";

			while (true) {
				if (j == a.size())
					break;
				relate = a.get(j).getRelate();
				String[] arr = relate.split(",");
				relateNum.add(arr.length);
				j += arr.length;

			}
		}
		vo.setName(vo.getName().replaceAll("/", ""));

		ArrayList<CompanyVO> company = new ArrayList<>();
		CompanyVO uvo = null;
		for (int i = 0; i < companyList.size(); i++) {
			uvo = new CompanyVO();
			uvo = mdao.getCompanyInfo(companyList.get(i));
			company.add(uvo);
		}
		for(int i=0;i<company.size();i++){
			System.out.println(company.get(i).getDelivery_costway());
		}

		request.setAttribute("relate", a);
		request.setAttribute("norelate", b);
		request.setAttribute("company", company);
		request.setAttribute("companyList", companyList);
		request.setAttribute("relateNum", relateNum);
		request.setAttribute("member", vo);

		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
