package com.sid.controller.action.basket;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.ProductDAO;

public class ToBasketAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "product/ItemResult.jsp";
		ProductDAO pDao = ProductDAO.getInstance();

		HttpSession session = request.getSession();
		
		String email=(String) session.getAttribute("email");

		int num=Integer.parseInt(request.getParameter("color_count"));
		int requestResult=1;
		int result = 0;

		for(int i=0;i<num;i++){
				
			if(i==0){
				result = pDao.checkBasket(Integer.parseInt(request.getParameter("pid"+(i+1))),email);
				if(result<0){ //리절트 에러나면 반환 후 종료
					requestResult=-1;
					break;
				}
			}
			if(result==1){
				//result가 1이면 이미 장바구니에 담겨있음 update action
				requestResult=pDao.updateBasketQuantity(Integer.parseInt(request.getParameter("pid"+(i+1))), email, Integer.parseInt(request.getParameter("quantity"+(i+1))));
			}else if(result==0){
				//result가 0이면 장바구니에 새로 넣음		
				String a=request.getParameter("pid"+((i+1)));
				requestResult=pDao.addToBasketWithQuantity(Integer.parseInt(a),email,Integer.parseInt(request.getParameter("quantity"+(i+1))));
			}
		}
		
		request.setAttribute("result", requestResult);


		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
