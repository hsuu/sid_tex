package com.sid.controller.action.basket;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.BasketDAO;
import com.sid.dao.ProductDAO;

public class DeleteBasketAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "product/ItemResult.jsp";

		HttpSession session = request.getSession();

		BasketDAO dao = BasketDAO.getInstance();
		String email=(String)session.getAttribute("email");
		String type=request.getParameter("type");
		System.out.println(type);
		int result=0;
		if(type.equals("relate")){
			String relate=request.getParameter("basketId");
			String[] arr=relate.split(",");
			for(int i=0;i<arr.length;i++){
				result = dao.deleteRelateBasket(Integer.parseInt(arr[i]), email);
			}
		}else{
			
			result = dao.deleteBasket(Integer.parseInt(request.getParameter("basketId")));
		}


		// 결과
		if (result > 0) {
			request.setAttribute("result", "1");
		} else {
			request.setAttribute("result", "0");
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
