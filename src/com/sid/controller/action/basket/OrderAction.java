package com.sid.controller.action.basket;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.JsonObject;
import com.sid.controller.Action;
import com.sid.dao.AlarmDAO;
import com.sid.dao.BasketDAO;
import com.sid.dao.CustomerDAO;
import com.sid.dao.MemberDAO;
import com.sid.dao.ProductDAO;
import com.sid.dao.PurchaseDAO;
import com.sid.dto.AlarmVO;
import com.sid.dto.BasketVO;
import com.sid.dto.CustomerVO;
import com.sid.dto.PurchaseVO;
import com.sid.dto.UserVO;

public class OrderAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "product/productResult.jsp";

		HttpSession session = request.getSession();

		BasketDAO dao = BasketDAO.getInstance();

		PurchaseVO vo = new PurchaseVO();

		ProductDAO prddao = ProductDAO.getInstance();
		PurchaseDAO pdao = PurchaseDAO.getInstance();
		MemberDAO mdao=MemberDAO.getInstance();
		AlarmVO avo=null;
		AlarmDAO aDao=AlarmDAO.getInstance();
		
		UserVO uvo=null;
		String bIdList = request.getParameter("idList");
		String costList = request.getParameter("costList");
		String countList = request.getParameter("countList");
		String deliveryList = request.getParameter("deliveryList");
		
		
		
		JSONParser parser=new JSONParser();
		JSONObject delivery_cost=null;
		try {
			delivery_cost=(JSONObject)parser.parse(deliveryList);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			System.out.println("에러 : "+e);
		}
		

		String email = (String) session.getAttribute("email");
		uvo=mdao.getMember(email);
		
		String recipientName = request.getParameter("buyername");
		String recipientPhone = request.getParameter("buyertel");
		String address = request.getParameter("roadAddrPart1") + "/" + request.getParameter("addrDetail") + "("
				+ request.getParameter("zipNum") + ")";
		String oid = request.getParameter("oid");
		String delivery_message=request.getParameter("delivery_message");

		String[] bId = bIdList.split(",");
		String[] cost = costList.split(",");
		String[] count = countList.split(",");

		StringBuffer buffer = null;
		Date today = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
		String pid = null;
		
		//결제 실패시 Purchase 테이블에 삭제
	      String oid2=pdao.readPayFlag(email);
	      if(oid2!=null){
	    	  pdao.deletePayFlag(oid2);
	      }
	      
	      
		int result1=0;
		int first = 0;
		for (int i = 0; i < count.length; i++) {
			String detail = "";
			int quantity=0;
			if (i != 0) {
				first += Integer.parseInt(count[i - 1]);
			}

			for (int j = first; j < first+Integer.parseInt(count[i]); j++) {
				vo = dao.listByBasketId(bId[j]);
				quantity+=vo.getQuantity();
				detail += (vo.getColorName() + " - " + vo.getQuantity() + "마" + "<br>");
			}

			// 주문번호 생성
			pid = sdf.format(today);

			buffer = new StringBuffer();
			for (int k = 0; k < 4; k++) {
				int n =(int) (Math.random() * 10);
				buffer.append(n); 
			}
			pid += buffer;
			vo = dao.listByBasketId(bId[first]);
			vo.setPurchaseId(pid);
			vo.setCost(Integer.parseInt(cost[i]));
			vo.setDelivery_cost((int)(long)delivery_cost.get(vo.getCompany()));
			vo.setPurchase_detail(detail);
			vo.setBuyerEmail(email);
			vo.setBuyerName(uvo.getName());
			vo.setBuyerPhone(uvo.getPhone());
			vo.setRecipientName(recipientName);
			vo.setRecipientPhone(recipientPhone);
			vo.setExpress(0);//택배:0, 퀵:1
			vo.setRecipientAddress(address);
			vo.setQuantity(quantity);
			vo.setState(0);
			vo.setOid(oid);
			vo.setDelivery_message(delivery_message);
			
			// 구매
			int result = pdao.insertOrder(vo);
		
			if (result < 1) {
				System.out.println("order action error");
			} else {
				for (int j = first; j < Integer.parseInt(count[i]); j++) {
					vo = dao.listByBasketId(bId[j]);
					int stock = prddao.selectProductStock(vo.getProductId());
					if (stock != -1) {
						result1 = prddao.reduceStockProduct(vo.getProductId(), vo.getQuantity());
						if (result1 < 0) {
							System.out.println("order action error");
						}
					} else {
						result1 = 1;
					}
				}
			}
	
				if(result1>0){
					avo=new AlarmVO();
					avo.setReceiver(vo.getSeller());
					avo.setSender(vo.getBuyerEmail());
					avo.setSort("상품주문");
					avo.setMessage(email+" 님이 "+vo.getItemName()+ " 상품을 '주문' 하셨습니다.");
					avo.setState(0);
					
				}
				aDao.sendAlarm(avo);
			
		}
		


		request.setAttribute("result", result1);

		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
