package com.sid.controller.action.customer;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.CustomerDAO;
import com.sid.dto.CustomerVO;

public class ListManageCustomerAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="product/customer_manage.jsp";
		
		HttpSession session=request.getSession();
		CustomerDAO dao=CustomerDAO.getInstance();
		ArrayList<CustomerVO> list=new ArrayList<>();
		list=dao.ListCustomer((String)session.getAttribute("email"));

		request.setAttribute("list", list);
		
		RequestDispatcher dispatcher=request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
