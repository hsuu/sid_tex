package com.sid.controller.action.index;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sid.controller.Action;
import com.sid.dao.ProductDAO;
import com.sid.dto.ProductVO;

public class IndexAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "home.jsp";

		// 홈 - 상품보기
		ProductDAO pDao = ProductDAO.getInstance();
		int num=1;//페이지
		
		try {
			if(request.getParameter("num")!=null){
			num=Integer.parseInt(request.getParameter("num"));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		int size=60;//한번에 불러올 개수
		int start=size*(num-1);//불러올 페이지
		ArrayList<ProductVO> plist = pDao.listAll_Product(start,size);

		Paging paging = new Paging();
		paging.setPageNo(num);
		paging.setPageSize(size);
		paging.setTotalCount(pDao.countList());

		request.setAttribute("plist", plist);
		request.setAttribute("paging", paging);

		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
