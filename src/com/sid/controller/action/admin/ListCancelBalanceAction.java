package com.sid.controller.action.admin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.AdminDAO;
import com.sid.dto.CancelBalanceVO;
import com.sid.dto.UserVO;

public class ListCancelBalanceAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="admin/admin_all_balance_cancel.jsp";
		
		HttpSession session = request.getSession();
		int admin=(int)session.getAttribute("admin");
		int cost=0;
		int tempcost=0;
		if(admin<2){
			AdminDAO dao = AdminDAO.getInstance();
			ArrayList<CancelBalanceVO> list = dao.listCancelBalance();

			
			//최종 정산금액 계산
			for(int i=0;i<list.size();i++){
				cost=dao.getTotalCostByCompany(list.get(i).getCompany(), list.get(i).getOid());
				tempcost=list.get(i).getCost();
				if(cost==tempcost){
					list.get(i).setTotalCost(list.get(i).getDelivery_cost()+tempcost);
				}else{
					list.get(i).setTotalCost(tempcost);
				}
			}
			
			request.setAttribute("list", list);
		}
		
		RequestDispatcher dispatcher =request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
