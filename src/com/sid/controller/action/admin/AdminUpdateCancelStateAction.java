package com.sid.controller.action.admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sid.controller.Action;
import com.sid.dao.AdminDAO;

public class AdminUpdateCancelStateAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="product/productResult.jsp";
		
		int result=-1;
		
		AdminDAO dao=AdminDAO.getInstance();
		
		String[] oid=request.getParameterValues("oid");
		
		for(int i=0; i<oid.length; i++){
			result=dao.updateCancelState(oid[i]);
			if(result<1){
				break;
			}
		}
		
		
		request.setAttribute("result", result);
		
		RequestDispatcher dispatcher=request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
