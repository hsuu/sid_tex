package com.sid.controller.action.admin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sid.controller.Action;
import com.sid.dao.AdminDAO;
import com.sid.dao.BasketDAO;
import com.sid.dao.ProductDAO;

public class AdminWriteFAQAction implements Action {
   @Override
   public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      String url = "admin/admin_main.jsp";
      
      AdminDAO dao = AdminDAO.getInstance();

      String sort=request.getParameter("sort");
      String question=request.getParameter("question");
      String answer=request.getParameter("answer");
      
      int result=dao.insertFAQ(sort, question, answer);
      
      RequestDispatcher dispatcher = request.getRequestDispatcher(url);
      dispatcher.forward(request, response);
   }
}