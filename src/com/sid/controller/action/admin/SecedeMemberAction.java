package com.sid.controller.action.admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sid.controller.Action;
import com.sid.dao.MemberDAO;

public class SecedeMemberAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "SidServlet?command=list_all_user";
		
		MemberDAO mDao=MemberDAO.getInstance();
		mDao.deleteMember(request.getParameter("email"));
		MemberDAO mDao2=MemberDAO.getInstance();
		mDao2.deleteMember_b(request.getParameter("email"));
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
