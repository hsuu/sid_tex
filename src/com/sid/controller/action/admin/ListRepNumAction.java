package com.sid.controller.action.admin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.AdminDAO;
import com.sid.dto.UserVO;

public class ListRepNumAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "admin/admin_rep_num.jsp";
		HttpSession session = request.getSession();
		int admin=(int)session.getAttribute("admin");
		if(admin<2){
			AdminDAO dao = AdminDAO.getInstance();
			ArrayList<UserVO> list = dao.listRepFlag();
			request.setAttribute("list", list);
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
