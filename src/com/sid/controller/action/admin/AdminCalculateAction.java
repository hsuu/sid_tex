package com.sid.controller.action.admin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sid.controller.Action;
import com.sid.dao.AdminDAO;
import com.sid.dto.AdminCalculateVO;

public class AdminCalculateAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="admin/admin_all_balance_wait.jsp";
		

		AdminDAO dao=AdminDAO.getInstance();
		
		String datepicker1=request.getParameter("datepicker1");
		String datepicker2=request.getParameter("datepicker2");
		
		ArrayList<AdminCalculateVO> list=dao.getAllCalculate(datepicker1,datepicker2);
		request.setAttribute("list", list);
		
		RequestDispatcher dispatcher=request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
