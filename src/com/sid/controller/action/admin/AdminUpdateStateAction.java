package com.sid.controller.action.admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sid.controller.Action;
import com.sid.dao.AdminDAO;

public class AdminUpdateStateAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="product/productResult.jsp";
		
		int result=-1;
		
		AdminDAO dao=AdminDAO.getInstance();
		
		String email=request.getParameter("email");
		String purchaseConfirm_date=request.getParameter("date");
		String[] email2=email.split(",");
		String[] purchaseConfirm_date2=purchaseConfirm_date.split(",");
		for(int i=0; i<email2.length; i++){
			result=dao.updateState(purchaseConfirm_date2[i],email2[i]);
		}
		
		request.setAttribute("result", result);
		
		RequestDispatcher dispatcher=request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
