package com.sid.controller.action.admin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sid.controller.Action;
import com.sid.dao.PurchaseDAO;
import com.sid.dto.PurchaseVO;

public class ListAllOrderAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="admin/admin_all_order.jsp";
		
		PurchaseDAO pDao=PurchaseDAO.getInstance();
		ArrayList<PurchaseVO> list= pDao.listAllPurchase_admin();
		
		ArrayList<PurchaseVO> newList=new ArrayList<>();
		ArrayList<Integer> relateNum = new ArrayList<>();
		int[] state = new int[list.size()];
		for(int i=0;i<list.size();i++){
			state[i]=1;
		}
		for(int i=0;i<list.size();i++){
			
			String pId=list.get(i).getPurchaseId();
			int count=0;
			for(int j=i;j<list.size();j++){
				if(state[j]!=0&&pId==list.get(j).getPurchaseId()){
					count++;
					state[j]=0;
					newList.add(list.get(j));
				}
			}
			if(count!=0){
				relateNum.add(count);
			}
			
		}
		request.setAttribute("list", newList);
		request.setAttribute("relateNum", relateNum);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
