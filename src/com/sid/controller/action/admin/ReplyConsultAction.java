package com.sid.controller.action.admin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.controller.action.member.EmailAuth;
import com.sid.dao.AdminDAO;
import com.sid.dto.ConsultVO;

public class ReplyConsultAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "member/joinResult.jsp";

		HttpSession session = request.getSession();
		int admin=(int)session.getAttribute("admin");
		if(admin<2){
			AdminDAO dao = AdminDAO.getInstance();
			String email = request.getParameter("email");
			String content = request.getParameter("textContent");
			int num = Integer.parseInt(request.getParameter("consultNum"));

			System.out.println(email + content + num);

			EmailAuth auth = new EmailAuth();
			int result = auth.sendConsult(email, content);   
			int result2 = 0;// update reply result
			if (result > 0) {

				result2 = dao.updateReply(num);
			} else {
				request.setAttribute("result", "0");
			}
			if (result2 > 0) {
				request.setAttribute("result", "1");
			}
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
