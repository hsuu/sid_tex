package com.sid.controller.action.admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sid.controller.Action;
import com.sid.dao.AdminDAO;
import com.sid.dao.MemberDAO;

public class UpdateRepNumFlagAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "product/productResult.jsp";
		
		
		AdminDAO dao=AdminDAO.getInstance();
		String email=request.getParameter("email");
		
		int result=dao.updateRepNumFlag(email);
		
		if(result>0)
			request.setAttribute("result", 1);
		else
			request.setAttribute("result", 0);
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
