package com.sid.controller.action.admin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sid.controller.Action;
import com.sid.dao.BasketDAO;
import com.sid.dao.ProductDAO;

public class AdminDeleteProductAction implements Action {
   @Override
   public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      String url = "SidServlet?command=main";
      int id = Integer.parseInt(request.getParameter("productId"));
      ProductDAO dao = ProductDAO.getInstance();
      BasketDAO bDao=BasketDAO.getInstance();
      
      //relate 삭제
      String rel = dao.selectRelateProduct(id);

      if (rel != null) {

         String[] relate = rel.split(",");

         System.out.println(relate);
         request.getParameter("productId");

         ArrayList<Integer> relateUpdate = new ArrayList<>();

         if(relate.length!=2){
         
            for (int i = 0; i < relate.length; i++) {
               if (Integer.parseInt(relate[i]) != id) {
                  relateUpdate.add(Integer.parseInt(relate[i]));
               }
            }
   
            String relateStr = relateUpdate.toString();
            relateStr = relateStr.replaceAll("\\p{Z}", "");
            relateStr = relateStr.substring(1, relateStr.length() - 1);
   
            // 연관상품 등록
            if (relateUpdate.size() != 1) {
               for (int i = 0; i < relateUpdate.size(); i++) {
                  dao.updateRelateProduct(relateStr, relateUpdate.get(i));
               }
            }
         }else{
            System.out.println("relate length :"+relate.length);
            for (int i = 0; i < relate.length; i++) {
                dao.updateRelateNullProduct(Integer.parseInt(relate[i]));
             }
         }
         
      }
         dao.deleteProduct(id);
         bDao.deleteBasketByProductId(id);
      
      RequestDispatcher dispatcher = request.getRequestDispatcher(url);
      dispatcher.forward(request, response);
   }
}