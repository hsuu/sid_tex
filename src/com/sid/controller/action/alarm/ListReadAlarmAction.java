package com.sid.controller.action.alarm;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.AlarmDAO;
import com.sid.dao.PurchaseDAO;
import com.sid.dto.AlarmVO;
import com.sid.dto.PurchaseVO;

public class ListReadAlarmAction implements Action{
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="alarm/ReadAlarm.jsp";
		
		HttpSession session=request.getSession();
		String receiver=(String)session.getAttribute("email");
		AlarmDAO dao=AlarmDAO.getInstance();
		ArrayList<AlarmVO> list=dao.listReadAlarm(receiver);
		int count=dao.checkNewAlarm(receiver);
		request.setAttribute("list",list);
		request.setAttribute("count",count);
		session.setAttribute("alarm", 0);
		
		RequestDispatcher dispatcher=request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
