package com.sid.controller.action.alarm;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.AlarmDAO;
import com.sid.dao.PurchaseDAO;
import com.sid.dto.AlarmVO;
import com.sid.dto.PurchaseVO;

public class ListUnreadAlarmAction implements Action{
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="alarm/UnreadAlarm.jsp";
		
		HttpSession session=request.getSession();
		String receiver=(String)session.getAttribute("email");
		AlarmDAO dao=AlarmDAO.getInstance();
		ArrayList<AlarmVO> list=dao.listUnreadAlarm(receiver);
		
		request.setAttribute("list",list);
	
		for(int i=0;i<list.size();i++){
			if(list.get(i).getState()==0)
				dao.updateAlarmState(list.get(i).getAlarmId(), 1);
		}
		int count=list.size();
		if(count>0){
			request.setAttribute("count",list.size());
		}else{
			request.setAttribute("count",0);
		}
		
		session.setAttribute("alarm", 0);
		
		RequestDispatcher dispatcher=request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
