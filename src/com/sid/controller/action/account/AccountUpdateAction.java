package com.sid.controller.action.account;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.AccountDAO;
import com.sid.dto.UserVO;

public class AccountUpdateAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "product/productResult.jsp";
		
		UserVO vo=new UserVO();
		AccountDAO dao=AccountDAO.getInstance();
		HttpSession session=request.getSession();
		
		String email=(String)session.getAttribute("email");
		String bank_name=(String)request.getParameter("bank_name");
		String account_number=(String)request.getParameter("account_number");
		String account_holder=(String)request.getParameter("account_holder");
		
		
		vo.setBank_name(bank_name);
		vo.setAccount_number(account_number);
		vo.setAccount_holder(account_holder);
		
		int result=dao.insertAccount(vo, email);
		
		if (result > 0) {

			request.setAttribute("result", 1);
		} else {
			request.setAttribute("result", 0);
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
