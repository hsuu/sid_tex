package com.sid.controller.action.account;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.AccountDAO;
import com.sid.dto.UserVO;

public class AccountAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "product/product_manage_balance_account.jsp";

		HttpSession session = request.getSession();
		String email=(String)session.getAttribute("email");
		UserVO vo=new UserVO();
		
		AccountDAO dao=AccountDAO.getInstance();
		vo=dao.getAccount(email);
		
		request.setAttribute("account", vo);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
