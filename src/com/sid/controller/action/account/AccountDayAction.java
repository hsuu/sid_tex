package com.sid.controller.action.account;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.AccountDAO;
import com.sid.dto.CalculateVO;
import com.sid.dto.UserVO;

public class AccountDayAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "product/product_manage_balance_day.jsp";
		
		HttpSession session = request.getSession();
		String email=(String)session.getAttribute("email");
		AccountDAO dao=AccountDAO.getInstance();
		
		String datepicker1=request.getParameter("datepicker1");
		String datepicker2=request.getParameter("datepicker2");
		
		ArrayList<CalculateVO> list=dao.getCalculateByDay(email,datepicker1,datepicker2);
		request.setAttribute("list", list);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
