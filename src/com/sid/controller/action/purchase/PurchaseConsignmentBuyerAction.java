package com.sid.controller.action.purchase;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.AccountDAO;
import com.sid.dao.AlarmDAO;
import com.sid.dao.MemberDAO;
import com.sid.dao.PayInfoDAO;
import com.sid.dao.PurchaseDAO;
import com.sid.dto.AlarmVO;
import com.sid.dto.CalculateVO;
import com.sid.dto.PayInfoVO;
import com.sid.dto.PurchaseVO;
import com.sid.dto.UserVO;

public class PurchaseConsignmentBuyerAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "member/joinResult.jsp";

		HttpSession session = request.getSession();
		String email = (String) session.getAttribute("email");

		MemberDAO mDao = MemberDAO.getInstance();
		UserVO uvo = mDao.getRepMember(email);
		PurchaseDAO pDao = PurchaseDAO.getInstance();
		AlarmDAO aDao = AlarmDAO.getInstance();
		AlarmVO vo = null;

		// 정산
		CalculateVO cVo = new CalculateVO();
		PurchaseVO pVo = new PurchaseVO();
		PurchaseVO pVo2 = new PurchaseVO();
		PayInfoVO iVo = new PayInfoVO();
		PayInfoDAO iDao = PayInfoDAO.getInstance();
		ArrayList<PurchaseVO> list = new ArrayList<>();
		AccountDAO tDao = AccountDAO.getInstance();

		int state = Integer.parseInt(request.getParameter("state"));
		String idStr = request.getParameter("idList");
		String[] idArr = idStr.split(",");
		String emailStr = request.getParameter("emailList");
		String[] eArr = emailStr.split(",");

		int result = 0;
		for (int i = 0; i < idArr.length; i++) {
			// 구매확정 상태변경
			result = pDao.updateState(idArr[i], state);

			// 알람
			if (result > 0) {
				vo = new AlarmVO();
				vo.setReceiver(eArr[i]);
				vo.setSender(email);
				vo.setSort("상태변경");
				vo.setMessage(email + " 님이 " + idArr[i] + " 상품을 '구매확정' 하셨습니다.");
				vo.setState(0);
			}
			aDao.sendAlarm(vo);

			pVo = pDao.getSellerAndOid(idArr[i]);

			// 같이 구매한 상품들이 모두 구매확정되었는지 확인 (판매사별)
			// 같이 구매한 상품들이 모두 구매확정(state=4)이면 보여줌
			
			int totalCost=0;
			if (pDao.checkState(pVo.getOid(), pVo.getSeller())) {

				// 정산
				pVo2 = pDao.getPayment_amount(pVo.getSeller(), pVo.getOid());
				totalCost=(pVo2.getTotalcost()+pVo2.getDelivery_cost());
				cVo.setPurchaseId(pVo.getPurchaseId());
				cVo.setPurchaseConfirm_date(getDate());		//구매확정일
				cVo.setNumber_of_orders(1);					//주문건수 - 자동으로 1건 입력
				cVo.setPayment_amount(totalCost); // 결제금액(A)
				cVo.setSurtax(totalCost * 0.1); // 부가세(B)

				iVo = iDao.getPayMethod(pVo.getOid());

				// 결제수단이 무통장입금일 경우 수수료(건당 300원)
				if (iVo.getPayMethod().equals("VBank")) {
					cVo.setBrokerage_fee(300); // 수수료(C)

					// 결제수단이 카드결제일 경우 수수료(3.2%)
				} else if (iVo.getPayMethod().equals("VCard")) {
					cVo.setBrokerage_fee(totalCost * 0.032); // 수수료(C)

					// 결제수단이 실시간 계좌이체일 경우 수수료
				} else if (iVo.getPayMethod().equals("DirectBank")) {

					// 수수료 1.8퍼센트 기준일때
					// 10000원 이하일때 수수료 200원 적용
					if ((totalCost * 0.018) <= 200) {
						cVo.setBrokerage_fee(200); // 수수료(C)
					} else {
						// 10000원 이상일때 2퍼센트로 적용
						cVo.setBrokerage_fee(totalCost * 0.018); // 수수료(C)
					}
				}

				cVo.setFee(totalCost * 0.03); // 중개료(D)
				cVo.setCalculate_amount(
						cVo.getPayment_amount() - cVo.getSurtax() - cVo.getBrokerage_fee() - cVo.getFee());// 정산금액(A-B-C-D)

				tDao.insertCalculate(cVo, pVo.getSeller());

			}
			//같이 구매한 상품들이 판매사 상관없이 구매확정되었는지 확인
			if(pDao.checkStateByOid(pVo.getOid())){
				//state 를 7(완료된거래)로 변경
				pDao.updateStateToComplete(pVo.getOid());
			}
			

		}

		request.setAttribute("result", result);
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}

	public String getDate() {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Calendar c1 = Calendar.getInstance();

		return sdf.format(c1.getTime());


	}

}
