package com.sid.controller.action.purchase;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.sid.controller.Action;
import com.sid.dao.PurchaseDAO;
import com.sid.dto.CancelOrderVO;

public class GetCancelReasonAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="member/joinResult.jsp";
		
		PurchaseDAO pDao=PurchaseDAO.getInstance();
		CancelOrderVO vo=pDao.getCancelReason(request.getParameter("purchaseId"));
		JSONObject object=new JSONObject();
		
		object.put("purchaseId", URLEncoder.encode(vo.getPurchaseId() , "UTF-8"));
		object.put("date",""+vo.getDate());
		object.put("reason", URLEncoder.encode(vo.getReason() , "UTF-8"));
		object.put("canceller", URLEncoder.encode(vo.getCanceller() , "UTF-8"));
		
		request.setAttribute("result", object.toJSONString());
		
		RequestDispatcher dispatcher =request.getRequestDispatcher(url);
		dispatcher.forward(request,response);
	}

}
