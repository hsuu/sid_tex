package com.sid.controller.action.purchase;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.sid.controller.Action;
import com.sid.dao.PayInfoDAO;
import com.sid.dao.PurchaseDAO;
import com.sid.dto.CancelOrderVO;
import com.sid.dto.PayInfoVO;

public class GetAccountInfoAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="member/joinResult.jsp";
		
		PayInfoDAO pDao=PayInfoDAO.getInstance();
		PayInfoVO vo=pDao.getAccountInfo(request.getParameter("oid"));
		JSONObject object=new JSONObject();
		
		object.put("price", ""+vo.getPrice());
		object.put("vact_num",vo.getVact_num());
		object.put("vact_bankName", URLEncoder.encode(vo.getVact_bankName() , "UTF-8"));
		object.put("vact_name", URLEncoder.encode(vo.getVact_name() , "UTF-8"));
		object.put("vact_inputName", URLEncoder.encode(vo.getVact_inputName() , "UTF-8"));
		object.put("vact_date", URLEncoder.encode(vo.getVact_date() , "UTF-8"));
		object.put("vact_time", URLEncoder.encode(vo.getVact_time(), "UTF-8"));
		
		request.setAttribute("result", object.toJSONString());
		RequestDispatcher dispatcher =request.getRequestDispatcher(url);
		dispatcher.forward(request,response);
	}

}
