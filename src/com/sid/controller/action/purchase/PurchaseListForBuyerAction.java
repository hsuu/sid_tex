package com.sid.controller.action.purchase;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.sid.controller.Action;
import com.sid.dao.PurchaseDAO;
import com.sid.dto.BasketVO;
import com.sid.dto.PayInfoVO;
import com.sid.dto.PurchaseVO;

//결제내역 띄우는 액션
public class PurchaseListForBuyerAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "mypage/payment_history3.jsp";

		HttpSession session = request.getSession();
		PurchaseDAO pDao = PurchaseDAO.getInstance();
		ArrayList<PayInfoVO> oidlist=pDao.listAllOid((String)session.getAttribute("email"),0);

		JSONObject obj=new JSONObject();
		JSONObject company=new JSONObject();
		JSONObject oidjson = new JSONObject();
		for(int i=0;i<oidlist.size();i++){
			
			obj.put(oidlist.get(i).getMoid(), oidlist.get(i).getPrice());
		}
		ArrayList<ArrayList<PurchaseVO>> list=new ArrayList<ArrayList<PurchaseVO>>();
		ArrayList<PurchaseVO> childlist=null;
		for(int i=0;i<oidlist.size();i++){
			company=pDao.listCompanyByOid(oidlist.get(i).getMoid());
			childlist=pDao.listAllByOid(oidlist.get(i).getMoid());
			list.add(childlist);
			oidjson.put(oidlist.get(i).getMoid(),company);
		}
		
		System.out.println("l : "+list.size());
		System.out.println("d " +oidjson.toJSONString());
		System.out.println("p" + obj.toJSONString());

		request.setAttribute("list", list);
		request.setAttribute("oidjson", oidjson.toJSONString());
		request.setAttribute("price", obj.toJSONString());

		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
