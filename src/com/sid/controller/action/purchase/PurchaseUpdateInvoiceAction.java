package com.sid.controller.action.purchase;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sid.controller.Action;
import com.sid.dao.PurchaseDAO;

public class PurchaseUpdateInvoiceAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="member/joinResult.jsp";
		
		PurchaseDAO pDao=PurchaseDAO.getInstance();
		int result=pDao.updateInvoiceNum(request.getParameter("oid"),request.getParameter("input"));
		
		request.setAttribute("result", result);
		RequestDispatcher dispatcher =request.getRequestDispatcher(url);
		dispatcher.forward(request,response);
	}

}
