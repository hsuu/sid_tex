package com.sid.controller.action.purchase;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.AlarmDAO;
import com.sid.dao.MemberDAO;
import com.sid.dao.PurchaseDAO;
import com.sid.dto.AlarmVO;
import com.sid.dto.PurchaseVO;
import com.sid.dto.UserVO;

public class PurchaseCancelOrderBuyerAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "member/joinResult.jsp";

		HttpSession session = request.getSession();
		String email = (String) session.getAttribute("email");

		MemberDAO mDao = MemberDAO.getInstance();
		UserVO uvo = mDao.getRepMember(email);
		PurchaseDAO pDao = PurchaseDAO.getInstance();
		AlarmDAO aDao = AlarmDAO.getInstance();
		AlarmVO vo = null;
		PurchaseVO pVo=new PurchaseVO();
		// state 5=취소 , 6=취소요청
		int state = Integer.parseInt(request.getParameter("state"));
		String idStr = request.getParameter("idList");
		String[] idArr = idStr.split(",");
		String emailStr = request.getParameter("emailList");
		String[] eArr = emailStr.split(",");
		String idStr2 = request.getParameter("idList2");
		String[] idArr2 = idStr2.split(",");
		String emailStr2 = request.getParameter("emailList2");
		String[] eArr2 = emailStr2.split(",");
		String reason = request.getParameter("reason");
		int result = 0;

		System.out.println("state : --------- "+state);
		if(idStr!=""){
			
		for (int i = 0; i < idArr.length; i++) {
			result = pDao.updateStateForBuyer(idArr[i], state);
			pDao.insertOrderCancel(idArr[i], reason,email);
			if (result > 0) {
				vo = new AlarmVO();
				vo.setReceiver(eArr[i]);
				vo.setSender(email);
				vo.setSort("주문취소");
				vo.setMessage(email + " 님이 " + idArr[i] + " 상품을 '주문 취소' 하셨습니다.");

				vo.setReason(reason);
				vo.setState(0);

				aDao.sendAlarm(vo);
			}
			//같이 구매한 상품들이 판매사 상관없이 구매확정되었는지 확인
			pVo=pDao.getSellerAndOid(idArr[i]);
			
			if(pDao.checkStateByOid(pVo.getOid())){
				//state 를 7(완료된거래)로 변경
				pDao.updateStateToComplete(pVo.getOid());
			}
			
		}
		}
		/*if(idStr2!=""){
		
		for (int i = 0; i < idArr2.length; i++) {
			result = pDao.updateState(idArr2[i], state+1);
			pDao.insertOrderCancel(idArr2[i], reason,email);
			if (result > 0) {
				vo = new AlarmVO();
				vo.setReceiver(eArr2[i]);
				vo.setSender(email);
				vo.setSort("주문취소");
				vo.setMessage(email + " 님이 " + idArr2[i] + " 상품을 '주문 취소 요청' 하셨습니다.");

				vo.setReason(reason);
				vo.setState(0);

			}
			aDao.sendAlarm(vo);
		}
		}*/
		

		request.setAttribute("result", result);
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}

}
