package com.sid.controller.action.purchase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;

import com.sid.controller.Action;
import com.sid.dao.PurchaseDAO;
import com.sid.dto.PayInfoVO;
import com.sid.dto.PurchaseVO;

//주문관리 띄우는 액션
public class PurchaseListAction implements Action{
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="product/product_manage_order0.jsp";
		
	
		HttpSession session=request.getSession();
		PurchaseDAO pDao=PurchaseDAO.getInstance();
		String email=(String)session.getAttribute("email");
		int state=Integer.parseInt(request.getParameter("state"));
		
		switch (state) {
		case 0:
			url="product/product_manage_order0.jsp";
			break;
		case 1:
			url="product/product_manage_order1.jsp";
			break;
		case 2:
			url="product/product_manage_order2.jsp";
			break;
		case 3:
			url="product/product_manage_order3.jsp";
			break;
		case 4:
			url="product/product_manage_order4.jsp";
			break;
		case 5:
			url="product/product_manage_order5.jsp";
			break;
		case 6:
			url="product/product_manage_order6.jsp";
			break;
		default:
			break;
		}		
		ArrayList<PayInfoVO> oidlist=pDao.listAllOidBySeller(email,state);
		
		System.out.println("oidlsit : "+oidlist.size());
	
		ArrayList<ArrayList<PurchaseVO>> list=new ArrayList<ArrayList<PurchaseVO>>();
		ArrayList<PurchaseVO> childlist=null;
		
		HashMap<String, Integer> map = new HashMap<>();
		map=pDao.countState(email);
		
		JSONObject oidjson = new JSONObject();
		for(int i=0;i<oidlist.size();i++){
			System.out.println(oidlist.get(i).getMoid());
			childlist=pDao.listAllByOidSeller(oidlist.get(i).getMoid(),email);
			list.add(childlist);
		}
		System.out.println("몇개?"+list.size());
		request.setAttribute("list", list);
		request.setAttribute("oidjson", oidjson.toJSONString());
		request.setAttribute("counter", map);
		RequestDispatcher dispatcher=request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
