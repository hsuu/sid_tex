package com.sid.controller.action.purchase;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.sid.controller.Action;
import com.sid.dao.PurchaseDAO;
import com.sid.dto.BasketVO;
import com.sid.dto.PayInfoVO;
import com.sid.dto.PurchaseVO;

//결제내역 띄우는 액션
public class PurchaseCancelReadyListForBuyerAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "mypage/payment_history_cancelready.jsp";

		HttpSession session = request.getSession();
		PurchaseDAO pDao = PurchaseDAO.getInstance();
		ArrayList<PurchaseVO> list=pDao.listCancelReady((String)session.getAttribute("email"));
		
		request.setAttribute("list", list);

		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
