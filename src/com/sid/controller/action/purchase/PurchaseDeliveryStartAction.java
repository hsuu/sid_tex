package com.sid.controller.action.purchase;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.AlarmDAO;
import com.sid.dao.MemberDAO;
import com.sid.dao.PurchaseDAO;
import com.sid.dto.AlarmVO;
import com.sid.dto.UserVO;

public class PurchaseDeliveryStartAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="member/joinResult.jsp";
		
		
		HttpSession session=request.getSession();
		String email=(String)session.getAttribute("email");
		MemberDAO mDao=MemberDAO.getInstance();
		UserVO uvo=mDao.getRepMember(email);
		PurchaseDAO pDao=PurchaseDAO.getInstance();
		AlarmDAO aDao=AlarmDAO.getInstance();
		AlarmVO vo=null;
		String[] id=request.getParameterValues("id");
		String[] info=request.getParameterValues("info");
		String[] eArr=request.getParameterValues("email");
		
		int result=1;
		for(int i=0;i<id.length;i++){
			result=pDao.startDelivery(id[i], info[i],email);
			if(result>0){
				vo=new AlarmVO();
				vo.setReceiver(eArr[i]);
				vo.setSender(email);
				vo.setSort("배송 상태변경");
				vo.setMessage(uvo.getCompany()+" 님이 "+id[i]+ " 상품의 상태를 '배송중' 으로 변경 하셨습니다.");
				vo.setState(0);
			}
			aDao.sendAlarm(vo);
		}
		
		request.setAttribute("result", result);
		RequestDispatcher dispatcher =request.getRequestDispatcher(url);
		dispatcher.forward(request,response);
	}

}
