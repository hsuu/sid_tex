package com.sid.controller.action.purchase;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.PayInfoDAO;
import com.sid.dao.PurchaseDAO;
import com.sid.dto.PayInfoVO;

public class PaymentSuccessAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "product/productResult.jsp";
		
		
		HttpSession session=request.getSession();
		PurchaseDAO dao=PurchaseDAO.getInstance();
		PayInfoVO payVo=new PayInfoVO();
		PayInfoDAO payDao=PayInfoDAO.getInstance();
		String email=(String)session.getAttribute("email");
		String oid=(String)request.getParameter("oid");
		int payFlag=Integer.parseInt(request.getParameter("payFlag"));
		int result=0;
		
		try{
			if(request.getParameter("auth").equals("1")){
			payVo.setTid(request.getParameter("tid"));
			if(request.getParameter("price")!=null){
				int price=Integer.parseInt(request.getParameter("price"));
				payVo.setPrice(price);
			}
			payVo.setMoid(request.getParameter("moid"));
			payVo.setApplDate(request.getParameter("applDate"));
			payVo.setApplTime(request.getParameter("applTime"));
			payVo.setPayMethod(request.getParameter("payMethod"));
			
			//가상계좌
			payVo.setVact_num(request.getParameter("vact_num"));
			payVo.setVact_bankCode(request.getParameter("vact_bankCode"));
			payVo.setVact_bankName(request.getParameter("vact_bankName"));
			payVo.setVact_name(request.getParameter("vact_name"));
			payVo.setVact_inputName(request.getParameter("vact_inputName"));
			payVo.setVact_date(request.getParameter("vact_date"));
			payVo.setVact_time(request.getParameter("vact_time"));
			
			//실시간 계좌이체
			payVo.setAcct_bankCode(request.getParameter("acct_bankCode"));
			payVo.setCshr_resultCode(request.getParameter("cshr_resultCode"));
			payVo.setCshr_type(request.getParameter("cshr_type"));
			
			//뱅크월렛 카카오
			payVo.setKwpy_cellPhone(request.getParameter("kwpy_cellPhone"));
			payVo.setKwpy_salesAmount(request.getParameter("kwpy_salesAmount"));
			payVo.setKwpy_amount(request.getParameter("kwpy_amount"));
			payVo.setKwpy_tax(request.getParameter("kwpy_tax"));
			payVo.setKwpy_serviceFee(request.getParameter("kwpy_serviceFee"));
			payVo.setKwpy_payMethod(request.getParameter("kwpy_payMethod"));
			payVo.setKwpy_resultCode(request.getParameter("kwpy_resultCode"));
			payVo.setKwpy_resultMsg(request.getParameter("kwpy_resultMsg"));
			payVo.setKwpy_tid(request.getParameter("kwpy_tid"));
			payVo.setKwpy_moid(request.getParameter("kwpy_moid"));
			payVo.setKwpy_price(request.getParameter("kwpy_price"));
			payVo.setKwpy_applDate(request.getParameter("kwpy_applDate"));
			payVo.setKwpy_applTime(request.getParameter("kwpy_applTime"));
			
			//카드
			payVo.setEventCode(request.getParameter("eventCode"));
			payVo.setCard_num(request.getParameter("card_num"));
			payVo.setCard_applNum(request.getParameter("card_applNum"));
			payVo.setCard_quota(request.getParameter("card_quota"));
			payVo.setCard_interest(request.getParameter("card_interest"));
			payVo.setCard_point(request.getParameter("card_point"));
			payVo.setCard_code(request.getParameter("card_code"));
			payVo.setCard_bankCode(request.getParameter("card_bankCode"));
			
			payVo.setOcb_num(request.getParameter("ocb_num"));
			payVo.setOcb_saveApplNum(request.getParameter("ocb_saveApplNum"));
			payVo.setOcb_payPrice(request.getParameter("ocb_payPrice"));
			
			payVo.setGspt_num(request.getParameter("gspt_num"));
			payVo.setGspt_remains(request.getParameter("gspt_remains"));
			payVo.setGspt_applPrice(request.getParameter("gspt_applPrice"));
			
			payVo.setUnpt_cardNum(request.getParameter("unpt_cardNum"));
			payVo.setUnpt_usablePoint(request.getParameter("unpt_usablePoint"));
			payVo.setUnpt_payPrice(request.getParameter("unpt_payPrice"));
			payVo.setEmail(email);
			
			payVo.setOid(oid);
			}
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("PaymentSuccessAction Error : "+e);
		}
		
		//결제정보 DB입력
		payDao.insertPayInfo(payVo);		
		
		
			//결제 성공시 Purchase 테이블에 입력
			result=dao.updatePayFlag(payFlag,oid,request.getParameter("payMethod"));
			
		if(payFlag==0){
			//결제 실패시 Purchase 테이블에 삭제
			result=dao.deletePayFlag(oid);
			System.out.println("결제 실패시 테이블에서 삭제 되는 지 여부 : "+result);
		}
		
		request.setAttribute("result",result);
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
}
