package com.sid.controller.action.purchase;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.AlarmDAO;
import com.sid.dao.MemberDAO;
import com.sid.dao.PurchaseDAO;
import com.sid.dto.AlarmVO;
import com.sid.dto.UserVO;

public class PurchaseConfirmCancelBuyerAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="member/joinResult.jsp";
		
		HttpSession session=request.getSession();
		String email=(String)session.getAttribute("email");
		
		PurchaseDAO pDao=PurchaseDAO.getInstance();
		AlarmDAO aDao=AlarmDAO.getInstance();
		AlarmVO vo=null;
		
		
		//state 5=취소 , 6=취소요청
		String id=request.getParameter("purchaseId");
		String seller=request.getParameter("seller");
		int result=0;
			result=pDao.updateState(id,5);
			if(result>0){
				vo=new AlarmVO();
				vo.setReceiver(seller);
				vo.setSender(email);
				vo.setSort("주문취소승인");
				vo.setMessage(email+" 님이 "+id +" 상품을 '주문 취소 승인' 하셨습니다.");
				vo.setState(0);
				
			}
			aDao.sendAlarm(vo);
		
		
		
		request.setAttribute("result", result);
		RequestDispatcher dispatcher =request.getRequestDispatcher(url);
		dispatcher.forward(request,response);
	}

}
