package com.sid.controller.action.purchase;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sid.controller.Action;
import com.sid.dao.AlarmDAO;
import com.sid.dao.MemberDAO;
import com.sid.dao.PurchaseDAO;
import com.sid.dto.AlarmVO;
import com.sid.dto.PurchaseVO;
import com.sid.dto.UserVO;

public class PurchaseCancelOrderAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="member/joinResult.jsp";
		
		HttpSession session=request.getSession();
		String email=(String)session.getAttribute("email");
		
		MemberDAO mDao=MemberDAO.getInstance();
		UserVO uvo=mDao.getRepMember(email);
		PurchaseDAO pDao=PurchaseDAO.getInstance();
		AlarmDAO aDao=AlarmDAO.getInstance();
		AlarmVO vo=null;
		PurchaseVO pVo=new PurchaseVO();
		
		//state 5=취소 , 6=취소요청
		int state=Integer.parseInt(request.getParameter("state"));
		String idStr=request.getParameter("idList");
		String[] idArr=idStr.split(",");
		String emailStr=request.getParameter("emailList");
		String[] eArr=emailStr.split(",");
		String reason=request.getParameter("reason");
		
		System.out.println("------------");
		System.out.println("idStr"+idStr);
		System.out.println("email str "+emailStr);
		
		int result=0;
		for(int i=0;i<idArr.length;i++){
			result=pDao.updateState(idArr[i],state);
			pDao.insertOrderCancel(idArr[i], reason,uvo.getCompany());
			if(result>0){
				vo=new AlarmVO();
				vo.setReceiver(eArr[i]);
				vo.setSender(email);
				if(state==5){					
					vo.setSort("주문취소");
					vo.setMessage(uvo.getCompany()+" 님이 "+idArr[i]+ " 상품을 '주문 취소' 하셨습니다.");
				}else{
					vo.setSort("주문취소 요청");
					vo.setMessage(uvo.getCompany()+" 님이 "+idArr[i]+ " 상품을 '주문 취소 요청' 하셨습니다.");
				}
				vo.setReason(reason);
				vo.setState(0);
				aDao.sendAlarm(vo);
			}
			
	
			pVo=pDao.getSellerAndOid(idArr[i]);
			
			if(pDao.checkStateByOid(pVo.getOid())){
				//state 를 7(완료된거래)로 변경
				pDao.updateStateToComplete(pVo.getOid());
			}
		}
		
		
		
		request.setAttribute("result", result);
		RequestDispatcher dispatcher =request.getRequestDispatcher(url);
		dispatcher.forward(request,response);
	}

}
