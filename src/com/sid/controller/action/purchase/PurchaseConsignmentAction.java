package com.sid.controller.action.purchase;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.set.SynchronizedSet;

import com.sid.controller.Action;
import com.sid.dao.AlarmDAO;
import com.sid.dao.MemberDAO;
import com.sid.dao.PurchaseDAO;
import com.sid.dto.AlarmVO;
import com.sid.dto.UserVO;

public class PurchaseConsignmentAction implements Action {
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="member/joinResult.jsp";
		
		HttpSession session=request.getSession();
		String email=(String)session.getAttribute("email");
		
		MemberDAO mDao=MemberDAO.getInstance();
		UserVO uvo=mDao.getRepMember(email);
		PurchaseDAO pDao=PurchaseDAO.getInstance();
		AlarmDAO aDao=AlarmDAO.getInstance();
		AlarmVO vo=null;
		
		int state=Integer.parseInt(request.getParameter("state"));
		String idStr=request.getParameter("idList");
		String[] idArr=idStr.split(",");
		String emailStr=request.getParameter("emailList");
		String[] eArr=emailStr.split(",");
		
		System.out.println("idlist"+idStr);
		System.out.println("email str "+emailStr);
		
		int result=0;
		for(int i=0;i<idArr.length;i++){
			result=pDao.updateState(idArr[i],state);
			if(result>0){
				vo=new AlarmVO();
				vo.setReceiver(eArr[i]);
				vo.setSender(email);
				vo.setSort("배송 상태변경");
				if(state==1){					
					vo.setMessage(uvo.getCompany()+" 님이 "+idArr[i]+ " 주문의 상태를 '배송준비' 로 변경 하셨습니다.");
					
				}else{
					vo.setMessage(uvo.getCompany()+" 님이 "+idArr[i]+ " 주문의 상태를 '배송완료' 로 변경 하셨습니다.");
				}
				vo.setState(0);
				
				aDao.sendAlarm(vo);
			}
		}
		
		
		request.setAttribute("result", result);
		RequestDispatcher dispatcher =request.getRequestDispatcher(url);
		dispatcher.forward(request,response);
	}

}
