package com.sid.controller;

import com.sid.controller.action.account.AccountAction;
import com.sid.controller.action.account.AccountCaseAction;
import com.sid.controller.action.account.AccountDayAction;
import com.sid.controller.action.account.AccountUpdateAction;
import com.sid.controller.action.admin.AdminCalculateAction;
import com.sid.controller.action.admin.AdminCalculateCompleteAction;
import com.sid.controller.action.admin.AdminDeleteProductAction;
import com.sid.controller.action.admin.AdminUpdateCancelStateAction;
import com.sid.controller.action.admin.AdminUpdateStateAction;
import com.sid.controller.action.admin.AdminWriteFAQAction;
import com.sid.controller.action.admin.ListAllBalanceAction;
import com.sid.controller.action.admin.ListAllConsultAction;
import com.sid.controller.action.admin.ListAllOrderAction;
import com.sid.controller.action.admin.ListAllUsersAction;
import com.sid.controller.action.admin.ListCancelBalanceAction;
import com.sid.controller.action.admin.ListRepNumAction;
import com.sid.controller.action.admin.ReplyConsultAction;
import com.sid.controller.action.admin.SecedeMemberAction;
import com.sid.controller.action.admin.UpdateRepNumFlagAction;
import com.sid.controller.action.alarm.ListReadAlarmAction;
import com.sid.controller.action.alarm.ListUnreadAlarmAction;
import com.sid.controller.action.basket.BasketListAction;
import com.sid.controller.action.basket.DeleteBasketAction;
import com.sid.controller.action.basket.DeleteColorOptionAction;
import com.sid.controller.action.basket.OrderAction;
import com.sid.controller.action.basket.OrderQuickAction;
import com.sid.controller.action.basket.OrderSwatchAction;
import com.sid.controller.action.basket.OrderSwatchQuickAction;
import com.sid.controller.action.basket.ToBasketAction;
import com.sid.controller.action.basket.ToPaymentAction;
import com.sid.controller.action.basket.ToSwatchPaymentAction;
import com.sid.controller.action.basket.UpdateColorOptionAction;
import com.sid.controller.action.customer.ListManageCustomerAction;
import com.sid.controller.action.index.IndexAction;
import com.sid.controller.action.member.CheckAlarmAction;
import com.sid.controller.action.member.CompanyCheckAction;
import com.sid.controller.action.member.ConsultMailAction;
import com.sid.controller.action.member.EmailCheckFormAction;
import com.sid.controller.action.member.GetUserAddressAction;
import com.sid.controller.action.member.JoinAction;
import com.sid.controller.action.member.JoinBAction;
import com.sid.controller.action.member.LeaveUserAction;
import com.sid.controller.action.member.LoginAction;
import com.sid.controller.action.member.LogoutAction;
import com.sid.controller.action.member.MyDeliveryAction;
import com.sid.controller.action.member.MyDeliveryUpdateAction;
import com.sid.controller.action.member.MyInfoAction;
import com.sid.controller.action.member.MyInfoBAction;
import com.sid.controller.action.member.PasswordCheckAction;
import com.sid.controller.action.member.SendPasswordAction;
import com.sid.controller.action.member.UpdateAuthAction;
import com.sid.controller.action.member.UpdateInfoAction;
import com.sid.controller.action.product.ProductAfterModifyAction;
import com.sid.controller.action.product.ProductBeforeModifyAction;
import com.sid.controller.action.product.ProductDeleteAction;
import com.sid.controller.action.product.ProductDeliveryeAction;
import com.sid.controller.action.product.ProductListAction;
import com.sid.controller.action.product.ProductManageOrderAction;
import com.sid.controller.action.product.ProductReadAction;
import com.sid.controller.action.product.ProductSearchAction;
import com.sid.controller.action.product.ProductStateUpdateAction;
import com.sid.controller.action.product.ProductSubmitAction;
import com.sid.controller.action.product.ProductUpdateAction;
import com.sid.controller.action.product.ProductUpdateDeliveryeAction;
import com.sid.controller.action.product.ProductUploadAction;
import com.sid.controller.action.purchase.GetAccountInfoAction;
import com.sid.controller.action.purchase.GetCancelReasonAction;
import com.sid.controller.action.purchase.PaymentSuccessAction;
import com.sid.controller.action.purchase.PurchaseCancelListForBuyerAction;
import com.sid.controller.action.purchase.PurchaseCancelOrderAction;
import com.sid.controller.action.purchase.PurchaseCancelOrderBuyerAction;
import com.sid.controller.action.purchase.PurchaseCancelReadyListForBuyerAction;
import com.sid.controller.action.purchase.PurchaseCompleteListForBuyerAction;
import com.sid.controller.action.purchase.PurchaseConfirmCancelBuyerAction;
import com.sid.controller.action.purchase.PurchaseConsignmentAction;
import com.sid.controller.action.purchase.PurchaseConsignmentBuyerAction;
import com.sid.controller.action.purchase.PurchaseDeliveryStartAction;
import com.sid.controller.action.purchase.PurchaseListAction;
import com.sid.controller.action.purchase.PurchaseListForBuyerAction;
import com.sid.controller.action.purchase.PurchaseUpdateInvoiceAction;

public class ActionFactory {
	private static ActionFactory instance = new ActionFactory();

	public ActionFactory() {
		super();
	}

	public static ActionFactory getInstance() {
		return instance;
	}

	public Action getAction(String command) {
		Action action = null;
		System.out.println("ActionFactory  :" + command);

			// 일반 회원 가입
		if (command.equals("join_user")) {
			action = new JoinAction();
		} else if (command.equals("join_user_b")) {
			action = new JoinBAction();
			// 이메일 인증
		} else if (command.equals("update_auth")) {
			action = new UpdateAuthAction();
			// 비밀번호 찾기
		} else if (command.equals("send_password")) {
			action = new SendPasswordAction();
			// 로그인
		} else if (command.equals("login")) {
			action = new LoginAction();
			// 로그아웃
		} else if (command.equals("logout")) {
			action = new LogoutAction();
			//탈퇴
		} else if (command.equals("leave_user")) {
			action = new LeaveUserAction();
			// 내정보 수정
		} else if (command.equals("update_info")) {
			action = new UpdateInfoAction();
		} else if (command.equals("email_check")) {
			action = new EmailCheckFormAction();
			//판매자 페이지 - 재고 관리
		} else if (command.equals("list_manage_stock")) {
			action = new ProductListAction();
			//판매자 페이지 - 배송 관리
		} else if (command.equals("list_manage_delivery")) {
			action = new ProductDeliveryeAction();
			//판매자 페이지 - 고객 관리
		} else if (command.equals("list_manage_customer")) {
			action = new ListManageCustomerAction();
			//판매자 페이지 - 배송 관리 저장
		} else if (command.equals("update_delivery_info")) {
			action = new ProductUpdateDeliveryeAction();	
			// 패스워드 확인
		} else if (command.equals("password_check")) {
			action = new PasswordCheckAction();
			//판매자명 중복확인
		} else if (command.equals("company_check")) {
			action = new CompanyCheckAction();

			// 판매자페이지 - 상품등록
		} else if (command.equals("product_submit")) {
			action = new ProductSubmitAction();

			//안읽은 알람 리스트
		} else if (command.equals("list_unread_alarm")) {
			action = new ListUnreadAlarmAction();
			//읽은 알람 리스트
		} else if (command.equals("list_read_alarm")) {
			action = new ListReadAlarmAction();
			//알람 체크
		} else if (command.equals("check_alarm")) {
			action = new CheckAlarmAction();
				
			// 홈
		} else if (command.equals("main")) {
			action = new IndexAction();

			// 장바구니 담기
		} else if (command.equals("toBasket")) {
			action = new ToBasketAction();
			// 장바구니 삭제
		} else if (command.equals("delete_basket")) {
			action = new DeleteBasketAction();
			// 장바구니 색상 옵션 수량 변경
		} else if (command.equals("delete_color_option")) {
			action = new DeleteColorOptionAction();
			// 장바구니 색상 수량 변경
		} else if (command.equals("update_color_option")) {
			action = new UpdateColorOptionAction();
			// 유저 주소 가져오기
		} else if (command.equals("get_user_address")) {
			action = new GetUserAddressAction();
			// 주문관리로 가는 액션
		} else if (command.equals("purchase_list")) {
			action = new PurchaseListAction();
			// 결제내역
		} else if (command.equals("purchase_list_buyer")) {
			action = new PurchaseListForBuyerAction();
			// 결제내역 - 완료된거래
					} else if (command.equals("purchase_list_complete_buyer")) {
						action = new PurchaseCompleteListForBuyerAction();
						// 결제내역 - 취소된
					} else if (command.equals("purchase_list_cancel_buyer")) {
						action = new PurchaseCancelListForBuyerAction();
						//결제내역 -취소대기
					} else if (command.equals("purchase_list_cancelready_buyer")) {
						action = new PurchaseCancelReadyListForBuyerAction();
			// 취소 요청 이유 가져오기
		} else if (command.equals("get_cancel_reason")) {
			action = new GetCancelReasonAction();
		} else if (command.equals("get_account_info")) {
			action = new GetAccountInfoAction();
			// 주문관리에서 탁송요청시 배송대기로 전환
		} else if (command.equals("request_consignment")) {
			action = new PurchaseConsignmentAction();
			// 거래내역에서 구매확정
		} else if (command.equals("request_consignment_buyer")) {
			action = new PurchaseConsignmentBuyerAction();
			// 주문 관리에서 송장번호 변경
		} else if (command.equals("update_invoice")) {
			action = new PurchaseUpdateInvoiceAction();
			// 주문 관리에서 배송 시작 (송장번호 혹은 배송요원 연락처 업데이트)
		} else if (command.equals("start_delivery")) {
			action = new PurchaseDeliveryStartAction();
			// 주문 관리에서 주문취소
		} else if (command.equals("cancel_order")) {
			action = new PurchaseCancelOrderAction();
			// 주문자가 거래내역에서 주문취소
		} else if (command.equals("cancel_order_buyer")) {
			action = new PurchaseCancelOrderBuyerAction();
		// 주문자가 거래내역에서 주문취소 승인
		} else if (command.equals("confirm_cancel_buyer")) {
				action = new PurchaseConfirmCancelBuyerAction();
				
			// 주문정보 입력 -아직 미완성
		} else if (command.equals("product_manage_order")) {
			action = new ProductManageOrderAction();
			//결제하기
		} else if (command.equals("order")) {
			action = new OrderAction();
			//결제하기 -퀵
		} else if (command.equals("order_quick")) {
			action = new OrderQuickAction();
			//결제하기 -퀵
		} else if (command.equals("order_swatch")) {
			action = new OrderSwatchAction();
			//결제하기 -퀵
		} else if (command.equals("order_swatch_quick")) {
			action = new OrderSwatchQuickAction();
		
			// 판매자페이지 - 상품등록페이지로
		} else if (command.equals("product_upload")) {
			action = new ProductUploadAction();
			// 판매자페이지 - 상품등록하기
		} else if (command.equals("product_submit")) {
			action = new ProductSubmitAction();

			// 일반회원 기본정보 보기
		} else if (command.equals("myinfo")) {
			action = new MyInfoAction();
			// 기업회원 기본정보 보기
		} else if (command.equals("myinfoB")) {
			action = new MyInfoBAction();

/*			// 내정보 수정
		} else if (command.equals("update_myinfo")) {
			action = new MyInfoUpdateAction();
*/
			//배송정보 보기
		}else if(command.equals("delivery")){
			action=new MyDeliveryAction();
			//배송정보 수정
		}else if(command.equals("update_delivery")){
			action=new MyDeliveryUpdateAction();
			// 특정 상품 보기
		} else if (command.equals("read_product")) {
			action = new ProductReadAction();
			// 재고관리 - 상품수정(한개씩)
		} else if (command.equals("update_product")) {
			action = new ProductUpdateAction();
			// 재고관리 - 수정버튼 클릭시 상품수정(전체)
		} else if (command.equals("before_modify_product")) {
			action = new ProductBeforeModifyAction();
		} else if (command.equals("after_modify_product")) {
			action =new ProductAfterModifyAction();
			//재고관리 - 삭제 버튼 클릭시 flag 1로 변경됨 (1로 변경시 보이지 않음, 완전한 의미에서 삭제는 아님)
		} else if (command.equals("delete_product")) {
			action = new ProductDeleteAction();
			//재고관리 - 판매상태 변경
		}else if(command.equals("update_state")){
			action =new ProductStateUpdateAction();
			//정산관리 보기-일별 정산내역
		}else if(command.equals("list_manage_balance_day")){
			action =new AccountDayAction();
					//정산관리 - 건별 정산내역
				}else if(command.equals("list_manage_balance_case")){
					action=new AccountCaseAction();
					//정산관리 - 계좌보기
				}else if(command.equals("list_manage_balance_account")){
					action=new AccountAction();
			//정산관리 - 계좌정보 입력
		}else if(command.equals("update_account_info")){
			action=new AccountUpdateAction();
			

		} else if (command.equals("search_product")) {
			action = new ProductSearchAction();

		} else if (command.equals("list_all_basket")) {
			action = new BasketListAction();

		} else if (command.equals("to_payment")) {
			action = new ToPaymentAction();
		} else if (command.equals("to_swatch_payment")) {
			action = new ToSwatchPaymentAction();
			
			//결제완료시 db입력
		}else if(command.equals("payment_success")){
			action=new PaymentSuccessAction();
			
		
			
			
		/*	관리자 페이지	*/
			
			// 결제내역 리스트
			// 이메일 상담
		} else if (command.equals("consult_mail")) {
			action = new ConsultMailAction();
			// 문의 목록 가져오기
		} else if (command.equals("list_all_consult")) {
			action = new ListAllConsultAction();
			// 문의 답장
		} else if (command.equals("consult_reply")) {
			action = new ReplyConsultAction();
		
			// 관리자페이지 - 회원목록 보기
		} else if (command.equals("list_all_user")) {
			action = new ListAllUsersAction();
			// 관리자페이지 - 주문관리 보기
		} else if (command.equals("list_all_order")) {
			action = new ListAllOrderAction();	
			// 관리자페이지 - 사업자등록 승인 페이지
		} else if (command.equals("list_rep_num")) {
			action = new ListRepNumAction();
			// 사업자 승인
		} else if (command.equals("update_rep_num_flag")) {
			action = new UpdateRepNumFlagAction();	
			// 관리자페이지 - 홈 화면에서 상품 직접 삭제(슈퍼관리자만 가능)
		} else if (command.equals("admin_delete_product")) {
			action = new AdminDeleteProductAction();
			// 관리자페이지 - 자주하는 질문 등록
		} else if (command.equals("write_faq")) {
			action = new AdminWriteFAQAction();
			// 관리자페이지 - 정산관리리스트 보기
		} else if(command.equals("list_all_balance")){
			action =new ListAllBalanceAction();
			
			// 관리자페이지 - 회원탈퇴(슈퍼관리자만 가능)
		} else if (command.equals("secede_member")) {
			action = new SecedeMemberAction();
			
			
			
			
			//정산 대기
		} else if(command.equals("admin_all_balance_wait")){
			action = new AdminCalculateAction();
			//정산 대기에서 완료로 상태변경
		} else if(command.equals("admin_update_state")){
			action =new AdminUpdateStateAction();
			//정산 완료
		}else if(command.equals("admin_all_balance_complete")){
			action =new AdminCalculateCompleteAction();
			//취소 정산 불러오기
		}else if(command.equals("admin_all_balance_cancel")){
			action=new ListCancelBalanceAction();
			//취소 정산 완료로 상태변경
		}else if(command.equals("admin_update_cancel_state")){
			action=new AdminUpdateCancelStateAction();
		}else {
			System.out.println("잘못된 주소 입니다");
		}

		return action;
	}
}
