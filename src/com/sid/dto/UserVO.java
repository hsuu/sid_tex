package com.sid.dto;

public class UserVO {

	// 개인고객
	private String email;
	private String pwd;
	private String name;
	private int nation;
	private String phone;
	private String address1;
	private String address2;
	private String address3;
	private String address4;
	private String zipnum1;
	private String zipnum2;
	private String zipnum3;
	private String zipnum4;
	private int admin;
	private int emailchk;
	private String salt;// 비밀번호 암호화
	private int outyn; // 탈퇴여부 1:탈퇴된 상태, 0:가입된 상태

	// 기업고객
	private String rep_name; // 대표명
	private String rep_num; // 사업자등록번호
	private String rep_phone; // 대표번호
	private String company; // 판매자명
	private String fullCompany; // 상호명
	private String companyAddress; // 사업장 소재지 주소
	private String companyZipnum; // 사업장 우편번호
	
	//기업고객 - 배송관리
	private int delivery; // 1: 퀵 10:택배
	private int delivery_way; // 1: 선불, 10:후불
	private String delivery_costway; // 무료, 유료, 조건부 무료
	private int delivery_cost; // 비용 -유료/조건부무료
	private int delivery_cost2; // 비용 -유료/조건부무료
	private int rep_num_flag;
	private int re_delivery_cost1; // 편도택배비
	private String deliveryCompany; // 판매자 지정택배사
	private String deliveryCompany_quick; //판매자 지정퀵
	private int delivery_period;		//예상되는 배송 기간
	private String returnZipnum;	//교환반품 우편번호
	private String returnRoadAddrPart1;	//교환반품 주소
	private String returnAddrDetail;	//교환반품 상세주소

	//정산관리 - 계좌
	private String bank_name;		//은행명
	private String account_number;	//계좌번호
	private String account_holder;	//예금주명

	private String ipAddress;

	
	
	
	public String getReturnZipnum() {
		return returnZipnum;
	}

	public void setReturnZipnum(String returnZipnum) {
		this.returnZipnum = returnZipnum;
	}

	public String getReturnRoadAddrPart1() {
		return returnRoadAddrPart1;
	}

	public void setReturnRoadAddrPart1(String returnRoadAddrPart1) {
		this.returnRoadAddrPart1 = returnRoadAddrPart1;
	}

	public String getReturnAddrDetail() {
		return returnAddrDetail;
	}

	public void setReturnAddrDetail(String returnAddrDetail) {
		this.returnAddrDetail = returnAddrDetail;
	}

	public String getDeliveryCompany_quick() {
		return deliveryCompany_quick;
	}

	public void setDeliveryCompany_quick(String deliveryCompany_quick) {
		this.deliveryCompany_quick = deliveryCompany_quick;
	}

	public int getDelivery_period() {
		return delivery_period;
	}

	public void setDelivery_period(int delivery_period) {
		this.delivery_period = delivery_period;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public String getBank_name() {
		return bank_name;
	}

	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}

	public String getAccount_number() {
		return account_number;
	}

	public void setAccount_number(String account_number) {
		this.account_number = account_number;
	}

	public String getAccount_holder() {
		return account_holder;
	}

	public void setAccount_holder(String account_holder) {
		this.account_holder = account_holder;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public int getDelivery_cost2() {
		return delivery_cost2;
	}

	public void setDelivery_cost2(int delivery_cost2) {
		this.delivery_cost2 = delivery_cost2;
	}

	public int getRep_num_flag() {
		return rep_num_flag;
	}

	public void setRep_num_flag(int rep_num_flag) {
		this.rep_num_flag = rep_num_flag;
	}

	public int getRe_delivery_cost1() {
		return re_delivery_cost1;
	}

	public void setRe_delivery_cost1(int re_delivery_cost1) {
		this.re_delivery_cost1 = re_delivery_cost1;
	}

	public String getDeliveryCompany() {
		return deliveryCompany;
	}

	public void setDeliveryCompany(String deliveryCompany) {
		this.deliveryCompany = deliveryCompany;
	}

	public int getDelivery() {
		return delivery;
	}

	public void setDelivery(int delivery) {
		this.delivery = delivery;
	}

	public int getDelivery_way() {
		return delivery_way;
	}

	public void setDelivery_way(int delivery_way) {
		this.delivery_way = delivery_way;
	}

	public String getDelivery_costway() {
		return delivery_costway;
	}

	public void setDelivery_costway(String delivery_costway) {
		this.delivery_costway = delivery_costway;
	}

	public int getDelivery_cost() {
		return delivery_cost;
	}

	public void setDelivery_cost(int delivery_cost) {
		this.delivery_cost = delivery_cost;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getCompanyZipnum() {
		return companyZipnum;
	}

	public void setCompanyZipnum(String companyZipnum) {
		this.companyZipnum = companyZipnum;
	}

	public String getFullCompany() {
		return fullCompany;
	}

	public void setFullCompany(String fullCompany) {
		this.fullCompany = fullCompany;
	}

	public int getEmailchk() {
		return emailchk;
	}

	public void setEmailchk(int emailchk) {
		this.emailchk = emailchk;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNation() {
		return nation;
	}

	public void setNation(int nation) {
		this.nation = nation;
	}

	public int getAdmin() {
		return admin;
	}

	public void setAdmin(int admin) {
		this.admin = admin;
	}

	public String getRep_name() {
		return rep_name;
	}

	public void setRep_name(String rep_name) {
		this.rep_name = rep_name;
	}

	public String getRep_num() {
		return rep_num;
	}

	public void setRep_num(String rep_num) {
		this.rep_num = rep_num;
	}

	public int getFax() {
		return fax;
	}

	public String getRep_phone() {
		return rep_phone;
	}

	public void setRep_phone(String rep_phone) {
		this.rep_phone = rep_phone;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public void setFax(int fax) {
		this.fax = fax;
	}

	private int fax;

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getAddress4() {
		return address4;
	}

	public void setAddress4(String address4) {
		this.address4 = address4;
	}

	public String getZipnum1() {
		return zipnum1;
	}

	public void setZipnum1(String zipnum1) {
		this.zipnum1 = zipnum1;
	}

	public String getZipnum2() {
		return zipnum2;
	}

	public void setZipnum2(String zipnum2) {
		this.zipnum2 = zipnum2;
	}

	public String getZipnum3() {
		return zipnum3;
	}

	public void setZipnum3(String zipnum3) {
		this.zipnum3 = zipnum3;
	}

	public String getZipnum4() {
		return zipnum4;
	}

	public void setZipnum4(String zipnum4) {
		this.zipnum4 = zipnum4;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public int getOutyn() {
		return outyn;
	}

	public void setOutyn(int outyn) {
		this.outyn = outyn;
	}
}
