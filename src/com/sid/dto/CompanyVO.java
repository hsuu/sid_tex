package com.sid.dto;

public class CompanyVO {
	private String email;
	private String company; //판매자명
	private int delivery; //1: 퀵 10:택배
	private int delivery_way; //1: 선불, 10:후불
	private String delivery_costway; //무료, 유료, 조건부 무료
	private int delivery_cost;  //비용 -유료/조건부무료
	private int delivery_cost2;  //비용 -유료/조건부무료
	private int rep_num_flag;
	private int re_delivery_cost1;	//왕복택배비
	private String deliveryCompany;	//판매자 지정택배사
	private int totalCost;
	
	
	public int getDelivery_cost2() {
		return delivery_cost2;
	}
	public void setDelivery_cost2(int delivery_cost2) {
		this.delivery_cost2 = delivery_cost2;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public int getDelivery() {
		return delivery;
	}
	public void setDelivery(int delivery) {
		this.delivery = delivery;
	}
	public int getDelivery_way() {
		return delivery_way;
	}
	public void setDelivery_way(int delivery_way) {
		this.delivery_way = delivery_way;
	}
	public String getDelivery_costway() {
		return delivery_costway;
	}
	public void setDelivery_costway(String delivery_costway) {
		this.delivery_costway = delivery_costway;
	}
	public int getDelivery_cost() {
		return delivery_cost;
	}
	public void setDelivery_cost(int delivery_cost) {
		this.delivery_cost = delivery_cost;
	}
	public int getRep_num_flag() {
		return rep_num_flag;
	}
	public void setRep_num_flag(int rep_num_flag) {
		this.rep_num_flag = rep_num_flag;
	}
	public int getRe_delivery_cost1() {
		return re_delivery_cost1;
	}
	public void setRe_delivery_cost1(int re_delivery_cost1) {
		this.re_delivery_cost1 = re_delivery_cost1;
	}
	public String getDeliveryCompany() {
		return deliveryCompany;
	}
	public void setDeliveryCompany(String deliveryCompany) {
		this.deliveryCompany = deliveryCompany;
	}
	public int getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(int totalCost) {
		this.totalCost = totalCost;
	}
	
}
