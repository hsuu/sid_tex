package com.sid.dto;

public class ConsultVO {

	private int consultNum;
	private String email;
	private String content;
	private String postdate;
	private int reply;
	public int getConsultNum() {
		return consultNum;
	}
	public void setConsultNum(int consultNum) {
		this.consultNum = consultNum;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getPostdate() {
		return postdate;
	}
	public void setPostdate(String postdate) {
		this.postdate = postdate;
	}
	public int getReply() {
		return reply;
	}
	public void setReply(int reply) {
		this.reply = reply;
	} 

	
}

