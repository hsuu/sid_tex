package com.sid.dto;

import java.util.Date;

public class CancelOrderVO {
	private String purchaseId;
	private String reason;
	private Date date;
	private String canceller;
	
	
	public String getCanceller() {
		return canceller;
	}
	public void setCanceller(String canceller) {
		this.canceller = canceller;
	}
	public String getPurchaseId() {
		return purchaseId;
	}
	public void setPurchaseId(String purchaseId) {
		this.purchaseId = purchaseId;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	
}
