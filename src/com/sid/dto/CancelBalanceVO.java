package com.sid.dto;

import java.util.Date;

public class CancelBalanceVO {//취소 정산

	private int state;	//정산상태 0==취소정산대기 1==취소정산완료
	private Date appldate; //주문일
	private Date date;//취소일
	private String company; //판매자명
	private String buyerEmail; //구매자이메일
	private int totalCost;//정산금액
 	private int cost; //상품 가격
 	private int price; //결제 금액
 	private int delivery_cost;
	private String oid; //결제번호
	private String canceller;//취소자
	private String cancelWay; //취소형태 (부분취소,전체취소);
	
	
	public int getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(int totalCost) {
		this.totalCost = totalCost;
	}
	public int getDelivery_cost() {
		return delivery_cost;
	}
	public void setDelivery_cost(int delivery_cost) {
		this.delivery_cost = delivery_cost;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public Date getAppldate() {
		return appldate;
	}
	public void setAppldate(Date appldate) {
		this.appldate = appldate;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getBuyerEmail() {
		return buyerEmail;
	}
	public void setBuyerEmail(String buyerEmail) {
		this.buyerEmail = buyerEmail;
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getOid() {
		return oid;
	}
	public void setOid(String oid) {
		this.oid = oid;
	}
	public String getCanceller() {
		return canceller;
	}
	public void setCanceller(String canceller) {
		this.canceller = canceller;
	}
	public String getCancelWay() {
		return cancelWay;
	}
	public void setCancelWay(String cancelWay) {
		this.cancelWay = cancelWay;
	}
	
	
	
}