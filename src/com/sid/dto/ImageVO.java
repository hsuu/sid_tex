package com.sid.dto;

public class ImageVO {
	
	private String mainImg;
	private String subImg;
	private String subImg2;
	private String subImg3;
	private String subImg4;
	private String subImg5;
	private int stock;
	private String colorName;
	private int mainyn;
	
	
	public int getMainyn() {
		return mainyn;
	}
	public void setMainyn(int mainyn) {
		this.mainyn = mainyn;
	}
	public String getMainImg() {
		return mainImg;
	}
	public void setMainImg(String mainImg) {
		this.mainImg = mainImg;
	}
	public String getSubImg() {
		return subImg;
	}
	public void setSubImg(String subImg) {
		this.subImg = subImg;
	}
	public String getSubImg2() {
		return subImg2;
	}
	public void setSubImg2(String subImg2) {
		this.subImg2 = subImg2;
	}
	public String getSubImg3() {
		return subImg3;
	}
	public void setSubImg3(String subImg3) {
		this.subImg3 = subImg3;
	}
	public String getSubImg4() {
		return subImg4;
	}
	public void setSubImg4(String subImg4) {
		this.subImg4 = subImg4;
	}
	public String getSubImg5() {
		return subImg5;
	}
	public void setSubImg5(String subImg5) {
		this.subImg5 = subImg5;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public String getColorName() {
		return colorName;
	}
	public void setColorName(String colorName) {
		this.colorName = colorName;
	}
	
	

}
