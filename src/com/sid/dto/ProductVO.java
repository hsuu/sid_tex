package com.sid.dto;

public class ProductVO {

	private int productID;		
	
	private String state;		//판매상태 default: 판매중, 판매중지 
	private String postDate;	//등록일
	private String email;		//판매자 이메일
	private String company;		//회사명
	private int delyn;			//1 - 판매자가 상품 삭제 할 경우, 0 - 디폴트
	
	//필수 정보
	private String itemName;	//상품명
	private String colorName;	//색상명
	private int retail;			//소매가
	private int retail_only;	//소매가-기업고객만 판매-1:기업고객만 판매(체크), 0:모든 고객 판매(체크해제)
	private int wholesale;		//도매가
	private int standard;		//도매 기준
	private String swatch;		//스와치 제공
	private int stock;			//재고량
	private int stock_unknown;  //재고량 알수 없음 - 1:재고량 알수 없음(체크), 0: 재고량 표시(체크해제)
//	private int express;		//배송 1퀵 10택배
	private int width;		//폭
	private String widthUnit;	//폭 직접입력시 단위
	private String thick;		//두께
	private String mainImg;		//메인이미지
	private String subImg; 
	private String subImg2;
	private String subImg3;
	private String subImg4;		
	private String subImg5;		
	private String expl; 		//설명
	
	//연관상품
	private String relate;
	//메인상품유무
	private int mainyn;
	
	private String origin;		//원산지
	private String[] uses;		//용도
	private String manufacturer;//제조사
	private String pattern;		//패턴
	private String manuDate;	//제조년월
	private String color;		//색상
	private String sortName;	//명칭
	private String[] fiber;		//섬유
	
	//상세 정보 - 피륙
	private int weight;			//중량
	private String weave;		//조직
	private int density_weight;	//밀도-질량
	private int density_volume;	//밀도-부피
	
	//상세 정보 - 원단
	private int thickWp;		//굵기-경사
	private int thickWt;		//굵기-위사
	private String thickUnit;	//굵기-단위
	private String lengthWp;	//길이-경사
	private String lengthWt;	//길이-위사
	private String twistWp;		//꼬임-경사
	private String twistWt;		//꼬임-위사
	
	//상세 정보 - 가공
	private String gagong;		//가공
	private String dye;			//염색
	
	//상세 정보 - 심미
	private String texture;		//질감
	private String through;		//비침
	private String gloss;		//광택
	private String elastic;		//신축
	private String pliability;	//유연
	
	
	
	
	public int getDelyn() {
		return delyn;
	}
	public void setDelyn(int delyn) {
		this.delyn = delyn;
	}
	public int getMainyn() {
		return mainyn;
	}
	public void setMainyn(int mainyn) {
		this.mainyn = mainyn;
	}
	public void setWidth(int d) {
		this.width = d;
	}
	
	public int getWidth() {
		return width;
	}
	public int getProductID() {
		return productID;
	}
	public void setProductID(int productID) {
		this.productID = productID;
	}
	public String getMainImg() {
		return mainImg;
	}
	public void setMainImg(String mainImg) {
		this.mainImg = mainImg;
	}
//	public int getExpress() {
//		return express;
//	}
//	public void setExpress(int express) {
//		this.express = express;
//	}
	public String getSubImg() {
		return subImg;
	}
	public void setSubImg(String subImg) {
		this.subImg = subImg;
	}
	public String getSubImg2() {
		return subImg2;
	}
	public void setSubImg2(String subImg2) {
		this.subImg2 = subImg2;
	}
	public String getSubImg3() {
		return subImg3;
	}
	public void setSubImg3(String subImg3) {
		this.subImg3 = subImg3;
	}
	public String getSubImg4() {
		return subImg4;
	}
	public void setSubImg4(String subImg4) {
		this.subImg4 = subImg4;
	}
	public String getSubImg5() {
		return subImg5;
	}
	public void setSubImg5(String subImg5) {
		this.subImg5 = subImg5;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getExpl() {
		return expl;
	}
	public void setExpl(String expl) {
		this.expl = expl;
	}
	public int getRetail() {
		return retail;
	}
	public void setRetail(int retail) {
		this.retail = retail;
	}
	public int getWholesale() {
		return wholesale;
	}
	public void setWholesale(int wholesale) {
		this.wholesale = wholesale;
	}
	public int getStandard() {
		return standard;
	}
	public void setStandard(int standard) {
		this.standard = standard;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getColorName() {
		return colorName;
	}
	public void setColorName(String colorName) {
		this.colorName = colorName;
	}
	
	public String getSwatch() {
		return swatch;
	}
	public void setSwatch(String swatch) {
		this.swatch = swatch;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getManuDate() {
		return manuDate;
	}
	public void setManuDate(String manuDate) {
		this.manuDate = manuDate;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String[] getUses() {
		return uses;
	}
	public void setUses(String[]  uses) {
		this.uses = uses;
	}
	public String getSortName() {
		return sortName;
	}
	public void setSortName(String sortName) {
		this.sortName = sortName;
	}
	public String[] getFiber() {
		return fiber;
	}
	public void setFiber(String[] fiber) {
		this.fiber = fiber;
	}
	public String getPattern() {
		return pattern;
	}
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getThick() {
		return thick;
	}
	public void setThick(String thick) {
		this.thick = thick;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getWeave() {
		return weave;
	}
	public void setWeave(String weave) {
		this.weave = weave;
	}
	public int getThickWp() {
		return thickWp;
	}
	public void setThickWp(int thickWp) {
		this.thickWp = thickWp;
	}
	public int getThickWt() {
		return thickWt;
	}
	public void setThickWt(int thickWt) {
		this.thickWt = thickWt;
	}
	
	public String getLengthWp() {
		return lengthWp;
	}
	public void setLengthWp(String lengthWp) {
		this.lengthWp = lengthWp;
	}
	public String getLengthWt() {
		return lengthWt;
	}
	public void setLengthWt(String lengthWt) {
		this.lengthWt = lengthWt;
	}
	public String getTwistWp() {
		return twistWp;
	}
	public void setTwistWp(String twistWp) {
		this.twistWp = twistWp;
	}
	public String getTwistWt() {
		return twistWt;
	}
	public void setTwistWt(String twistWt) {
		this.twistWt = twistWt;
	}
	public String getGagong() {
		return gagong;
	}
	public void setGagong(String gagong) {
		this.gagong = gagong;
	}
	public String getDye() {
		return dye;
	}
	public void setDye(String dye) {
		this.dye = dye;
	}
	public String getTexture() {
		return texture;
	}
	public void setTexture(String texture) {
		this.texture = texture;
	}
	public String getThrough() {
		return through;
	}
	public void setThrough(String through) {
		this.through = through;
	}
	public String getGloss() {
		return gloss;
	}
	public void setGloss(String gloss) {
		this.gloss = gloss;
	}
	public String getElastic() {
		return elastic;
	}
	public void setElastic(String elastic) {
		this.elastic = elastic;
	}
	public String getPliability() {
		return pliability;
	}
	public void setPliability(String pliability) {
		this.pliability = pliability;
	}
	public String getPostDate() {
		return postDate;
	}
	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public int getRetail_only() {
		return retail_only;
	}
	public void setRetail_only(int retail_only) {
		this.retail_only = retail_only;
	}
	public int getDensity_weight() {
		return density_weight;
	}
	public void setDensity_weight(int density_weight) {
		this.density_weight = density_weight;
	}
	public int getDensity_volume() {
		return density_volume;
	}
	public void setDensity_volume(int density_volume) {
		this.density_volume = density_volume;
	}
	public String getThickUnit() {
		return thickUnit;
	}
	public void setThickUnit(String thickUnit) {
		this.thickUnit = thickUnit;
	}
	public String getWidthUnit() {
		return widthUnit;
	}
	public void setWidthUnit(String widthUnit) {
		this.widthUnit = widthUnit;
	}
	public String getRelate() {
		return relate;
	}
	public void setRelate(String relate) {
		this.relate = relate;
	}
	
	public int getStock_unknown() {
		return stock_unknown;
	}
	public void setStock_unknown(int stock_unknown) {
		this.stock_unknown = stock_unknown;
	}
	
	
}
