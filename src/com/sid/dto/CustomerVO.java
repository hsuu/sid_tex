package com.sid.dto;

public class CustomerVO {

	private String sellerEmail;
	private String buyerEmail;
	private String buyerName;
	private String company;
	private String buyerPhone;
	private int transaction_count;
	private int transaction_price;
	public String getSellerEmail() {
		return sellerEmail;
	}
	public void setSellerEmail(String sellerEmail) {
		this.sellerEmail = sellerEmail;
	}
	public String getBuyerEmail() {
		return buyerEmail;
	}
	public void setBuyerEmail(String buyerEmail) {
		this.buyerEmail = buyerEmail;
	}
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getBuyerPhone() {
		return buyerPhone;
	}
	public void setBuyerPhone(String buyerPhone) {
		this.buyerPhone = buyerPhone;
	}
	public int getTransaction_count() {
		return transaction_count;
	}
	public void setTransaction_count(int transaction_count) {
		this.transaction_count = transaction_count;
	}
	public int getTransaction_price() {
		return transaction_price;
	}
	public void setTransaction_price(int transaction_price) {
		this.transaction_price = transaction_price;
	}

	
}
