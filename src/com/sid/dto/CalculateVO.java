package com.sid.dto;

public class CalculateVO {

	private String purchaseConfirm_date;	//구매확정일
	private String calculate_date;			//정산완료일
	private int number_of_orders;			//주문건수
	private String purchaseId;				//주문번호
	private double calculate_amount;		//정산금액(A-B-C-D)
	private double payment_amount;			//결제금액(A)
	private double surtax;					//부가세(B)
	private double fee;						//수수료(C)
	private double brokerage_fee;			//중개료(D)
	
	private int calculate_amount2;
	private int payment_amount2;
	private int surtax2;
	private int fee2;
	private int brokerage_fee2;
	
	
	
	public String getPurchaseId() {
		return purchaseId;
	}
	public void setPurchaseId(String purchaseId) {
		this.purchaseId = purchaseId;
	}
	public int getCalculate_amount2() {
		return calculate_amount2;
	}
	public void setCalculate_amount2(int calculate_amount2) {
		this.calculate_amount2 = calculate_amount2;
	}
	public int getPayment_amount2() {
		return payment_amount2;
	}
	public void setPayment_amount2(int payment_amount2) {
		this.payment_amount2 = payment_amount2;
	}
	public int getSurtax2() {
		return surtax2;
	}
	public void setSurtax2(int surtax2) {
		this.surtax2 = surtax2;
	}
	public int getFee2() {
		return fee2;
	}
	public void setFee2(int fee2) {
		this.fee2 = fee2;
	}
	public int getBrokerage_fee2() {
		return brokerage_fee2;
	}
	public void setBrokerage_fee2(int brokerage_fee2) {
		this.brokerage_fee2 = brokerage_fee2;
	}
	public String getPurchaseConfirm_date() {
		return purchaseConfirm_date;
	}
	public void setPurchaseConfirm_date(String purchaseConfirm_date) {
		this.purchaseConfirm_date = purchaseConfirm_date;
	}
	public String getCalculate_date() {
		return calculate_date;
	}
	public void setCalculate_date(String calculate_date) {
		this.calculate_date = calculate_date;
	}
	public int getNumber_of_orders() {
		return number_of_orders;
	}
	public void setNumber_of_orders(int number_of_orders) {
		this.number_of_orders = number_of_orders;
	}
	public double getCalculate_amount() {
		return calculate_amount;
	}
	public void setCalculate_amount(double calculate_amount) {
		this.calculate_amount = calculate_amount;
	}
	public double getPayment_amount() {
		return payment_amount;
	}
	public void setPayment_amount(double payment_amount) {
		this.payment_amount = payment_amount;
	}
	public double getSurtax() {
		return surtax;
	}
	public void setSurtax(double surtax) {
		this.surtax = surtax;
	}
	public double getFee() {
		return fee;
	}
	public void setFee(double fee) {
		this.fee = fee;
	}
	public double getBrokerage_fee() {
		return brokerage_fee;
	}
	public void setBrokerage_fee(double brokerage_fee) {
		this.brokerage_fee = brokerage_fee;
	}
	
	
}