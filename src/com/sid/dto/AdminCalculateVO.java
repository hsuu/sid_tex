package com.sid.dto;

public class AdminCalculateVO {
	private int state;
	private String purchaseConfirm_date; // 상태 0 - 정산대기 1 - 정산완료
	private String company; // 구매확정일
	private String fullCompany; // 상호명
	private int calculate_amount;// 정산금액
	private String bank_name; // 은행명
	private String account_holder; // 예금주명
	private String account_number; // 계좌번호
	private String email;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getPurchaseConfirm_date() {
		return purchaseConfirm_date;
	}

	public void setPurchaseConfirm_date(String purchaseConfirm_date) {
		this.purchaseConfirm_date = purchaseConfirm_date;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getFullCompany() {
		return fullCompany;
	}

	public void setFullCompany(String fullCompany) {
		this.fullCompany = fullCompany;
	}


	public int getCalculate_amount() {
		return calculate_amount;
	}

	public void setCalculate_amount(int calculate_amount) {
		this.calculate_amount = calculate_amount;
	}

	public String getBank_name() {
		return bank_name;
	}

	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}

	public String getAccount_holder() {
		return account_holder;
	}

	public void setAccount_holder(String account_holder) {
		this.account_holder = account_holder;
	}

	public String getAccount_number() {
		return account_number;
	}

	public void setAccount_number(String account_number) {
		this.account_number = account_number;
	}

}
