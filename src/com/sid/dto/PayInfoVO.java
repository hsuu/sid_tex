package com.sid.dto;

public class PayInfoVO {

	private String tid;				//거래번호
	private int price;				//결제완료금액
	private String moid;			//주문번호
	private String applDate;		//승인날짜
	private String applTime;		//승인시간
	private String payMethod;		//결제방법(지불수단)
	
	//가상계좌
	private String vact_num;		//입금 계좌번호
	private String vact_bankCode;	//입금 은행코드
	private String vact_bankName;	//입금 은행명
	private String vact_name;		//예금주명
	private String vact_inputName;	//송금주명
	private String vact_date;		//송금일자
	private String vact_time;		//송금시간
	
	//실시간 계좌이체
	private String acct_bankCode;	//은행코드
	private String cshr_resultCode;	//현금영수증 발급결과코드
	private String cshr_type;		//현금영수증 발급구분코드
	
	//뱅크월렛 카카오
	private String kwpy_cellPhone;	//휴대폰번호
	private String kwpy_salesAmount;//거래금액
	private String kwpy_amount;		//공급가액
	private String kwpy_tax;		//부가세
	private String kwpy_serviceFee;	//봉사료
	private String kwpy_payMethod;	//결제방법
	private String kwpy_resultCode;	//결과코드
	private String kwpy_resultMsg;	//결과내용
	private String kwpy_tid;		//거래번호
	private String kwpy_moid;		//주문번호
	private String kwpy_price;		//결제완료금액
	private String kwpy_applDate;	//사용일자
	private String kwpy_applTime;	//사용시간
	
	//카드
	private String eventCode;		//이벤트코드
	private String card_num;		//카드번호
	private String card_applNum;	//승인번호
	private String card_quota;		//할부기간
	private String card_interest;	//할부유형
	private String card_point;		//포인트사용여부
	private String card_code;		//카드 종류
	private String card_bankCode;	//카드 발급사
	
	private String ocb_num;			//OK CASHBAG 카드번호
	private String ocb_saveApplNum;	//OK CASHBAG 적립 승인번호
	private String ocb_payPrice;	//OK CASHBAG 포인트지불금액
	
	private String gspt_num;		//GS&Point 카드번호
	private String gspt_remains;	//GS&Point 잔여한도
	private String gspt_applPrice;	//GS&Point 승인금액
	
	private String unpt_cardNum;	//U-Point 카드번호
	private String unpt_usablePoint;//U-Point 가용포인트
	private String unpt_payPrice;	//U-Point 포인트지불금액
	
	private String oid;				//purchase 테이블과 조인시 필요
	private String email;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getOid() {
		return oid;
	}
	public void setOid(String oid) {
		this.oid = oid;
	}
	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getMoid() {
		return moid;
	}
	public void setMoid(String moid) {
		this.moid = moid;
	}
	public String getApplDate() {
		return applDate;
	}
	public void setApplDate(String applDate) {
		this.applDate = applDate;
	}
	public String getApplTime() {
		return applTime;
	}
	public void setApplTime(String applTime) {
		this.applTime = applTime;
	}
	public String getPayMethod() {
		return payMethod;
	}
	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}
	public String getVact_num() {
		return vact_num;
	}
	public void setVact_num(String vact_num) {
		this.vact_num = vact_num;
	}
	public String getVact_bankCode() {
		return vact_bankCode;
	}
	public void setVact_bankCode(String vact_bankCode) {
		this.vact_bankCode = vact_bankCode;
	}
	public String getVact_bankName() {
		return vact_bankName;
	}
	public void setVact_bankName(String vact_bankName) {
		this.vact_bankName = vact_bankName;
	}
	public String getVact_name() {
		return vact_name;
	}
	public void setVact_name(String vact_name) {
		this.vact_name = vact_name;
	}
	public String getVact_inputName() {
		return vact_inputName;
	}
	public void setVact_inputName(String vact_inputName) {
		this.vact_inputName = vact_inputName;
	}
	public String getVact_date() {
		return vact_date;
	}
	public void setVact_date(String vact_date) {
		this.vact_date = vact_date;
	}
	public String getVact_time() {
		return vact_time;
	}
	public void setVact_time(String vact_time) {
		this.vact_time = vact_time;
	}
	public String getAcct_bankCode() {
		return acct_bankCode;
	}
	public void setAcct_bankCode(String acct_bankCode) {
		this.acct_bankCode = acct_bankCode;
	}
	public String getCshr_resultCode() {
		return cshr_resultCode;
	}
	public void setCshr_resultCode(String cshr_resultCode) {
		this.cshr_resultCode = cshr_resultCode;
	}
	public String getCshr_type() {
		return cshr_type;
	}
	public void setCshr_type(String cshr_type) {
		this.cshr_type = cshr_type;
	}
	public String getKwpy_cellPhone() {
		return kwpy_cellPhone;
	}
	public void setKwpy_cellPhone(String kwpy_cellPhone) {
		this.kwpy_cellPhone = kwpy_cellPhone;
	}
	public String getKwpy_salesAmount() {
		return kwpy_salesAmount;
	}
	public void setKwpy_salesAmount(String kwpy_salesAmount) {
		this.kwpy_salesAmount = kwpy_salesAmount;
	}
	public String getKwpy_amount() {
		return kwpy_amount;
	}
	public void setKwpy_amount(String kwpy_amount) {
		this.kwpy_amount = kwpy_amount;
	}
	public String getKwpy_tax() {
		return kwpy_tax;
	}
	public void setKwpy_tax(String kwpy_tax) {
		this.kwpy_tax = kwpy_tax;
	}
	public String getKwpy_serviceFee() {
		return kwpy_serviceFee;
	}
	public void setKwpy_serviceFee(String kwpy_serviceFee) {
		this.kwpy_serviceFee = kwpy_serviceFee;
	}
	public String getKwpy_payMethod() {
		return kwpy_payMethod;
	}
	public void setKwpy_payMethod(String kwpy_payMethod) {
		this.kwpy_payMethod = kwpy_payMethod;
	}
	public String getKwpy_resultCode() {
		return kwpy_resultCode;
	}
	public void setKwpy_resultCode(String kwpy_resultCode) {
		this.kwpy_resultCode = kwpy_resultCode;
	}
	public String getKwpy_resultMsg() {
		return kwpy_resultMsg;
	}
	public void setKwpy_resultMsg(String kwpy_resultMsg) {
		this.kwpy_resultMsg = kwpy_resultMsg;
	}
	public String getKwpy_tid() {
		return kwpy_tid;
	}
	public void setKwpy_tid(String kwpy_tid) {
		this.kwpy_tid = kwpy_tid;
	}
	public String getKwpy_moid() {
		return kwpy_moid;
	}
	public void setKwpy_moid(String kwpy_moid) {
		this.kwpy_moid = kwpy_moid;
	}
	public String getKwpy_price() {
		return kwpy_price;
	}
	public void setKwpy_price(String kwpy_price) {
		this.kwpy_price = kwpy_price;
	}
	public String getKwpy_applDate() {
		return kwpy_applDate;
	}
	public void setKwpy_applDate(String kwpy_applDate) {
		this.kwpy_applDate = kwpy_applDate;
	}
	public String getKwpy_applTime() {
		return kwpy_applTime;
	}
	public void setKwpy_applTime(String kwpy_applTime) {
		this.kwpy_applTime = kwpy_applTime;
	}
	public String getEventCode() {
		return eventCode;
	}
	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}
	public String getCard_num() {
		return card_num;
	}
	public void setCard_num(String card_num) {
		this.card_num = card_num;
	}
	public String getCard_applNum() {
		return card_applNum;
	}
	public void setCard_applNum(String card_applNum) {
		this.card_applNum = card_applNum;
	}
	public String getCard_quota() {
		return card_quota;
	}
	public void setCard_quota(String card_quota) {
		this.card_quota = card_quota;
	}
	public String getCard_interest() {
		return card_interest;
	}
	public void setCard_interest(String card_interest) {
		this.card_interest = card_interest;
	}
	public String getCard_point() {
		return card_point;
	}
	public void setCard_point(String card_point) {
		this.card_point = card_point;
	}
	public String getCard_code() {
		return card_code;
	}
	public void setCard_code(String card_code) {
		this.card_code = card_code;
	}
	public String getCard_bankCode() {
		return card_bankCode;
	}
	public void setCard_bankCode(String card_bankCode) {
		this.card_bankCode = card_bankCode;
	}
	public String getOcb_num() {
		return ocb_num;
	}
	public void setOcb_num(String ocb_num) {
		this.ocb_num = ocb_num;
	}
	public String getOcb_saveApplNum() {
		return ocb_saveApplNum;
	}
	public void setOcb_saveApplNum(String ocb_saveApplNum) {
		this.ocb_saveApplNum = ocb_saveApplNum;
	}
	public String getOcb_payPrice() {
		return ocb_payPrice;
	}
	public void setOcb_payPrice(String ocb_payPrice) {
		this.ocb_payPrice = ocb_payPrice;
	}
	public String getGspt_num() {
		return gspt_num;
	}
	public void setGspt_num(String gspt_num) {
		this.gspt_num = gspt_num;
	}
	public String getGspt_remains() {
		return gspt_remains;
	}
	public void setGspt_remains(String gspt_remains) {
		this.gspt_remains = gspt_remains;
	}
	public String getGspt_applPrice() {
		return gspt_applPrice;
	}
	public void setGspt_applPrice(String gspt_applPrice) {
		this.gspt_applPrice = gspt_applPrice;
	}
	public String getUnpt_cardNum() {
		return unpt_cardNum;
	}
	public void setUnpt_cardNum(String unpt_cardNum) {
		this.unpt_cardNum = unpt_cardNum;
	}
	public String getUnpt_usablePoint() {
		return unpt_usablePoint;
	}
	public void setUnpt_usablePoint(String unpt_usablePoint) {
		this.unpt_usablePoint = unpt_usablePoint;
	}
	public String getUnpt_payPrice() {
		return unpt_payPrice;
	}
	public void setUnpt_payPrice(String unpt_payPrice) {
		this.unpt_payPrice = unpt_payPrice;
	}
	
	

	
}
