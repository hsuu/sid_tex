<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ include file="../include/header.jsp"%>
<!DOCTYPE html>
<html>
<head>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<style>
.table th, td {
	text-align: center;
}

.table td {
	font-size: 10pt;
}
.newMessage{
	color:red;
}
#unread_badge {
	background-color: #ff9999;
}
</style>
</head>
<body>
	<div class="col-md-3">
		<ul class="nav nav-pills nav-stacked">
			<li role="presentation"><a href="../SidServlet?command=list_unread_alarm">안읽은쪽지함<span class="badge" id="unread_badge">
			${count}</span></a></li>
			<li class="active" role="presentation"><a href="../SidServlet?command=list_read_alarm">읽은 쪽지함</a></li>
		</ul>
	</div>

	<div class="col-md-9">
		<br>
		<table class="table table-striped">
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				
				
				<c:forEach items="${list}" var="list">
					<tr>
						<td class="2">${list.sort}</td>
						<td class="3">${list.message}&nbsp;<c:if test="${list.sort == '주문취소'||list.sort=='주문취소 요청'}"><button type="button" class="btn btn-primary" data-container="body" data-toggle="popover" data-placement="right" data-content="<p>취소사유</p>${list.reason}">
						?</button></c:if></td>
						<td class="4">${list.date}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>

<script>
$(function () {
	  $('[data-toggle="popover"]').popover({
		  html : true
	  })
	})
</script>
</html>
<%@ include file="../include/footer.jsp"%>
