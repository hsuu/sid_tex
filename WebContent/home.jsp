<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../include/header.jsp"%>
<%@ page import="com.sid.dao.ProductDAO"%>
<%@ page import="com.sid.dto.ProductVO"%>
<%@ page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>

<html>
<style>
#thick{
font-family: 'Hanna', serif;
}
#ma{
font-family: 'Hanna', serif;
font-size:80%;
}
.text-center{
font-family: 'Hanna', serif;
}
td a:hover {
	text-decoration: none;
}

td a:focus {
	border: 2px solid black;
	text-decoration: none;
}

#more_1 {
	float: right;
}

h4 {
	margin: 30px;
	text-align: center;
}

#logo {
	width: 40%;
	padding: 15px
}

#logo {
	width: 40%;
	padding: 15px
}

.cog {
	padding-left: 0px;
	margin: 0px;
}

.input-group-btn {
	padding-right: 0px;
	margin: 0px;
}

</style>

<!--색상 스타일시트 -->

<link rel="stylesheet" type="text/css" href="../css/color.css">
<body>
	<div class="container-fluid">
		<form method="post" name="frm">
			<div class="row">
				<div class="col-xs-6 col-xs-offset-3">
					<div class="input-group">
						<div class="input-group-btn search-panel" id="keyword">
							<button type="button" class="dropdown-toggle btn btn-primary"
								data-toggle="dropdown">
								<span id="search_concept">전체</span> <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="#company">회사명</a></li>
								<li><a href="#itemName">상품명</a></li>
								<li><a href="#colorName">색상명</a></li>
								<li class="divider"></li>
								<li><a href="#all">전체</a></li>
							</ul>
						</div>

						<input type="hidden" name="search_param" value="all"
							id="search_param"> <input type="text"
							class="form-control" id="searchValue" name="value"
							placeholder="Search"> <span class="input-group-btn">
							<button class="btn btn-default" type="button"
								onclick="return checkSearch()">
								<span class="glyphicon glyphicon-search"></span>
							</button>
						</span>
					</div>
				</div>
			</div>
						<div class="cog">
							<button type="button" class="btn btn-primary" id="more_1">
								<i class="fa fa-cog"></i>
							</button>

						</div>
		</form>
	</div>
	<br>
	<div class="inner cover">
		<div class="col-md-12">
			<div id="basic_search">
				<form>
					<table style="margin-bottom: 0" class="table" id="table_1">
						<tr>
							<td style="width: 18px;">명칭</td>
							<td style="width: 80px;">
								<ul style="overflow-y: auto; list-style: none; height: 120px;">
									<li><a data-toggle="pill" href="#0">우븐패브릭</a></li>
									<li><a data-toggle="pill" href="#1">팬시패브릭</a></li>
									<li><a data-toggle="pill" href="#2">기타패브릭</a></li>
									<li><a data-toggle="pill" href="#3">편물</a></li>
								</ul>
							</td>
							<td style="width: 140px;">
								<div class="tab-content">
									<div id="0" class="tab-pane fade in active">
										<ul style="overflow-y: auto; list-style: none; height: 120px;"
											class="wovenfabric">
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" id="all_woven">전체
													선택
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="gabardine">개버딘
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="warp_sateen">경
													새티인
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="gingham">깅엄
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="twill_flannel">능직
													플란넬
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="ninon">니논
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="duck">덕
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="denim">데님
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="dimity">디미티
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="lining_twill">라이닝
													능직
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="lown">론
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="ripstop_nylon">립스톱
													나일론
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="muslin">머슬린
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="batiste">바티스트
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="burlap">버랩
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="sail_cloth">범포
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="bedford_cord">베드퍼드
													코드
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="voile">보일
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="broad_cloth">브로드
													클로스
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="shantung">산퉁
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="satin">새틴
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="challis">샬리
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="chambray">샴브레이
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="chiffon">시폰
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="outing_flannel">아우팅
													플란넬
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="antique_satin">앤디크
													새틴
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="organdy">오간디
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="organza">오간자
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="oxford_chambray">옥스퍼드
													샴브레이
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="oxford_cloth">옥스퍼드
													클로스
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="filling_sateen">위
													새티인
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="georgette">조젯
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="true_crepe">진짜
													크레이프
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="china_silk">차이나
													견
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="chino">치노
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="cheese_cloth">치즈
													클로스
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="canvas">캔버스
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="crash">크래시
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="crepe_de_chine">크레이프
													드 신
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="crepe_back_satin">크레이프
													백 새틴
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="taffeta">타프타
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="tweed">트위드
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="twill_flannel">트윌
													플란넬
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="broken_twill">파능직
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="faille">파유
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="fancy_twill">팬시
													능직
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="percale">퍼케일
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="poplin">포플린
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="pongee">폰지
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="foulard">풀라드
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="print_cloth">프린트
													클로스
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="flannel">플란넬
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="flannelette">플란넬리트
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="habutai">하부타이
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="hound_stooth">하운드
													투스
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="herringbone">헤링본
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="wovenfabric" value="honan">호난
											</label></li>
										</ul>
									</div>
									<div id="1" class="tab-pane fade">
										<ul style="overflow-y: auto; list-style: none; height: 120px;"
											class="fancyfabric">
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="fancyfabric" id="all_fancy">전체
													선택
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="fancyfabric" value="damask">다마스크
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="fancyfabric" value="madras">마드라스
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="fancyfabric" value="marquisette">마퀴제트
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="fancyfabric" value="matelasse">마틀라세
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="fancyfabric" value="momie">모미
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="fancyfabric" value="bark_cloth">바크
													클로스
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="fancyfabric" value="bedford_cord">베드퍼드
													코드
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="fancyfabric" value="velveteen">벨베틴
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="fancyfabric" value="belvet">벨벳
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="fancyfabric" value="brocade">브로케이드
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="fancyfabric" value="eyelash">아이래쉬
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="fancyfabric"
													value="double_faced_fabrics">양면조직
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="fancyfabric" value="waffle_cloth">와플
													클로스
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="fancyfabric" value="corduroy">코듀로이
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="fancyfabric" value="crushed">크러시드
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="fancyfabric"
													value="jacquard_tapestry">태피스트리
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="fancyfabric" value="terry_cloth">테리
													클로스
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="fancyfabric" value="panne_velvet">팬
													벨벳
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="fancyfabric" value="pocket_cloth">포켓
													클로스
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="fancyfabric" value="frieze">프리즈
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="fancyfabric" value="pique">피케
											</label></li>
										</ul>
									</div>
									<div id="2" class="tab-pane fade">
										<ul style="overflow-y: auto; list-style: none; height: 120px;"
											class="otherfabric">
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="otherfabric" id="all_other">전체
													선택
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="otherfabric" value="leather">가죽
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="otherfabric"
													value="knit_through_fabric">니트를 통한 패브릭
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="otherfabric" value="laminate">라미네이트
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="otherfabric" value="lace">레이스
											</label></li>

											<li><label class="checkbox-inline"> <input
													type="checkbox" name="otherfabric"
													value="netlike_structure">망상구조물
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="otherfabric" value="fur">모피
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="otherfabric" value="foams">발포제
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="otherfabric" value="batting">배팅
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="otherfabric" value="non_woven_fabric">부직포
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="otherfabric" value="braid">브레이드
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="otherfabric" value="suede">스웨이드
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="otherfabric" value="wadding">웨딩
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="otherfabric" value="imitation_suede">인조
													스웨이드
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="otherfabric" value="plain_film">일반
													필름
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="otherfabric" value="coated_fabric">코팅패브릭
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="otherfabric" value="quilt_fabric">퀄트
													패브릭
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="otherfabric" value="tufted_fabric">터프트
													패브릭
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="otherfabric" value="expanded_film">팽창
													필름
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="otherfabric" value="felt">펠트
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="otherfabric" value="fiberfill">화이버필
											</label></li>
										</ul>
									</div>
									<div id="3" class="tab-pane fade">
										<ul style="overflow-y: auto; list-style: none; height: 120px;"
											class="knit">
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" id="all_knit">전체 선택
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" value="plain_tricot">평
													트리코트
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" value="brushed_tricot">브러시
													트리코트
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" value="warp_knit_velour">경편
													벨루어
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" value="satin_tricot">새틴
													트리코트
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" value="tulle">튤
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" value="lace">레이스
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" value="thermal_cloth">써멀
													클로스
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" value="power_net">파워 네트
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit"
													value="weft_insertion_warp_knit">위사 삽입 경편물
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" value="warp_insertion_knit">경사
													삽입 경편물
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit"
													value="warp_and_weft_insertion_knit">경사 및 위사 삽입 경편물
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" value="jersey">저지
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" value="stockinette">스토키니트
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" value="jacquard_jersey">자카드
													저지
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" value="intarsia">인타시아
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" value="knit_terry_cloth">테리
													클로스 편물
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" value="velour">벨루어
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" value="fake_fur">인조모피
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" value="french_terry">프렌치
													테리
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" value="fleece">플리스
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" value="rib_knit">리브 편물
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" value="inter_lock">인터로크
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" value="jacquard_double_knits">자카드
													더블 편물
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" value="fancy_double_knits">팬시
													더블 편물
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="knit" value="purl_knit">펄 편물
											</label></li>
										</ul>
									</div>
								</div>
								<div style="padding-left: 40px">
									<button type="button" class="btn btn-primary" id="add_space">넓히기</button>
								</div>
							</td>
							<td style="width: 280px;">
								<div class="col-md-12"
									style="margin-left: 0; margin-right: 0; padding-right: 0">
									<label class="checkbox-inline col-md-3 "
										style="margin-left: 10px;"> <input type="checkbox"
										name="otherfabric" value="1">개버딘
									</label> <label class="checkbox-inline col-md-3 "> <input
										type="checkbox" name="otherfabric" value="1">데님
									</label> <label class="checkbox-inline col-md-3 "> <input
										type="checkbox" name="otherfabric" value="1">머슬린
									</label> <label class="checkbox-inline col-md-2 "> <input
										type="checkbox" name="otherfabric" value="1">트위드
									</label> <label class="checkbox-inline col-md-3 "> <input
										type="checkbox" name="otherfabric" value="1">헤링본
									</label> <label class="checkbox-inline col-md-3 "> <input
										type="checkbox" name="otherfabric" value="1">벨벳
									</label> <label class="checkbox-inline col-md-3 "> <input
										type="checkbox" name="otherfabric" value="1">코듀로이
									</label> <label class="checkbox-inline col-md-2 "> <input
										type="checkbox" name="otherfabric" value="1">스웨이드
									</label> <label class="checkbox-inline col-md-3 "> <input
										type="checkbox" name="otherfabric" value="1">저지
									</label> <label class="checkbox-inline col-md-3 "> <input
										type="checkbox" name="otherfabric" value="1">벨루어
									</label> <label class="checkbox-inline col-md-3 "> <input
										type="checkbox" name="otherfabric" value="1">레이스
									</label> <label class="checkbox-inline col-md-2 "> <input
										type="checkbox" name="otherfabric" value="1">인조모피
									</label>
								</div>
							</td>
						</tr>

						<tr>
							<td style="width: 18px;">섬유</td>
							<td style="width: 80px;">
								<ul style="overflow-y: auto; list-style: none; height: 120px;">
									<li><a data-toggle="pill" href="#20">식물섬유</a></li>
									<li><a data-toggle="pill" href="#21">동물섬유</a></li>
									<li><a data-toggle="pill" href="#22">재생섬유</a></li>
									<li><a data-toggle="pill" href="#23">합성섬유</a></li>

								</ul>
							</td>
							<td style="width: 140px;">
								<div class="tab-content">
									<div id="20" class="tab-pane fade in active">
										<ul style="overflow-y: auto; list-style: none; height: 120px;"
											class="plantfiber">
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="plantfiber" id="all_plantfiber">전체
													선택
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="plantfiber" value="cotton">면
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="plantfiber" value="linen">아마
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="plantfiber" value="ramie">저마
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="plantfiber" value="hemp">대마
											</label></li>
										</ul>

									</div>
									<div id="21" class="tab-pane fade">
										<ul style="overflow-y: auto; list-style: none; height: 120px;"
											class="animalfiber">
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="animalfiber" id="all_animalfiber">전체
													선택
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="animalfiber" value="wool">양모
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="animalfiber" value="mohair">모헤어
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="animalfiber" value="angora">앙고라
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="animalfiber" value="cashmere">캐시미어
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="animalfiber" value="llama">라마
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="animalfiber" value="alpaca">알파카
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="animalfiber" value="slik">견
											</label></li>

										</ul>
									</div>
									<div id="22" class="tab-pane fade">
										<ul style="overflow-y: auto; list-style: none; height: 120px;"
											class="regeneratedfiber">
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="regeneratedfiber"
													id="all_regeneratedfiber">전체 선택
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="regeneratedfiber" value="rayon">레이온
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="regeneratedfiber" value="lyocell">리오셀
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="regeneratedfiber" value="acetate">아세테이트
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="regeneratedfiber" value="azlon">아즐론
											</label></li>
										</ul>
									</div>
									<div id="23" class="tab-pane fade">
										<ul
											style="overflow-y: scroll; list-style: none; height: 120px;"
											class="syntheticfiber">
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="syntheticfiber"
													id="all_syntheticfiber">전체 선택
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="syntheticfiber" value="nylon">나일론
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="syntheticfiber" value="polyester">폴리에스테르
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="syntheticfiber" value="olefin">올레핀
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="syntheticfiber" value="acryl">아크릴
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="syntheticfiber" value="elastomer">탄성중합체
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="syntheticfiber" value="aramid">아라미드
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="syntheticfiber" value="glass">유리
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="syntheticfiber"
													value="metallic_fibers">금속 섬유
											</label></li>
											<li><label class="checkbox-inline"> <input
													type="checkbox" name="syntheticfiber"
													value="polylactic_acid">플리락트산
											</label></li>

										</ul>
									</div>
									<div style="padding-left: 40px">
										<input type="checkbox"> 함량 100%만 검색
									</div>
								</div>
							</td>
							<td style="width: 280px;">
								<div class="col-md-12"
									style="margin-left: 0; margin-right: 0; padding-right: 0"`>
									<label class="checkbox-inline col-md-3"
										style="margin-left: 10px;"> <input type="checkbox"
										name="otherfabric" value="1">면
									</label> <label class="checkbox-inline col-md-3"> <input
										type="checkbox" name="otherfabric" value="1">양모
									</label> <label class="checkbox-inline col-md-3"> <input
										type="checkbox" name="otherfabric" value="1">앙고라
									</label> <label class="checkbox-inline col-md-2"> <input
										type="checkbox" name="otherfabric" value="1">레이온
									</label> <label class="checkbox-inline col-md-3"> <input
										type="checkbox" name="otherfabric" value="1">폴리에스테르
									</label> <label class="checkbox-inline col-md-3"> <input
										type="checkbox" name="otherfabric" value="1">나일론
									</label> <label class="checkbox-inline col-md-3"> <input
										type="checkbox" name="otherfabric" value="1">캐시미어
									</label> <label class="checkbox-inline col-md-2"> <input
										type="checkbox" name="otherfabric" value="1">아크릴
									</label>
								</div>
							</td>
						</tr>
					</table>

					<table class="table" id="table_2">
						<tr>
							<td style="width:100px">용도</td>
							<td><label class="checkbox-inline"> <input
									type="checkbox" name="uses" value="t_shirt">티셔츠
							</label> <label class="checkbox-inline"> <input type="checkbox"
									name="uses" value="shirt">셔츠
							</label><label class="checkbox-inline"> <input type="checkbox"
									name="uses" value="jumper">점퍼
							</label><label class="checkbox-inline"> <input type="checkbox"
									name="uses" value="jacket">재킷
							</label><label class="checkbox-inline"> <input type="checkbox"
									name="uses" value="coat">코트
							</label><label class="checkbox-inline"> <input type="checkbox"
									name="uses" value="onepiece">원피스
							</label><label class="checkbox-inline"> <input type="checkbox"
									name="uses" value="pants">팬츠
							</label><label class="checkbox-inline"> <input type="checkbox"
									name="uses" value="skirt">스커트
							</label><label class="checkbox-inline"> <input type="checkbox"
									name="uses" value="curtain">커튼
							</label><label class="checkbox-inline"> <input type="checkbox"
									name="uses" value="bedding">침구
							</label></td>
						</tr>
						<tr>
							<td>패턴</td>
							<td><label class="checkbox-inline"> <input
									type="checkbox" name="pattern" value="no_pattern">무지
							</label><label class="checkbox-inline"> <input type="checkbox"
									name="pattern" value="dot">도트
							</label><label class="checkbox-inline"> <input type="checkbox"
									name="pattern" value="stripe">스트라이프
							</label><label class="checkbox-inline"> <input type="checkbox"
									name="pattern" value="check">체크
							</label></td>
						</tr>
						<tr>
							<td>색상</td>
							<td><div>
									<a href="#" class="colorBox"
										style="background-color: rgb(255, 192, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color1" name="color1" value="RL00">
									<a href="#" class="colorBox"
										style="background-color: rgb(255, 224, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color2" name="color1" value="YRL0">
									<a href="#" class="colorBox"
										style="background-color: rgb(255, 255, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color3" name="color1" value="YL00">
									<a href="#" class="colorBox"
										style="background-color: rgb(224, 255, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color4" name="color1" value="GYL0">
									<a href="#" class="colorBox"
										style="background-color: rgb(192, 255, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color5" name="color1" value="GL00">
									<a href="#" class="colorBox"
										style="background-color: rgb(192, 255, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color6" name="color1" value="BGL0">
									<a href="#" class="colorBox"
										style="background-color: rgb(192, 192, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color7" name="color1" value="BL00">
									<a href="#" class="colorBox"
										style="background-color: rgb(224, 192, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color8" name="color1" value="PBL0">
									<a href="#" class="colorBox"
										style="background-color: rgb(255, 192, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color9" name="color1" value="PL00">
									<a href="#" class="colorBox"
										style="background-color: rgb(255, 192, 224)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color10" name="color1" value="RPL0">
									<a href="#" class="colorBox"
										style="background-color: rgb(255, 255, 255); border: 0.5px solid grey">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color11" name="color1" value="WGBL">
								</div>
								<div>
									<a href="#" class="colorBox"
										style="background-color: rgb(255, 0, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color12" name="color1" value="RS00">
									<a href="#" class="colorBox"
										style="background-color: rgb(255, 192, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color13" name="color1" value="YRS0">
									<a href="#" class="colorBox"
										style="background-color: rgb(255, 255, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color14" name="color1" value="YS00">
									<a href="#" class="colorBox"
										style="background-color: rgb(192, 255, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color15" name="color1" value="GYS0">
									<a href="#" class="colorBox"
										style="background-color: rgb(0, 255, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color16" name="color1" value="GS00">
									<a href="#" class="colorBox"
										style="background-color: rgb(0, 255, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color17" name="color1" value="BGS0">
									<a href="#" class="colorBox"
										style="background-color: rgb(0, 0, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color18" name="color1" value="BS00">
									<a href="#" class="colorBox"
										style="background-color: rgb(192, 0, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color19" name="color1" value="PBS0">
									<a href="#" class="colorBox"
										style="background-color: rgb(255, 0, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color20" name="color1" value="PS00">
									<a href="#" class="colorBox"
										style="background-color: rgb(255, 0, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color21" name="color1" value="RPS0">
									<a href="#" class="colorBox"
										style="background-color: rgb(128, 128, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color22" name="color1" value="WGBS">
								</div>
								<div>
									<a href="#" class="colorBox"
										style="background-color: rgb(128, 0, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color23" name="color1" value="RD00">
									<a href="#" class="colorBox"
										style="background-color: rgb(128, 64, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color24" name="color1" value="YRD0">
									<a href="#" class="colorBox"
										style="background-color: rgb(128, 128, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color25" name="color1" value="YD00">
									<a href="#" class="colorBox"
										style="background-color: rgb(64, 128, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color26" name="color1" value="GYD0">
									<a href="#" class="colorBox"
										style="background-color: rgb(0, 128, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color27" name="color1" value="GD00">
									<a href="#" class="colorBox"
										style="background-color: rgb(0, 64, 64)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color28" name="color1" value="BGD0">
									<a href="#" class="colorBox"
										style="background-color: rgb(0, 0, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color29" name="color1" value="BD00">
									<a href="#" class="colorBox"
										style="background-color: rgb(64, 0, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color30" name="color1" value="PBD0">
									<a href="#" class="colorBox"
										style="background-color: rgb(128, 0, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color31" name="color1" value="PD00">
									<a href="#" class="colorBox"
										style="background-color: rgb(128, 0, 64)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color32" name="color1" value="RPD0">
									<a href="#" class="colorBox"
										style="background-color: rgb(0, 0, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									<input type="radio" class="color33" name="color1" value="WGBD">
								</div></td>
						</tr>
						<tr>
							<td>소매가</td>
							<td><input type="number" name="retail" value="">&nbsp;&nbsp;~&nbsp;<input
								type="number" name="retail" value="">&nbsp;원
						</tr>
						<tr>
							<td>도매가</td>
							<td><input type="number" name="wholesale" value="">&nbsp;&nbsp;~&nbsp;<input
								type="number" name="wholesale" value="">&nbsp;원
						</tr>
					</table>
					<div class="text-center">
						<button type="button" class="btn btn-primary" style="display:">적용하기</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="all">
		<div class="col-md-12">
			<c:forEach items="${plist }" var="plist">
				<!-- item -->
				<div class="col-md-2 col-xs-6"
					style="line-height: 0.7em; padding: 0.5px; height: 340px;">
					<div class="col-md-12">
						<hr>
						<div style="height: 203px">
							<a
								href="../SidServlet?command=read_product&productId=${plist.productID }">
								<img src="${plist.mainImg }"
								style="max-width: 100%; width: 100%; max-height: 200px;">
							</a>
						</div>
						<div>
							<p class="text-center" style="line-height:120%">
								${plist.itemName }<br><span style="font-size:100%">(${plist.colorName })</span><br>
							</p>
						</div>
						<a class="text-center" >
							[${plist.company }]</a>
						<div style="float: right;">
							<a href="#" data-toggle="tooltip" data-placement="left" class="text-center"
								title="폭 (cm)" style="font-size: 85%;" onclick="return false;">${plist.width }</a> / <a id="thick"
								href="#" data-toggle="tooltip" data-placement="left" onclick="return false;"
								title=<c:choose>
									<c:when test="${plist.thick=='매우 얇음' }">"매우얇음"</c:when>
									<c:when test="${plist.thick=='얇음' }">얇음</c:when>
									<c:when test="${plist.thick=='보통' }">보통</c:when>
									<c:when test="${plist.thick=='두꺼움' }">두꺼움</c:when>
									<c:when test="${plist.thick=='매우 두꺼움' }">매우두꺼움</c:when>
								</c:choose>
								style="font-size: 90%"> <c:choose>
									<c:when test="${plist.thick=='매우 얇음' }">
								1</c:when>
									<c:when test="${plist.thick=='얇음' }">
								2</c:when>
									<c:when test="${plist.thick=='보통' }">
								3</c:when>
									<c:when test="${plist.thick=='두꺼움' }">
								4</c:when>
									<c:when test="${plist.thick=='매우 두꺼움' }">
								5</c:when>
								</c:choose>
							</a>
						</div>
						<hr style="padding: 3px; margin: 3px">
						<div class="col-md-9" style="padding: 0px;">
							<span style="font-size: 90%; line-height: 1.3em" class="text-center">
								<img src="../img/so3.gif" style="width:13%"> ${plist.retail }원
								<%
							if (session.getAttribute("admin") != null && session.getAttribute("admin").equals(0)) {
						%>
							<a
								href="../SidServlet?command=admin_delete_product&productId=${plist.productID}"><i
								class="fa fa-trash-o"></i></a>
						<%
							}
						%><br><img src="../img/do3.gif" style="width:13%"> ${plist.wholesale }원
							</span>
								&nbsp;&nbsp;<span id="ma">${plist.standard}마↑</span>
						</div>
						<div class="col-md-3" style="padding: 0px;">

							<p style="font-size: 90%; line-height: 1.3em">
								<a href="#" onclick="return false;" data-toggle="popover" data-trigger="hover"
									data-placement="bottom"
									data-content='
								
								  <c:set var="color" value="${plist.color}" />
                           <c:if test='${fn:contains(color, "RS00")}'>
                              <a href="#" onclick="return false;" class="colorBox"
                                 style="background-color: rgb(255, 0, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>

									</c:if>

									<c:if test='${fn:contains(color, "YRS0")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(255, 192, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "YS00")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(255, 255, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "GYS0")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(192, 255, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "GS00")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(0, 255, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "BGS0")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(0, 255, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "BS00")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(0, 0, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "PBS0")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(192, 0, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "PS00")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(255, 0, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "RPS0")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(255, 0, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "WGBS")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(128, 128, 128); border: 1px solid black">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>

									<c:if test='${fn:contains(color, "RL00")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(255, 192, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "YRL0")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(255, 224, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "YL00")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(255, 255, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "GYL0")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(224, 255, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "GL00")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(192, 255, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "BGL0")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(192, 255, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "BL00")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(192, 192, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "PBL0")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(224, 192, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "PL00")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(255, 192, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "RPL0")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(255, 192, 224)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "WGBL")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(255, 255, 255); border: 0.5px solid grey">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>

									<c:if test='${fn:contains(color, "RD00")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(128, 0, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "YRD0")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(128, 64, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "YD00")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(128, 128, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "GYD0")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(64, 128, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "GD00")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(0, 128, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "BGD0")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(0, 64, 64)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "BD00")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(0, 0, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "PBD0")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(64, 0, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "PD00")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(128, 0, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "RPD0")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(128, 0, 64)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "WGBD")}'>
										<a href="#" class="colorBox" onclick="return false;"
											style="background-color: rgb(0, 0, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
								
								
								
								'><img src="../img/colorwheel.png" style="width:80%">
								</a>
							</p>

						</div>

<<<<<<< HEAD
=======
						
>>>>>>> branch 'master' of https://secrecy27@bitbucket.org/hsuu/sid_tex.git
					</div>
				</div>
			</c:forEach>
		</div>
	</div>

	<jsp:include page="paging.jsp" flush="true">
		<jsp:param name="firstPageNo" value="${paging.firstPageNo}" />
		<jsp:param name="prevPageNo" value="${paging.prevPageNo}" />
		<jsp:param name="startPageNo" value="${paging.startPageNo}" />
		<jsp:param name="pageNo" value="${paging.pageNo}" />
		<jsp:param name="endPageNo" value="${paging.endPageNo}" />
		<jsp:param name="nextPageNo" value="${paging.nextPageNo}" />
		<jsp:param name="finalPageNo" value="${paging.finalPageNo}" />
	</jsp:include>
	
</body>

<script>
	//다른색상보기 팝업
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			html : true,
			container : 'body'
		});
	});
	$("#searchValue").keydown(function(e) {
		if (e.keyCode == 13)
			checkSearch();
	})

	//검색
	$(document).ready(function(e) {
		$('#keyword').find('a').click(function(e) {
			e.preventDefault();
			var param = $(this).attr("href").replace("#", "");
			var concept = $(this).text();
			$('.search-panel span#search_concept').text(concept);
			$('.input-group #search_param').val(param);
		});
	});

	//검색
	function checkSearch() {
		var action;

		action = "../SidServlet?command=search_product"

		document.frm.action = action;
		document.frm.submit();
	}


	//폭, 넓이 툴팁
	$(function() {
		$('[data-toggle="tooltip"]').tooltip()
	});


	//검색 목록
	$("#basic_search").hide();
	$(document).ready(function() {
		$("#more_1").click(function() {
			
			alert("상세검색 - 준비중입니다.")
			//상세 검색 토글 - 나중에 완성
			/* $("#basic_search").toggle(); */
		});

	});

	/* // 장바구니 담기
	function toBasket(id) {
		$.ajax({
			type : "POST",
			url : "../SidServlet?command=toBasket&email=${sessionScope.email }&id=" + id,
			success : function(result) {
				if (result == 2) {
					alert("담기 성공")
				} else if (result == 1) {
					alert("수량 증가")
				} else {
					alert("담기 실패")
				}
			},
			error : function(request, status, error) {
				alert("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
			}
		});
	} */

	//전체선택
	//전체선택
	$("#all_woven").click(function() {
		if ($("#all_woven").prop("checked")) {
			$("input[name=wovenfabric]:checkbox").each(function() {
				this.checked = true;
			});
		} else {
			$("input[name=wovenfabric]:checkbox").each(function() {
				this.checked = false;
			});
		}
	});
	$("#all_fancy").click(function() {
		if ($("#all_fancy").prop("checked")) {
			$("input[name=fancyfabric]:checkbox").each(function() {
				this.checked = true;
			});
		} else {
			$("input[name=fancyfabric]:checkbox").each(function() {
				this.checked = false;
			});
		}
	});
	$("#all_other").click(function() {
		if ($("#all_other").prop("checked")) {
			$("input[name=otherfabric]:checkbox").each(function() {
				this.checked = true;
			});
		} else {
			$("input[name=otherfabric]:checkbox").each(function() {
				this.checked = false;
			});
		}
	});
	$("#all_knit").click(function() {
		if ($("#all_knit").prop("checked")) {
			$("input[name=knit]:checkbox").each(function() {
				this.checked = true;
			});
		} else {
			$("input[name=knit]:checkbox").each(function() {
				this.checked = false;
			});
		}
	});
	$("#all_plantfiber").click(function() {
		if ($("#all_plantfiber").prop("checked")) {
			$("input[name=plantfiber]:checkbox").each(function() {
				this.checked = true;
			});
		} else {
			$("input[name=plantfiber]:checkbox").each(function() {
				this.checked = false;
			});
		}
	});
	$("#all_animalfiber").click(function() {
		if ($("#all_animalfiber").prop("checked")) {
			$("input[name=animalfiber]:checkbox").each(function() {
				this.checked = true;
			});
		} else {
			$("input[name=animalfiber]:checkbox").each(function() {
				this.checked = false;
			});
		}
	});
	$("#all_regeneratedfiber").click(function() {
		if ($("#all_regeneratedfiber").prop("checked")) {
			$("input[name=regeneratedfiber]:checkbox").each(function() {
				this.checked = true;
			});
		} else {
			$("input[name=regeneratedfiber]:checkbox").each(function() {
				this.checked = false;
			});
		}
	});
	$("#all_syntheticfiber").click(function() {
		if ($("#all_syntheticfiber").prop("checked")) {
			$("input[name=syntheticfiber]:checkbox").each(function() {
				this.checked = true;
			});
		} else {
			$("input[name=syntheticfiber]:checkbox").each(function() {
				this.checked = false;
			});
		}
	});


	//체크박스-라디오 버튼처럼 한개만
	/* 	$(document).ready(function() {
	    //라디오 요소처럼 동작시킬 체크박스 그룹 셀렉터
	    $('input[type="checkbox"][name="color"]').click(function(){
	        //클릭 이벤트 발생한 요소가 체크 상태인 경우
	        if ($(this).prop('checked')) {
	            //체크박스 그룹의 요소 전체를 체크 해제후 클릭한 요소 체크 상태지정
	            $('input[type="checkbox"][name="color"]').prop('checked', false);
	            $(this).prop('checked', true);
	        }
	    });
	}); */


	/* 	//체크박스-복수 선택시 alert
	 	$(document).ready(function() {
		    //라디오 요소처럼 동작시킬 체크박스 그룹 셀렉터
		    $('input[type="checkbox"][name="color"]').click(function(){
		        //클릭 이벤트 발생한 요소가 선택 상태일 경우
		        //체크된 요소 확인후 복수개 선택되있을 경우 체크 해제
		        if ($(this).prop('checked') 
		        && $('input[type="checkbox"][name="color"]:checked').size()>1) {
		            $(this).prop('checked', false);
		      //  alert('두개 이상 선택할 수 없습니다.');
		        }
		    });
		});  */
	var space = 0;
	$("#add_space").click(function() {
		if (space == 0) {
			$("#0").children().css("height", "300px");
			$("#1").children().css("height", "300px");
			$("#2").children().css("height", "300px");
			$("#3").children().css("height", "300px");
			space = 1;
			$("#add_space").text("줄이기");
		} else if (space == 1) {
			$("#0").children().css("height", "120px");
			$("#1").children().css("height", "120px");
			$("#2").children().css("height", "120px");
			$("#3").children().css("height", "120px");
			space = 0;
			$("#add_space").text("넓히기");
		}
	});
</script>

</html>

<%@ include file="../include/footer.jsp"%>