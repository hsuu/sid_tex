<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../include/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">

<title>SID - Snow in Dawn</title>

<script type="text/javascript" src="../css/custom.js"></script>
</head>
<body>
	<div class="col-md-12">
		<div class="col-md-12">

			<table class="table table-condensed">
				<thead>
					<tr>
						<th colspan="3">${board.title}</th>

					</tr>
				</thead>
				<tbody>
					<tr>
						<td>${board.content}</td>

					</tr>
					<tr>
						<td>작성일 : ${board.date}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<br>
		<div class="col-md-12">
			<ul class="list-inline pull-right">
				<li><a href="../BoardServlet?command=board_list"
					class="btn btn-primary">목록으로</a></li>
				<%
					if (session.getAttribute("admin") != null && session.getAttribute("admin").equals(0)) {
				%>
				<li><a
					href="../BoardServlet?command=board_update&num=${board.contentNum}" class="btn btn-primary">수정</a></li>
				<li><a
					href="../BoardServlet?command=board_delete&num=${board.contentNum}"
					type="button" class="btn btn-primary">삭제</a></li>
				<%
					}
				%>
			</ul>
		</div>
	</div>
	<%@ include file="../include/footer.jsp"%>
</body>
</html>