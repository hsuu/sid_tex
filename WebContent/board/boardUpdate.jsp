<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../include/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">

<script type="text/javascript" src="../css/custom.js"></script>
<script type="text/javascript" src="../editor/js/HuskyEZCreator.js"
	charset="utf-8"></script>
</head>
<body>
	<div class="col-md-12">
		<h1>공지사항 수정</h1>
		<form name="frm" method="post" action="BoardServlet">
			<input type="hidden" name="command" value="board_write">
			<div class="form-group">
			<input type="hidden" name="contentNum" value="${vo.contentNum }" >
				<label class="control-label">제목</label> <input type="text"
					class="form-control" style="width: 70%" name="title" value="${vo.title }">
			</div>
			<div class="form-group">
				<label class="control-label">분류</label> <input type="text"
					class="form-control" style="width: 70%" name="sort" value="${vo.sort }">
			</div>
			<div class="form-group">

				<label class="control-label">내용</label>
				<textarea name="content" id="smarteditor" rows="10" cols="100"
					style="width: 766px; height: 412px;">${vo.content }</textarea>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary" id="submit_upload"
					onclick="return boardCheck()">등록</button>
				<input type="reset" class="btn btn-primary" value="다시 작성">
				<button type="button" class="btn btn-primary"
					onclick='location.href="../BoardServlet?command=board_list"'>목록</button>
			</div>
		</form>
	</div>
	<%@ include file="../include/footer.jsp"%>
</body>
<script>
	// smarteditor
	$(function() {

		//전역변수선언
		var editor_object = [];

		nhn.husky.EZCreator.createInIFrame({
			oAppRef : editor_object,
			elPlaceHolder : "smarteditor", //연결지을 textarea의 id명
			sSkinURI : "/editor/SmartEditor2Skin2.html", //에디터 스킨 경로
			htParams : {
				// 툴바 사용 여부 (true:사용/ false:사용하지 않음)
				bUseToolbar : true,
				// 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
				bUseVerticalResizer : true,
				// 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
				bUseModeChanger : true,
			}
		});

		//전송버튼 클릭이벤트
		$("#submit_upload").click(function() {
			//id가 smarteditor인 textarea에 에디터에서 대입
			editor_object.getById["smarteditor"].exec("UPDATE_CONTENTS_FIELD", []);

			// 이부분에 에디터 validation 검증
			document.frm.action = "../BoardServlet?command=board_update2";
			document.frm.submit();
			//폼 submit
			//	$("#fm").submit();

		})
	})
</script>
</html>