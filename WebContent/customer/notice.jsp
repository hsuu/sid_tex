
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../include/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD html 4.01 Transitional//EN" "http://www.w3.org/TR/jsp4/loose.dtd">
<head>
<style>
th, td{
	text-align:center;
}
</style>
</head>
<body>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">
			<li class="active" role="presentation"><a
				href="../BoardServlet?command=board_list">공지사항</a></li>
			<li role="presentation"><a href="../BoardServlet?command=list_faq">자주 하는
					질문</a></li>
			<li role="presentation"><a href="../customer/consult_phone.jsp">1:1 상담</a></li>
		</ul>
	</div>

	<div class="col-md-12">
	<br><br>
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
					<th width="20%">날짜</th>
					<th width="20%">구분</th>
					<th width="60%">제목</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="board" items="${boardList}">
					<tr>
							<td>${board.date}</td>
						<td>${board.sort}</td><td>
						<a href="../BoardServlet?command=board_view&num=${board.contentNum}">${board.title}</a></td>
									</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>

</body>
</html>
	<%@ include file="../include/footer.jsp"%>