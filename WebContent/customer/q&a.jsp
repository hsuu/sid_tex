
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../include/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">
			<li role="presentation"><a
				href="../BoardServlet?command=board_list">공지사항</a></li>
			<li class="active" role="presentation"><a
				href="../BoardServlet?command=list_faq">자주 하는 질문</a></li>
			<li role="presentation"><a href="../customer/consult_phone.jsp">1:1
					상담</a></li>
		</ul>
	</div>
	<br>
	<div class="col-md-12">
		<ul id="nav-tabs-wrapper" class="nav nav-tabs nav-tabs-horizontal">
			<li class="active"><a href="#htab1" data-toggle="tab">전체</a></li>
			<li><a href="#htab2" data-toggle="tab">상품 관련</a></li>
			<li><a href="#htab3" data-toggle="tab">회원 관련</a></li>
			<li><a href="#htab4" data-toggle="tab">배송 관련</a></li>
			<li><a href="#htab5" data-toggle="tab">주문 관련</a></li>
			<li><a href="#htab6" data-toggle="tab">결제/취소/환불</a></li>
			<li><a href="#htab7" data-toggle="tab">기타</a></li>

		</ul>
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane fade in active" id="htab1">
				<h3>전체</h3>
				<table class="table table-hover">
					<thead>
						<tr>
							<th style="width: 190px">분류</th>
							<th colspan="2">내용</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${list}" var="list">
							<tr>
								<td>${list.sort}</td>
								<td class="question"><div>${list.question}</div>
									<div class="answer well well-sm">${list.answer}</div></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div role="tabpanel" class="tab-pane fade" id="htab2">
				<h3>상품 관련</h3>
				<table class="table table-hover">
					<thead>
						<tr>
							<th style="width: 190px">분류</th>
							<th colspan="2">내용</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${list}" var="list">
							<c:if test="${list.sort == '상품 관련'}">
								<tr>
									<td>${list.sort}</td>
									<td class="question"><div>${list.question}</div>
										<div class="answer well well-sm">${list.answer}</div></td>
								</tr>
							</c:if>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div role="tabpanel" class="tab-pane fade in" id="htab3">
				<h3>회원 관련</h3>
				<table class="table table-hover">
					<thead>
						<tr>
							<th style="width: 190px">분류</th>
							<th colspan="2">내용</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${list}" var="list">
							<c:if test="${list.sort == '회원 관련'}">
								<tr>
									<td>${list.sort}</td>
									<td class="question"><div>${list.question}</div>
										<div class="answer well well-sm">${list.answer}</div></td>
								</tr>
							</c:if>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div role="tabpanel" class="tab-pane fade" id="htab4">
				<h3>배송 관련</h3>
				<table class="table table-hover">
					<thead>
						<tr>
							<th style="width: 190px">분류</th>
							<th colspan="2">내용</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${list}" var="list">
							<c:if test="${list.sort == '배송 관련'}">
								<tr>
									<td>${list.sort}</td>
									<td class="question"><div>${list.question}</div>
										<div class="answer well well-sm">${list.answer}</div></td>
								</tr>
							</c:if>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div role="tabpanel" class="tab-pane fade" id="htab5">
				<h3>배송 관련</h3>
				<table class="table table-hover">
					<thead>
						<tr>
							<th style="width: 190px">분류</th>
							<th colspan="2">내용</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${list}" var="list">
							<c:if test="${list.sort == '주문 관련'}">
								<tr>
									<td>${list.sort}</td>
									<td class="question"><div>${list.question}</div>
										<div class="answer well well-sm">${list.answer}</div></td>
								</tr>
							</c:if>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div role="tabpanel" class="tab-pane fade" id="htab6">
				<h3>결제/취소/환불</h3>
				<table class="table table-hover">
					<thead>
						<tr>
							<th style="width: 190px">분류</th>
							<th colspan="2">내용</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${list}" var="list">
							<c:if test="${list.sort == '결제/취소/환불'}">
								<tr>
									<td>${list.sort}</td>
									<td class="question"><div>${list.question}</div>
										<div class="answer well well-sm">${list.answer}</div></td>
								</tr>
							</c:if>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div role="tabpanel" class="tab-pane fade" id="htab7">
				<h3>기타</h3>
				<table class="table table-hover">
					<thead>
						<tr>
							<th style="width: 190px">분류</th>
							<th colspan="2">내용</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${list}" var="list">
							<c:if test="${list.sort == '기타'}">
								<tr>
									<td>${list.sort}</td>
									<td class="question"><div>${list.question}</div>
										<div class="answer well well-sm">${list.answer}</div></td>
								</tr>
							</c:if>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>



</body>

<script>

	$(document).ready(function() {
		$(".answer").each(function() {
			$(this).hide();
		})

		$(".question").click(function() {
			$(this).find(".answer").toggle();
		});


	});
</script>
</html>

<%@ include file="../include/footer.jsp"%>
