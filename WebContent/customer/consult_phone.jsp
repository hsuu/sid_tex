
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../include/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<style>
.panel {
	height: 300px;
}
</style>
<body>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">
			<li role="presentation"><a
				href="../BoardServlet?command=board_list">공지사항</a></li>
			<li role="presentation"><a href="../BoardServlet?command=list_faq">자주 하는
					질문</a></li>
			<li class="active" role="presentation"><a
				href="../customer/consult_phone.jsp">1:1 상담</a></li>
		</ul>
	</div>
	<br>
	<br>
	<div class="col-md-12">

		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="text-center">
						<h1>
							<i class="fa fa-envelope-o"></i>
						</h1>
						<h2 class="text-center">이메일</h2>
						<div class="panel-body">
							<br> <br> <a href="../customer/consult_mail.jsp"
								class="btn btn-primary">이메일 보내기</a>

						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="text-center">
						<h1>
							<i class="fa fa-commenting-o"></i>
						</h1>
						<h2 class="text-center">카카오톡</h2>
						<div class="panel-body">
							<br> <br> 시드텍스 검색 후, 친구 추가!

						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="text-center">
						<h1>
							<i class="fa fa-phone"></i>
						</h1>
						<h2 class="text-center">전화</h2>
						<div class="panel-body">
							<br><br> 070 - 8808 -2222

						</div>
					</div>
				</div>
			</div>
		</div>
		<br>
	</div>
	
	<div style="text-align: center">
		<h4>상담 가능시간 : 오전 10시 ~ 오후 5시</h4>
	</div>

</body>
</html>
<%@ include file="../include/footer.jsp"%>