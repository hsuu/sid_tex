
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../include/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">
			<li role="presentation"><a
				href="../BoardServlet?command=board_list">공지사항</a></li>
			<li role="presentation"><a href="../BoardServlet?command=list_faq">자주 하는
					질문</a></li>
			<li class="active" role="presentation"><a
				href="../customer/consult_phone.jsp">1:1 상담</a></li>
		</ul>
	</div>
	<br>
	<br>
	<div class="col-md-12">
		<div class="col-md-8 col-md-offset-2">
			<div id="beforeSend">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="text-center">
							<h1>
								<i class="fa fa-envelope-o"></i>
							</h1>
							<h2 class="text-center">1:1 상담</h2>
							<div class="panel-body">
								<div class="form-group">
									<label>답장 받을 이메일</label> <input type="text"
										class="form-control" placeholder="E-mail"
										value="<%if (session.getAttribute("email") != null)
				out.print(session.getAttribute("email"));%>"
										id="email" required />
								</div>
								<div class="form-group">
									<label>내용</label>
									<textarea class="form-control" rows="7" id="content"></textarea>

								</div>
								<button type="button" id="send"
									class="btn btn-lg btn-primary btn-block">문의</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="afterSend">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="text-center">
							<h1>
								<i class="fa fa-commenting-o"></i>
							</h1>
							<h2 class="text-center">1:1 상담</h2>
							<div class="panel-body">

								<h1>완료</h1>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

</body>
<script>
	$(function() {

		$("#afterSend").hide();
		//문의 버튼 클릭
		$("#send").click(function(e) {
			var email = $("#email").val();
			var content=$("#content").val();

			//문의하기
			$.ajax({
				url : '../SidServlet?command=consult_mail',
				data : 'email=' + email+'&content='+content,
				success : function(data) {
					if (data == "1") {
						$("#afterSend").show();
						$("#beforeSend").hide();
					} else {
						alert("error");
					}
				}
			});


		})

	});
</script>
</body>
</html>
	<%@ include file="../include/footer.jsp"%>