<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../include/header.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
body {
	padding-left: 30px;
	padding-right: 30px;
}
</style>
</head>
<body>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">
			<li role="presentation"><a
				href="../SidServlet?command=purchase_list&state=0">주문 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_manage_stock">재고 관리</a></li>
			<li role="presentation"><a href="../SidServlet?command=product_upload">상품
					등록</a></li>
			<li role="presentation"><a
				href="../product/product_manage_balance_day.jsp">정산 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_manage_delivery">배송 관리</a></li>
			<li class="active" role="presentation"><a
				href="../SidServlet?command=list_manage_customer">고객 관리</a></li>
		</ul>
	</div>
	<div class="col-md-12">
		<br>
		<div role="tabpanel">
			<!-- Nav tabs -->
			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane in active" id="home">
					<div class="panel panel-default">
						<div class="panel-body">
							<div>
								회원 관리
							</div>
						</div>
					</div>
					<div class="row" id="resizeDiv">
						<div class="table-responsive panel panel-default">
							<table class="table table-striped table-bordered resizeTable"
								id="resizeTable1">
								<thead>
									<tr>
										<th class="1"></th>
										<th class="2">이메일</th>
										<th class="3">구매자명</th>
										<th class="4">연락처</th>
										<th class="5">거래건수</th>
										<th class="6">거래금액</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${list}" var="list">
									<tr>
										<td class="1"><input type="checkbox" name="checktd"
										value="${list.buyerEmail}"></td>
										<td class="2">${list.buyerEmail}</td>
										<td class="3">${list.buyerName}</td>
										<td class="4">${list.buyerPhone}</td>
										<td class="5">${list.transaction_count}</td>
										<td class="6">${list.transaction_price}</td>
									</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</body>

<%@ include file="../include/footer.jsp"%>
</html>