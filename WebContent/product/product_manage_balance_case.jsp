<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../include/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
body {
	padding-left: 30px;
	padding-right: 30px;
}

thead th,tbody td{
text-align:center;
}

</style>
</head>
<body>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">
			<li role="presentation"><a
				href="../SidServlet?command=purchase_list&state=0">주문 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_manage_stock">재고 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=product_upload">상품 등록</a></li>
			<li class="active" role="presentation"><a
				href="../SidServlet?command=list_manage_balance_day">정산 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_manage_delivery">배송 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_manage_customer">고객 관리</a></li>
		</ul>
	</div>
	<br>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">

			<li role="presentation"><a href="../SidServlet?command=list_manage_balance_day">일별 정산내역</a></li>
			<li class="active"><a href="../SidServlet?command=list_manage_balance_case">건별 정산내역</a></li>
			<li role="presentation"><a href="../SidServlet?command=list_manage_balance_account">계좌관리</a></li>
		</ul>
	</div>
	<div class="col-md-12">
		<br>
		<div role="tabpanel">
			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active">
					<div class="panel panel-default">
						<div class="panel-body">참고하세요.
검색을 통해 해당 일자 또는 주문번호의 건별 정산내역을 확인하실 수 있습니다.</div>
					</div>
					<div class="row" id="resizeDiv">
						<div class="table-responsive panel panel-default">
							<table class="table table-striped table-bordered resizeTable"
								id="resizeTable2">
								<thead>
									<tr>
										<th class="1">구매확정일</th>
										<th class="2">정산완료일</th>
										<th class="3">주문번호</th>
										<th class="4">정산금액(A-B-C-D)</th>
										<th class="5">결제금액(A)</th>
										<th class="6">부가세(B)</th>
										<th class="7">수수료(C)</th>
										<th class="8">중개료(D)</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${list}" var="list">
										<tr>
											<td class="1">${list.purchaseConfirm_date }</td>
											<td class="2">${list.calculate_date }</td>
											<td class="3">${list.purchaseId }</td>
											<td class="4">${list.calculate_amount2 }원</td>
											<td class="5">${list.payment_amount2 }원</td>
											<td class="6">${list.surtax2 }원</td>
											<td class="7">${list.fee2 }원</td>
											<td class="8">${list.brokerage_fee2 }원</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				
			</div>

		</div>
	</div>
</body>

<%@ include file="../include/footer.jsp"%>
</html>