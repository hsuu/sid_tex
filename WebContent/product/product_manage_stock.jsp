<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../include/header.jsp"%>
<%@ page import="javax.servlet.http.HttpSession"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<style>
table a {
	color: #2F7CA3;
}

body {
	padding-left: 30px;
	padding-right: 30px;
}

table {
	white-space: nowrap;
}

#resizeTable td {
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
}

#resizeTable th {
	overflow: hidden;
	text-overflow: ellipsis;
}

#headtype th {
	text-align: center;
}

#bodytype td {
	text-align: center;
}

</style>
<body>

	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">
			<li role="presentation"><a
				href="../SidServlet?command=purchase_list&state=0">주문 관리</a></li>
			<li class="active" role="presentation"><a
				href="../SidServlet?command=list_manage_stock">재고 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=product_upload">상품 등록</a></li>
			<li role="presentation"><a
				href="../product/product_manage_balance_day.jsp">정산 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_manage_delivery">배송 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_manage_customer">고객 관리</a></li>
		</ul>
	</div>
	<br>
	</div>
	<div class="col-md-12">

		<!-- <h1>재고 관리</h1>
		 <div class="row">
			<div class="alert alert-info" role="alert">
				<p>
					신규주문 <a href="#" class="alert-link">0</a>건 / 배송준비 <a href="#"
						class="alert-link">0</a>건 / 발송전 취소요청 <a href="#"
						class="alert-link">0</a>건 / 발송전 배송지 변경 <a href="#"
						class="alert-link">0</a>건 / 자동처리예정 <a href="#" class="alert-link">0</a>건
				</p>
			</div>

		</div> -->
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<button type="button" class="btn btn-primary" id="checkAll">전체
						선택</button>
					<button type="button" class="btn btn-primary" id="uncheckAll">선택
						해제</button>

					<div class="checkbox">
						<label class="checkbox-inline"> <input type="checkbox"
							id="c1" name="box" value="1" checked> 판매자상품코드
						</label> <label class="checkbox-inline"> <input type="checkbox"
							id="c2" name="box" value="2" checked> 판매상태
						</label> <label class="checkbox-inline"> <input type="checkbox"
							id="c3" name="box" value="3" checked> 상품명
						</label> <label class="checkbox-inline"> <input type="checkbox"
							id="c4" name="box" value="4" checked> 색상명
						</label> <label class="checkbox-inline"> <input type="checkbox"
							id="c5" name="box" value="5" checked> 재고량
						</label>
						<!-- <label class="checkbox-inline"> <input type="checkbox"
							id="c6" name="box" value="6" checked> 홍보문구
						</label> <label class="checkbox-inline"> <input type="checkbox"
							id="c7" name="box" value="7" checked> 이미지
						</label> -->
						<label class="checkbox-inline"> <input type="checkbox"
							id="c6" name="box" value="6" checked> 소매가
						</label> <label class="checkbox-inline"> <input type="checkbox"
							id="c7" name="box" value="7" checked> 도매가
						</label> <label class="checkbox-inline"> <input type="checkbox"
							id="c8" name="box" value="8" checked> 도매 기준
						</label><br> <label class="checkbox-inline"> <input
							type="checkbox" id="c9" name="box" value="9" checked> 스와치
							제공&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</label> <label class="checkbox-inline"> <input type="checkbox"
							id="c10" name="box" value="10" checked>
							폭&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</label> <label class="checkbox-inline"> <input type="checkbox"
							id="c11" name="box" value="11" checked>
							두께&nbsp;&nbsp;&nbsp;
						</label> <label class="checkbox-inline"> <input type="checkbox"
							id="c12" name="box" value="12" checked> 등록일
						</label>
					</div>
				</div>
			</div>
			<div class="panel-body">
				<div class="col-md-1" style="padding-left: 0">
					<button type="button" class="btn btn-primary" id="sel_del">선택
						삭제</button>
				</div>
				<div class="dropdown col-md-2" style="padding-left: 3px">
					<button class="btn btn-primary dropdown-toggle" type="button"
						data-toggle="dropdown">
						판매상태 변경 <span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						<li><a href="#" id="onsale">판매중</a></li>
						<li><a href="#" id="stopsale">판매중지</a></li>
					</ul>
				</div>
				<div>
					전체 검색 <input class="search" class="form-control" type="search"
						data-column="all">
				</div>

			</div>


			<div class="row" id="resizeDiv">
				<div class="table-responsive panel panel-default">
					<br>
					<table class="table table-striped table-bordered" id="resizeTable"
						style="width: auto">
						<thead id="headtype">
							<tr>
								<th data-sorter="false" class="filter-false"><input
									type="checkbox" id="checkth"></th>
								<th data-sorter="false" class="filter-false">수정</th>
								<!-- <th data-sorter="false" class="filter-false">삭제</th> -->
								<th class="1">판매자상품코드</th>
								<th class="2">판매상태</th>
								<th class="3">상품명</th>
								<th data-sorter="false" class="filter-false 4">색상명</th>
								<th data-sorter="false" class="filter-false 5">재고량</th>
								<!-- <th data-sorter="false" class="filter-false" class="6">홍보문구</th>
								<th data-sorter="false" class="filter-false" class="7">이미지</th> -->
								<th class="6">소매가</th>
								<th class="7">도매가</th>
								<th class="8">도매 기준</th>
								<th class="9">스와치 제공</th>
								<th class="10">폭</th>
								<th class="11">두께</th>
								<th class="12">등록일</th>
							</tr>
						</thead>
						<tbody id="bodytype">
							<c:forEach items="${list}" var="product">
								<tr class="${product.relate}">
									<td><input type="checkbox" name="checktd"
										value="${product.productID}"></td>
									<td><a href="#" class="modify"
										onclick="modifyItem(${product.productID})">수정</a></td>
									<%-- <td><a href="#" class="copy"
												onclick="copyItem(${product.productID})">삭제</a></td> --%>
									<!--판매자 상품코드 -->
									<td class="1">${product.productID}</td>
									<!-- 판매상태 -->
									<td class="2">
										<%-- <a href="#" data-toggle="modal"
										data-target="#myModal" class="updateModal" data-column="state"
										data-before="${product.state}" data-id="${product.productID}"> --%>
										${product.state}
									</td>
									<!-- 상품명 -->
									<td class="3"><a href="#" data-toggle="modal"
										data-target="#myModal" class="updateModal"
										data-relate="${product.relate}" data-column="itemName"
										data-before="${product.itemName}"
										data-id="${product.productID}">${product.itemName}</a></td>
									<!-- 색상명 -->
									<td class="4"><a href="#" data-toggle="modal"
										data-target="#myModal" class="updateModal"
										data-column="colorName" data-before="${product.colorName}"
										data-id="${product.productID}">${product.colorName}</a></td>
									<!-- 재고량 -->
									<td class="5"><a href="#" data-toggle="modal"
										data-target="#myModal" class="updateModal" data-column="stock"
										data-before="${product.stock}" data-id="${product.productID}">
											<c:choose>
												<c:when test="${product.stock eq -1 }">
												알 수 없음
												</c:when>
												<c:otherwise>
												${product.stock}마
												</c:otherwise>
											</c:choose>

									</a></td>
									<%-- <!-- 홍보문구 -->
											<td class="6"><a href="#" data-toggle="modal"
												data-target="#myModal" class="updateModal"
												data-relate="${product.relate}"
												data-column="expl" data-before='${product.expl}'
												data-id="${product.productID}">홍보문구</a></td>
											${product.expl}

											<!-- 이미지 -->
											<td class="7"><a href="#" data-toggle="modal"
												data-target="#myModal" class="updateModal"
												data-column="image" data-id="${product.productID}">이미지</a></td>
											${product.mainImg} --%>

									<!-- 소매가 -->
									<td class="6"><a href="#" data-toggle="modal"
										data-target="#myModal" class="updateModal"
										data-relate="${product.relate}" data-column="retail"
										data-before="${product.retail}" data-id="${product.productID}">${product.retail}원</a></td>
									<!-- 도매가 -->
									<td class="7"><a href="#" data-toggle="modal"
										data-target="#myModal" class="updateModal"
										data-relate="${product.relate}" data-column="wholesale"
										data-before="${product.wholesale}"
										data-id="${product.productID}">${product.wholesale}원</a></td>
									<!-- 도매기준 -->
									<td class="8"><a href="#" data-toggle="modal"
										data-target="#myModal" class="updateModal"
										data-relate="${product.relate}" data-column="standard"
										data-before="${product.standard}"
										data-id="${product.productID}">${product.standard}마</a></td>
									<!-- 스와치 제공 -->
									<td class="9"><a href="#" data-toggle="modal"
										data-target="#myModal_swatch" class="updateModal_swatch"
										data-relate="${product.relate}" data-column="swatch"
										data-before="${product.swatch}" data-id="${product.productID}">${product.swatch}</a></td>
									<!-- 폭 -->
									<td class="10"><a href="#" data-toggle="modal"
										data-target="#myModal" class="updateModal"
										data-relate="${product.relate}" data-column="width"
										data-before="${product.width}" data-id="${product.productID}">${product.width}cm</a></td>
									<!-- 두께 -->
									<td class="11"><a href="#" data-toggle="modal"
										data-target="#myModal_thick" class="updateModal_thick"
										data-relate="${product.relate}" data-column="thick"
										data-before="${product.thick}" data-id="${product.productID}">${product.thick}</a></td>
									<!-- 등록일 -->
									<td class="12">${product.postDate }</td>
								</tr>

							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<!-- Modal -->
			<div class="modal fade" id="myModal" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title" id="modal_column2">Modal Header</h4>
						</div>
						<div class="modal-body">
							<form method="post" name="frm2">
								<!-- diplay none -->
								<input type="text" class="form-control" name="modal_productId"
									id="modal_productId" style="display: none;"> <input
									type="text" class="form-control" name="modal_column"
									id="modal_column" style="display: none">
								<!-- ----------- -->

								<div class="form-group">
									<div class="col-md-5">
										<p id="modal_before"></p>
									</div>
									<div class="col-md-2">
										<i class="fa fa-arrow-right"></i>
									</div>
									<div class="col-md-5">
										<input type="text" class="form-control" name="modal_input"
											id="modal_input" maxlength="12">
										<div style="display: none" id="stock_div">
											<input type="checkbox" name="stock_unknown"
												id="stock_unknown">알 수 없음
										</div>
									</div>
								</div>
								<br> <br> <br>

							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary"
								onclick="updateProduct(document.frm2.modal_productId.value,document.frm2.modal_column.value,document.frm2.modal_input.value)">변경</button>
							<button type="button" class="btn btn-default"
								data-dismiss="modal">닫기</button>
						</div>
					</div>

				</div>
			</div>

			<!-- Modal 스와치 -->
			<div class="modal fade" id="myModal_swatch" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title" id="modal_column2_swatch">스와치 제공 변경</h4>
						</div>
						<div class="modal-body">
							<form method="post" name="frm">
								<!-- diplay none -->
								<input type="text" class="form-control"
									name="modal_productId_swatch" id="modal_productId_swatch"
									style="display: none;">
								<!-- ----------- -->
								<div>
									<div>
										<input type="radio" name="swatch" value="제공" id="1">제공
										<input type="radio" name="swatch" value="사업자에게만 제공" id="2">사업자에게만
										제공 <input type="radio" name="swatch" value="미제공" id="3">미제공
									</div>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary"
								onclick="updateProductSwatch(document.frm.modal_productId_swatch.value)">변경</button>
							<button type="button" class="btn btn-default"
								data-dismiss="modal">닫기</button>
						</div>
					</div>

				</div>
			</div>
			<!-- Modal 두께 -->
			<div class="modal fade" id="myModal_thick" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title" id="modal_column2_thick">두께 변경</h4>
						</div>
						<div class="modal-body">
							<form method="post" name="frm3">
								<!-- diplay none -->
								<input type="text" class="form-control"
									name="modal_productId_thick" id="modal_productId_thick"
									style="display: none;">
								<!-- ----------- -->
								<div>
									<div>
										<input type="radio" name="thick" value="매우 얇음" id="t1">매우
										얇음 <input type="radio" name="thick" value="얇음" id="t2">얇음
										<input type="radio" name="thick" value="보통" id="t3">보통
										<input type="radio" name="thick" value="두꺼움" id="t4">두꺼움
										<input type="radio" name="thick" value="매우 두꺼움" id="t5">매우
										두꺼움
									</div>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary"
								onclick="updateProductThick(document.frm3.modal_productId_thick.value)">변경</button>
							<button type="button" class="btn btn-default"
								data-dismiss="modal">닫기</button>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

</body>
<script>


	//sortable

	$(function() {
		$("#resizeTable")

			.tablesorter({
				widgets : [ 'zebra', 'filter', 'resizable' ],
				widgetOptions : {
					// filter_anyMatch replaced! Instead use the filter_external option
					// Set to use a jQuery selector (or jQuery object) pointing to the
					// external filter (column specific or any match)
					filter_external : '.search',
					// add a default type search to the first name column
					filter_defaultFilter : {
						1 : '~{query}'
					},
					// include column filters
					filter_columnFilters : true,
					filter_saveFilters : true,
					filter_reset : '.reset',
					filter_liveSearch : {
						// when false, the user must press enter to blur the input to trigger the search
						3 : false,
						// the query will initiate when 5 or more characters are entered into the filter
						4 : 5,
						// no live search on the last three columns (using a header class name)
						'.last-3-columns' : false,
						// for columns that aren't defined; this will set the fallback value
						// otherwise the fallback defaults to false.
						'fallback' : true
					},

					resizable_addLastColumn : true,
					resizable_widths : [ '40px', '50px', '120px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '170px', '170px' ]
				}
			});
		$('button[data-column]').on('click', function() {
			var $this = $(this),
				totalColumns = $table[0].config.columns,
				col = $this.data('column'), // zero-based index or "all"
				filter = [];

			// text to add to filter
			filter[col === 'all' ? totalColumns : col] = $this.text();
			$table.trigger('search', [ filter ]);
			return false;
		});

	});

	//modal 클릭시 input value 설정
	$(document).on("click", ".updateModal", function() {
		var column = $(this).data('column');
		var productId = $(this).data('id');
		var before = $(this).data('before');
		var column2;
		$("#modal_productId").val(productId);
		$("#modal_column").val(column);
		$("#stock_div").css("display", "none");
		switch (column) {
		case 'itemName':
			column2 = '상품명 변경';
			break;
		case 'colorName':
			column2 = '색상명 변경';
			break;
		case 'stock':
			column2 = '재고량 변경';
			if (before == -1) {
				before = '알 수 없음';
			}
			$("#stock_div").css("display", "");
			break;
		case 'expl':
			column2 = '홍보문구 변경';
			break;
		case 'retail':
			column2 = '소매가 변경';
			break;
		case 'wholesale':
			column2 = '도매가 변경';
			break;
		case 'standard':
			column2 = '도매 기준 변경';
			break;
		case 'swatch':
			column2 = '스와치 제공 변경';
			break;
		case 'width':
			column2 = '폭 변경';
			break;
		case 'thick':
			column2 = '두께 변경';
			break;

		}
		$("#modal_column2").text(column2);
		$("#modal_before").text(before);

	});

	//modal 클릭시 input value 설정-스와치
	$(document).on("click", ".updateModal_swatch", function() {
		var productId = $(this).data('id');
		var before = $(this).data('before');
		$("#modal_productId_swatch").val(productId);

		switch (before) {
		case '제공':
			$("#1").prop("checked", true);
			break;
		case '사업자에게만 제공':
			$("#2").prop("checked", true);
			break;
		case '미제공':
			$("#3").prop("checked", true);
			break;
		}
		$("#modal_before").text(before);

	});

	//modal 클릭시 input value 설정-두께
	$(document).on("click", ".updateModal_thick", function() {
		var productId = $(this).data('id');
		var before = $(this).data('before');
		$("#modal_productId_thick").val(productId);

		switch (before) {
		case '매우 얇음':
			$("#t1").prop("checked", true);
			break;
		case '얇음':
			$("#t2").prop("checked", true);
			break;
		case '보통':
			$("#t3").prop("checked", true);
			break;
		case '두꺼움':
			$("#t4").prop("checked", true);
			break;
		case '매우 두꺼움':
			$("#t5").prop("checked", true);
			break;
		}

		$("#modal_before").text(before);

	});




	$(document).on('change', '#stock_unknown', function(e) {
		var unknown = $("#modal_input");
		if (this.checked) {
			unknown.val("-1");
			unknown.attr("disabled", true);
		} else {
			unknown.removeAttr("disabled");
			unknown.val("");
		}

	});
	/* 	var postbackSample = function() {
			$("#resizeTable").colResizable({
				resizeMode : 'overflow',
				minWidth : 70,
				liveDrag : true,
				postbackSafe : true,
				partialRefresh : true
			});
		} */

	/* $(function() {
		//resizable Table
		$("#resizeTable").colResizable({
			resizeMode : 'overflow',
			liveDrag : true,
			partialRefresh:true
		}); */
	//아이템 수정

	$(function() {
		//postbackSample();

		//선택 삭제
		$("#sel_del").click(function() {
			if (confirm("정말로 삭제하시겠습니까?") == true) { //확인
				$("input[name=checktd]:checkbox").each(function() {
					if (this.checked) {

						console.log(this.value + " 삭제")
						$.ajax({
							url : '../SidServlet?command=delete_product',
							data : 'productId=' + this.value,
							success : function(data) {
								if (data == "1") {

									console.log("상품 삭제 완료");

								} else {
									console.log("상품 삭제 실패");
								}
								location.reload();

							}
						});
					}
				});
			} else { //취소
				return;
			}



		});

		//판매중으로 상태 변경
		$("#onsale").click(function() {
		
			var pList=new Array();
			$("input[name=checktd]:checkbox").each(function() {
				if (this.checked) {
					pList.push(this.value);
				}
			});
			
			
				$.ajax({
					url : '../SidServlet?command=update_state',
					data : 'productId=' + pList+ '&state=on',
					success : function(data) {
						if (data >0) {
							alert("판매상태 변경완료");
						} else {
							alert("판매상태 변경실패 : 관리자에게 문의하세요");
						}
						location.reload();

					}
				});
			
			
		});
		//판매중지로 상태 변경
		$("#stopsale").click(function() {
			var pList=new Array();
		
			$("input[name=checktd]:checkbox").each(function() {
				if (this.checked) {
					pList.push(this.value);
				}
			});
			$.ajax({
				url : '../SidServlet?command=update_state',
				data : 'productId=' + pList + '&state=stop',
				success : function(data) {
					if (data>0) {
						alert("판매상태 변경완료");

					} else {
						alert("판매상태 변경실패 : 관리자에게 문의하세요");
					}
					location.reload();

				}
			});
		});

		//테이블 체크박스 전체 선택,해제
		$("#checkth").click(function() {
			if ($("#checkth").prop("checked")) {
				$("input[name=checktd]:checkbox").each(function() {
					this.checked = true;
				});
			} else {
				$("input[name=checktd]:checkbox").each(function() {
					this.checked = false;
				});
			}

		});
		//개별 선택시 전체선택 버튼 해제
		$("input[name=checktd]:checkbox").click(function() {
			if ($("#checkth").prop("checked")) {
				$("#checkth").prop("checked", false);
			}
		});



		//항목 체크 박스 모두 체크
		$("#checkAll").click(function() {
			$("input[name=box]:checkbox").each(function() {
				this.checked = true;
				if (this.checked) {
					console.log("check : " + this.value);
					$("." + this.value).show();
				}
			});
		});

		//항목 체크 박스 모두 해제
		$("#uncheckAll").click(function() {
			$("input[name=box]:checkbox").each(function() {
				this.checked = false;
				if (!this.checked) {
					console.log("uncheck : " + this.value);
					$("." + this.value).hide();
				}
			});
		});
		//항목 체크 박스 선택, 해제
		$("input[name=box]:checkbox").click(function() {
			if (this.checked) {
				$("." + this.value).show();
			} else {
				$("." + this.value).hide();
			}

		});
	});


	//modal
	function updateProduct(productId, column, input) {
		$('#myModal').modal('hide');
		if (input == "") {
			alert("값을 입력하세요.");
		} else {
			$.ajax({
				url : '../SidServlet?command=update_product',
				data : 'productId=' + encodeURIComponent(productId) + "&column=" + encodeURIComponent(column) + "&input=" + encodeURIComponent(input),
				contentType : "application/x-www-form-urlencoded; charset=UTF-8",
				success : function(data) {
					if (data == "1") {
						alert("변경되었습니다.");
						history.go(0);
					} else {
						alert("정확한 값을 입력하세요.");
					}

				}
			});
		}
	}

	function updateProductThick(productId) {
		updateProduct(productId, 'thick', $("input[type=radio][name=thick]:checked").val());
	}

	function updateProductSwatch(productId) {
		updateProduct(productId, 'swatch', $("input[type=radio][name=swatch]:checked").val());
	}

	//전부 수정하기
	function modifyItem(productId) {
		var pop_title = "popupOpener";
		var url = "../SidServlet?command=before_modify_product&productId=" + productId;
		console.log("productId : " + productId);
		window.open("", pop_title, "toolbar=no, menubar=no, scrollbars=yes, resizable=yes, width=1200, height=800");
		var frmData = document.frm;
		frmData.target = pop_title ;
		frmData.action = "../SidServlet?command=before_modify_product&productId=" + productId;
		frmData.submit() ;
	}
</script>
<%@ include file="../include/footer.jsp"%>
</html>