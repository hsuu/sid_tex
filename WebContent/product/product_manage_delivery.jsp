<%@page import="com.sid.dto.UserVO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../include/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<style>
#caution {
	font-size: 9pt;
	color: grey;
}
</style>
<body>

	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">
			<li role="presentation"><a
				href="../SidServlet?command=purchase_list&state=0">주문 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_manage_stock">재고 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=product_upload">상품 등록</a></li>
			<li role="presentation"><a
				href="../product/product_manage_balance_day.jsp">정산 관리</a></li>
			<li role="presentation" class="active"><a
				href="../SidServlet?command=list_manage_delivery">배송 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_manage_customer">고객 관리</a></li>
		</ul>
	</div>
	<br>
	<div class="panel panel-default col-md-8 col-md-offset-2">
		<div class="panel-body">
			<div class="text-center">
				<h1>
					<i class="fa fa-edit"></i>
				</h1>
				<h2 class="text-center">배송 정보 수정</h2>
				<br>
			</div>
			<div>
				<form method="post" name="frm" id="delivery">
					<div style="text-align: left">
						<div class="col-md-12">
							<div>
								<h3>일반 배송 안내</h3>
								<div>
									예상되는 배송 기간 : 주문 이후 <input type="number" style="width:10%" name="delivery_period" value="${list.delivery_period }">일 이내 발송
								</div>
								<div>거래 관련 처리 안내 : 본 사이트에서 진행되는 거래의 책임과 배송, 교환, 환불, 민원은 직접
									관리하고 있음</div>
								<div>소비자와 사업자 사이의 분쟁처리 : 소비자분쟁해결기준(공정거래위원회 고시) 및 관계법령에 따름</div>
							</div>
						</div>
						<div class="panel-body col-md-6">
							<c:choose>
								<c:when test="${list.delivery ==1 }">
									<h3>
										<input type="checkbox" name="delivery" value="1" checked>퀵
									</h3>
								</c:when>
								<c:when test="${list.delivery ==11 }">
									<h3>
										<input type="checkbox" name="delivery" value="1" checked>퀵
									</h3>
									</c:when>
								<c:otherwise>
									<h3>
										<input type="checkbox" name="delivery" value="1">퀵
									</h3>
								</c:otherwise>
							</c:choose>
							<div>
								배송사 : <input type="text" name="deliveryCompany_quick" style="width:45%" value="${list.deliveryCompany_quick }"> <br> 배송료 : 착불(거리에 따라 상이)<br>
								교환배송비 : 착불(환불금액에서 차감)<br> 반품배송비 : 착불(환불금액에서 차감)
							</div>
						</div>
					</div>
					<div class="panel-body col-md-6">
						<c:choose>
							<c:when test="${list.delivery ==10 }">
								<h3>
									<input type="checkbox" name="delivery" value="10" checked>택배
								</h3>
							</c:when>
							<c:when test="${list.delivery ==11 }">
								<h3>
									<input type="checkbox" name="delivery" value="10" checked>택배
								</h3>
							</c:when>
							<c:otherwise>
								<h3>
									<input type="checkbox" name="delivery" value="10">택배
								</h3>
							</c:otherwise>
						</c:choose>
						<div>
							<%
								UserVO vo = (UserVO) request.getAttribute("list");
							%>
							배송사 : <input type="text" name="deliveryCompany"
								value="${list.deliveryCompany }" style="width:30%"> <br>
							
							배송료 : <input type="radio" name="delivery_detail" value="무료"
								class="detail1"
								<%if (vo.getDelivery_costway().equals("무료")) {
									out.print("checked");
								}%>>무료<br>

							<input type="radio" name="delivery_detail" value="유료"
								<%if (vo.getDelivery_costway().equals("유료")) {
									out.print("checked");
								}%>
								class="detail2">&nbsp;유료&nbsp;<input type="number"
								name="delivery_cost" class="cost1" style="width: 30%" min="0"
								<%if (vo.getDelivery_costway().equals("유료")) {
									out.print("value='" + vo.getDelivery_cost() + "'");
								}%>>원<br>

							<input type="radio" name="delivery_detail" value="조건부무료"
								<%if (vo.getDelivery_costway().equals("조건부무료")) {
									out.print("checked");
								}%>
								class="detail3">&nbsp;조건부무료<br> <input type="number"
								name="delivery_cost3" class="cost3" style="width: 50%" min="0"
								<%if (vo.getDelivery_costway().equals("조건부무료")) {
									out.print("value='" + vo.getDelivery_cost2() + "'");
								}%>>원
							<input type="number" name="delivery_cost2" class="cost2"
								style="width: 50%" min="0" min="0"
								<%if (vo.getDelivery_costway().equals("조건부무료")) {
									out.print("value='" + vo.getDelivery_cost() + "'");
								}%>>원
							이상 무료<br>
편도 택배비 : <input type="number" name="re_delivery_cost1" min="0"
									style="width: 30%" value="${list.re_delivery_cost1 }">원<br>
						</div>
					</div>


					<div class="panel-body col-md-12">
						<h3>교환/반품 안내</h3>
						<div>
							보내실 곳 :
							<div class="form-group form-inline">
											<input type="text" class="form-control" id="sample6_postcode"
												name="returnZipnum" placeholder="우편번호" readonly value="${list.returnZipnum}">
											<button type="button" class="btn btn-primary"
												onclick="sample6_execDaumPostcode()">우편번호 찾기</button>
										</div>

										<div class="form-group">
											<input type="text" class="form-control" id="sample6_address"
												placeholder="주소" name="returnRoadAddrPart1" value="${list.returnRoadAddrPart1}">

										</div>
										<div class="form-group">
											<input type="text" class="form-control" id="sample6_address2"
												placeholder="상세주소" name="returnAddrDetail" value="${list.returnAddrDetail}">
										</div>
										 <br> (구매자 귀책사유로 인한 교환 및 반품 시에는 구매자가, 판매자
							귀책사유로 인한 교환 및 반품 시에는 판매자가 교환 및 반품 배송비용을 부담합니다.)<br> <br>
							- 원단은 롤에서 고객님의 주문수량만큼 컷팅되어서 배송이 되므로 고객님의 단순변심에 의한 교환 및 반품은 불가합니다.<br>
							- 상품의 이미지는 모니터 사양에 따라 약간의 색상차이가 있을 수 있으며, 원단 재가공시 약간의 색상차이가 나는
							부분은 원단 가공시 나타나는 현상이므로 교환 및 반품이 불가능합니다.<br> - 세탁, 재단, 미싱 등으로
							원단이 훼손되거나 변형된 경우는 교환 및 반품이 불가능합니다.<br> - 상품 주문후 주문상태가 상품준비중일
							경우에는 교환 및 반품이 불가능합니다.
						</div>

						<h3>교환/반품 기준</h3>
						<div>
							상품 수령 후 7일 이내에 신청하실 수 있습니다. 단, 제품이 표시광고 내용과 다르거나 불량 등 계약과 다르게 이행된
							경우는 <br> 제품 수령일부터 3개월 이내, 그 사실을 안 날 또는 알 수 있었던 날로부터 30일 이내에
							교환/반품이 가능합니다.<br> <br> 단, 다음의 경우 해당하는 반품/교환은 불가능할 수
							있습니다.<br> - 소비자의 책임 있는 사유로 상품 등이 멸실 또는 훼손된 경우<br> - 소비자의 사용 또는
							소비에 의해 상품 등의 가치가 현저히 감소한 경우<br> - 시간의 경과에 의해 재판매가 곤란할 정도로 상품
							등의 가치가 현저히 감소한 경우<br> - 복제가 가능한 상품 등의 포장을 훼손한 경우<br> -
							소비자의 주문에 따라 개별적으로 생산되는 상품이 제작에 들어간 경우
						</div>

						<h3>주의사항</h3>
						<div id="caution">
							전자상거래 등에서의 소비자보호에 관한 법률에 의한 반품규정이 판매자가 지정한 반품 조건보다 우선합니다.<br>
							전자상거래 등에서의 소비자 보호에 관한 법률에 의거하여 미성년자가 물품을 구매하는 경우, 법정대리인이 동의하지 않으면
							미성년자 본인 또는 법정대리인이 구매를 취소할 수 있습니다.<br> 시드텍스에 등록된 판매상품과 상품의
							내용은 판매자가 등록한 것으로 (주)시드월드엔터프라이즈에서는 그 등록내역에 대하여 일체의 책임을 지지 않습니다.<br>
							시드텍스의 결제시스템을 이용하지 않고 판매자와 직접 거래하실 경우 상품을 받지 못하거나 구매한 상품과 상이한 상품을
							받는 등 피해가 발생할 수 있으니 유의하시기 바랍니다.
						</div>
					</div>
					<div class="form-group">
							<button type="button" class="btn btn-primary col-md-12"
								onclick=" updateDelivery()">저장</button>
							<br> <br>
						</div>
				</form>
			</div>
		</div>
	</div>

	<%-- 
				<form method="post" name="frm" id="delivery">
					<div class="form-group">
						<table class="table" id="seller_table">
							<tr>
								<td class="title" style="width: 100px;">방법</td>
								<td style="width: 150px;">비용</td>
								<td class="title" style="width: 100px;">결제</td>
							</tr>
							<tr>
								<c:choose>
									<c:when test="${list.delivery ==1 }">
										<td class="title"><input type="checkbox" name="delivery"
											value="1" checked>퀵</td>
									</c:when>
									<c:when test="${list.delivery ==11 }">
										<td class="title"><input type="checkbox" name="delivery"
											value="1" checked>퀵</td>
									</c:when>
									<c:otherwise>
										<td class="title"><input type="checkbox" name="delivery"
											value="1">퀵</td>
									</c:otherwise>
								</c:choose>
								<td></td>
								<td class="title"></td>
							</tr>
							<tr>
								<c:choose>
									<c:when test="${list.delivery ==10 }">
										<td class="title"><input type="checkbox" name="delivery"
											value="10" checked>택배</td>
									</c:when>
									<c:when test="${list.delivery ==11 }">
										<td class="title"><input type="checkbox" name="delivery"
											value="10" checked>택배</td>
									</c:when>
									<c:otherwise>
										<td class="title"><input type="checkbox" name="delivery"
											value="10">택배</td>
									</c:otherwise>
								</c:choose>
								
								<td class="title">
									<input type="radio" name="delivery_detail" value="무료" class="detail1" <%if(vo.getDelivery_costway().equals("무료")){out.print("checked");} %>>무료<br>
									
									<input type="radio" name="delivery_detail" value="유료" min="0" <%if(vo.getDelivery_costway().equals("유료")){out.print("checked");} %> class="detail2">유료
									<input type="number" name="delivery_cost" class="cost1"	style="width: 50%" <%if(vo.getDelivery_costway().equals("유료")){out.print("value='"+vo.getDelivery_cost()+"'");} %>>원<br>
									
									<input type="radio" name="delivery_detail" value="조건부무료" min="0" <%if(vo.getDelivery_costway().equals("조건부무료")){out.print("checked");} %> class="detail3">조건부무료<br>
									<input type="number" name="delivery_cost3" class="cost3" style="width: 50%" min="0" <%if(vo.getDelivery_costway().equals("조건부무료")){out.print("value='"+vo.getDelivery_cost2()+"'");} %>>원
									<input type="number" name="delivery_cost2" class="cost2" style="width: 50%" min="0" <%if(vo.getDelivery_costway().equals("조건부무료")){out.print("value='"+vo.getDelivery_cost()+"'");} %>>원 이상 무료
									
								</td>
								
								<td><c:choose>
										<c:when test="${list.delivery_way ==1 }">
											<input type="checkbox" name="delivery_way" value="1" checked
												class="way1">선불<br>
											<input type="checkbox" name="delivery_way" value="10"
												class="way2">후불
									</c:when>
										<c:when test="${list.delivery_way ==10 }">
											<input type="checkbox" name="delivery_way" value="1"
												class="way1">선불<br>
											<input type="checkbox" name="delivery_way" value="10" checked
												class="way2">후불
									</c:when>
										<c:when test="${list.delivery_way ==11 }">
											<input type="checkbox" name="delivery_way" value="1" checked
												class="way1">선불<br>
											<input type="checkbox" name="delivery_way" value="10" checked
												class="way2">후불
									</c:when>
										<c:otherwise>
											<input type="checkbox" name="delivery_way" value="1"
												class="way1">선불<br>
											<input type="checkbox" name="delivery_way" value="10"
												class="way2">후불
									</c:otherwise>
									</c:choose></td>
							</tr>
						</table>
						<table class="table" id="seller_table">
							<h3 style="text-align: center">반품 배송비</h3>
							<tr>
								<td style="width: 60px;">퀵</td>
								<td style="width: 100px;">판매자가 우선 지급 후, 환불금에서 차감</td>
							</tr>
							<tr>
								<td>택배</td>
								<td>편도 택배비 : <input type="number" name="re_delivery_cost1"
									style="width: 30%" value="${list.re_delivery_cost1 }">원<br>
									판매자 지정 택배사 : <input type="text" name="deliveryCompany"
									value="${list.deliveryCompany }" style="width: 30%">
								</td>
							</tr>
						</table>
						<div class="form-group">
							<button type="button" class="btn btn-primary col-md-12"
								onclick=" updateDelivery()">저장</button>
							<br> <br>
						</div>
					</div>
				</form>
 --%>
</body>
<script>
	$(".detail1").on('click', function() {
		$('.cost1').val("");
		$('.cost2').val("");
		$('.cost3').val("");
		$('.way1').prop("checked", false);
		$('.way2').prop("checked", false);
		$(".way1").attr("disabled", true);
		$(".way2").attr("disabled", true);
	})
	$(".detail2").on('click', function() {
		$('.cost2').val("");
		$('.cost3').val("");
		$(".way1").attr("disabled", false);
		$(".way2").attr("disabled", false);
	})
	$(".detail3").on('click', function() {
		$('.cost1').val("");
		$(".way1").attr("disabled", false);
		$(".way2").attr("disabled", false);
	})
	function updateDelivery() {
		/* 	if (document.frm2.pwd.value == "") {
				alert("비밀번호는 반드시 입력해야 합니다.");
				frm2.pwd.focus();
				return false;
			} */
		//정보 수정 ajax
		var flag = 1;	

		if(document.frm.delivery_period.value.length==0){
			alert("예상 배송 기간을 입력하세요.")
			frm.delivery_period.focus();
			flag=0;
			return false;
		} 
	
		var del="";
		$("input[name=delivery]:checked").each(function(){
			del+=$(this).val();
		});
		if(del=="1"){//퀵
			if(document.frm.deliveryCompany_quick.value.length==0){
				alert("퀵 배송사를 입력하세요.");
				frm.deliveryCompany_quick.focus();
				flag=0;
				return false;
			}
		}else if(del=="10"){//택배
			if(document.frm.deliveryCompany.value.length==0){
				alert("택배 배송사를 입력하세요.");
				frm.deliveryCompany.focus();
				flag=0;
				return false;
			}
			if($("input[name=delivery_detail]:checked").val()==undefined){
				alert("택배 배송료를 체크하세요.");
				flag=0;
				return false;
			}
			if($("input[name=delivery_detail]:checked").val()=="유료"){
				if(document.frm.delivery_cost.value.length==0){
					alert("유료 배송료를 입력하세요.");
					frm.delivery_cost.focus();
					flag=0;
					return false;
				}
				
			}else if($("input[name=delivery_detail]:checked").val()=="조건부무료"){
				if(document.frm.delivery_cost3.value.length==0){
					alert("조건부 무료 배송료를 입력하세요.");
					frm.delivery_cost3.focus();
					flag=0;
					return false;
				}	
				if(document.frm.delivery_cost2.value.length==0){
					alert("조건부 무료 배송료를 입력하세요.");
					frm.delivery_cost2.focus();
					flag=0;
					return false;
				}	
				
			}
		}else if(del=="110"){//퀵,택배
			if(document.frm.deliveryCompany_quick.value.length==0){
				alert("퀵 배송사를 입력하세요.");
				frm.deliveryCompany_quick.focus();
				flag=0;
				return false;
			}
			if(document.frm.deliveryCompany.value.length==0){
				alert("택배 배송사를 입력하세요.");
				frm.deliveryCompany.focus();
				flag=0;
				return false;
			}
			if($("input[name=delivery_detail]:checked").val()==undefined){
				alert("택배 배송료를 체크하세요.");
				flag=0;
				return false;
			}
			if($("input[name=delivery_detail]:checked").val()=="유료"){
				if(document.frm.delivery_cost.value.length==0){
					alert("유료 배송료를 입력하세요.");
					frm.delivery_cost.focus();
					flag=0;
					return false;
				}
				
			}else if($("input[name=delivery_detail]:checked").val()=="조건부무료"){
				if(document.frm.delivery_cost2.value.length==0){
					alert("조건부 무료 배송료를 입력하세요.");
					frm.delivery_cost2.focus();
					flag=0;
					return false;
				}	
				if(document.frm.delivery_cost3.value.length==0){
					alert("조건부 무료 배송료를 입력하세요.");
					frm.delivery_cost3.focus();
					flag=0;
					return false;
				}	
			}
		}else{
			alert("최소한 한개의 배송방법을 선택하세요.");
			flag=0;
			return false;
		}
		
		if(document.frm.re_delivery_cost1.value.length==0){
			alert("편도 택배비를 입력하세요.");
			frm.re_delivery_cost1.focus();
			flag=0;
			return false;
		}
		if($("#sample6_postcode").val()==""){
			alert("교환/반품 주소지 우편번호를 입력하세요.")
			flag=0;
		}else if($("#sample6_address").val()==""){
			alert("교환/반품 주소지 주소를 입력하세요.")
			flag=0;
		}else if($("#sample6_address2").val()==""){
			alert("교환/반품 주소지 상세주소를 입력하세요.")
			flag=0;
		}

		if (flag == 1) {

			$.ajax({
				url : "../SidServlet?command=update_delivery_info",
				data : $("#delivery").serialize(),
				success : function(data) {
					if (data == 1) {
						alert("배송 정보가 수정되었습니다.");

					} else {
						alert("수정 실패");
					}
					location.reload();
				}
			});
		}
	}
	function sample6_execDaumPostcode() {
		new daum.Postcode({
			oncomplete : function(data) {
				// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

				// 각 주소의 노출 규칙에 따라 주소를 조합한다.
				// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
				var fullAddr = ''; // 최종 주소 변수
				var extraAddr = ''; // 조합형 주소 변수

				// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
				if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
					fullAddr = data.roadAddress;

				} else { // 사용자가 지번 주소를 선택했을 경우(J)
					fullAddr = data.jibunAddress;
				}

				// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
				if (data.userSelectedType === 'R') {
					//법정동명이 있을 경우 추가한다.
					if (data.bname !== '') {
						extraAddr += data.bname;
					}
					// 건물명이 있을 경우 추가한다.
					if (data.buildingName !== '') {
						extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
					}
					// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
					fullAddr += (extraAddr !== '' ? ' (' + extraAddr + ')' : '');
				}

				// 우편번호와 주소 정보를 해당 필드에 넣는다.
				document.getElementById('sample6_postcode').value = data.zonecode; //5자리 새우편번호 사용
				document.getElementById('sample6_address').value = fullAddr;

				// 커서를 상세주소 필드로 이동한다.
				document.getElementById('sample6_address2').focus();
			}
		}).open();
	}
</script>
</html>
<%@ include file="../include/footer.jsp"%>
