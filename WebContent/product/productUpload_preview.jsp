<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy"%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="com.sid.dto.UserVO"%>
<%@page import="com.sid.dao.MemberDAO"%>
<%@page import="javax.servlet.http.HttpSession" %>
<!DOCTYPE html>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>SID-TEX</title>

<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="../css/cover.css" rel="stylesheet">


<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="../js/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/docs.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../js/ie10-viewport-bug-workaround.js"></script>
<script src="../js/colResizable-1.6.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link href="../css/custom.css" rel="stylesheet">
<!-- table sorter -->
<link rel="stylesheet" href="../css/theme.default.css">
<script type="text/javascript" src="../js/table/jquery.tablesorter.js"></script>
<script type="text/javascript" src="../js/table/widget-filter.js"></script>
<script type="text/javascript" src="../js/table/widget-resizable.js"></script>
<script type="text/javascript" src="../js/table/widget-stickyHeaders.js"></script>
<script type="text/javascript" src="../js/table/widget-print.js"></script>
<style>
@import url(http://fonts.googleapis.com/earlyaccess/hanna.css);
body {
   font-family: 'Hanna', serif;
}

#caution {
	font-size: 9pt;
	color : grey;
}
.left {
	font-weight: bold;
}

h4 {
	color: #424242;
}

.panel-body {
	border: 2px solid #A4A4A4;
}

.panel {
	border: none;
}

.col-md-4 {
	padding-top: 1.5%;
}

h2 {
	margin-top: 0;
}
.colorDiv {
	padding: 5px
}

#seller_table th, td {
	text-align: center;
}

table, td, th {
	border: 1px solid #ddd;
	text-align: left;
}

table {
	border-collapse: collapse;
	width: 100%;
}

th, td {
	padding: 15px;
}

.title {
	background-color: #585858;
	color: white;
	text-align: center;
}

.table td {
	text-align: center;
}
</style>
<body>
	<div class="all">
		<div class="col-md-12">
			<div class="col-md-6">
				<%
					int sizeLimit = 20 * 1024 * 1024;
					//String savePath = "/tomcat/webapps/ROOT/img";		//서버
					//String savePath = "C:/Users/hs/sid_tex/sid_tex/WebContent/img"; //놋북
					String savePath="C:/Users/sid/git/sid_tex/WebContent/img";		//아이맥
					MultipartRequest multi = new MultipartRequest(request, savePath, sizeLimit, "UTF-8",
							new DefaultFileRenamePolicy());
				%>
				<div class="form-group" style="text-align: center">
					<img src="<%=multi.getParameter("mainImg1")%>"
						style="max-width: 80%; width: 100%;">
				</div>
			</div>
			<div class="col-md-6" style="padding: 15px;">
				<form>
					<div class="form-group row">
						<label class="col-sm-4 control-label">상품명</label>
						<div class="col-sm-8">
							<p>
								<%=multi.getParameter("itemName")%>
							</p>
						</div>
						<label class="col-sm-4 control-label">색상명</label>
						<div class="col-sm-8">
							<p><%=multi.getParameter("colorName1")%></p>
						</div>
						<label class="col-sm-4 control-label">판매자명</label>
						<div class="col-sm-8">
							<p><%=session.getAttribute("company")%></p>
						</div>
						<%-- <label class="col-sm-4 control-label">배송방법</label>
						<div class="col-sm-8">
							<p>
								<%
									int exp2 = 0;
									String[] express2 = multi.getParameterValues("express");
									if (express2 != null) {
										for (int i = 0; i < express2.length; i++) {
											exp2 += Integer.parseInt(express2[i]);
										}
										if (exp2 == 1) {
											out.println("퀵");
										} else if (exp2 == 10) {
											out.println("택배");
										} else if (exp2 == 11) {
											out.println("퀵, 택배");
										}
									}
								%>
							</p>
						</div> --%>
						<label class="col-sm-4 control-label">폭</label>
						<div class="col-sm-8">
							<p><%=multi.getParameter("width_radio")%>cm
							</p>
						</div>
						<label class="col-sm-4 control-label">두께</label>
						<div class="col-sm-8">
							<%
								if (multi.getParameter("thick") == null) {
							%>
							<p>선택안함</p>
							<%
								} else {
							%>
							<p><%=multi.getParameter("thick")%>
							</p>
							<%
								}
							%>
						</div>
						<label class="col-sm-4 control-label">스와치</label>
						<div class="col-sm-8">
							<p><%=multi.getParameter("swatch")%>
							</p>
						</div>
						<label class="col-sm-4 control-label">소매가</label>
						<div class="col-sm-8">
							<p><%=multi.getParameter("retail")%>원
							</p>
						</div>
						<label class="col-sm-4 control-label">도매가</label>
						<div class="col-sm-8">
							<p><%=multi.getParameter("wholesale")%>원
							<p id="wholesale"><%=multi.getParameter("wholesale")%>원&nbsp;(<%=multi.getParameter("standard")%>↑)
							</p>
						</div>
						<div class="col-sm-12">
							<label class="col-sm-4 control-label" style="padding-left: 0"><%=multi.getParameter("colorName1")%>(재고 : <%=multi.getParameter("stock1") %> )</label>
							<div class="col-sm-3" style="padding-left: 0">
								<input type="number" name="quantity"
									onchange="getCost(${product.retail},${product.wholesale},${product.standard},this.value)"
									class="quantity col-md-8" style="padding-right: 0" min="1"
									step="1" value="1">
							</div>
						</div>
						<label class="col-sm-4 control-label">가격</label>
						<div class="col-sm-8">
							<p id="total"><%=multi.getParameter("retail")%>원
							</p>
						</div>
						<div class="col-md-6" >
						<button type="button" class="btn btn-primary">
								<span class="glyphicon glyphicon-shopping-cart"
									aria-hidden="true">&nbsp;장바구니</span>
							</button>
							</div>
						<hr>
					</div>
					<!--  장바구니로 
					<div class="form-group text-center">
						<div class="btn-group">
							
							<button type="button" class="btn btn-primary dropdown-toggle"
								data-toggle="dropdown">
								<span class="caret"></span> <span class="sr-only">Toggle
									Dropdown</span>
							</button>
							<ul class="dropdown-menu">
								<li class="active"><a href="#">장바구니1에 담기</a></li>
								<li><a href="#">장바구니2에 담기</a></li>
								<li><a href="#">장바구니3에 담기</a></li>
							</ul>
						</div>
					</div> -->

					<hr>
				</form>
			</div>
		</div>
		<div class="col-md-12">
			<div id="section1" class="container-fluid">
				<ul class="nav nav-pills nav-justified">
					<li class="active"><a href="#" onclick="toSection('1')">색상옵션</a></li>
					<li><a href="#" onclick="toSection('2')">상품 정보</a></li>
					<li><a href="#" onclick="toSection('3')">상품 이미지</a></li>
					<li><a href="#" onclick="toSection('4')">배송 정보</a></li>
					<li><a href="#" onclick="toSection('5')">판매자 정보</a></li>
				</ul>
				<div class="col-md-12">

					<%
						int num = Integer.parseInt(multi.getParameter("add_count"));
						for (int i = 0; i < num; i++) {
							out.print("<div class='col-md-2 col-xs-6' style='line-height: 0.7em; padding: 0.5px;'>"
									+ "<div class='col-md-12'><hr><div style='height: 202px;'><img src='"
									+ multi.getParameter("mainImg" + (i + 1))
									+ "' style='max-width: 100%; width: 100%; max-height: 200px;'></div></div></div>");
						}
					%>
				</div>
				<!-- /item -->
			</div>
		</div>
		<hr>

		<div id="section2" class="container-fluid">
			<ul class="nav nav-pills nav-justified">
				<li><a href="#" onclick="toSection('1')">색상옵션</a></li>
				<li class="active"><a href="#" onclick="toSection('2')">상품
						정보</a></li>
				<li><a href="#" onclick="toSection('3')">상품 이미지</a></li>
				<li><a href="#" onclick="toSection('4')">배송 정보</a></li>
				<li><a href="#" onclick="toSection('5')">판매자 정보</a></li>
			</ul>
			<br> <br>
			<div class="panel panel-default">
				<div class="panel-body">
					<h2>기본</h2>
					<div class="col-md-6 ">
						<h4 class="col-md-3">
							<label class="control-label">상품명</label>
						</h4>
						<div class="col-md-4"><%=multi.getParameter("itemName")%></div>
					</div>
					<div class="col-md-6 ">
						<h4 class="col-md-3">
							<label class="control-label">스와치</label>
						</h4>
						<div class="col-md-4"><%=multi.getParameter("swatch")%>
						</div>
					</div>
					<div class="col-md-6 ">
						<h4 class="col-md-3">
							<label class="control-label">두께</label>
						</h4>
						<div class="col-md-4">
							<%
								String thick = multi.getParameter("thick");
								if (thick != null) {
									out.println(thick);
								} else {
									out.println("");
								}
							%>
						</div>
					</div>
					<div class="col-md-6 ">
						<h4 class="col-md-3">
							<label class="control-label">소매가</label>
						</h4>
						<div class="col-md-4"><%=multi.getParameter("retail")%>원
						</div>
					</div>
					<div class="col-md-6 ">
						<h4 class="col-md-3">
							<label class="control-label">폭</label>
						</h4>
						<div class="col-md-4"><%=multi.getParameter("width_radio")%>cm
						</div>
					</div>
					<div class="col-md-6 ">
						<h4 class="col-md-3">
							<label class="control-label">도매가</label>
						</h4>
						<div class="col-md-4"><%=multi.getParameter("wholesale")%>원
						</div>
					</div>
					<div class="col-md-6 ">
						<h4 class="col-md-3">
							<label class="control-label">색상명</label>
						</h4>
						<div class="col-md-4"><%=multi.getParameter("colorName1")%></div>
					</div>

					<%-- <div class="col-md-6 ">
						<h4 class="col-md-3">
							<label class="control-label">재고량</label>
						</h4>
						<div class="col-md-4">
							<%
								String stock1 = multi.getParameter("stock1");
								String stock_unknown = multi.getParameter("stock_unknown");
								if (stock1 != null) {
									out.println(stock1 + "마");
								} else if (stock_unknown.equals("1")) {
									out.println("확인불가");
								}
							%>
						</div>
					</div> --%>
					<div class="col-md-6 ">
						<h4 class="col-md-3">
							<label class="control-label">색상</label>
						</h4>
						<div class="col-md-9" style="padding-top: 2%">
							<%
								String bb = "";
								String[] color = multi.getParameterValues("color1");
								if (color != null) {
									for (int i = 0; i < color.length; i++) {
										bb += color[i];
									}
								}
								if (bb.contains("RS00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(255, 0, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>

							<%
								}
								if (bb.contains("YRS0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(255, 192, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>

							<%
								}
								if (bb.contains("YS00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(255, 255, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("GYS0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(192, 255, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>

							<%
								}
								if (bb.contains("GS00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(0, 255, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>

							<%
								}
								if (bb.contains("BGS0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(0, 255, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("BS00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(0, 0, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("PBS0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(192, 0, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("PS00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(255, 0, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("RPS0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(255, 0, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("WGBS")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(128, 128, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("RL00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(255, 192, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("YRL0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(255, 224, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("YL00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(255, 255, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("GYL0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(224, 255, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("GL00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(192, 255, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("BGL0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(192, 255, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("BL00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(192, 192, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("PBL0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(224, 192, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("PL00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(255, 192, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("RPL0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(255, 192, 224)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("WGBL")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(255, 255, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("RD00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(128, 0, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("YRD0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(128, 64, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("YD00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(128, 128, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("GYD0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(64, 128, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("GD00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(0, 128, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("BGD0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(0, 64, 64)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("BD00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(0, 0, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("PBD0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(64, 0, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("PD00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(128, 0, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("RPD0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(128, 0, 64)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (bb.contains("WGBD")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(0, 0, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
							%>


						</div>
					</div>
					<%-- <div class="col-md-6">
						<h4 class="col-md-3">
							<label class="control-label">배송</label>
						</h4>
						<div class="col-md-4">
							<%
								int exp = 0;
								String[] express = multi.getParameterValues("express");
								if (express != null) {
									for (int i = 0; i < express.length; i++) {
										exp += Integer.parseInt(express[i]);
									}
									if (exp == 1) {
										out.println("퀵");
									} else if (exp == 10) {
										out.println("택배");
									} else if (exp == 11) {
										out.println("퀵, 택배");
									}
								}
							%>
						</div>
					</div> --%>
				</div>
				<div class="panel panel-default" style="padding-top: 3%">
					<div class="panel-body">
					<h2>핵심</h2>
						<div class="col-md-6 ">
							<h4 class="col-md-3">
								<label class="control-label">원산지</label>
							</h4>
							<div class="col-md-4"><%=multi.getParameter("origin")%></div>
						</div>
						<div class="col-md-6 ">
							<h4 class="col-md-3">
								<label class="control-label">용도</label>
							</h4>
							<div class="col-md-9" style="padding-top: 2%">
								<%
									String aa = "";
									String[] uses = multi.getParameterValues("uses");
									if (uses != null) {
										for (int i = 0; i < uses.length; i++) {
											aa += uses[i] + ", ";
										}
										aa = aa.substring(0, aa.lastIndexOf(","));
										out.println(aa);
									}
								%>
							</div>
						</div>
						<div class="col-md-6 ">
							<h4 class="col-md-3">
								<label class="control-label">제조사</label>
							</h4>
							<div class="col-md-4"><%=multi.getParameter("manufacturer")%></div>
						</div>
						<div class="col-md-6 ">
							<h4 class="col-md-3">
								<label class="control-label">패턴</label>
							</h4>
							<div class="col-md-4">
								<%
									String pattern = multi.getParameter("pattern");
									if (pattern != null) {
										out.println(pattern);
									} else {
										out.println("");
									}
								%>
							</div>
						</div>
						<div class="col-md-6 ">
							<h4 class="col-md-3">
								<label class="control-label">제조년월</label>
							</h4>
							<div class="col-md-4"><%=multi.getParameter("manuDate")%></div>
						</div>
						<div class="col-md-6 ">
							<h4 class="col-md-3">
								<label class="control-label">명칭</label>
							</h4>
							<div class="col-md-4">
								<%
									String sortName = multi.getParameter("sortName");
									if (sortName != null) {
										out.println(sortName);
									} else {
										out.println("");
									}
								%>
							</div>
						</div>
						<div class="col-md-6 ">
							<h4 class="col-md-3">
								<label class="control-label">섬유</label>
							</h4>
							<div class="col-md-9" style="padding-top: 2%">
								<%
									String cc = "";
									String[] fiber = multi.getParameterValues("fiber");
									if (fiber != null) {
										for (int i = 0; i < fiber.length; i++) {
											cc += fiber[i] + ", ";
										}
										cc = cc.substring(0, cc.lastIndexOf(","));
										out.println(cc);
									}
								%>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default col-md-6" style="padding-left: 0">
					<div class="panel-body">
						<h2>피륙</h2>
						<div class="col-md-12">
							<h4 class="col-md-3">
								<label class="control-label">중량</label>
							</h4>
							<div class="col-md-4">
								<%=multi.getParameter("weight")%>g/㎡
							</div>
						</div>
						<div class="col-md-12">
							<h4 class="col-md-3">
								<label class="control-label">조직</label>
							</h4>
							<div class="col-md-4"><%=multi.getParameter("weave")%></div>
						</div>
						<div class="col-md-12">
							<h4 class="col-md-3">
								<label class="control-label">밀도</label>
							</h4>
							<div class="col-md-4">
								<%=multi.getParameter("density_volume")%>
								*
								<%=multi.getParameter("density_weight")%>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default col-md-6" style="padding-right: 0">
					<div class="panel-body">
						<h2>원단</h2>
						<div class="col-md-12">
							<h4 class="col-md-3">
								<label class="control-label">굵기</label>
							</h4>
							<div class="col-md-6" style="padding-top: 2%">
								<%=multi.getParameter("thickWp")%>
								-
								<%=multi.getParameter("thickWt")%>
								&nbsp;단위 :
								<%
									String thickUnit = multi.getParameter("thickUnit");
									if (thickUnit.equals("0")) {
										out.println("");
									} else {
										out.println(thickUnit);
									}
								%>
							</div>
						</div>
						<div class="col-md-12">
							<h4 class="col-md-3">
								<label class="control-label">길이</label>
							</h4>
							<div class="col-md-6" style="padding-top: 2%">
								<%
									String lengthWp = multi.getParameter("lengthWp");
									String lengthWt = multi.getParameter("lengthWt");
									if (lengthWp.equals("0") || lengthWt.equals("0")) {
										out.println("");
									} else {
										out.println(lengthWp + " - " + lengthWt);
									}
								%>
							</div>
						</div>
						<div class="col-md-12">
							<h4 class="col-md-3">
								<label class="control-label">꼬임</label>
							</h4>
							<div class="col-md-6" style="padding-top: 2%">
								<%
									String twistWp = multi.getParameter("twistWp");
									String twistWt = multi.getParameter("twistWt");
									if (twistWp.equals("0") || twistWt.equals("0")) {
										out.println("");
									} else {
										out.println(twistWp + " - " + twistWt);
									}
								%>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default col-md-12"
					style="padding-right: 0; padding-left: 0">
					<div class="panel-body">
						<h2>가공</h2>
						<div class="col-md-6">
							<h4 class="col-md-3">
								<label class="control-label">가공</label>
							</h4>
							<div class="col-md-4"><%=multi.getParameter("gagong")%></div>
						</div>
						<div class="col-md-6">
							<h4 class="col-md-3">
								<label class="control-label">염색</label>
							</h4>
							<div class="col-md-4">
								<%
									String dye = multi.getParameter("dye");
									if (dye.equals("0")) {
										out.println("");
									} else {
										out.println(dye);
									}
								%>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default col-md-12"
					style="padding-right: 0; padding-left: 0">
					<div class="panel-body">
						<h2>심미</h2>
						<div class="col-md-6">
							<h4 class="col-md-3">
								<label class="control-label">질감</label>
							</h4>
							<div class="col-md-4"><%=multi.getParameter("texture")%></div>
						</div>
						<div class="col-md-6">
							<h4 class="col-md-3">
								<label class="control-label">비침</label>
							</h4>
							<div class="col-md-4">
								<%
									String through = multi.getParameter("through");
									if (through != null) {
										out.println(through);
									} else {
										out.println("");
									}
								%>
							</div>
						</div>
						<div class="col-md-6">
							<h4 class="col-md-3">
								<label class="control-label">광택</label>
							</h4>
							<div class="col-md-4">
								<%
									String gloss = multi.getParameter("gloss");
									if (gloss != null) {
										out.println(gloss);
									} else {
										out.println("");
									}
								%>
							</div>
						</div>
						<div class="col-md-6">
							<h4 class="col-md-3">
								<label class="control-label">신축</label>
							</h4>
							<div class="col-md-4">
								<%
									String elastic = multi.getParameter("elastic");
									if (elastic != null) {
										out.println(elastic);
									} else {
										out.println("");
									}
								%>
							</div>
						</div>
						<div class="col-md-6">
							<h4 class="col-md-3">
								<label class="control-label">유연</label>
							</h4>
							<div class="col-md-4">
								<%
									String pliability = multi.getParameter("pliability");
									if (pliability != null) {
										out.println(pliability);
									} else {
										out.println("");
									}
								%>
							</div>
						</div>
					</div>
				</div>


			</div>
			<hr>
			<div id="section3" class="container-fluid text-center">
				<ul class="nav nav-pills nav-justified">
					<li><a href="#" onclick="toSection('1')">색상옵션</a></li>
					<li><a href="#" onclick="toSection('2')">상품 정보</a></li>
					<li class="active"><a href="#" onclick="toSection('3')">상품
							이미지</a></li>
					<li><a href="#" onclick="toSection('4')">배송 정보</a></li>
					<li><a href="#" onclick="toSection('5')">판매자 정보</a></li>
				</ul>
				<br> <br>
				<div class="col-md-12"><%=multi.getParameter("expl")%></div>
			</div>
			<hr>
			<div id="section4" class="container-fluid">
				<ul class="nav nav-pills nav-justified">
					<li><a href="#" onclick="toSection('1')">색상옵션</a></li>
					<li><a href="#" onclick="toSection('2')">상품 정보</a></li>
					<li><a href="#" onclick="toSection('3')">상품 이미지</a></li>
					<li class="active"><a href="#" onclick="toSection('4')">배송 정보</a></li>
					<li><a href="#" onclick="toSection('5')">판매자 정보</a></li>
				</ul>
				<br> <br>
				<div style="text-align: left">
						<div class="panel panel-default" >
							<div class="panel-body" style="padding: 30px; padding-top: 10px">
								<h3>일반 배송 안내</h3>
								<%
								UserVO vo=new UserVO();
								MemberDAO mDao=MemberDAO.getInstance();
								String email=(String)session.getAttribute("email");
								vo=mDao.getDelivery(email);
								
								%>
								<div>
									배송 가능 방법 :
									<%
									if(vo.getDelivery() ==1 ){
										out.println("퀵");
									}else if(vo.getDelivery() ==10){
										out.println("택배");
									}else if(vo.getDelivery() ==11){
										out.println("퀵, 택배");
									}
									 %>
								</div>
								<div>예상되는 배송 기간 : 주문 이후 7일 이내 발송</div>
								<div>거래 관련 처리 안내 : 본 사이트에서 진행되는 거래에 책임과 배송, 교환, 환불, 민원은 직접
									관리하고 있음</div>
								<div>소비자와 사업자 사이의 분쟁처리 : 소비자분쟁해결기준(공정거래위원회 고시) 및 관계법령에 따름</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-body col-md-6"
								style="margin: 0; padding: 30px; padding-top: 10px">
								<h3>퀵</h3>
								<div>
									배송사 : <br> 배송료 : 착불(거리에 따라 상이)<br> 교환배송비 : 착불(환불금액에서
									차감)<br> 반품배송비 : 착불(환불금액에서 차감)
								</div>
							</div>
						</div>		
						<div class="panel panel-default">
							<div class="panel-body col-md-6"
								style="padding: 30px; padding-top: 10px">
								<h3>택배</h3>
								<div>
									배송사 :
									<% 
									if(vo.getDeliveryCompany()!=null){
										out.println(vo.getDeliveryCompany());
									}
									%>
									
									<br> 배송료 :
									<%
								if(vo.getDelivery_costway().equals("무료")){
								out.println("무료");
								} else if(vo.getDelivery_costway().equals("유료")){
									out.println("유료 "+vo.getDelivery_cost()+"원");
								} else if(vo.getDelivery_costway().equals("조건부무료")){
									out.println(vo.getDelivery_cost2()+"원 / "+ vo.getDelivery_cost()+"원 이상 구매시 무료");
								}
								%>
									
									<br> 교환배송비 : 왕복(<% out.println(vo.getRe_delivery_cost1()*2);%>)<br>
									반품배송비 : 편도(<%out.println(vo.getRe_delivery_cost1()); %>원) / 최초 배송비 무료인 경우
									(<%out.println(vo.getRe_delivery_cost1()*2); %>)원
								</div>
							</div>
						</div>		
							<br> <br> <br> <br> <br> <br> <br>
						<br> <br> <br>
						<div class="panel panel-default">
							<div class="panel-body col-md-12"
								style="padding: 30px; padding-top: 10px">
								<h3>교환/반품 안내</h3>
								<div>
									보내실 곳 : 입력하는 대로 <br> (구매자 귀책사유로 인한 교환 및 반품 시에는 구매자가, 판매자
									귀책사유로 인한 교환 및 반품 시에는 판매자가 교환 및 반품 배송비용을 부담합니다.)<br> <br>
									- 단은 롤에서 고객님의 주문수량만큼 컷팅되어서 배송이 되므로 고객님의 단순변심에 의한 교환 및 반품은
									불가합니다.<br> - 상품의 이미지는 모니터 사양에 따라 약간의 색상차이가 있을 수 있으며, 원단
									재가공시 약간의 색상차이가 나는 부분은 원단 가공시 나타나는 현상이므로 교환 및 반품이 불가능합니다.<br>
									- 세탁, 재단, 미싱 등으로 원단이 훼손되거나 변형된 경우는 교환 및 반품이 불가능합니다.<br> -
									상품 주문후 주문상태가 상품준비중일 경우에는 교환 및 반품이 불가능합니다.
								</div>

								<h3>교환/반품 기준</h3>
								<div>
									상품 수령 후 7일 이내에 신청하실 수 있습니다. 단, 제품이 표시광고 내용과 다르거나 불량 등 계약과 다르게
									이행된 경우는 <br> 제품 수령일부터 3개월 이내, 그 사실을 안 날 또는 알 수 있었던 날로부터
									30일 이내에 교환/반품이 가능합니다.<br> <br> 단, 다음의 경우 해당하는 반품/교환은
									불가능할 수 있습니다. - 소비자의 책임 있는 사유로 상품 등이 멸실 또는 훼손된 경우<br> -
									소비자의 사용 또는 소비에 의해 상품 등의 가치가 현저히 감소한 경우<br> - 시간의 경과에 의해
									재판매가 곤란할 정도로 상품 등의 가치가 현저히 감소한 경우<br> - 복제가 가능한 상품 등의 포장을
									훼손한 경우<br> - 소비자의 주문에 따라 개별적으로 생산되는 상품이 제작에 들어간 경우
								</div>
							</div>
						</div>
						<br> <br> <br> <br> <br> <br> <br> <br> <br>
						<br> <br> <br> <br> <br> <br> <br> <br> <br>
						<br> <br> <br> <br>
						<div style="padding: 30px; padding-top: 10px">
							<h3>주의사항</h3>
							<div id="caution">
								전자상거래 등에서의 소비자보호에 관한 법률에 의한 반품규정이 판매자가 지정한 반품 조건보다 우선합니다.<br>
								전자상거래 등에서의 소비자 보호에 관한 법률에 의거하여 미성년자가 물품을 구매하는 경우, 법정대리인이 동의하지
								않으면 미성년자 본인 또는 법정대리인이 구매를 취소할 수 있습니다.<br> 시드텍스에 등록된 판매상품과
								상품의 내용은 판매자가 등록한 것으로 (주)시드월드엔터프라이즈에서는 그 등록내역에 대하여 일체의 책임을 지지
								않습니다.<br> 시드텍스의 결제시스템을 이용하지 않고 판매자와 직접 거래하실 경우 상품을 받지 못하거나
								구매한 상품과 상이한 상품을 받는 등 피해가 발생할 수 있으니 유의하시기 바랍니다.
							</div>
						</div>
					</div>	
								
								
								
								
						
					</div>


			</div>
			<hr>
			<div id="section5" class="container-fluid">
				<ul class="nav nav-pills nav-justified">
					<li><a href="#" onclick="toSection('1')">색상옵션</a></li>
					<li><a href="#" onclick="toSection('2')">상품 정보</a></li>
					<li><a href="#" onclick="toSection('3')">상품 이미지</a></li>
					<li><a href="#" onclick="toSection('4')">배송 정보</a></li>
					<li class="active"><a href="#" onclick="toSection('5')">판매자 정보</a></li>
				</ul>
				<br> <br>
				<table class="table" id="seller_table">
						<tr>
							<td class="title">판매자</td>
							<td><%=session.getAttribute("company") %></td>
							<td class="title">사업자등록번호</td>
							<td><%=session.getAttribute("rep_num") %></td>
						</tr>
						<tr>
							<td class="title">상호명</td>
							<td><%=session.getAttribute("fullCompany") %></td>
							<td class="title">통신판매업번호</td>
							<td>-</td>
						</tr>
						<tr>
							<td class="title">대표자</td>
							<td><%=session.getAttribute("rep_name") %></td>
							<td class="title">사업장소재지</td>
							<td><%=session.getAttribute("companyAddress") %></td>
						</tr>
						<tr>
							<td class="title">대표전화</td>
							<td><%=session.getAttribute("rep_phone") %></td>
							<td class="title">메일</td>
							<td><%=session.getAttribute("email") %></td>
						</tr>
					</table>

			</div>
			<hr>
		</div>
	</div>


	<script>
		function toSection(seq) {
			var offset = $("#section" + seq).offset();
			$('html, body').animate({
				scrollTop : offset.top
			}, 400);
	
		}
	
		//가격 계산
		function getCost(retail, wholesale, standard, quantity) {
			console.log(" retail : " + retail + " wholesale : " + wholesale + " standard : " + standard + " this val : " + quantity);
			var cost = retail * quantity;
			var total = retail * quantity;
			var dc = 0;
	
			if (quantity >= standard) { //도매 기준 수량을 넘었을 경우
				total = wholesale * quantity;
				dc = total - cost;
			}
	
			console.log("cost : " + cost + " total : " + total + " dc : " + dc);
			$("#cost").text(cost);
			$("#total").text(total + "원");
			$("#dc").text(dc);
	
		}
		
		
		
	</script>

	<%@ include file="../include/footer.jsp"%>
</body>
