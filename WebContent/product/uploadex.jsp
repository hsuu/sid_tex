<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../include/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript"
	src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="../editor/js/HuskyEZCreator.js"
	charset="utf-8"></script>


<link href="../css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link href="../css/jquery.filer-dragdropbox-theme.css" type="text/css"
	rel="stylesheet" />
<script src="http://code.jquery.com/jquery-3.1.0.min.js"></script>
<script src="../js/jquery.filer.min.js"></script>
<style>

/* 업로드  */
.image {
	border: 1px solid #aaa;
}

div.upload {
	width: 300px;
	height: 300px;
	background: url("../img/plus.png"); 
	overflow: hidden;
	background-repeat: no-repeat;
}

div.upload input {
	display: block !important;
	width: 300px !important;
	height: 300px !important;
	opacity: 0 !important;
	overflow: hidden !important;
}

/* 상단바 스타일  */
.menubar {
	background-color: #eee;
	padding: 10px 0px;
}

.menubar h4 {
	text-align: left;
	display: inline;
}

.menubar.fix {
	position: fixed;
	top: 0;
	background: #eee;
	width: 100%;
	z-index: 1;
}

body {
	padding-left: 30px;
}
</style>
</head>
<body>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">
			<li role="presentation"><a href="../product/product_main.jsp">홈</a></li>
			<li class="active" role="presentation"><a
				href="../product/productUpload.jsp">상품 등록</a></li>
			<li role="presentation"><a
				href="../product/product_manage_stock.jsp">재고 관리</a></li>
			<li role="presentation"><a
				href="../product/product_manage_order.jsp">주문 관리</a></li>
			<li role="presentation"><a
				href="../product/product_manage_balance.jsp">정산 관리</a></li>
		</ul>
	</div>
	<div class="col-md-12">
		<div class="menubar">
			<h4>
				<label class="control-label">상품등록</label>
			</h4>
			<button class="btn btn-primary">미리보기</button>
			<button class="btn btn-primary">등록하기</button>
		</div>
		<div class="content">
			<form enctype="multipart/form-data" method="post" name="frm">
					<h4>
						<label class="control-label">이미지등록</label>
					</h4>
					<div id="mainImage1" class="col-md-3 image">
						<div class="upload">
							<input type='file' id="imgInp1" name="imageFile1" /> <br> <img
								id="image1" src="#" alt="이미지"
								style="display: none; width: 50%; max-width: 100%;" />
						</div>
					</div>
					<div id="mainImage2" class="col-md-3 image">
						<input type='file' id="imgInp2" name="imageFile2" /> <br> <img
							id="image2" src="#" alt="이미지"
							style="display: none; width: 50%; max-width: 100%;" />
					</div>

					<div id="mainImage3" class="col-md-3 image">
						<input type='file' id="imgInp3" name="imageFile3" /> <br> <img
							id="image3" src="#" alt="이미지"
							style="display: none; width: 50%; max-width: 100%;" />
					</div>
					<div id="mainImage4" class="col-md-3 image">
						<input type='file' id="imgInp4" name="imageFile4" /> <br> <img
							id="image4" src="#" alt="이미지"
							style="display: none; width: 50%; max-width: 100%;" />
					</div>
				<hr>
				<div class="required_fields">
					<div class="col-md-12">
						<h4>
							<label class="control-label">상품명</label>
						</h4>
						<div class="col-md-6">
							<input type="text" class="form-control" maxlength="12"
								placeholder="12칸 이내로만 작성가능합니다">
						</div>
					</div>

					<div class="col-md-12 form-inline">
						<h4>
							<label class="control-label">소매가</label>
						</h4>
						<div class="col-md-6">
							<input type="text" class="form-control" maxlength="11"
								placeholder="10원 단위의 숫자만 입력해주세요"> 원
						</div>
					</div>

					<div class="col-md-12">
						<h4>
							<label class="control-label">도매가</label>
						</h4>
						<div class="col-md-8 form-inline">
							<input type="text" class="form-control" placeholder="마 단위">
							마 이상 구매 시 가격 <input type="text" class="form-control"
								placeholder="10원 단위의 숫자만 입력해주세요"> 원
						</div>
					</div>
					<div class="col-md-12">
						<h4>
							<label class="control-label">재고량</label>
						</h4>
						<div class="col-md-3 form-inline">
							<input type="text" class="form-control" placeholder="마 단위">
							마
						</div>
					</div>
					<div class="col-md-12">
						<h4>
							<label class="control-label">출고지 주소</label>
						</h4>
						<div class="col-md-6">
							<input type="text" class="form-control" placeholder="주소">
						</div>
					</div>
					<div class="col-md-12">
						<h4>
							<label class="control-label">배송방법</label>
						</h4>
						<div class="col-md-9">
							<label class="checkbox-inline"> <input type="checkbox"
								name="delivery" value="quick">퀵
							</label> <label class="checkbox-inline"> <input type="checkbox"
								name="delivery" value="parcel">택배
							</label> <label class="checkbox-inline"> <input type="checkbox"
								name="delivery" value="email">익일특급 등기 우편
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<h4>
							<label class="control-label">스와치 제공</label>
						</h4>
						<div class="col-md-6">
							<label> <input type="radio" name="swatch"
								value="swatch_yes">가능
							</label> <label> <input type="radio" name="swatch"
								value="swatch_no">불가능
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<h4>
							<label class="control-label">제조사</label>
						</h4>
						<div class="col-md-6">
							<input type="text" class="form-control"
								placeholder="6칸 이내로만 작성가능합니다">
						</div>
					</div>
					<div class="col-md-12">
						<h4>
							<label class="control-label">제조년월</label>
						</h4>
						<div class="col-md-3">
							<input class="form-control" type="date" value="0000-00-00"
								name="standardDate">
						</div>
					</div>
					<div class="col-md-12">
						<h4>
							<label class="control-label">원산지</label>
						</h4>
						<div class="col-md-6">
							<input type="text" class="form-control" placeholder="원산지를 입력하세요">
						</div>
					</div>
					<div class="col-md-12">
						<h4>
							<label class="control-label">드라이클리닝</label>
						</h4>
						<div class="col-md-6">
							<input type="text" class="form-control" placeholder="">
						</div>
					</div>

					<div class="col-md-12">
						<h4>
							<label class="control-label">용도</label>
						</h4>
						<div class="col-md-12">
							<label class="checkbox-inline"> <input type="checkbox"
								name="use" value="">
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<h4>
							<label class="control-label">제직</label>
						</h4>
						<div class="col-md-12">
							<input type="radio" name="" value="">직물 <input
								type="radio" name="" value="">편물 <input type="radio"
								name="" value="">팬시 패브릭 <input type="radio" name=""
								value="">기타 패브릭 <input type="radio" name="" value="">소폭
							소재 <input type="radio" name="" value="">부자재
						</div>
					</div>
					<div class="col-md-12">
						<h4>
							<label class="control-label">명칭</label>
						</h4>
						<div class="col-md-12">
							<input type="radio" name="" value="">직물 <input
								type="radio" name="" value="">편물 <input type="radio"
								name="" value="">팬시 패브릭 <input type="radio" name=""
								value="">기타 패브릭 <input type="radio" name="" value="">소폭
							소재 <input type="radio" name="" value="">부자재
						</div>
					</div>
					<div class="col-md-12">
						<h4>
							<label class="control-label">섬유</label>
						</h4>
						<div class="col-md-9">
							<button class="btn btn-primary dropdown-toggle" type="button"
								data-toggle="dropdown"></button>
							<ul class="dropdown-menu">
								<li><a href="">번수</a></li>
								<li><a href="">데니어</a></li>
							</ul>
						</div>

					</div>
					<div class="col-md-12">
						<h4>
							<label class="control-label">패턴과 색상</label>
						</h4>
						<div class="col-md-12">
							<input type="radio" name="" value="">무지 <input
								type="radio" name="" value="">도트 <input type="radio"
								name="" value="">스트라이프 <input type="radio" name=""
								value="">체크 <input type="radio" name="" value="">기타
						</div>
					</div>
				</div>
				<div class="panel panel-default">
						<div class="col-md-6 form-inline">
							<h4 class="col-md-2">
								<label class="control-label">두께</label>
							</h4>
							<input type="text" class="form-control" placeholder="두께를 입력하세요">
						</div>

						<div class="col-md-6 form-inline">
							<h4 class="col-md-2">
								<label class="control-label">중량</label>
							</h4>
							<input type="text" class="form-control col-md-2"
								placeholder="중량을 입력하세요">
						</div>

						<div class="col-md-6 form-inline">
							<h4 class="col-md-2">
								<label class="control-label">밀도</label>
							</h4>
							<input type="text" class="form-control" placeholder="밀도를 입력하세요">
						</div>

						<div class="col-md-6 form-inline">
							<h4 class="col-md-2">
								<label class="control-label">질감</label>
							</h4>
							<input type="text" class="form-control"
								placeholder="ex)부드러움/매우 거침">

						</div>
						<div class="col-md-6 form-inline">
							<h4 class="col-md-2">
								<label class="control-label">비침</label>
							</h4>
							<select class="form-control">
								<option>비침없음</option>
								<option>약간 비침</option>
								<option>많이 비침</option>
							</select>
						</div>
						<div class="col-md-6 form-inline">
							<h4 class="col-md-2">
								<label class="control-label">광택</label>
							</h4>
							<select class="form-control">
								<option>광택 없음</option>
								<option>약간 빛남</option>
								<option>많이 빛남</option>
							</select>
						</div>
						<div class="col-md-6 form-inline">
							<h4 class="col-md-2">
								<label class="control-label">신축</label>
							</h4>
							<select class="form-control">
								<option>신축 없음</option>
								<option>약간 신축</option>
								<option>많이 신축</option>
							</select>
						</div>
						<div class="col-md-6 form-inline">
							<h4 class="col-md-2">
								<label class="control-label">유연</label>
							</h4>
							<select class="form-control">
								<option>매우 부드러운</option>
								<option>부드러운</option>
								<option>보통</option>
								<option>뻣뻣한</option>
								<option>매우 뻣뻣한</option>
							</select>

						</div>

						<div class="col-md-6 form-inline">
							<h4 class="col-md-2">
								<label class="control-label">굵기</label>
							</h4>
							<input type="text" class="form-control" placeholder="경사 입력">
							<select class="form-control">
								<option>번수</option>
								<option>데니어</option>
							</select> <input type="text" class="form-control" placeholder="위사 입력">
							<select class="form-control">
								<option>번수</option>
								<option>데니어</option>
							</select>

						</div>
						<div class="col-md-6 form-inline">
							<h4 class="col-md-2">
								<label class="control-label">폭</label>
							</h4>
							<input type="radio" class="form-control" name="width"
								value="90cm">90cm <input type="radio"
								class="form-control" name="width" value="110cm">110cm <input
								type="radio" class="form-control" name="width" value="150cm">150cm
							<input type="text" class="form-control" placeholder="직접 입력">
							<select class="form-control">
								<option>cm</option>
								<option>inch</option>
							</select>
						</div>


						<div class="col-md-6 form-inline">
							<h4 class="col-md-2">
								<label class="control-label">길이</label>
							</h4>
							<h5 class="form-control">경사</h5>
							<select class="form-control">
								<option>필라멘트사</option>
								<option>스테이플사</option>
							</select>
							<h5 class="form-control">위사</h5>
							<select class="form-control">
								<option>필라멘트사</option>
								<option>스테이플사</option>
							</select>
						</div>

						<div class="col-md-6 form-inline">
							<h4 class="col-md-2">
								<label class="control-label">꼬임</label>
							</h4>
							<h5 class="form-control">경사</h5>
							<select class="form-control">
								<option>무연사</option>
								<option>약연사</option>
								<option>강연사</option>
							</select>
							<h5 class="form-control">위사</h5>
							<select class="form-control">
								<option>무연사</option>
								<option>약연사</option>
								<option>강연사</option>
							</select>
						</div>
						<div class="col-md-6 form-inline">
							<h4 class="col-md-2">
								<label class="control-label">가공</label>
							</h4>
							<input type="text" class="form-control" placeholder="굵기를 입력하세요">
							<button class="btn btn-primary dropdown-toggle " type="button"
								data-toggle="dropdown">단위선택</button>
							<ul class="dropdown-menu">
								<li><input type="checkbox">번수</li>
								<li><input type="checkbox">데니어</li>
							</ul>
						</div>
						<div class="col-md-6 form-inline">
							<h4 class="col-md-2">
								<label class="control-label">염색</label>
							</h4>
							<select class="form-control">
								<option>무염</option>
								<option>침염</option>
								<option>날염</option>
							</select>
						</div>
					</div>
				

			</form>
			<div class="col-md-12">
				<form action="" method="post" id="fm">
					<textarea name="smarteditor" id="smarteditor" rows="10" cols="100"
						style="width: 766px; height: 412px;"></textarea>
					<input type="button" id="savebutton" value="등록" />
				</form>
			</div>
		</div>
	</div>
	<script>
		// smarteditor
		$(function() {
			//전역변수선언
			var editor_object = [];
	
			nhn.husky.EZCreator.createInIFrame({
				oAppRef : editor_object,
				elPlaceHolder : "smarteditor", //연결지을 textarea의 id명
				sSkinURI : "/editor/SmartEditor2Skin.html", //에디터 스킨 경로
				htParams : {
					// 툴바 사용 여부 (true:사용/ false:사용하지 않음)
					bUseToolbar : true,
					// 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
					bUseVerticalResizer : true,
					// 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
					bUseModeChanger : true,
				}
			});
	
			//전송버튼 클릭이벤트
			$("#savebutton").click(function() {
				//id가 smarteditor인 textarea에 에디터에서 대입
				editor_object.getById["smarteditor"].exec("UPDATE_CONTENTS_FIELD", []);
	
				// 이부분에 에디터 validation 검증
	
				//폼 submit
				$("#fm").submit();
			})
		})
	
		// 이미지 업로드   
		$(document).ready(function() {
			function readURL1(input) {
				if (input.files && input.files[0]) {
					var reader = new FileReader(); //파일을 읽기 위한 FileReader객체 생성
					reader.onload = function(e) {
						//파일 읽어들이기를 성공했을때 호출되는 이벤트 핸들러
						//이미지 Tag의 SRC속성에 읽어들인 File내용을 지정
						//(아래 코드에서 읽어들인 dataURL형식)
						$('.upload').attr('src', e.target.result);
						//	$('#image1').css("display", "");
						$('.upload').css("background", "url('" + e.target.result + "')");
						$('.upload').css("width", "auto");
						$('.upload').css("max-height", "100%");
						$('.upload').css("background-repeat", "no-repeat");
						$('.upload').css("overflow", "hidden");
					}
					reader.readAsDataURL(input.files[0]);
				//File내용을 읽어 dataURL형식의 문자열로 저장
				}
			} //readURL()--
			//file 양식으로 이미지를 선택(값이 변경) 되었을때 처리하는 코드
			$("#imgInp1").change(function() {
				//alert(this.value); //선택한 이미지 경로 표시
				readURL1(this);
				/* $('#imageUrl').val(this.value);
				                         */
	
			});
			function readURL2(input) {
				if (input.files && input.files[0]) {
					var reader = new FileReader(); //파일을 읽기 위한 FileReader객체 생성
					reader.onload = function(e) {
						$('#image2').attr('src', e.target.result);
						$('#image2').css("display", "");
					}
					reader.readAsDataURL(input.files[0]);
				}
			} //readURL()--
			$("#imgInp2").change(function() {
				readURL2(this);
			});
	
			function readURL3(input) {
				if (input.files && input.files[0]) {
					var reader = new FileReader(); //파일을 읽기 위한 FileReader객체 생성
					reader.onload = function(e) {
						$('#image3').attr('src', e.target.result);
						$('#image3').css("display", "");
					}
					reader.readAsDataURL(input.files[0]);
				}
			} //readURL()--
			$("#imgInp3").change(function() {
				readURL3(this);
			});
	
			function readURL4(input) {
				if (input.files && input.files[0]) {
					var reader = new FileReader(); //파일을 읽기 위한 FileReader객체 생성
					reader.onload = function(e) {
						$('#image4').attr('src', e.target.result);
						$('#image4').css("display", "");
					}
					reader.readAsDataURL(input.files[0]);
				}
			} //readURL()--
			$("#imgInp4").change(function() {
				readURL4(this);
			});
		});
	
		//상단바 고정
		$(function() {
			var top_pos = $('.menubar').offset().top;
			var width = $('.menubar').width();
			win = window;
			$(win).on('scroll',
				function() {
					var pos = $(this).scrollTop();
					$('#content').attr('value', pos);
					if (pos >= top_pos) {
						$('.menubar').addClass('fix');
						$('.fix').width(width);
					} else {
						$('.menubar').removeClass('fix');
					}
				});
		});
	
	</script>
</body>
</html>