<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../include/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
body {
	padding-left: 30px;
	padding-right: 30px;
}

thead th, tbody td {
	text-align: center;
}

#datepicker1, #datepicker2 {
	text-align: center;
	width: 100px;
}
</style>
<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css"
	type="text/css" />
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>
</head>
<body>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">
			<li role="presentation"><a
				href="../SidServlet?command=purchase_list&state=0">주문 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_manage_stock">재고 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=product_upload">상품 등록</a></li>
			<li class="active" role="presentation"><a
				href="../product/product_manage_balance_day.jsp">정산 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_manage_delivery">배송 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_manage_customer">고객 관리</a></li>
		</ul>
	</div>
	<br>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">

			<li class="active"><a
				href="../product/product_manage_balance_day.jsp">일별 정산내역</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_manage_balance_case">건별 정산내역</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_manage_balance_account">계좌관리</a></li>
		</ul>
	</div>
	<div class="col-md-12">
		<br>
		<div role="tabpanel">
			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane in active" id="home">
					<div class="panel panel-default">
						<div class="panel-body">
							<div>
								꼭 확인하세요!<br>
정산은 구매확정일 이후 +8영업일에 일정산 됩니다. 정산금액이 마이너스 금액이면, 오늘 입금 받으실 금액은 '0'원이 되고, 정산하지 못한 금액은 마이너스 금액으로 기록됩니다. 
<br>해당 금액은 차후 플러스 정산금액이 발생 시, 차감하여 정산됩니다.

							</div>
						</div>
					</div>
					<form id="balance" method="post" action="../SidServlet?command=list_manage_balance_day">
						<div>
							조회기간 : <input type="text" id="datepicker1" name="datepicker1">
							~ <input type="text" id="datepicker2" name="datepicker2">
							<button class="btn btn-primary" type="submit">적용하기</button>
							<br>
						</div>
					</form>
					<br>
					<div class="row" id="resizeDiv">
						<div class="table-responsive panel panel-default">
							<table class="table table-striped table-bordered resizeTable"
								id="resizeTable1">
								<thead>
									<tr>
										<th class="1">구매확정일</th>
										<th class="2">정산완료일</th>
										<th class="3">주문건수</th>
										<th class="4">정산금액(A-B-C-D)</th>
										<th class="5">결제금액(A)</th>
										<th class="6">부가세(B)</th>
										<th class="7">수수료(C)</th>
										<th class="8">중개료(D)</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${list}" var="list">
										<tr>
											<td class="1">${list.purchaseConfirm_date }</td>
											<td class="2">${list.calculate_date }</td>
											<td class="3">${list.number_of_orders }</td>
											<td class="4">${list.calculate_amount2 }원</td>
											<td class="5">${list.payment_amount2 }원</td>
											<td class="6">${list.surtax2 }원</td>
											<td class="7">${list.fee2 }원</td>
											<td class="8">${list.brokerage_fee2 }원</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>

		</div>
	</div>
</body>
<script>
	$.datepicker.setDefaults({
    	 dateFormat: 'yy-mm-dd',
    	    prevText: '이전 달',
    	    nextText: '다음 달',
    	    monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
    	    monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
    	    dayNames: ['일','월','화','수','목','금','토'],
    	    dayNamesShort: ['일','월','화','수','목','금','토'],
    	    dayNamesMin: ['일','월','화','수','목','금','토'],
    	    showMonthAfterYear: true,
    	    yearSuffix: '년'
    });
	$(function() {
	    $("#datepicker1, #datepicker2").datepicker();
	  });
	

</script>

<%@ include file="../include/footer.jsp"%>
</html>