<%@page import="java.util.HashMap"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.sid.dto.PurchaseVO"%>
<%@ include file="../include/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
.row {
	margin-top: 40px;
	padding: 0 10px;
}

.clickable {
	cursor: pointer;
}

.panel-heading div {
	margin-top: -18px;
	font-size: 15px;
}

.panel-heading div span {
	margin-left: 5px;
}

.panel-body {
	display: none;
}

body {
	padding-left: 30px;
	padding-right: 30px;
}

table a {
	color: #4d94ff;
}

th {
	white-space: nowrap;
	text-align: center;
}

td {
	white-space: nowrap;
	text-align: center;
}

#express5,#finish_delivery{
	background-color: #80b3ff;
	border-color: #4d94ff;
}

#express6{
	background-color: #ff9999;
	
}
</style>
</head>
<body>

	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">
			<li class="active" role="presentation"><a href="../SidServlet?command=purchase_list&state=0">주문 관리</a></li>
			<li role="presentation"><a href="../SidServlet?command=list_manage_stock">재고 관리</a></li>
			<li role="presentation"><a href="../SidServlet?command=product_upload">상품 등록</a></li>
			<li role="presentation"><a href="../product/product_manage_balance_day.jsp">정산 관리</a></li>
			<li role="presentation"><a href="../SidServlet?command=list_manage_delivery">배송 관리</a></li>
			<li role="presentation"><a href="../SidServlet?command=list_manage_customer">고객 관리</a></li>
		</ul>
	</div>
	</div>
	<br>
	   <c:set value="${counter}" var="map"/>

	<div role="tabpanel">
		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation"><a href="../SidServlet?command=purchase_list&state=0">결제완료&nbsp;<span class="badge"
					>${map.state0}</span></a></li>
			<li role="presentation"><a href="../SidServlet?command=purchase_list&state=1">배송준비&nbsp;<span class="badge"
					>${map.state1}</span></a></li>
			<li role="presentation" class="active"><a href="../SidServlet?command=purchase_list&state=2">배송중&nbsp;<span class="badge"
					>${map.state2}</span></a></li>
			<li role="presentation"><a href="../SidServlet?command=purchase_list&state=3">배송완료&nbsp;<span class="badge"
					>${map.state3}</span></a></li>
				<li role="presentation"><a href="../SidServlet?command=purchase_list&state=4">완료된 거래&nbsp;<span id="express5" class="badge">${map.state4}</span></a></li>
			<li role="presentation"><a href="../SidServlet?command=purchase_list&state=5">취소된 거래&nbsp;<span id="express6" class="badge">${map.state5}</span></a></li>
		
		</ul>
</div>
<div style="padding:30px">
		<!-- Tab panes -->
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane in active" id="home">
				<!-- <div class="panel panel-default">
						<div class="panel-body"> -->
				<br>
				<button type="button" class="btn btn-primary" id="finish_delivery">배송완료</button>
				<!-- <button type="button" class="btn btn-primary" id="print_order"
						onclick="tableprint('1')">주문내용인쇄</button> -->
				<!-- 	</div>
					</div>
					 -->
				<div class="row" id="resizeDiv">
					<div class="table-responsive panel panel-default">
						<table class="table table-striped table-bordered resizeTable" id="resizeTable1">
							<caption>배송중</caption>
							<thead>
								<tr>
									<th data-sorter="false" class="filter-false"><input type="checkbox" id="checkth2"></th>
									<th class="1">결제번호</th>
									<th>주문번호</th>
									<th class="2">상품번호</th>
									<th>상태</th>
									<th>배송정보</th>
									<th class="3">상품명</th>
									<th class="4">색상명</th>
									<th class="6">수취인명</th>
									<th class="7">수취인연락처</th>
									<th class="8">주소</th>
									<th>배송메시지</th>
									<th class="9">구매자이메일</th>
									<th class="10">구매자명</th>
									<th class="11">구매자연락처</th>
									<th class="12">결제금액</th>
									<th class="13">결제방법</th>
									<th class="14">상품가격</th>
									<th>배송비</th>
									<th class="15">할인금액</th>
									<th class="16">주문시간</th>
								</tr>
							</thead>
							<tbody>

							<% 
								ArrayList<ArrayList<PurchaseVO>> list = (ArrayList<ArrayList<PurchaseVO>>) request.getAttribute("list");
								ArrayList<PurchaseVO> childlist = null;
								int totalprice=0;
								
									for (int i = 0; i < list.size(); i++) {
										System.out.println("list size" + list.size());
										for (int j = 0; j < list.get(i).size(); j++) {
											System.out.println("list get i size" + list.get(i).size());

											childlist = list.get(i);
											totalprice=0;
											if(childlist.get(j).getState()==2||childlist.get(j).getState()==5){
												if(j==0){
													out.print("<tr>");
													out.print("<td rowspan=" + list.get(i).size()
															+ "><input type='checkbox' class='state2'><input type='hidden' class='oid' value="
															+ childlist.get(j).getOid() + "><input type='hidden' class='seller' value="
															+ childlist.get(j).getSeller() + "></td>");
													}
											
												if (j == 0) {
													
													out.print("<td class='oidtd' rowspan=" + list.get(i).size() + ">"
															+ childlist.get(j).getOid()+"</td>");
													
												}
												
												out.print("<td class='1'>" + childlist.get(j).getPurchaseId() + "<input type='hidden' class='"
														+ childlist.get(j).getOid() + "' value=" + childlist.get(j).getPurchaseId()
														+ " ><input type='hidden' class='buyerEmail' value=" + childlist.get(j).getBuyerEmail()
														+ "></td>");
												out.print("<td class='9'>" + childlist.get(j).getProductId() + "</td>");
												if(childlist.get(j).getState()==5){
													out.print("<td><p style='color:red'>주문취소</p></td>");
												}else{
													
													out.print("<td><p>배송중</p></td>");
												}
												if(j==0){
													out.print("<td rowspan="+list.get(i).size()+"><a href='#' onclick='updateInvoiceModal(\""+childlist.get(j).getOid()+"\",\""+childlist.get(j).getInvoiceNum()+"\")'>"+childlist.get(j).getInvoiceNum()+"</a></td>");
												
												}
												out.print("<td class='8'>"+ childlist.get(j).getItemName() + "</td>");
												out.print("<td>" + childlist.get(j).getPurchase_detail() + "</td>");
												
												if(j==0){
													
												out.print("<td rowspan=" + list.get(i).size() + ">"+childlist.get(j).getRecipientName()+"</td>");
												out.print("<td rowspan=" + list.get(i).size() + ">"+childlist.get(j).getRecipientPhone()+"</td>");
												out.print("<td rowspan=" + list.get(i).size() + ">"+childlist.get(j).getRecipientAddress()+"</td>");
												if(childlist.get(j).getDelivery_message()!=null&&(childlist.get(j).getDelivery_message().length()>3)){
													
													out.print("<td rowspan=" + list.get(i).size() + "><a tabindex='0' role='button' data-trigger='focus' data-container='body' data-toggle='popover' data-placement='right' data-content='"+
															childlist.get(j).getDelivery_message()+"'>"+ childlist.get(j).getDelivery_message().substring(0,3)
															+ "...펼치기</a></td>");
													}else{
														out.print("<td rowspan=" + list.get(i).size() + ">"+childlist.get(j).getDelivery_message()+"</td>");
													}
												out.print("<td rowspan=" + list.get(i).size() + ">"+childlist.get(j).getBuyerEmail()+"</td>");
												out.print("<td rowspan=" + list.get(i).size() + ">"+childlist.get(j).getBuyerName()+"</td>");
												out.print("<td rowspan=" + list.get(i).size() + ">"+childlist.get(j).getBuyerPhone()+"</td>");
												
												out.print("<td rowspan=" + list.get(i).size() + ">"+(childlist.get(j).getTotalcost()+childlist.get(j).getDelivery_cost())+"</td>");
												}
												switch(childlist.get(j).getPayOption()){
												case "Card":
													out.print("<td>신용카드</td>");
													break;
												case "VBank":
												out.print("<td>무통장입금</td>");
													break;
												case "DirectBank":
												out.print("<td>실시간계좌이체</td>");
													break;
												default:
													out.print("<td>"+childlist.get(j).getPayOption()+"</td>");
													break;
											}
												out.print("<td>"+childlist.get(j).getCost()+"</td>");
												if(j==0){
												out.print("<td rowspan=" + list.get(i).size() + ">");
												if(childlist.get(j).getExpress()==1){//퀵일경우
													out.print("후불(퀵)");
												}else{
													out.print(childlist.get(j).getDelivery_cost());
												}
												out.print("</td>");
												}
												out.print("<td>0</td>");
	
													out.print("<td>" + childlist.get(j).getOrderDate() + "</td>");
												out.print("</tr>");
											}
											
										}

									}
									%>

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
<!-- Modal -->
		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="modal_column2">배송사원 연락처 또는 운송장 번호를 입력해주세요. (30자 이내)</h4>
					</div>
					<div class="modal-body">
						<form method="post" name="frm">

							<input type="text" class="form-control" name="modal_oid" id="modal_oid" style="display: none;">

							<blockquote>
								<p id="modal_before"></p>
							</blockquote>
							<input type="text" class="form-control" name="modal_input" id="modal_input" />
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" onclick="updateInvoice(document.frm.modal_oid.value,document.frm.modal_input.value)">submit</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>
	</div>
	<div class="modal fade" id="finishDelivery" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-body">
					<p class="text-center">
						<span id="finishMessage"></span><br>배송인이 정확하게 배송을 완료하였습니까? (허위일 경우 구매자의 반품사유가 될 수 있습니다.) 구매자에게 수취확인 요청 메시지가 전달됩니다.
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" onclick="finishDelivery()">배송완료</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>
</body>
<script>
$('[data-toggle="popover"]').popover();		
	$(function() {
		$(".resizeTable")

			.tablesorter({
				widgets : [ 'filter', 'resizable', 'print' ],
				widgetOptions : {
					// filter_anyMatch replaced! Instead use the filter_external option
					// Set to use a jQuery selector (or jQuery object) pointing to the
					// external filter (column specific or any match)
					filter_external : '.search',
					// add a default type search to the first name column
					filter_defaultFilter : {
						1 : '~{query}'
					},
					// include column filters
					filter_columnFilters : true,
					filter_saveFilters : true,
					filter_reset : '.reset',
					filter_liveSearch : {
						// when false, the user must press enter to blur the input to trigger the search
						3 : false,
						// the query will initiate when 5 or more characters are entered into the filter
						4 : 5,
						// no live search on the last three columns (using a header class name)
						'.last-3-columns' : false,
						// for columns that aren't defined; this will set the fallback value
						// otherwise the fallback defaults to false.
						'fallback' : true
					},

					resizable_addLastColumn : true,
					resizable_widths : [ '50px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '130px', '130px' ],
					print_title : '', // this option > caption > table id > "table"
					print_dataAttrib : 'data-name', // header attrib containing modified header name
					print_rows : 'f', // (a)ll, (v)isible, (f)iltered, or custom css selector
					print_columns : 's', // (a)ll, (v)isible or (s)elected (columnSelector widget)
					print_extraCSS : '', // add any extra css definitions for the popup window here
					//print_styleSheet : '../css/theme.blue.css', // add the url of your print stylesheet
					print_now : true, // Open the print dialog immediately if true
					// callback executed when processing completes - default setting is null
					print_callback : function(config, $table, printStyle) {
						// do something to the $table (jQuery object of table wrapped in a div)
						// or add to the printStyle string, then...
						// print the table using the following code
						$.tablesorter.printTable.printOutput(config, $table.html(), printStyle);

					}
				}
			});
		$('button[data-column]').on('click', function() {
			var $this = $(this),
				totalColumns = $table[0].config.columns,
				col = $this.data('column'), // zero-based index or "all"
				filter = [];

			// text to add to filter
			filter[col === 'all' ? totalColumns : col] = $this.text();
			$table.trigger('search', [ filter ]);
			return false;
		});
	});

	function tableprint(num) {
		switch (num) {
		case '1':
			$('#resizeTable1').trigger('printTable');
			break;
		case '2':
			$('#resizeTable2').trigger('printTable');
			break;
		case '3':
			$('#resizeTable3').trigger('printTable');
			break;
		case '4':
			$('#resizeTable4').trigger('printTable');
			break;
		default:
			break;
		}
	}


	//modal 클릭시 input value 설정
	$(document).on("click", ".updateModal", function() {
		var purchaseId = $(this).data('id');
		var before = $(this).data('before');
		$("#modal_purchaseId").val(purchaseId);
		$("#modal_before").text(before);
	});
	
	//배송완료 버튼 클릭시 유효성 체크
		$("#finish_delivery").click(function() {
			var check = false;
			var count = 0;
			
			$(".state2").each(function() {
				if (this.checked) {
					check = true;
					count++;
				}
			});

			if (check) {
				$("#finishDelivery").modal();
				$("#finishMessage").text(count + "개의 주문이 선택되었습니다.");
			} else {
				alert("주문을 선택해주세요")
			}
		});
		function finishDelivery() {
			var oid;
			var check = false;
			var count = 0;
			
			var idList = new Array();
			var emailList = new Array();
			$(".state2").each(function() {
				if (this.checked) {
					check = true;
					count++;
					oid=$(this).siblings(".oid").val();
					$("."+oid).each(function(){
						idList.push($(this).val());
						emailList.push($(this).siblings(".buyerEmail").val());
					})
				}
			});
			
			$.ajax({
				url : "../SidServlet?command=request_consignment",
				data : "idList="+idList.toString()+"&emailList="+emailList.toString()+"&state=3",
				success : function(data) {
					if (data > 0) {

						console.log("success request consingment");
						
					} else {
						console.log("fail");
					}
					location.reload();
				}
			});
		}
	
	$(function() {
		//개별 선택시 전체선택 버튼 해제
		$(".state2").click(function() {
			if ($("#checkth2").prop("checked")) {
				$("#checkth2").prop("checked", false);
			}
		});
		//테이블 체크박스 전체 선택,해제
		$("#checkth2").change(function() {
			if ($("#checkth2").prop("checked")) {
				$(".state2").each(function() {
					this.checked = true;
				});
			} else {
				$(".state2").each(function() {
					this.checked = false;
				});
			}

		});

	})
	
	function updateInvoiceModal(oid,before){
		$("#modal_oid").val(oid);
		$("#modal_before").text(before);
		$("#myModal").modal();
	}
	function updateInvoice(oid, input) {
		$('#myModal').modal('hide');
		if(input==""){
			alert("송장번호를 입력하세요.");
		}else if(input.length>30){
			alert("30자 이내로 입력해주세요.")
		}else{
			
		
		$.ajax({
			url : '../SidServlet?command=update_invoice',
			data : 'oid=' + oid + "&input=" + input,
			success : function(data) {
				if (data > 0) {
					alert("성공적으로 변경되었습니다.");
					location.reload();
				} else {
					alert("변경에 실패하였습니다. 관리자에게 문의하세요.");
				}

			}
		});
		}
		
	}
</script>

<%@ include file="../include/footer.jsp"%>