<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../include/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript"
	src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="../editor/js/HuskyEZCreator.js"
	charset="utf-8"></script>

<!--색상 스타일시트 -->
<link rel="stylesheet" type="text/css" href="../css/color.css">
<script src="http://code.jquery.com/jquery-3.1.0.min.js"></script>
<style>
h4 {
	color: #424242;
}

.panel-body {
	border: 2px solid #A4A4A4;
}

#imageName {
	text-align: center;
}

/* 업로드  */
.image {
	border: 1px solid #A4A4A4;
}

.previewImg {
	display: block;
	width: 100%;
	max-width: 100%;
	margin: 0 auto 25px auto;
	padding: 25px;
	color: #6E6E6E;
	background: #F2F2F2;
	border: 2px dashed #6E6E6E;
	text-align: center;
	-webkit-transition: box-shadow 0.3s, border-color 0.3s;
	-moz-transition: box-shadow 0.3s, border-color 0.3s;
	transition: box-shadow 0.3s, border-color 0.3s;
	height: 320px;
	max-width: 100%;
	padding: 0;
	margin: 0;
}

/* 상단바 스타일  */
.menubar {
	background-color: #fff;
	padding: 10px 10px;
}

.menubar h2 {
	display: inline;
}

.menubar.fix {
	position: fixed;
	top: 0;
	background: #BDBDBD;
	width: 90%;
	z-index: 1;
}

body {
	padding-left: 30px;
}

.menubar button {
	float: right;
}
</style>
<base target="_self">
</head>
<body>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">
			<li role="presentation"><a
				href="../SidServlet?command=purchase_list&state=0">주문 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_manage_stock">재고 관리</a></li>
			<li class="active" role="presentation"><a
				href="../SidServlet?command=product_upload">상품 등록</a></li>
			<li role="presentation"><a
				href="../product/product_manage_balance_day.jsp">정산 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_manage_delivery">배송 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_manage_customer">고객 관리</a></li>
		</ul>
	</div>
	<div class="col-md-12">
		<form enctype="multipart/form-data" method="post" name="frm">
			<div class="menubar ">
				<button type="button" class="btn btn-primary" id="submit_upload">등록하기</button>
				<button type="button" class="btn btn-primary" onclick="openPop();">미리보기</button>
			</div>
			<div class="content">
				<h2>필수 정보</h2>
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-md-6 " style="padding: 0">
							<h4 class="col-md-2" style="padding: 0">
								<label class="control-label">상품명</label>
							</h4>
							<div class="col-md-4"
								style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
								<input type="text" class="form-control" maxlength="12"
									name="itemName" placeholder="12글자 이내">
							</div>
						</div>
						<div class="col-md-6">
							<h4 class="col-md-2" style="padding: 0">
								<label class="control-label">소매가</label>
							</h4>
							<div class="col-md-3"
								style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
								<input type="number" class="form-control numberInput" min="0"
									name="retail" placeholder="소매가">
							</div>
							<h5 class="col-md-1" style="margin-left: 0; padding-left: 0">원</h5>
							<h5>
								&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox"
									name="retail_only" value="1">사업자에게만 판매
							</h5>
						</div>
						<div class="col-md-6" style="padding: 0">
							<h4 class="col-md-2" style="padding: 0">
								<label class="control-label">스와치</label>
							</h4>
							<div class="col-md-10"
								style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
								<h5 class="col-md-2"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<input type="radio" name="swatch" value="제공" id="swatch">제공
								</h5>
								<h5 class="col-md-4"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<input type="radio" name="swatch" value="사업자에게만 제공" id="swatch">사업자에게만
									제공
								</h5>
								<h5 class="col-md-3"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<input type="radio" name="swatch" value="미제공" id="swatch">미제공
								</h5>
							</div>
						</div>
						<div class="col-md-6">
							<h4 class="col-md-2" style="padding: 0">
								<label class="control-label">도매가</label>
							</h4>
							<div class="col-md-3"
								style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
								<input type="number" class="form-control numberInput"
									placeholder="도매가" min="0" name="wholesale">
							</div>
							<h5 class="col-md-1"
								style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">원</h5>
							<div class="col-md-3" style="margin-right: 0; padding-right: 0">
								<input type="number" class="form-control numberInput"
									placeholder="마 단위" min="0" name="standard">
							</div>
							<h5 class="col-md-3" style="margin-left: 0; padding-left: 0">마
								이상 구매시</h5>
						</div>

						<div class="col-md-6" style="padding: 0">
							<h4 class="col-md-2" style="padding: 0">
								<label class="control-label">폭</label>
							</h4>
							<div class="col-md-4"
								style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
								<h5 class="col-md-4"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<input type="radio" class="width_radio" name="width_radio"
										value="90">90cm
								</h5>
								<h5 class="col-md-4"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<input type="radio" class="width_radio" name="width_radio"
										value="110">110cm
								</h5>
								<h5 class="col-md-4"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<input type="radio" class="width_radio" name="width_radio"
										value="150">150cm
								</h5>
							</div>
							<div class="col-md-2"
								style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
								<input type="number" class="form-control numberInput"
									id="width_input" placeholder="입력" name="width_input" min="0">
								<input type="hidden" name="width" id="take_width">
							</div>
							<div class="col-md-2"
								style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
								<select class="form-control" name="widthUnit">
									<option value="0">단위</option>
									<option value="cm">cm</option>
									<option value="inch">inch</option>
								</select>
							</div>

						</div>
						<!-- <div class="col-md-6" >
							<h4 class="col-md-2" style="padding: 0">
								<label class="control-label">배송</label>
							</h4>
							<h5 class="col-md-2"
								style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
								<input type="checkbox" name="express" value="1">퀵
							</h5>
							<h5 class="col-md-2"
								style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
								<input type="checkbox" name="express" value="10">택배
							</h5>
						</div> -->
						<div class="col-md-6">
							<h4 class="col-md-2" style="padding: 0">
								<label class="control-label">두께</label>
							</h4>
							<div class="col-md-10" style="padding-left: 0">
								<h5 class="col-md-3"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<input type="radio" name="thick" value="매우 얇음">매우 얇음
								</h5>
								<h5 class="col-md-2"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<input type="radio" name="thick" value="얇음">얇음
								</h5>
								<h5 class="col-md-2"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<input type="radio" name="thick" value="보통">보통
								</h5>
								<h5 class="col-md-2"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<input type="radio" name="thick" value="두꺼움">두꺼움
								</h5>
								<h5 class="col-md-3"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<input type="radio" name="thick" value="매우 두꺼움">매우 두꺼움
								</h5>
							</div>
						</div>
						<div class="col-md-12" style="padding: 0; margin: 0">
							<h4>
								<label class="control-label">이미지등록</label>
							</h4>
						</div>
						<div class="col-md-12" style="margin: 0; padding: 0"
							id="imageForm1">
							<div id="mainImage1" class="col-md-2 image">
								<a href="#"> <img id="image1" src="../img/no_image.png"
									class="previewImg requireImg" /> <input type="file"
									style="display: none" class='inputFile_1' name="mainImg_1" />
									<input type="hidden" name="mainImg1" value=""
									class="hidden_input"></a>

								<p id="imageName">메인이미지</p>
							</div>
							<div class="col-md-10">
								<h4 class="col-md-2">
									<label class="control-label">색상명</label>
								</h4>
								<div class="col-md-2"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<input type="text" class="form-control colorName" maxlength="10"
										name="colorName1" placeholder="10글자 이내"> <br>
								</div>
							</div>
							<div class="col-md-10">
								<h4 class="col-md-2">
									<label class="control-label">색상</label>
								</h4>
								<div class="col-md-10" style="padding-left: 0; margin-left: 0;">
									<div>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 192, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color1" name="color1" value="RL00">
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 224, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color2" name="color1" value="YRL0">
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 255, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color3" name="color1" value="YL00">
										<a href="#" class="colorBox"
											style="background-color: rgb(224, 255, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color4" name="color1" value="GYL0">
										<a href="#" class="colorBox"
											style="background-color: rgb(192, 255, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color5" name="color1" value="GL00">
										<a href="#" class="colorBox"
											style="background-color: rgb(192, 255, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color6" name="color1" value="BGL0">
										<a href="#" class="colorBox"
											style="background-color: rgb(192, 192, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color7" name="color1" value="BL00">
										<a href="#" class="colorBox"
											style="background-color: rgb(224, 192, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color8" name="color1" value="PBL0">
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 192, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color9" name="color1" value="PL00">
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 192, 224)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color10" name="color1" value="RPL0">
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 255, 255); border: 0.5px solid grey">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color11" name="color1" value="WGBL">
									</div>
									<div>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 0, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color12" name="color1" value="RS00">
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 192, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color13" name="color1" value="YRS0">
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 255, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color14" name="color1" value="YS00">
										<a href="#" class="colorBox"
											style="background-color: rgb(192, 255, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color15" name="color1" value="GYS0">
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 255, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color16" name="color1" value="GS00">
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 255, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color17" name="color1" value="BGS0">
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 0, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color18" name="color1" value="BS00">
										<a href="#" class="colorBox"
											style="background-color: rgb(192, 0, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color19" name="color1" value="PBS0">
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 0, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color20" name="color1" value="PS00">
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 0, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color21" name="color1" value="RPS0">
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 128, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color22" name="color1" value="WGBS">
									</div>
									<div>
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 0, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color23" name="color1" value="RD00">
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 64, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color24" name="color1" value="YRD0">
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 128, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color25" name="color1" value="YD00">
										<a href="#" class="colorBox"
											style="background-color: rgb(64, 128, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color26" name="color1" value="GYD0">
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 128, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color27" name="color1" value="GD00">
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 64, 64)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color28" name="color1" value="BGD0">
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 0, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color29" name="color1" value="BD00">
										<a href="#" class="colorBox"
											style="background-color: rgb(64, 0, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color30" name="color1" value="PBD0">
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 0, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color31" name="color1" value="PD00">
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 0, 64)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color32" name="color1" value="RPD0">
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 0, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color33" name="color1" value="WGBD">
									</div>
									<br>
								</div>
							</div>
							<div class="col-md-10">
								<h4 class="col-md-2">
									<label class="control-label">재고량</label>
								</h4>
								<div class="col-md-2"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<input type="number" class="form-control stock numberInput"
										placeholder="마 단위" name="stock1" id="stock1" min="0">
								</div>
								<h5 class="col-md-1" style="margin-left: 0; padding-left: 0">마</h5>
								<h5 class="col-md-3">
									<input type="hidden" value="1"> <input type="checkbox"
										name="stock_unknown" value="1" class="stock_unknown">
									알 수 없음
								</h5>
							</div>
							<!-- <div id="mainImage2" class="col-md-2 image">

							<a href="#"> <img id="image2" src="../img/no_image.png"
								class="previewImg" /> <input type="file" style="display: none"
								class='inputFile_1' name="subImg_1" /> <input type="hidden"
								name="subImg" value="" class="hidden_input"></a>

							<p id="imageName">서브이미지1</p>
						</div>
						<div id="mainImage3" class="col-md-2 image">
							<a href="#"> <img id="image3" src="../img/no_image.png"
								class="previewImg" /> <input type="file" style="display: none"
								class='inputFile_1' name="subImg2_1" /> <input type="hidden"
								name="subImg2" value="" class="hidden_input"></a>

							<p id="imageName">서브이미지2</p>
						</div>
						<div id="mainImage4" class="col-md-2 image">
							<a href="#"> <img id="image4" src="../img/no_image.png"
								class="previewImg" /> <input type="file" style="display: none"
								class='inputFile_1' name="subImg3_1" /> <input type="hidden"
								name="subImg3" value="" class="hidden_input"></a>

							<p id="imageName">서브이미지3</p>
						</div>
						<div id="mainImage5" class="col-md-2 image">
							<a href="#"> <img id="image5" src="../img/no_image.png"
								class="previewImg" /> <input type="file" style="display: none"
								class='inputFile_1' name="subImg4_1" /> <input type="hidden"
								name="subImg4" value="" class="hidden_input"></a>

							<p id="imageName">서브이미지4</p>
						</div>
						<div id="mainImage6" class="col-md-2 image">

							<a href="#"> <img id="image6" src="../img/no_image.png"
								class="previewImg" /> <input type="file" style="display: none"
								class='inputFile_1' name="subImg5_1" /> <input type="hidden"
								name="subImg5" value="" class="hidden_input"></a>

							<p id="imageName">서브이미지5</p>
						</div> -->
						</div>
						<input type="hidden" id="add_count" name="add_count" value="1">
						<div id="add_div" class="col-md-12"
							style="width: 100%; margin: 0; padding: 0">
							<button type="button" class="btn btn-primary" id="add_button">색상
								추가</button>
							<button type="button" class="btn btn-danger" id="remove_button">X</button>
						</div>
						<input type="hidden" id="add_color" name="add_color" value="1">
						<div class="col-md-12 " style="padding: 0">
							<h4 style="padding: 0">
								<label class="control-label">상품 설명</label>
							</h4>
							<textarea name="expl" id="smarteditor" rows="10" cols="100"
								style="height: 412px; width: 100%"></textarea>
						</div>
					</div>

				</div>
				<div>
					<button id="more_1" class="btn btn-primary">추가정보 입력</button>
				</div>
				<br>
				<div id="more_info" style="padding: 0; margin: 0">
					<h2>핵심 정보</h2>
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="col-md-4 " style="padding: 0">
								<h4 class="col-md-4">
									<label class="control-label">원산지</label>
								</h4>
								<div class="col-md-6" style="padding: 0">
									<input type="text" class="form-control" placeholder="원산지"
										name="origin">
								</div>
							</div>
							<div class="col-md-8 " style="padding: 0; margin: 0">
								<h4 class="col-md-1"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<label class="control-label">용도</label>
								</h4>
								<div class="col-md-11"
									style="padding-top: 1%; margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0;">

									<label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="티셔츠">티셔츠
									</label> <label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="셔츠">셔츠
									</label> <label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="점퍼">점퍼
									</label> <label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="재킷">재킷
									</label> <label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="코트">코트
									</label> <label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="원피스">원피스
									</label> <label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="팬츠">팬츠
									</label> <label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="스커트">스커트
									</label> <label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="커튼">커튼
									</label> <label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="침구">침구
									</label>

								</div>
							</div>
							<div class="col-md-4 " style="padding: 0">
								<h4 class="col-md-4">
									<label class="control-label">제조사</label>
								</h4>
								<div class="col-md-6" style="padding: 0">
									<input type="text" class="form-control" placeholder="6칸이내"
										maxlength="6" name="manufacturer">
								</div>
							</div>

							<div class="col-md-8 " style="padding: 0; margin: 0">
								<h4 class="col-md-1"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<label class="control-label">패턴</label>
								</h4>
								<div class="col-md-11"
									style="padding-top: 1%; margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">

									<label class="checkbox-inline" style="padding-left: 0"><input
										type="radio" name="pattern" value="무지" id="no_pattern">무지</label>
									<label class="checkbox-inline"> <input type="radio"
										name="pattern" value="도트">도트
									</label> <label class="checkbox-inline"> <input type="radio"
										name="pattern" value="스트라이프">스트라이프
									</label> <label class="checkbox-inline"> <input type="radio"
										name="pattern" value="체크">체크
									</label> <label class="checkbox-inline"> <input type="radio"
										name="pattern" value="기타">기타
									</label>
								</div>
							</div>

							<div class="col-md-4 " style="padding: 0">
								<h4 class="col-md-4">
									<label class="control-label">제조년월</label>
								</h4>
								<div class="col-md-6" style="padding: 0">
									<input class="form-control" type="date" value="0000-00-00"
										name="manuDate">
								</div>
							</div>


							<div class="col-md-12">
								<table class="table" id="table_1">
									<tr>
										<td class="col-md-1" style="padding: 0"><h4>
												<label class="control-label">명칭</label>
											</h4></td>
										<td class="col-md-2">
											<ul
												style="overflow-y: auto; list-style: none; height: 100px;">
												<li><a data-toggle="pill" href="#0">우븐패브릭</a></li>
												<li><a data-toggle="pill" href="#1">팬시패브릭</a></li>
												<li><a data-toggle="pill" href="#2">기타패브릭</a></li>
												<li><a data-toggle="pill" href="#3">편물</a></li>
											</ul>
										</td>
										<td class="col-md-3">
											<div class="tab-content">
												<div id="0" class="tab-pane fade in active">
													<ul
														style="overflow-y: auto; list-style: none; height: 100px;"
														class="wovenfabric">
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="개버딘">개버딘
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="경 새티인">경 새티인
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="깅엄">깅엄
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="능직 플란넬">능직
																플란넬
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="니논">니논
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="덕">덕
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="데님">데님
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="디미티">디미티
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="라이닝 능직">라이닝
																능직
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="론">론
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="립스톱">립스톱 나일론
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="머슬린">머슬린
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="바티스트">바티스트
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="버랩">버랩
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="범포">범포
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="베드퍼드">베드퍼드
																코드
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="보일">보일
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="브로드 클로스">브로드
																클로스
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="산퉁">산퉁
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="새틴">새틴
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="샬리">샬리
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="샴브레이">샴브레이
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="시폰">시폰
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="아우팅 플란넬">아우팅
																플란넬
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="앤디크 새틴">앤디크
																새틴
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="오간디">오간디
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="오간자">오간자
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="옥스퍼드 샴브레이">옥스퍼드
																샴브레이
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="옥스퍼드 클로스">옥스퍼드
																클로스
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="위 새티인">위 새티인
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="조젯">조젯
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="진짜 크레이프">진짜
																크레이프
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="차이나 견">차이나 견
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="치노">치노
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="치즈 클로스">치즈
																클로스
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="캔버스">캔버스
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="크래시">크래시
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="크레이프 드 신">크레이프
																드 신
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="크레이프 백 새틴">크레이프
																백 새틴
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="타프타">타프타
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="트위드">트위드
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="트윌 플란넬">트윌
																플란넬
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="파능직">파능직
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="파유">파유
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="팬시 능직">팬시 능직
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="퍼케일">퍼케일
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="포플린">포플린
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="폰지">폰지
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="풀라드">풀라드
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="프린트 클로스">프린트
																클로스
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="플란넬">플란넬
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="플란넬리트">플란넬리트
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="하부타이">하부타이
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="하운드 투스">하운드
																투스
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="헤링본">헤링본
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="호난">호난
														</label></li>
													</ul>
												</div>
												<div id="1" class="tab-pane fade">
													<ul
														style="overflow-y: auto; list-style: none; height: 100px;"
														class="fancyfabric">
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="다마스크">다마스크
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="마드라스">마드라스
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="마퀴제트">마퀴제트
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="마틀라세">마틀라세
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="모미">모미
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="바크 클로스">바크
																클로스
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="베드퍼드 코드">베드퍼드
																코드
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="벨베틴">벨베틴
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="벨벳">벨벳
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="브로케이드">브로케이드
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="아이래쉬">아이래쉬
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="양면">양면
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="와플 클로스">와플
																클로스
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="코듀로이">코듀로이
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="크러시드">크러시드
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="태피스트리">태피스트리
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="테리 클로스">테리
																클로스
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="팬 벨벳">팬 벨벳
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="포켓 클로스">포켓
																클로스
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="프리즈">프리즈
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="피케">피케
														</label></li>
													</ul>
												</div>
												<div id="2" class="tab-pane fade">
													<ul
														style="overflow-y: auto; list-style: none; height: 100px;"
														class="otherfabric">
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="가죽">가죽
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="니트를 통한 패브릭">니트를
																통한 패브릭
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="라미네이트">라미네이트
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="레이스">레이스
														</label></li>

														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="망상구조물">망상구조물
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="모피">모피
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="발포제">발포제
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="배팅">배팅
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="부직포">부직포
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="브레이드">브레이드
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="스웨이드">스웨이드
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="웨딩">웨딩
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="인조 스웨이드">인조
																스웨이드
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="일반 필름">일반 필름
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="코팅패브릭">코팅패브릭
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="퀄트 패브릭">퀄트
																패브릭
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="터프트 패브릭">터프트
																패브릭
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="팽창 필름">팽창 필름
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="펠트">펠트
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="화이버필">화이버필
														</label></li>
													</ul>
												</div>
												<div id="3" class="tab-pane fade">
													<ul
														style="overflow-y: auto; list-style: none; height: 100px;"
														class="knit">
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="펑 트리코트">평
																트리코트
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="브러시 트리코트">브러시
																트리코트
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="경편 벨루어">경편
																벨루어
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="새틴 트리코트">새틴
																트리코트
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="튤">튤
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="레이스">레이스
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="써멀 클로스">써멀
																클로스
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="파워 네트">파워 네트
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="위사 삽입 경편물">위사
																삽입 경편물
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="경사 삽입 경편물">경사
																삽입 경편물
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="경사 및 위사 삽입 경편물">경사
																및 위사 삽입 경편물
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="저지">저지
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="스토키니트">스토키니트
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="자카드 저지">자카드
																저지
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="인타시아">인타시아
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="테리 클로스 편물">테리
																클로스 편물
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="벨루어">벨루어
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="인조모피">인조모피
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="프렌치 테리">프렌치
																테리
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="플리스">플리스
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="리브 편물">리브 편물
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="인터로크">인터로크
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="자카드 더블 편물">자카드
																더블 편물
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="팬시 더블 편물">팬시
																더블 편물
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="radio" name="sortName" value="펄 편물">펄 편물
														</label></li>
													</ul>
												</div>
											</div>
										</td>
										<td class="col-md-1" style="padding-right: 0"><h4>
												<label class="control-label">섬유</label>
											</h4></td>
										<td class="col-md-2">
											<ul
												style="overflow-y: auto; list-style: none; height: 100px;">
												<li><a data-toggle="pill" href="#20">식물섬유</a></li>
												<li><a data-toggle="pill" href="#21">동물섬유</a></li>
												<li><a data-toggle="pill" href="#22">재생섬유</a></li>
												<li><a data-toggle="pill" href="#23">합성섬유</a></li>

											</ul>
										</td>
										<td class="col-md-3">
											<div class="tab-content">
												<div id="20" class="tab-pane fade in active">
													<input type="hidden" id="count_fiber" value="0">
													<ul
														style="overflow-y: auto; list-style: none; height: 100px;"
														class="plantfiber">
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="면">면
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="아마">아마
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="저마">저마
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="대마">대마
														</label></li>
													</ul>

												</div>
												<div id="21" class="tab-pane fade">
													<ul
														style="overflow-y: auto; list-style: none; height: 100px;"
														class="animalfiber">
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="양모">양모
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="모헤어">모헤어
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="앙고라">앙고라
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="캐시미어">캐시미어
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="라마">라마
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="알파카">알파카
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="견">견
														</label></li>

													</ul>
												</div>
												<div id="22" class="tab-pane fade">
													<ul
														style="overflow-y: auto; list-style: none; height: 100px;"
														class="regeneratedfiber">
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="레이온">레이온
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="리오셀">리오셀
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="아세테이트">아세테이트
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="아즐론">아즐론
														</label></li>
													</ul>
												</div>
												<div id="23" class="tab-pane fade">
													<ul
														style="overflow-y: scroll; list-style: none; height: 100px;"
														class="syntheticfiber">
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="나일론">나일론
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="폴리에스테르">폴리에스테르
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="올레핀">올레핀
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="아크릴">아크릴
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="탄성중합체">탄성중합체
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="아라미드">아라미드
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="유리">유리
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="금속 섬유">금속 섬유
														</label></li>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="플리락트산">플리락트산
														</label></li>


													</ul>
												</div>
											</div>
										</td>
									</tr>
								</table>
							</div>
							<!-- <div class="col-md-12 " style="padding: 0">
							<div id="show_fiber">
								<div class="col-md-4 " style="padding: 0" style="display:hidden">
									<h4 class="col-md-4">
										<label class="control-label">면</label>
									</h4>
									<div class="col-md-6" style="padding: 0">
										<input type="text" class="form-control" placeholder="원산지"
											name="origin">
									</div>
								</div>
							</div>
						</div> -->
						</div>
					</div>

					<h2>상세 정보</h2>
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="col-md-6">
								<h3 class="col-md-12"
									style="padding-left: 0; padding-right: 0; margin-left: 0; margin-right: 0;">피륙</h3>
								<div class="col-md-12 ">
									<h4 class="col-md-2">
										<label class="control-label">중량</label>
									</h4>
									<div class="col-md-5" style="padding-left: 0; padding-right: 0">
										<input type="number" class="form-control" name="weight"
											min="0" placeholder="중량을 입력하세요">
									</div>
									<div class="col-md-2" style="padding-top: 1%">
										<h5>g/㎡</h5>
									</div>
								</div>
								<div class="col-md-12">
									<h4 class="col-md-2">
										<label class="control-label">조직</label>
									</h4>
									<div class="col-md-5" style="padding-left: 0; padding-right: 0">
										<input type="text" class="form-control"
											placeholder="조직을 입력하세요" name="weave">
									</div>
								</div>
								<div class="col-md-12 ">
									<h4 class="col-md-2">
										<label class="control-label">밀도</label>
									</h4>
									<div class="col-md-2" style="padding-left: 0; padding-right: 0">
										<input type="number" class="form-control" placeholder="질량"
											min="0" name="density_weight">
									</div>
									<div class="col-md-1">*</div>
									<div class="col-md-2" style="padding-left: 0; padding-right: 0">
										<input type="text" class="form-control" placeholder="부피"
											min="0" name="density_volume">
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<h3 class="col-md-12"
									style="padding-left: 0; padding-right: 0; margin-left: 0; margin-right: 0;">원단</h3>

								<div class="col-md-12">
									<h4 class="col-md-2">
										<label class="control-label">굵기</label>
									</h4>
									<div class="col-md-3" style="padding-left: 0; padding-right: 0">
										<input type="number" class="form-control" placeholder="경사 입력"
											min="0" name="thickWp">
									</div>
									<div class="col-md-3" style="padding-left: 0; padding-right: 0">
										<input type="number" class="form-control" placeholder="위사 입력"
											min="0" name="thickWt">
									</div>
									<div class="col-md-3" style="padding-left: 0; padding-right: 0">
										<select name="thickUnit" class="form-control">
											<option value="0">단위선택</option>
											<option value="번수">번수</option>
											<option value="데니어">데니어</option>
										</select>
									</div>
								</div>

								<div class="col-md-12">
									<h4 class="col-md-2">
										<label class="control-label">길이</label>
									</h4>
									<div class="col-md-3" style="padding-left: 0; padding-right: 0">
										<select class="form-control" name="lengthWp">
											<option value="0">경사</option>
											<option value="필라멘트사">필라멘트사</option>
											<option value="스테이플사">스테이플사</option>
										</select>
									</div>
									<div class="col-md-3" style="padding-left: 0; padding-right: 0">
										<select class="form-control" name="lengthWt">
											<option value="0">위사</option>
											<option value="필라멘트사">필라멘트사</option>
											<option value="스테이플사">스테이플사</option>
										</select>
									</div>
								</div>

								<div class="col-md-12">
									<h4 class="col-md-2">
										<label class="control-label">꼬임</label>
									</h4>
									<div class="col-md-3" style="padding-left: 0; padding-right: 0">
										<select class="form-control" name="twistWp">
											<option value="0">경사</option>
											<option value="무연사">무연사</option>
											<option value="약연사">약연사</option>
											<option value="강연사">강연사</option>
										</select>
									</div>
									<div class="col-md-3" style="padding-left: 0; padding-right: 0">
										<select class="form-control" name="twistWt">
											<option value="0">위사</option>
											<option value="무연사">무연사</option>
											<option value="약연사">약연사</option>
											<option value="강연사">강연사</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<h3 class="col-md-12"
									style="padding-left: 0; padding-right: 0; margin-left: 0; margin-right: 0;">가공</h3>
								<div class="col-md-6">
									<h4 class="col-md-2">
										<label class="control-label">가공</label>
									</h4>
									<div class="col-md-5" style="padding-left: 0">
										<input type="text" class="form-control" placeholder="가공"
											name="gagong">
									</div>

								</div>

								<div class="col-md-6 ">
									<div class="col-md-12">
										<h4 class="col-md-2">
											<label class="control-label">염색</label>
										</h4>
										<div class="col-md-3" style="padding-left: 0">
											<select class="form-control" name="dye">
												<option value="0">선택</option>
												<option value="무염">무염</option>
												<option value="침염">침염</option>
												<option value="날염">날염</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<h3 class="col-md-12"
									style="padding-left: 0; padding-right: 0; margin-left: 0; margin-right: 0;">심미</h3>
								<div class="col-md-6">
									<h4 class="col-md-2">
										<label class="control-label">질감</label>
									</h4>
									<div class="col-md-5" style="padding-left: 0">
										<input type="text" class="form-control" name="texture"
											placeholder="ex)부드러움/매우 거침">
									</div>

								</div>
								<div class="col-md-6">
									<div class="col-md-12">
										<h4 class="col-md-2">
											<label class="control-label">비침</label>
										</h4>
										<div class="col-md-9" style="padding-left: 0">
											<h5 class="col-md-4"
												style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
												<label class="checkbox-inline" style="padding-left: 0"><input
													type="radio" name="through" value="비침 없음">비침 없음</label>
											</h5>
											<h5 class="col-md-4"
												style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
												<label class="checkbox-inline" style="padding-left: 0"><input
													type="radio" name="through" value="약간 비침">약간 비침</label>
											</h5>
											<h5 class="col-md-4"
												style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
												<label class="checkbox-inline" style="padding-left: 0"><input
													type="radio" name="through" value="많이 비침">많이 비침</label>
											</h5>
										</div>
									</div>
								</div>



								<div class="col-md-6 ">
									<h4 class="col-md-2">
										<label class="control-label">광택</label>
									</h4>
									<div class="col-md-7" style="padding-left: 0">
										<h5 class="col-md-4"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<label class="checkbox-inline" style="padding-left: 0"><input
												type="radio" name="gloss" value="광택 없음">광택 없음</label>
										</h5>
										<h5 class="col-md-4"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<label class="checkbox-inline" style="padding-left: 0"><input
												type="radio" name="gloss" value="약간 빛남">약간 빛남</label>
										</h5>
										<h5 class="col-md-4"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<label class="checkbox-inline" style="padding-left: 0"><input
												type="radio" name="gloss" value="많이 빛남">많이 빛남</label>
										</h5>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="col-md-12">
										<h4 class="col-md-2">
											<label class="control-label">신축</label>
										</h4>
										<div class="col-md-9" style="padding-left: 0">
											<h5 class="col-md-4"
												style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
												<label class="checkbox-inline" style="padding-left: 0"><input
													type="radio" name="elastic" value="신축 없음">신축 없음</label>
											</h5>
											<h5 class="col-md-4"
												style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
												<label class="checkbox-inline" style="padding-left: 0"><input
													type="radio" name="elastic" value="약간 신축">약간 신축</label>
											</h5>
											<h5 class="col-md-4"
												style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
												<label class="checkbox-inline" style="padding-left: 0"><input
													type="radio" name="elastic" value="많이 신축">많이 신축</label>
											</h5>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<h4 class="col-md-1">
										<label class="control-label">유연</label>
									</h4>
									<div class="col-md-6" style="padding-left: 0">
										<h5 class="col-md-3"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<label class="checkbox-inline" style="padding-left: 0"><input
												type="radio" name="pliability" value="매우 부드러운">매우
												부드러운</label>
										</h5>
										<h5 class="col-md-2"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<label class="checkbox-inline" style="padding-left: 0"><input
												type="radio" name="pliability" value="부드러운">부드러운</label>
										</h5>
										<h5 class="col-md-2"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<label class="checkbox-inline" style="padding-left: 0"><input
												type="radio" name="pliability" value="적당한">적당한</label>
										</h5>
										<h5 class="col-md-2"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<label class="checkbox-inline" style="padding-left: 0"><input
												type="radio" name="pliability" value="빳빳한">빳빳한</label>
										</h5>
										<h5 class="col-md-3"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<label class="checkbox-inline" style="padding-left: 0"><input
												type="radio" name="pliability" value="매우 빳빳한">매우 빳빳한</label>
										</h5>
									</div>

								</div>
							</div>


						</div>
					</div>
				</div>
			</div>
		</form>
	</div>


	<script>
		var id = 0;
		var opentf;
	
	
		//폭- 하나만 선택 가능하게
	
		$(function() {
			$("#width_input").on('keydown', function() {
				$('.width_radio').each(function() {
					this.checked = false;
				})
			})
			$(".width_radio").on('click', function() {
				$('#width_input').val("");
			})
		})
	
		// 이미지 사이즈 조절
		$(window).resize(function() {
			// This will execute whenever the window is resized
	
			$(".previewImg").height($(".previewImg").width());
		});
	
		//추가 정보 펼치기
		$("#more_info").hide();
		$("#more_1").click(function() {
			$("#more_info").toggle();
			return false;
	
		});
	
		// 이미지 업로드 
		$(document).on('click', '.previewImg', function(e) {
	
			//do whatever
			e.preventDefault();
	
			$(this).next().trigger('click');
	
		});
	
		// 색상 박스
		$(document).on('click', '.colorBox', function(e) {
			//do whatever
			e.preventDefault();
			var name = $(this).next().attr("name");
			$("input[name=" + name + "]:radio").each(function() {
	
				$(this).prev().css("border", "");
			});
			$(this).css("border", "2px solid black");
			$(this).next().trigger('click');
		});
	
	
		$(document).on('click', '.stock_unknown', function(e) {
			var unknown = $(this).prev().val();
			var stockid = "#stock" + unknown
			if (this.checked) {
				$(stockid).val("-1");
				$(stockid).attr("disabled", true);
			} else {
				$(stockid).removeAttr("disabled");
				$(stockid).val("");
			}
	
		});
	
		//색상 추가
		$('#add_button').on('click', function(e) {
			e.preventDefault();
			$("#add_count").val(parseInt($("#add_count").val()) + 1);
			if ($("#add_count").val() == "1")
				$("#remove_button").hide();
			else
				$("#remove_button").show();
	
			$("#add_div").after("<div id='colorDiv" + $("#add_count").val() + "'><div class='col-md-12' style='margin: 0; padding: 0'><div class='col-md-2 image'><a href='#'><img src='../img/no_image.png' class='previewImg requireImg'/>"
				+ "<input type='file' style='display: none' class='inputFile_1' name='mainImg_" + $("#add_count").val() + "' />"
				+ "<input type='hidden' name='mainImg" + $("#add_count").val() + "' value='' class='hidden_input'></a>"
				+ "<p style='text-align:center'>메인이미지</p></div>"
				/* + "<div class='col-md-2 image'><a href='#'><img src='../img/no_image.png' class='previewImg'/>"
				+ "<input type='file' style='display: none' class='inputFile_1' name='subImg_" + $("#add_count").val() + "' /></a>"
				+ "<p>서브이미지1</p></div>"
				+ "<div class='col-md-2 image'><a href='#'><img src='../img/no_image.png' class='previewImg'/>"
				+ "<input type='file' style='display: none' class='inputFile_1' name='subImg2_" + $("#add_count").val() + "' /></a>"
				+ "<p>서브이미지2</p></div>"
				+ "<div class='col-md-2 image'><a href='#'><img src='../img/no_image.png' class='previewImg'/>"
				+ "<input type='file' style='display: none' class='inputFile_1' name='subImg3_" + $("#add_count").val() + "' /></a>"
				+ "<p>서브이미지3</p></div>"
				+ "<div class='col-md-2 image'><a href='#'><img src='../img/no_image.png' class='previewImg'/>"
				+ "<input type='file' style='display: none' class='inputFile_1' name='subImg_4" + $("#add_count").val() + "' /></a>"
				+ "<p>서브이미지4</p></div>"
				+ "<div class='col-md-2 image'><a href='#'><img src='../img/no_image.png' class='previewImg'/>"
				+ "<input type='file' style='display: none' class='inputFile_1' name='subImg_5" + $("#add_count").val() + "' /></a>"
				+ "<p>서브이미지5</p></div> */
				+ "<div class='col-md-10'><div class='col-md-12' style='padding: 0'><h4 class='col-md-2'>"
				+ "<label class='control-label'>색상명</label></h4>"
				+ "<div class='col-md-2' style='margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0'>"
				+ "<input type='text' class='colorName form-control' maxlength='6' name='colorName" + $("#add_count").val() + "' placeholder='10글자 이내'></div></div>"
				+ "<div class='col-md-12' style='padding-top: 1%; margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0'>"
				+ "<h4 class='col-md-2'>"
				+ "<label class='control-label'>색상</label></h4>"
				+ "<div>"
				+ "<a href='#' class='colorBox' style='background-color: rgb(255, 192, 192)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color1' name='color" + $("#add_count").val() + "'value='RL00'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(255, 224, 192)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color2' name='color" + $("#add_count").val() + "'value='YRL0'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(255, 255, 192)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color3' name='color" + $("#add_count").val() + "'value='YL00'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(224, 255, 192)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color4' name='color" + $("#add_count").val() + "'value='GYL0'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(192, 255, 192)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color5' name='color" + $("#add_count").val() + "' value='GL00'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(192, 255, 255)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color6' name='color" + $("#add_count").val() + "' value='BGL0'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(192, 192, 255)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color7' name='color" + $("#add_count").val() + "' value='BL00'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(224, 192, 255)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color8' name='color" + $("#add_count").val() + "'value='PBL0'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(255, 192, 255)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color9' name='color" + $("#add_count").val() + "'value='PL00'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(255, 192, 224)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color10' name='color" + $("#add_count").val() + "' value='RPL0'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(255, 255, 255);border:0.5px solid grey'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color11' name='color" + $("#add_count").val() + "' value='WGBL'>&nbsp;"
				+ "</div>"
				+ "<div>"
				+ "<a href='#' class='colorBox' style='background-color: rgb(255, 0, 0)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color12' name='color" + $("#add_count").val() + "' value='RS00'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(255, 192, 0)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color13' name='color" + $("#add_count").val() + "' value='YRS0'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(255, 255, 0)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color14' name='color" + $("#add_count").val() + "' value='YS00'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(192, 255, 0)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color15' name='color" + $("#add_count").val() + "' value='GYS0'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(0, 255, 0)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color16' name='color" + $("#add_count").val() + "' value='GS00'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(0, 255, 255)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color17' name='color" + $("#add_count").val() + "' value='BGS0'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(0, 0, 255)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color18' name='color" + $("#add_count").val() + "' value='BS00'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(192, 0, 255)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color19' name='color" + $("#add_count").val() + "' value='PBS0'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(255, 0, 255)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color20' name='color" + $("#add_count").val() + "' value='PS00'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(255, 0, 192)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color21' name='color" + $("#add_count").val() + "' value='RPS0'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(128, 128, 128)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color22' name='color" + $("#add_count").val() + "' value='WGBS'>&nbsp;"
				+ "</div>"
				+ "<div>"
				+ "<a href='#' class='colorBox' style='background-color: rgb(128, 0, 0)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color23' name='color" + $("#add_count").val() + "' value='RD00'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(128, 64, 0)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color24' name='color" + $("#add_count").val() + "' value='YRD0'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(128, 128, 0)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color25' name='color" + $("#add_count").val() + "' value='YD00'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(64, 128, 0)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color26' name='color" + $("#add_count").val() + "' value='GYD0'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(0, 128, 0)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color27' name='color" + $("#add_count").val() + "' value='GD00'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(0, 64, 64)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color28' name='color" + $("#add_count").val() + "' value='BGD0'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(0, 0, 128)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color29' name='color" + $("#add_count").val() + "' value='BD00'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(64, 0, 128)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color30' name='color" + $("#add_count").val() + "' value='PBD0'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(128, 0, 128)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color31' name='color" + $("#add_count").val() + "' value='PD00'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(128, 0, 64)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color32' name='color" + $("#add_count").val() + "' value='RPD0'>&nbsp;"
				+ "<a href='#' class='colorBox' style='background-color: rgb(0, 0, 0)'>&nbsp;&nbsp;&nbsp;&nbsp;</a>"
				+ "<input type='radio' class='color33' name='color" + $("#add_count").val() + "' value='WGBD'>&nbsp;"
				+ "</div>"
				+ "<br></div>"
				+ "<div class='col-md-12' style='padding: 0'>"
				+ "<h4 class='col-md-2'>"
				+ "<label class='control-label'>재고량</label></h4>"
				+ "<div class='col-md-2' style='margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0'>"
				+ "<input type='number' class='form-control stock numberInput' id='stock" + $("#add_count").val() + "' min='0' placeholder='마 단위' name='stock" + $("#add_count").val() + "' required></div>"
				+ "<h5 class='col-md-1' style='margin-left: 0; padding-left: 0'>마</h5>"
				+ "<h5 class='col-md-3'><input type='hidden' value='" + $("#add_count").val() + "'><input type='checkbox' name='stock_unknown" + $("#add_count").val() + "' value='1' class='stock_unknown' required> 알 수 없음	</h5>"
				+ "<br><br><br><br></div>"
				+ "</div></div>");
	
	
			$(".previewImg").height($(".previewImg").width());
		});
	
		$('#remove_button').on('click', function(e) {
			var count = $("#add_count").val();
			$("#colorDiv" + count).remove();
			$("#add_count").val(parseInt($("#add_count").val()) - 1);
	
			if ($("#add_count").val() == "1")
				$("#remove_button").hide();
			else
				$("#remove_button").show();
		});
	
	
		function readURL1(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader(); //파일을 읽기 위한 FileReader객체 생성
				reader.onload = function(e) {
					//파일 읽어들이기를 성공했을때 호출되는 이벤트 핸들러
					//이미지 Tag의 SRC속성에 읽어들인 File내용을 지정
					//(아래 코드에서 읽어들인 dataURL형식)
					$(input).siblings('.previewImg').attr('src', e.target.result);
					$(input).siblings('.hidden_input').val(e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			//File내용을 읽어 dataURL형식의 문자열로 저장
			}
		} //readURL()--
		//file 양식으로 이미지를 선택(값이 변경) 되었을때 처리하는 코드
		$(document).on('change', '.inputFile_1', function() {
	
			//alert(this.value); //선택한 이미지 경로 표시
			readURL1(this);
		/* $('#imageUrl').val(this.value);
		                         */
		});
	
		//상단바 고정
		$(function() {
			var top_pos = $('.menubar').offset().top;
			var width = $('.menubar').width();
			win = window;
			$(win).on('scroll',
				function() {
					var pos = $(this).scrollTop();
					$('#content').attr('value', pos);
					if (pos >= top_pos) {
						$('.menubar').addClass('fix');
						$('.fix').width(width);
					} else {
						$('.menubar').removeClass('fix');
					}
				});
	
		});
	
		// 팝업오픈하여 폼데이터 Post 전송
		function openPop() {
			var pop_title = "popupOpener";
	
			window.open("", pop_title, "width=1200, height=700, toolbar=no, menubar=no, scrollbars=yes, resizable=yes") ;
			var frmData = document.frm;
			frmData.target = pop_title ;
			frmData.action = "../product/productUpload_preview.jsp";
			frmData.submit() ;
		}
		;
	
	
	
	
		/* 	//섬유 선택시 %창 띄우기
			$('input:checkbox[name="fiber"]').click(function() {
				console.log("show " + this.checked);
					if (this.checked) { //checked 처리된 항목의 값
						console.log("show" + this.value);
						$("#count_fiber").val(parseInt(("#count_fiber").val())+1);
						$("#show_fiber").before("<div class='col-md-4' style='padding: 0' id='"+$("#count_fiber").val()+"'><h4 class='col-md-4'><label class='control-label'>"+this.value+"</label></h4>"
						+"<div class='col-md-6' style='padding: 0'><input type='text' class='form-control'name='origin'></div></div>");
					};
		
					if (!this.checked) {
						 console.log("remove" + this.value);
						$("#count_fiber").val().remove();
					};
					
					
			}); */
	
	
		$(document).on('click', '#submit_upload', function(e) {
	
			flag = 1;
			if (document.frm.itemName.value.length == 0) {
				alert("상품명을 써주세요");
				frm.itemName.focus();
				flag = 0;
				return false;
			}
	
			$(".colorName").each(function() {
				if (this.value == "") {
					alert("색상명을 써주세요");
					this.focus();
					flag = 0;
					return false;
				}
			});
			if (!flag)
				return false;
			if (document.frm.retail.value.length == 0) {
				alert("소매가를 입력하세요.");
				frm.retail.focus();
				flag = 0;
				return false;
			}
			if (document.frm.wholesale.value.length == 0) {
				alert("도매가를 입력하세요.");
				frm.wholesale.focus();
				flag = 0;
				return false;
			}
			if (document.frm.standard.value.length == 0) {
				alert("도매가 기준을 입력하세요.");
				frm.standard.focus();
				flag = 0;
				return false;
			}
			if (!$('input[name=swatch]:radio:checked').val()) {
				alert("스와치 제공여부를 선택하세요.");
				flag = 0;
				return false;
			}
			$(".stock").each(function() {
				if (this.value == "") {
					alert("재고량을 써주세요");
					this.focus();
					flag = 0;
					return false;
				}
			});
			if (!flag)
				return false;
			if (document.getElementById("width_input").value == "" && !$('input[name=width_radio]:radio:checked').val()) {
				alert("폭을 선택하세요.");
				flag = 0;
				return false;
			}
			if (document.getElementById("width_input").value.length != 0 && document.frm.widthUnit.value == 0) {
				alert("폭의 단위를 선택하세요.");
				flag = 0;
				return false;
			}
			if (!$('input[name=thick]:radio:checked').val()) {
				alert("두께를 선택하세요.");
				flag = 0;
				return false;
			}
			var count = $("#add_count").val();
			for (var i = 0; i < count; i++) {
				if (!$('input[name=color' + (i + 1) + ']:radio:checked').val()) {
					alert("색상을 선택하세요.");
					flag = 0;
					return false;
				}
			}
			/* if (!$('input[name=express]:checkbox:checked').val()) {
				alert("배송 방법을 선택하세요.");
				flag = 0;
				return false;
			} */
			//이미지
			$(".requireImg").each(function() {
				if ($(this).attr('src') == "../img/no_image.png") {
					alert("이미지를 넣어주세요");
					this.focus();
					flag = 0;
					return false;
				}
			});
			if (!flag)
				return false;
	
			if (flag == 1)
				if ($('input[name=width_radio]:radio:checked').val()) {
					document.getElementById("take_width").value = $('input[name=width_radio]:radio:checked').val();
				} else {
					document.getElementById("take_width").value = document.frm.width_input.value;
			}
			submitProduct();
	
		});
		// smarteditor
		$(".previewImg").height($(".previewImg").width());
		//전역변수선언
		var editor_object = [];
	
		nhn.husky.EZCreator.createInIFrame({
			oAppRef : editor_object,
			elPlaceHolder : "smarteditor", //연결지을 textarea의 id명
			sSkinURI : "/editor/SmartEditor2Skin.html", //에디터 스킨 경로
			htParams : {
				// 툴바 사용 여부 (true:사용/ false:사용하지 않음)
				bUseToolbar : true,
				// 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
				bUseVerticalResizer : true,
				// 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
				bUseModeChanger : true,
			}
		});
	
		//전송버튼 클릭이벤트
		function submitProduct() {
			//id가 smarteditor인 textarea에 에디터에서 대입\
			var input = confirm("등록하시겠습니까?");
			if (input == true) {
	
				editor_object.getById["smarteditor"].exec("UPDATE_CONTENTS_FIELD", []);
				// 이부분에 에디터 validation 검증
	
				document.frm.action = "../SidServlet?command=product_submit";
				document.frm.target = "_self";
				document.frm.submit();
	
			} else {
				return;
			}
			//폼 submit
			//	$("#fm").submit();
	
		}
		;
	
		$(document).on('change', '.numberInput', function(e) {
	
			if (this.value > 999999) {
				this.value = 999999;
			}
			if (this.value < 1) {
				this.value = 1;
			}
		});
	</script>
</body>
</html>
<%@ include file="../include/footer.jsp"%>