<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>SID-TEX</title>

<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="../css/cover.css" rel="stylesheet">


<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="../js/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/docs.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../js/ie10-viewport-bug-workaround.js"></script>
<script src="../js/colResizable-1.6.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link href="../css/custom.css" rel="stylesheet">
<!-- table sorter -->
<link rel="stylesheet" href="../css/theme.default.css">
<script type="text/javascript" src="../js/table/jquery.tablesorter.js"></script>
<script type="text/javascript" src="../js/table/widget-filter.js"></script>
<script type="text/javascript" src="../js/table/widget-resizable.js"></script>
<script type="text/javascript" src="../js/table/widget-stickyHeaders.js"></script>
<script type="text/javascript" src="../js/table/widget-print.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript"
	src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="../editor/js/HuskyEZCreator.js"
	charset="utf-8"></script>

<!--색상 스타일시트 -->
<link rel="stylesheet" type="text/css" href="../css/color.css">
<script src="http://code.jquery.com/jquery-3.1.0.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
h4 {
	color: #424242;
}

.panel-body {
	border: 2px solid #A4A4A4;
}

#imageName {
	text-align: center;
}

/* 업로드  */
.image {
	border: 1px solid #A4A4A4;
}

.previewImg {
	display: block;
	width: 100%;
	max-width: 100%;
	margin: 0 auto 25px auto;
	padding: 25px;
	color: #6E6E6E;
	background: #F2F2F2;
	border: 2px dashed #6E6E6E;
	text-align: center;
	-webkit-transition: box-shadow 0.3s, border-color 0.3s;
	-moz-transition: box-shadow 0.3s, border-color 0.3s;
	transition: box-shadow 0.3s, border-color 0.3s;
	height: 320px;
	max-width: 100%;
	padding: 0;
	margin: 0;
}

/* 상단바 스타일  */
.menubar {
	background-color: #fff;
	padding: 10px 10px;
}

.menubar h2 {
	display: inline;
}

.menubar.fix {
	position: fixed;
	top: 0;
	background: #BDBDBD;
	width: 90%;
	z-index: 1;
}

body {
	padding-left: 30px;
}

.menubar button {
	float: right;
}
</style>
</head>
<body>
	<br>
	<br>
	<div class="col-md-12">
		<form enctype="multipart/form-data" method="post" name="frm">
			<div class="menubar ">
				<button type="button" class="btn btn-primary" id="submit_upload">수정하기</button>
			</div>
			<div class="content">
				<h2>필수 정보</h2>
				<div class="panel panel-default">
					<div class="panel-body">
							<input type="hidden" name="productId" value=${product.productID}>
						<%-- <div class="col-md-6 " style="padding: 0">
							<h4 class="col-md-2" style="padding: 0">
								<label class="control-label">상품명</label>
							</h4>
							<div class="col-md-4"
								style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
								<input type="text" class="form-control" maxlength="6"
									name="itemName" placeholder="6글자 이내"
									value=${product.itemName
									} readonly>
							</div>
						</div>
						<div class="col-md-6" >
							<h4 class="col-md-2" style="padding: 0">
								<label class="control-label">소매가</label>
							</h4>
							<div class="col-md-3"
								style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
								<input type="number" class="form-control" maxlength="10" min="0"
									name="retail" placeholder="소매가" value=${product.retail }
									readonly>
							</div>
							<h5 class="col-md-1" style="margin-left: 0; padding-left: 0">원</h5>
							<h5>
								<c:choose>
									<c:when test="${product.retail_only eq 1 }">
										&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="retail_only" value="1" checked>사업자에게만 판매
									</c:when>
									<c:otherwise>
										&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="retail_only" value="1">사업자에게만 판매
									</c:otherwise>
								</c:choose>

							</h5>
						</div>
						<div class="col-md-6" style="padding: 0">
							<h4 class="col-md-2" style="padding: 0">
								<label class="control-label">스와치</label>
							</h4>
							<div class="col-md-10"
								style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
								<h5 class="col-md-2"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<c:choose>
										<c:when test="${product.swatch eq '제공' }">
											<input type="radio" name="swatch" value="제공" id="swatch"
												checked disabled>제공
										</c:when>
										<c:otherwise>
											<input type="radio" name="swatch" value="제공" id="swatch"
												disabled>제공
										</c:otherwise>
									</c:choose>
								</h5>
								<h5 class="col-md-4"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<c:choose>
										<c:when test="${product.swatch eq '사업자에게만 제공' }">
											<input type="radio" name="swatch" value="사업자에게만 제공"
												id="swatch" checked disabled>사업자에게만 제공
										</c:when>
										<c:otherwise>
											<input type="radio" name="swatch" value="사업자에게만 제공"
												id="swatch" disabled>사업자에게만 제공
										</c:otherwise>
									</c:choose>
								</h5>
								<h5 class="col-md-3"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<c:choose>
										<c:when test="${product.swatch eq '미제공' }">
											<input type="radio" name="swatch" value="미제공" id="swatch"
												checked disabled>미제공
										</c:when>
										<c:otherwise>
											<input type="radio" name="swatch" value="미제공" id="swatch"
												disabled>미제공
										</c:otherwise>
									</c:choose>
								</h5>
							</div>
						</div>
						<div class="col-md-6" >
							<h4 class="col-md-2" style="padding: 0">
								<label class="control-label">도매가</label>
							</h4>
							<div class="col-md-3"
								style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
								<input type="number" class="form-control" placeholder="도매가"
									min="0" name="wholesale" value=${product.wholesale } readonly>
							</div>
							<h5 class="col-md-1"
								style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">원</h5>
							<div class="col-md-3" style="margin-right: 0; padding-right: 0">
								<input type="number" class="form-control" placeholder="마 단위"
									min="0" name="standard" maxlength="5"
									value=${product.standard }>
							</div>
							<h5 class="col-md-3" style="margin-left: 0; padding-left: 0">마
								이상 구매시</h5>
						</div>

						<div class="col-md-6 " style="padding: 0">
							<h4 class="col-md-2" style="padding: 0">
								<label class="control-label">폭</label>
							</h4>

							<c:choose>
								<c:when test="${product.width eq 90 }">
									<div class="col-md-4"
										style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
										<h5 class="col-md-4"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<input type="radio" class="width_radio" name="width_radio"
												value="90" checked>90cm
										</h5>
										<h5 class="col-md-4"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<input type="radio" class="width_radio" name="width_radio"
												value="110">110cm
										</h5>
										<h5 class="col-md-4"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<input type="radio" class="width_radio" name="width_radio"
												value="150">150cm
										</h5>
									</div>
									<div class="col-md-3 "
										style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
										<input type="number" class="form-control" id="width_input"
											placeholder="직접 입력" name="width_input" min="0"> <input
											type="hidden" name="width" id="take_width">
									</div>
									<div class="col-md-2"
										style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
										<select class="form-control" name="widthUnit">
											<option value="0">단위</option>
											<option value="cm">cm</option>
											<option value="inch">inch</option>
										</select>
									</div>
								</c:when>
								<c:when test="${product.width eq 110 }">
									<div class="col-md-4"
										style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
										<h5 class="col-md-4"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<input type="radio" class="width_radio" name="width_radio"
												value="90">90cm
										</h5>
										<h5 class="col-md-4"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<input type="radio" class="width_radio" name="width_radio"
												value="110" checked>110cm
										</h5>
										<h5 class="col-md-4"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<input type="radio" class="width_radio" name="width_radio"
												value="150">150cm
										</h5>
									</div>
									<div class="col-md-3 "
										style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
										<input type="number" class="form-control" id="width_input"
											placeholder="직접 입력" name="width_input" min="0"> <input
											type="hidden" name="width" id="take_width">
									</div>
									<div class="col-md-2"
										style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
										<select class="form-control" name="widthUnit">
											<option value="0">단위</option>
											<option value="cm">cm</option>
											<option value="inch">inch</option>
										</select>
									</div>
								</c:when>
								<c:when test="${product.width eq 150 }">
									<div class="col-md-4"
										style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
										<h5 class="col-md-4"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<input type="radio" class="width_radio" name="width_radio"
												value="90">90cm
										</h5>
										<h5 class="col-md-4"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<input type="radio" class="width_radio" name="width_radio"
												value="110">110cm
										</h5>
										<h5 class="col-md-4"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<input type="radio" class="width_radio" name="width_radio"
												value="150" checked>150cm
										</h5>
									</div>
									<div class="col-md-3 "
										style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
										<input type="number" class="form-control" id="width_input"
											placeholder="직접 입력" name="width_input" min="0"> <input
											type="hidden" name="width" id="take_width">
									</div>
									<div class="col-md-2"
										style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
										<select class="form-control" name="widthUnit">
											<option value="0">단위</option>
											<option value="cm">cm</option>
											<option value="inch">inch</option>
										</select>
									</div>
								</c:when>
								<c:otherwise>
									<div class="col-md-4"
										style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
										<h5 class="col-md-4"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<input type="radio" class="width_radio" name="width_radio"
												value="90">90cm
										</h5>
										<h5 class="col-md-4"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<input type="radio" class="width_radio" name="width_radio"
												value="110">110cm
										</h5>
										<h5 class="col-md-4"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<input type="radio" class="width_radio" name="width_radio"
												value="150">150cm
										</h5>
									</div>
									<div class="col-md-3 "
										style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
										<input type="number" class="form-control" id="width_input"
											placeholder="직접 입력" name="width_input" min="0"
											value=${product.width }> <input type="hidden"
											name="width" id="take_width">
									</div>
									<div class="col-md-2"
										style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
										<select class="form-control" name="widthUnit">
											<option value="0">단위</option>
											<option value="cm" selected>cm</option>
											<option value="inch">inch</option>
										</select>
									</div>
								</c:otherwise>
							</c:choose>
						</div>
						
						<div class="col-md-6" style="padding: 0">
							<h4 class="col-md-2">
								<label class="control-label">배송</label>
							</h4>
							<c:choose>
								<c:when test="${product.express eq 1 }">
									<h5 class="col-md-2"
										style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
										<input type="checkbox" name="express" value="1" checked>퀵
									</h5>
									<h5 class="col-md-2"
										style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
										<input type="checkbox" name="express" value="10">택배
									</h5>
								</c:when>
								<c:when test="${product.express eq 10 }">
									<h5 class="col-md-2"
										style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
										<input type="checkbox" name="express" value="1">퀵
									</h5>
									<h5 class="col-md-2"
										style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
										<input type="checkbox" name="express" value="10" checked>택배
									</h5>
								</c:when>
								<c:when test="${product.express eq 11 }">
									<h5 class="col-md-2"
										style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
										<input type="checkbox" name="express" value="1" checked>퀵
									</h5>
									<h5 class="col-md-2"
										style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
										<input type="checkbox" name="express" value="10" checked>택배
									</h5>
								</c:when>
								<c:otherwise>
									<h5 class="col-md-2"
										style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
										<input type="checkbox" name="express" value="1">퀵
									</h5>
									<h5 class="col-md-2"
										style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
										<input type="checkbox" name="express" value="10">택배
									</h5>
								</c:otherwise>
							</c:choose>

						</div>
						<div class="col-md-6" >
							<h4 class="col-md-2" style="padding: 0">
								<label class="control-label">두께</label>
							</h4>
							<div class="col-md-10" style="padding-left: 0">
								<h5 class="col-md-3"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<c:choose>
										<c:when test="${product.thick eq '매우 얇음' }">
											<input type="radio" name="thick" value="매우 얇음" checked >매우 얇음
									</c:when>
										<c:otherwise>
											<input type="radio" name="thick" value="매우 얇음">매우 얇음
									</c:otherwise>
									</c:choose>
								</h5>
								<h5 class="col-md-2"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<c:choose>
										<c:when test="${product.thick eq '얇음' }">
											<input type="radio" name="thick" value="얇음" checked>얇음
									</c:when>
										<c:otherwise>
											<input type="radio" name="thick" value="얇음">얇음
									</c:otherwise>
									</c:choose>
								</h5>
								<h5 class="col-md-2"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<c:choose>
										<c:when test="${product.thick eq '보통' }">
											<input type="radio" name="thick" value="보통" checked>보통
									</c:when>
										<c:otherwise>
											<input type="radio" name="thick" value="보통">보통
									</c:otherwise>
									</c:choose>
								</h5>
								<h5 class="col-md-2"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<c:choose>
										<c:when test="${product.thick eq '두꺼움' }">
											<input type="radio" name="thick" value="두꺼움" checked>두꺼움
									</c:when>
										<c:otherwise>
											<input type="radio" name="thick" value="두꺼움">두꺼움
									</c:otherwise>
									</c:choose>
								</h5>
								<h5 class="col-md-3"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<c:choose>
										<c:when test="${product.thick eq '매우 두꺼움' }">
											<input type="radio" name="thick" value="매우 두꺼움" checked>매우 두꺼움
									</c:when>
										<c:otherwise>
											<input type="radio" name="thick" value="매우 두꺼움">매우 두꺼움
									</c:otherwise>
									</c:choose>
								</h5>
							</div>
						</div> --%>
						<div class="col-md-12" style="padding: 0; margin: 0">
							<h4>
								<label class="control-label">이미지등록</label>
							</h4>
						</div>
						<div class="col-md-12" style="margin: 0; padding: 0"
							id="imageForm1">
							<div id="mainImage1" class="col-md-2 image">
								<a href="#"> <img id="image1" src=${product.mainImg }
									class="previewImg requireImg" /> <input type="file"
									style="display: none" class='inputFile_1' name="mainImg_1"
									value=${product.mainImg } /> <input type="hidden"
									name="mainImg1" class="hidden_input"></a>

								<p id="imageName">메인이미지</p>
							</div>
							<div class="col-md-10">
								<h4 class="col-md-2">
									<label class="control-label">색상명</label>
								</h4>
								<div class="col-md-2"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<input type="text" class="form-control colorName" maxlength="6"
										name="colorName1" placeholder="6글자 이내"
										value=${product.colorName}>
								</div>
							</div>
							<div class="col-md-10">
								<h4 class="col-md-2">
									<label class="control-label">색상</label>
								</h4>
								<div class="col-md-10"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<div>
										<%
											String arr3 = ((String) request.getAttribute("color"));
											arr3 = arr3.substring(1, arr3.length() - 1);
											System.out.println("color : " + arr3);
											if (arr3.contains("RL00")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 192, 192); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color1" name="color1" value="RL00"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 192, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color1" name="color1" value="RL00">
										<%
											}
										%>
										<%
											if (arr3.contains("YRL0")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 224, 192); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color2" name="color1" value="YRL0"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 224, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color2" name="color1" value="YRL0">
										<%
											}
										%>
										<%
											if (arr3.contains("YL00")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 255, 192); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color3" name="color1" value="YL00"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 255, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color3" name="color1" value="YL00">
										<%
											}
										%>

										<%
											if (arr3.contains("GYL0")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(224, 255, 192); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color4" name="color1" value="GYL0"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(224, 255, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color4" name="color1" value="GYL0">
										<%
											}
										%>

										<%
											if (arr3.contains("GL00")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(192, 255, 192); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color5" name="color1" value="GL00"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(192, 255, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color5" name="color1" value="GL00">
										<%
											}
										%>

										<%
											if (arr3.contains("BGL0")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(192, 255, 255); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color6" name="color1" value="BGL0"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(192, 255, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color6" name="color1" value="BGL0">
										<%
											}
										%>

										<%
											if (arr3.contains("BL00")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(192, 192, 255); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color7" name="color1" value="BL00"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(192, 192, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color7" name="color1" value="BL00">
										<%
											}
										%>

										<%
											if (arr3.contains("PBL0")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(224, 192, 255); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color8" name="color1" value="PBL0"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(224, 192, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color8" name="color1" value="PBL0">
										<%
											}
										%>

										<%
											if (arr3.contains("PL00")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 192, 255); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color9" name="color1" value="PL00"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 192, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color9" name="color1" value="PL00">
										<%
											}
										%>

										<%
											if (arr3.contains("RPL0")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 192, 224); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color10" name="color1" value="RPL0"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 192, 224)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color10" name="color1" value="RPL0">
										<%
											}
										%>

										<%
											if (arr3.contains("WGBL")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 255, 255); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color11" name="color1" value="WGBL"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 255, 255);">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color11" name="color1" value="WGBL">
										<%
											}
										%>

									</div>
									<div>
										<%
											if (arr3.contains("RS00")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 0, 0); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color12" name="color1" value="RS00"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 0, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color12" name="color1" value="RS00">
										<%
											}
										%>

										<%
											if (arr3.contains("YRS0")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 192, 0); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color13" name="color1" value="YRS0"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 192, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color13" name="color1" value="YRS0">
										<%
											}
										%>

										<%
											if (arr3.contains("YS00")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 255, 0); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color14" name="color1" value="YS00"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 255, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color14" name="color1" value="YS00">
										<%
											}
										%>

										<%
											if (arr3.contains("GYS0")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(192, 255, 0); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color15" name="color1" value="GYS0"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(192, 255, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color15" name="color1" value="GYS0">
										<%
											}
										%>

										<%
											if (arr3.contains("GS00")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 255, 0); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color16" name="color1" value="GS00"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 255, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color16" name="color1" value="GS00">
										<%
											}
										%>

										<%
											if (arr3.contains("BGS0")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 255, 255); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color17" name="color1" value="BGS0"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 255, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color17" name="color1" value="BGS0">
										<%
											}
										%>

										<%
											if (arr3.contains("BS00")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 0, 255); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color18" name="color1" value="BS00"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 0, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color18" name="color1" value="BS00">
										<%
											}
										%>

										<%
											if (arr3.contains("PBS0")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(192, 0, 255); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color19" name="color1" value="PBS0"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(192, 0, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color19" name="color1" value="PBS0">
										<%
											}
										%>

										<%
											if (arr3.contains("PS00")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 0, 255); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color20" name="color1" value="PS00"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 0, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color20" name="color1" value="PS00">
										<%
											}
										%>

										<%
											if (arr3.contains("RPS0")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 0, 192); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color21" name="color1" value="RPS0"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 0, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color21" name="color1" value="RPS0">
										<%
											}
										%>

										<%
											if (arr3.contains("WGBS")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 128, 128); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color22" name="color1" value="WGBS"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 128, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color22" name="color1" value="WGBS">
										<%
											}
										%>

									</div>
									<div>
										<%
											if (arr3.contains("RD00")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 0, 0); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color23" name="color1" value="RD00"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 0, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color23" name="color1" value="RD00">
										<%
											}
										%>
										<%
											if (arr3.contains("YRD0")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 64, 0); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color24" name="color1" value="YRD0"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 64, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color24" name="color1" value="YRD0">
										<%
											}
										%>
										<%
											if (arr3.contains("YD00")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 128, 0); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color25" name="color1" value="YD00"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 128, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color25" name="color1" value="YD00">
										<%
											}
										%>
										<%
											if (arr3.contains("GYD0")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(64, 128, 0); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color26" name="color1" value="GYD0"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(64, 128, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color26" name="color1" value="GYD0">
										<%
											}
										%>
										<%
											if (arr3.contains("GD00")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 128, 0); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color27" name="color1" value="GD00"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 128, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color27" name="color1" value="GD00">
										<%
											}
										%>
										<%
											if (arr3.contains("BGD0")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 64, 64); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color28" name="color1" value="BGD0"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 64, 64)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color28" name="color1" value="BGD0">
										<%
											}
										%>
										<%
											if (arr3.contains("BD00")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 0, 128); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color29" name="color1" value="BD00"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 0, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color29" name="color1" value="BD00">
										<%
											}
										%>


										<%
											if (arr3.contains("PBD0")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(64, 0, 128); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color30" name="color1" value="PBD0"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(64, 0, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color30" name="color1" value="PBD0">
										<%
											}
										%>


										<%
											if (arr3.contains("PD00")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 0, 128); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color31" name="color1" value="PD00"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 0, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color31" name="color1" value="PD00">
										<%
											}
										%>
										<%
											if (arr3.contains("RPD0")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 0, 64); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color32" name="color1" value="RPD0"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 0, 64)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color32" name="color1" value="RPD0">
										<%
											}
										%>
										<%
											if (arr3.contains("WGBD")) {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 0, 0); border: 2px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color33" name="color1" value="WGBD"
											checked>
										<%
											} else {
										%>
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 0, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
										<input type="radio" class="color33" name="color1" value="WGBD">
										<%
											}
										%>
									</div>
								</div>
							</div>
							<div class="col-md-10">
								<h4 class="col-md-2">
									<label class="control-label">재고량</label>
								</h4>
								<c:choose>
									<c:when test="${product.stock eq -1 }">
										<div class="col-md-2"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<input type="number" class="form-control stock" disabled
												placeholder="마 단위" name="stock1" id="stock1" min="0"
												value=-1>
										</div>
										<h5 class="col-md-1" style="margin-left: 0; padding-left: 0">마</h5>
										<h5 class="col-md-3">
											<input type="hidden" value="1"> <input
												type="checkbox" name="stock_unknown" value="1"
												class="stock_unknown" checked> 알 수 없음
										</h5>
									</c:when>
									<c:otherwise>
										<div class="col-md-2"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<input type="number" class="form-control stock"
												placeholder="마 단위" name="stock1" id="stock1" min="0"
												value=${product.stock}>
										</div>
										<h5 class="col-md-1" style="margin-left: 0; padding-left: 0">마</h5>
										<h5 class="col-md-3">
											<input type="hidden" value="1"> <input
												type="checkbox" name="stock_unknown" value="1"
												class="stock_unknown"> 알 수 없음
										</h5>
									</c:otherwise>
								</c:choose>
							</div>
							<%-- <div id="mainImage2" class="col-md-2 image">
								<c:choose>
									<c:when test="${product.subImg eq '../img/null' }">
										<a href="#"> <img id="image2" src="../img/no_image.png"
											class="previewImg" /><input type="file"
											style="display: none" class='inputFile_1' name="subImg_1" />
											<input type="hidden" name="subImg" value=""
											class="hidden_input"></a>

									</c:when>
									<c:otherwise>
										<a href="#"> <img id="image2"
											src=${product.subImg
											} class="previewImg" /><input
											type="file" style="display: none" class='inputFile_1'
											name="subImg_1" value=${product.subImg
											} /> <input
											type="hidden" name="subImg" value="" class="hidden_input"></a>
									</c:otherwise>
								</c:choose>

								<p id="imageName">서브이미지1</p>
							</div>
							<div id="mainImage3" class="col-md-2 image">
								<c:choose>
									<c:when test="${product.subImg2 eq '../img/null' }">
										<a href="#"> <img id="image3" src="../img/no_image.png"
											class="previewImg" /><input type="file"
											style="display: none" class='inputFile_1' name="subImg2_1" />
											<input type="hidden" name="subImg2" value=""
											class="hidden_input"></a>
									</c:when>
									<c:otherwise>
										<a href="#"> <img id="image3"
											src=${product.subImg2
											} class="previewImg" /><input
											type="file" style="display: none" class='inputFile_1'
											name="subImg2_1" value=${product.subImg2
											} /> <input
											type="hidden" name="subImg2" value="" class="hidden_input"></a>
									</c:otherwise>
								</c:choose>


								<p id="imageName">서브이미지2</p>
							</div>
							<div id="mainImage4" class="col-md-2 image">
								<c:choose>
									<c:when test="${product.subImg3 eq '../img/null' }">
										<a href="#"> 
										<img id="image4" src="../img/no_image.png"
											class="previewImg" />
											<input type="file"
											style="display: none" class='inputFile_1' name="subImg3_1" />
											<input type="hidden" name="subImg3" value=""
											class="hidden_input"></a>
									</c:when>
									<c:otherwise>
										<a href="#"> <img id="image4"
											src=${product.subImg3
											} class="previewImg" />
											<input
											type="file" style="display: none" class='inputFile_1'
											name="subImg3_1" value=${product.subImg3
											} />
											 <input
											type="hidden" name="subImg3" value="" class="hidden_input"></a>
									</c:otherwise>
								</c:choose>


								<p id="imageName">서브이미지3</p>
							</div>
							<div id="mainImage5" class="col-md-2 image">
								<c:choose>
									<c:when test="${product.subImg4 eq '../img/null' }">
										<a href="#"> <img id="image5" src="../img/no_image.png"
											class="previewImg" /><input type="file"
											style="display: none" class='inputFile_1' name="subImg4_1" />
											<input type="hidden" name="subImg4" value=""
											class="hidden_input"></a>
									</c:when>
									<c:otherwise>
										<a href="#"> <img id="image5"
											src=${product.subImg4
											} class="previewImg" /><input
											type="file" style="display: none" class='inputFile_1'
											name="subImg4_1" value=${product.subImg4
											} /> <input
											type="hidden" name="subImg4" value="" class="hidden_input"></a>
									</c:otherwise>
								</c:choose>


								<p id="imageName">서브이미지4</p>
							</div>
							<div id="mainImage6" class="col-md-2 image">
								<c:choose>
									<c:when test="${product.subImg5 eq '../img/null' }">
										<a href="#"> <img id="image6" src="../img/no_image.png"
											class="previewImg" /><input type="file"
											style="display: none" class='inputFile_1' name="subImg5_1" />
											<input type="hidden" name="subImg5" value=""
											class="hidden_input"></a>
									</c:when>
									<c:otherwise>
										<a href="#"> <img id="image6"
											src=${product.subImg5
											} class="previewImg" /><input
											type="file" style="display: none" class='inputFile_1'
											name="subImg5_1" value=${product.subImg5
											} /> <input
											type="hidden" name="subImg5" value="" class="hidden_input"></a>
									</c:otherwise>
								</c:choose>


								<p id="imageName">서브이미지5</p>
							</div> --%>
						</div>
						<input type="hidden" id="add_count" name="add_count" value="1">
						<!-- <div id="add_div" class="col-md-12"
							style="width: 100%; margin: 0; padding: 0">
							<button type="button" class="btn btn-primary" id="add_button">색상
								추가</button>
							<button type="button" class="btn btn-danger" id="remove_button">X</button>
						</div> -->
						<input type="hidden" id="add_color" name="add_color" value="1">
						<div class="col-md-12 " style="padding: 0">
							<h4 style="padding: 0">
								<label class="control-label">상품 설명</label>
							</h4>
							<textarea name="expl" id="smarteditor" rows="10" cols="100"
								style="height: 412px; width: 100%">${product.expl }</textarea>
						</div>
					</div>

				</div>
				<div>
					<button id="more_1" class="btn btn-primary">추가정보 입력</button>
				</div>
				<br>
				<div id="more_info" style="padding: 0; margin: 0">
					<h2>핵심 정보</h2>
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="col-md-4 " style="padding: 0">
								<h4 class="col-md-4">
									<label class="control-label">원산지</label>
								</h4>
								<div class="col-md-6" style="padding: 0">
									<c:choose>
										<c:when test="${product.origin eq null }">
											<input type="text" class="form-control" placeholder="원산지"
												name="origin">
										</c:when>
										<c:otherwise>
											<input type="text" class="form-control" placeholder="원산지"
												name="origin" value=${product.origin }>
										</c:otherwise>
									</c:choose>

								</div>
							</div>
							<div class="col-md-8 " style="padding: 0; margin: 0">
								<h4 class="col-md-1"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<label class="control-label">용도</label>
								</h4>
								<div class="col-md-11"
									style="padding-top: 1%; margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0;">
									<%
										String arr2 = ((String) request.getAttribute("uses"));
										arr2 = arr2.substring(1, arr2.length() - 1);

										if (arr2.contains("티셔츠")) {
									%><label class="checkbox-inline"> <input
										type="checkbox" name="uses" value="티셔츠" checked>티셔츠
									</label>
									<%
										} else {
									%><label class="checkbox-inline"> <input
										type="checkbox" name="uses" value="티셔츠">티셔츠
									</label>
									<%
										}
									%>
									<%
										if (arr2.contains("셔츠")) {
									%>
									<label class="checkbox-inline"><input type="checkbox"
										name="uses" value="셔츠" checked>셔츠 </label>
									<%
										} else {
									%>
									<label class="checkbox-inline"><input type="checkbox"
										name="uses" value="셔츠">셔츠 </label>
									<%
										}
									%>
									<%
										if (arr2.contains("점퍼")) {
									%>
									<label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="점퍼" checked>점퍼
									</label>
									<%
										} else {
									%>
									<label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="점퍼">점퍼
									</label>
									<%
										}
									%>
									<%
										if (arr2.contains("재킷")) {
									%>
									<label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="재킷" checked>재킷
									</label>
									<%
										} else {
									%>
									<label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="재킷">재킷
									</label>
									<%
										}
									%>
									<%
										if (arr2.contains("코트")) {
									%>
									<label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="코트" checked>코트
									</label>
									<%
										} else {
									%>
									<label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="코트">코트
									</label>
									<%
										}
									%>
									<%
										if (arr2.contains("원피스")) {
									%>
									<label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="원피스" checked>원피스
									</label>
									<%
										} else {
									%>
									<label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="원피스">원피스
									</label>
									<%
										}
									%>
									<%
										if (arr2.contains("바지")) {
									%>
									<label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="바지" checked>바지
									</label>
									<%
										} else {
									%>
									<label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="바지">바지
									</label>
									<%
										}
									%>
									<%
										if (arr2.contains("치마")) {
									%>
									<label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="치마" checked>치마
									</label>
									<%
										} else {
									%>
									<label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="치마">치마
									</label>
									<%
										}
									%>
									<%
										if (arr2.contains("커튼")) {
									%>
									<label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="커튼" checked>커튼
									</label>
									<%
										} else {
									%>
									<label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="커튼">커튼
									</label>
									<%
										}
									%>
									<%
										if (arr2.contains("침구")) {
									%>
									<label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="침구" checked>침구
									</label>
									<%
										} else {
									%>
									<label class="checkbox-inline"> <input type="checkbox"
										name="uses" value="침구">침구
									</label>
									<%
										}
									%>

								</div>
							</div>
							<div class="col-md-4 " style="padding: 0">
								<h4 class="col-md-4">
									<label class="control-label">제조사</label>
								</h4>
								<div class="col-md-6" style="padding: 0">
									<c:choose>
										<c:when test="${product.manufacturer eq null }">
											<input type="text" class="form-control" placeholder="6칸이내"
												maxlength="6" name="manufacturer">
										</c:when>
										<c:otherwise>
											<input type="text" class="form-control" placeholder="6칸이내"
												maxlength="6" name="manufacturer"
												value=${product.manufacturer }>
										</c:otherwise>
									</c:choose>
								</div>
							</div>

							<div class="col-md-8 " style="padding: 0; margin: 0">
								<h4 class="col-md-1"
									style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<label class="control-label">패턴</label>
								</h4>
								<div class="col-md-11"
									style="padding-top: 1%; margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
									<c:choose>
										<c:when test="${product.pattern eq '무지' }">
											<label class="checkbox-inline" style="padding-left: 0"><input
												type="radio" name="pattern" value="무지" id="no_pattern"
												checked>무지</label>
										</c:when>
										<c:otherwise>
											<label class="checkbox-inline" style="padding-left: 0"><input
												type="radio" name="pattern" value="무지" id="no_pattern">무지</label>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${product.pattern eq '도트' }">
											<label class="checkbox-inline"> <input type="radio"
												name="pattern" value="도트" checked>도트
											</label>
										</c:when>
										<c:otherwise>
											<label class="checkbox-inline"> <input type="radio"
												name="pattern" value="도트">도트
											</label>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${product.pattern eq '스트라이프' }">
											<label class="checkbox-inline"> <input type="radio"
												name="pattern" value="스트라이프" checked>스트라이프
											</label>
										</c:when>
										<c:otherwise>
											<label class="checkbox-inline"> <input type="radio"
												name="pattern" value="스트라이프">스트라이프
											</label>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${product.pattern eq '체크' }">
											<label class="checkbox-inline"> <input type="radio"
												name="pattern" value="체크" checked>체크
											</label>
										</c:when>
										<c:otherwise>
											<label class="checkbox-inline"> <input type="radio"
												name="pattern" value="체크">체크
											</label>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${product.pattern eq '기타' }">
											<label class="checkbox-inline"> <input type="radio"
												name="pattern" value="기타" checked>기타
											</label>
										</c:when>
										<c:otherwise>
											<label class="checkbox-inline"> <input type="radio"
												name="pattern" value="기타">기타
											</label>
										</c:otherwise>
									</c:choose>

								</div>
							</div>

							<div class="col-md-4 " style="padding: 0">
								<h4 class="col-md-4">
									<label class="control-label">제조년월</label>
								</h4>
								<div class="col-md-6" style="padding: 0">
									<c:choose>
										<c:when test="${product.manuDate ne null }">
											<input class="form-control" type="date"
												value=${product.manuDate } name="manuDate">
										</c:when>
										<c:when test="${product.manuDate eq null }">
											<input class="form-control" type="date" value="0000-00-00"
												name="manuDate">
										</c:when>
									</c:choose>

								</div>
							</div>


							<div class="col-md-12">
								<table class="table" id="table_1">
									<tr>
										<td class="col-md-1" style="padding: 0"><h4>
												<label class="control-label">명칭</label>
											</h4></td>
										<td class="col-md-2">
											<ul
												style="overflow-y: auto; list-style: none; height: 100px;">
												<li><a data-toggle="pill" href="#0">우븐패브릭</a></li>
												<li><a data-toggle="pill" href="#1">팬시패브릭</a></li>
												<li><a data-toggle="pill" href="#2">기타패브릭</a></li>
												<li><a data-toggle="pill" href="#3">편물</a></li>
											</ul>
										</td>
										<td class="col-md-3">
											<div class="tab-content">
												<div id="0" class="tab-pane fade in active">
													<ul
														style="overflow-y: auto; list-style: none; height: 100px;"
														class="wovenfabric">
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '개버딘' }">
																		<input type="radio" name="sortName" value="개버딘"
																			checked>개버딘
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="개버딘">개버딘
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '경 새티인' }">
																		<input type="radio" name="sortName" value="경 새티인"
																			checked>경 새티인
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="경 새티인">경 새티인
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '깅엄' }">
																		<input type="radio" name="sortName" value="깅엄" checked>깅엄
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="깅엄">깅엄
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '능직 플란넬' }">
																		<input type="radio" name="sortName" value="능직 플란넬"
																			checked>능직 플란넬
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="능직 플란넬">능직 플란넬
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '니논' }">
																		<input type="radio" name="sortName" value="니논" checked>니논
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="니논">니논
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '덕' }">
																		<input type="radio" name="sortName" value="덕" checked>덕
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="덕">덕
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '데님' }">
																		<input type="radio" name="sortName" value="데님" checked>데님
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="데님">데님
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '디미티' }">
																		<input type="radio" name="sortName" value="디미티"
																			checked>디미티
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="디미티">디미티
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '라이닝 능직' }">
																		<input type="radio" name="sortName" value="라이닝 능직"
																			checked>라이닝 능직
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="라이닝 능직">라이닝 능직
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '론' }">
																		<input type="radio" name="sortName" value="론" checked>론
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="론">론
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '립스톱 나일론' }">
																		<input type="radio" name="sortName" value="립스톱"
																			checked>립스톱 나일론
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="립스톱">립스톱 나일론
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '머슬린' }">
																		<input type="radio" name="sortName" value="머슬린"
																			checked>머슬린
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="머슬린">머슬린
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '바티스트' }">
																		<input type="radio" name="sortName" value="바티스트"
																			checked>바티스트
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="바티스트">바티스트
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '버랩' }">
																		<input type="radio" name="sortName" value="버랩" checked>버랩
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="버랩">버랩
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '범포' }">
																		<input type="radio" name="sortName" value="범포" checked>범포
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="범포">범포
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '베드퍼드' }">
																		<input type="radio" name="sortName" value="베드퍼드"
																			checked>베드퍼드
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="베드퍼드">베드퍼드
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '보일' }">
																		<input type="radio" name="sortName" value="보일" checked>보일
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="보일">보일
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '브로드 클로스' }">
																		<input type="radio" name="sortName" value="브로드 클로스"
																			checked>브로드 클로스
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="브로드 클로스">브로드 클로스
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '산퉁' }">
																		<input type="radio" name="sortName" value="산퉁" checked>산퉁
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="산퉁">산퉁
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '새틴' }">
																		<input type="radio" name="sortName" value="새틴" checked>새틴
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="새틴">새틴
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '샬리' }">
																		<input type="radio" name="sortName" value="샬리" checked>샬리
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="샬리">샬리
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '샴브레이' }">
																		<input type="radio" name="sortName" value="샴브레이"
																			checked>샴브레이
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="샴브레이">샴브레이
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '시폰' }">
																		<input type="radio" name="sortName" value="시폰" checked>시폰
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="시폰">시폰
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '아우팅 플란넬' }">
																		<input type="radio" name="sortName" value="아우팅 플란넬"
																			checked>아우팅 플란넬
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="아우팅 플란넬">아우팅 플란넬
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '앤디크 새틴' }">
																		<input type="radio" name="sortName" value="앤디크 새틴"
																			checked>앤디크 새틴																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="앤디크 새틴">앤디크 새틴
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '오간디' }">
																		<input type="radio" name="sortName" value="오간디"
																			checked>오간디
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="오간디">오간디
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '오간자' }">
																		<input type="radio" name="sortName" value="오간자"
																			checked>오간자
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="오간자">오간자
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '옥스퍼드 샴브레이' }">
																		<input type="radio" name="sortName" value="옥스퍼드 샴브레이"
																			checked>옥스퍼드 샴브레이
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="옥스퍼드 샴브레이">옥스퍼드 샴브레이
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"><c:choose>
																	<c:when test="${product.sortName eq '옥스퍼드 클로스' }">
																		<input type="radio" name="sortName" value="옥스퍼드 클로스"
																			checked>옥스퍼드 클로스
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="옥스퍼드 클로스">옥스퍼드 클로스
																	</c:otherwise>
																</c:choose> </label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '위 새티인' }">
																		<input type="radio" name="sortName" value="위 새티인"
																			checked>위 새티인
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="위 새티인">위 새티인
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '조젯' }">
																		<input type="radio" name="sortName" value="조젯" checked>조젯
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="조젯">조젯
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '진짜 크레이프' }">
																		<input type="radio" name="sortName" value="진짜 크레이프"
																			checked>진짜 크레이프
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="진짜 크레이프">진짜 크레이프
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '차이나 견' }">
																		<input type="radio" name="sortName" value="차이나 견"
																			checked>차이나 견
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="차이나 견">차이나 견
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '치노' }">
																		<input type="radio" name="sortName" value="치노" checked>치노
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="치노">치노
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '치즈 클로스' }">
																		<input type="radio" name="sortName" value="치즈 클로스"
																			checked>치즈 클로스
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="치즈 클로스">치즈 클로스
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '캔버스' }">
																		<input type="radio" name="sortName" value="캔버스"
																			checked>캔버스
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="캔버스">캔버스
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '크래시' }">
																		<input type="radio" name="sortName" value="크래시"
																			checked>크래시
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="크래시">크래시
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '크레이프 드 신' }">
																		<input type="radio" name="sortName" value="크레이프 드 신"
																			checked>크레이프 드 신
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="크레이프 드 신">크레이프 드 신
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '크레이프 백 새틴' }">
																		<input type="radio" name="sortName" value="크레이프 백 새틴"
																			checked>크레이프 백 새틴
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="크레이프 백 새틴">크레이프 백 새틴
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '타프타' }">
																		<input type="radio" name="sortName" value="타프타"
																			checked>타프타
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="타프타">타프타
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '트위드' }">
																		<input type="radio" name="sortName" value="트위드"
																			checked>트위드
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="트위드">트위드
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '트윌 플란넬' }">
																		<input type="radio" name="sortName" value="트윌 플란넬"
																			checked>트윌 플란넬
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="트윌 플란넬">트윌 플란넬
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '파능직' }">
																		<input type="radio" name="sortName" value="파능직"
																			checked>파능직
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="파능직">파능직
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '파유' }">
																		<input type="radio" name="sortName" value="파유" checked>파유
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="파유">파유
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '팬시 능직' }">
																		<input type="radio" name="sortName" value="팬시 능직"
																			checked>팬시 능직
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="팬시 능직">팬시 능직
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '퍼케일' }">
																		<input type="radio" name="sortName" value="퍼케일"
																			checked>퍼케일
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="퍼케일">퍼케일
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '포플린' }">
																		<input type="radio" name="sortName" value="포플린"
																			checked>포플린
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="포플린">포플린
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '폰지' }">
																		<input type="radio" name="sortName" value="폰지" checked>폰지
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="폰지">폰지
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '풀라드' }">
																		<input type="radio" name="sortName" value="풀라드"
																			checked>풀라드
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="풀라드">풀라드
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '프린트 클로스' }">
																		<input type="radio" name="sortName" value="프린트 클로스"
																			checked>프린트 클로스
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="프린트 클로스">프린트 클로스
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '플란넬' }">
																		<input type="radio" name="sortName" value="플란넬"
																			checked>플란넬
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="플란넬">플란넬
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '플란넬리트' }">
																		<input type="radio" name="sortName" value="플란넬리트"
																			checked>플란넬리트
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="플란넬리트">플란넬리트
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '하부타이' }">
																		<input type="radio" name="sortName" value="하부타이"
																			checked>하부타이
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="하부타이">하부타이
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '하운드 투스' }">
																		<input type="radio" name="sortName" value="하운드 투스"
																			checked>하운드 투스
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="하운드 투스">하운드 투스
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '헤링본' }">
																		<input type="radio" name="sortName" value="헤링본"
																			checked>헤링본
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="헤링본">헤링본
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '호난' }">
																		<input type="radio" name="sortName" value="호난" checked>호난
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="호난">호난
																	</c:otherwise>
																</c:choose>
														</label></li>
													</ul>
												</div>
												<div id="1" class="tab-pane fade">
													<ul
														style="overflow-y: auto; list-style: none; height: 100px;"
														class="fancyfabric">
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '다마스크' }">
																		<input type="radio" name="sortName" value="다마스크"
																			checked>다마스크
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="다마스크">다마스크
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '마드라스' }">
																		<input type="radio" name="sortName" value="마드라스"
																			checked>마드라스
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="마드라스">마드라스
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '마퀴제트' }">
																		<input type="radio" name="sortName" value="마퀴제트"
																			checked>마퀴제트
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="마퀴제트">마퀴제트
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '마틀라세' }">
																		<input type="radio" name="sortName" value="마틀라세"
																			checked>마틀라세
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="마틀라세">마틀라세
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '모미' }">
																		<input type="radio" name="sortName" value="모미" checked>모미
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="모미">모미
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '바크 클로스' }">
																		<input type="radio" name="sortName" value="바크 클로스"
																			checked>바크 클로스
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="바크 클로스">바크 클로스
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '베드퍼드 코드' }">
																		<input type="radio" name="sortName" value="베드퍼드 코드"
																			checked>베드퍼드 코드
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="베드퍼드 코드">베드퍼드 코드
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '벨베틴' }">
																		<input type="radio" name="sortName" value="벨베틴"
																			checked>벨베틴
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="벨베틴">벨베틴
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '벨벳' }">
																		<input type="radio" name="sortName" value="벨벳" checked>벨벳
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="벨벳">벨벳
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '브로케이드' }">
																		<input type="radio" name="sortName" value="브로케이드"
																			checked>브로케이드
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="브로케이드">브로케이드
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '아이래쉬' }">
																		<input type="radio" name="sortName" value="아이래쉬"
																			checked>아이래쉬
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="아이래쉬">아이래쉬
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '양면' }">
																		<input type="radio" name="sortName" value="양면" checked>양면
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="양면">양면
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '와플 클로스' }">
																		<input type="radio" name="sortName" value="와플 클로스"
																			checked>와플 클로스
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="와플 클로스">와플 클로스
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '코듀로이' }">
																		<input type="radio" name="sortName" value="코듀로이"
																			checked>코듀로이
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="코듀로이">코듀로이
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '크러시드' }">
																		<input type="radio" name="sortName" value="크러시드"
																			checked>크러시드
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="크러시드">크러시드
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '태피스트리' }">
																		<input type="radio" name="sortName" value="태피스트리"
																			checked>태피스트리
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="태피스트리">태피스트리
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '테리 클로스' }">
																		<input type="radio" name="sortName" value="테리 클로스"
																			checked>테리 클로스
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="테리 클로스">테리 클로스
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '팬 벨벳' }">
																		<input type="radio" name="sortName" value="팬 벨벳"
																			checked>팬 벨벳
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="팬 벨벳">팬 벨벳
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '포켓 클로스' }">
																		<input type="radio" name="sortName" value="포켓 클로스"
																			checked>포켓 클로스
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="포켓 클로스">포켓 클로스
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '프리즈' }">
																		<input type="radio" name="sortName" value="프리즈"
																			checked>프리즈
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="프리즈">프리즈
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '피케' }">
																		<input type="radio" name="sortName" value="피케" checked>피케
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="피케">피케
																	</c:otherwise>
																</c:choose>

														</label></li>
													</ul>
												</div>
												<div id="2" class="tab-pane fade">
													<ul
														style="overflow-y: auto; list-style: none; height: 100px;"
														class="otherfabric">
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '가죽' }">
																		<input type="radio" name="sortName" value="가죽" checked>가죽
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="가죽">가죽
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '니트를 통한 패브릭' }">
																		<input type="radio" name="sortName" value="니트를 통한 패브릭"
																			checked>니트를 통한 패브릭
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="니트를 통한 패브릭">니트를 통한 패브릭
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '라미네이트' }">
																		<input type="radio" name="sortName" value="라미네이트"
																			checked>라미네이트
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="라미네이트">라미네이트
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '레이스' }">
																		<input type="radio" name="sortName" value="레이스"
																			checked>레이스
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="레이스">레이스
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '망상구조물' }">
																		<input type="radio" name="sortName" value="망상구조물"
																			checked>망상구조물
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="망상구조물">망상구조물
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '모피' }">
																		<input type="radio" name="sortName" value="모피" checked>모피
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="모피">모피
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '발포제' }">
																		<input type="radio" name="sortName" value="발포제"
																			checked>발포제
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="발포제">발포제
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '배팅' }">
																		<input type="radio" name="sortName" value="배팅" checked>배팅
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="배팅">배팅
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '부직포' }">
																		<input type="radio" name="sortName" value="부직포"
																			checked>부직포
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="부직포">부직포
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '브레이드' }">
																		<input type="radio" name="sortName" value="브레이드"
																			checked>브레이드
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="브레이드">브레이드
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '스웨이드' }">
																		<input type="radio" name="sortName " value="스웨이드"
																			checked>스웨이드
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName " value="스웨이드">스웨이드
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '웨딩' }">
																		<input type="radio" name="sortName" value="웨딩" checked>웨딩
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="웨딩">웨딩
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '인조 스웨이드' }">
																		<input type="radio" name="sortName" value="인조 스웨이드"
																			checked>인조 스웨이드
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="인조 스웨이드">인조 스웨이드
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '일반 필름' }">
																		<input type="radio" name="sortName" value="일반 필름"
																			checked>일반 필름
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="일반 필름">일반 필름
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '코팅패브릭' }">
																		<input type="radio" name="sortName" value="코팅패브릭"
																			checked>코팅패브릭
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="코팅패브릭">코팅패브릭
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '퀄트 패브릭' }">
																		<input type="radio" name="sortName" value="퀄트 패브릭"
																			checked>퀄트 패브릭
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="퀄트 패브릭">퀄트 패브릭
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '터프트 패브릭' }">
																		<input type="radio" name="sortName" value="터프트 패브릭"
																			checked>터프트 패브릭
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="터프트 패브릭">터프트 패브릭
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '팽창 필름' }">
																		<input type="radio" name="sortName" value="팽창 필름"
																			checked>팽창 필름
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="팽창 필름">팽창 필름
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '펠트' }">
																		<input type="radio" name="sortName" value="펠트" checked>펠트
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="펠트">펠트
																	</c:otherwise>
																</c:choose>
														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '화이버필' }">
																		<input type="radio" name="sortName" value="화이버필"
																			checked>화이버필
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="화이버필">화이버필
																	</c:otherwise>
																</c:choose>

														</label></li>
													</ul>
												</div>
												<div id="3" class="tab-pane fade">
													<ul
														style="overflow-y: auto; list-style: none; height: 100px;"
														class="knit">
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '평 트리코트' }">
																		<input type="radio" name="sortName" value="펑 트리코트"
																			checked>평 트리코트
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="펑 트리코트">평 트리코트
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '브러시 트리코트' }">
																		<input type="radio" name="sortName" value="브러시 트리코트"
																			checked>브러시 트리코트
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="브러시 트리코트">브러시 트리코트
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '경편 벨루어' }">
																		<input type="radio" name="sortName" value="경편 벨루어"
																			checked>경편 벨루어
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="경편 벨루어">경편 벨루어
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '새틴 트리코트' }">
																		<input type="radio" name="sortName" value="새틴 트리코트"
																			checked>새틴 트리코트
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="새틴 트리코트">새틴 트리코트
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '튤' }">
																		<input type="radio" name="sortName" value="튤" checked>튤
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="튤">튤
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '레이스' }">
																		<input type="radio" name="sortName" value="레이스"
																			checked>레이스
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="레이스">레이스
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '써멀 클로스' }">
																		<input type="radio" name="sortName" value="써멀 클로스"
																			checked>써멀 클로스
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="써멀 클로스">써멀 클로스
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '파워 네트' }">
																		<input type="radio" name="sortName" value="파워 네트"
																			checked>파워 네트
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="파워 네트">파워 네트
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '위사 삽입 경편물' }">
																		<input type="radio" name="sortName" value="위사 삽입 경편물"
																			checked>위사 삽입 경편물
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="위사 삽입 경편물">위사 삽입 경편물
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '경사 삽입 경편물' }">
																		<input type="radio" name="sortName" value="경사 삽입 경편물"
																			checked>경사 삽입 경편물
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="경사 삽입 경편물">경사 삽입 경편물
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '경사 및 위사 삽입 경편물' }">
																		<input type="radio" name="sortName"
																			value="경사 및 위사 삽입 경편물" checked>경사 및 위사 삽입 경편물
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName"
																			value="경사 및 위사 삽입 경편물">경사 및 위사 삽입 경편물
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '저지' }">
																		<input type="radio" name="sortName" value="저지" checked>저지
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="저지">저지
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '스토키니트' }">
																		<input type="radio" name="sortName" value="스토키니트"
																			checked>스토키니트
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="스토키니트">스토키니트
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '자카드 저지' }">
																		<input type="radio" name="sortName" value="자카드 저지"
																			checked>자카드 저지
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="자카드 저지">자카드 저지
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '인타시아' }">
																		<input type="radio" name="sortName" value="인타시아"
																			checked>인타시아
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="인타시아">인타시아
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '테리 클로스 편물' }">
																		<input type="radio" name="sortName" value="테리 클로스 편물"
																			checked>테리 클로스 편물
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="테리 클로스 편물">테리 클로스 편물
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '벨루어' }">
																		<input type="radio" name="sortName" value="벨루어"
																			checked>벨루어
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="벨루어">벨루어
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '인조모피' }">
																		<input type="radio" name="sortName" value="인조모피"
																			checked>인조모피
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="인조모피">인조모피
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '프렌치 테리' }">
																		<input type="radio" name="sortName" value="프렌치 테리"
																			checked>프렌치 테리
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="프렌치 테리">프렌치 테리
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '플리스' }">
																		<input type="radio" name="sortName" value="플리스"
																			checked>플리스
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="플리스">플리스
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '리브 편물' }">
																		<input type="radio" name="sortName" value="리브 편물"
																			checked>리브 편물
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="리브 편물">리브 편물
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '인터로크' }">
																		<input type="radio" name="sortName" value="인터로크"
																			checked>인터로크
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="인터로크">인터로크
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '자카드 더블 편물' }">
																		<input type="radio" name="sortName" value="자카드 더블 편물"
																			checked>자카드 더블 편물
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="자카드 더블 편물">자카드 더블 편물
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '팬시 더블 편물' }">
																		<input type="radio" name="sortName" value="팬시 더블 편물"
																			checked>팬시 더블 편물
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="팬시 더블 편물">팬시 더블 편물
																	</c:otherwise>
																</c:choose>

														</label></li>
														<li><label class="checkbox-inline"> <c:choose>
																	<c:when test="${product.sortName eq '펄 편물' }">
																		<input type="radio" name="sortName" value="펄 편물"
																			checked>펄 편물
																	</c:when>
																	<c:otherwise>
																		<input type="radio" name="sortName" value="펄 편물">펄 편물
																	</c:otherwise>
																</c:choose>

														</label></li>
													</ul>
												</div>
											</div>
										</td>
										<td class="col-md-1" style="padding-right: 0"><h4>
												<label class="control-label">섬유</label>
											</h4></td>
										<td class="col-md-2">
											<ul
												style="overflow-y: auto; list-style: none; height: 100px;">
												<li><a data-toggle="pill" href="#20">식물섬유</a></li>
												<li><a data-toggle="pill" href="#21">동물섬유</a></li>
												<li><a data-toggle="pill" href="#22">재생섬유</a></li>
												<li><a data-toggle="pill" href="#23">합성섬유</a></li>

											</ul>
										</td>
										<td class="col-md-3">
											<div class="tab-content">
												<div id="20" class="tab-pane fade in active">
													<input type="hidden" id="count_fiber" value="0">
													<ul
														style="overflow-y: auto; list-style: none; height: 100px;"
														class="plantfiber">
														<%
															String arr = ((String) request.getAttribute("fiber"));
															arr = arr.substring(1, arr.length() - 1);

															if (arr.contains("면")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="면" checked>면
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="면">면
														</label></li>
														<%
															}
														%>
														<%
															if (arr.contains("아마")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="아마" checked>아마
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="아마">아마
														</label></li>
														<%
															}
														%>
														<%
															if (arr.contains("저마")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="저마" checked>저마
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="저마">저마
														</label></li>
														<%
															}
														%>
														<%
															if (arr.contains("대마")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="대마" checked>대마
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="대마">대마
														</label></li>
														<%
															}
														%>

													</ul>
												</div>
												<div id="21" class="tab-pane fade">
													<ul
														style="overflow-y: auto; list-style: none; height: 100px;"
														class="animalfiber">
														<%
															if (arr.contains("양모")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="양모" checked>양모
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="양모">양모
														</label></li>
														<%
															}
														%>

														<%
															if (arr.contains("모헤어")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="모헤어" checked>모헤어
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="모헤어">모헤어
														</label></li>
														<%
															}
														%>

														<%
															if (arr.contains("앙고라")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="앙고라" checked>앙고라
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="앙고라">앙고라
														</label></li>
														<%
															}
														%>


														<%
															if (arr.contains("캐시미어")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="캐시미어" checked>캐시미어
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="캐시미어">캐시미어
														</label></li>
														<%
															}
														%>

														<%
															if (arr.contains("라마")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="라마" checked>라마
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="라마">라마
														</label></li>
														<%
															}
														%>

														<%
															if (arr.contains("알파카")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="알파카" checked>알파카
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="알파카">알파카
														</label></li>
														<%
															}
														%>

														<%
															if (arr.contains("견")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="견" checked>견
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="견">견
														</label></li>
														<%
															}
														%>


													</ul>
												</div>
												<div id="22" class="tab-pane fade">
													<ul
														style="overflow-y: auto; list-style: none; height: 100px;"
														class="regeneratedfiber">
														<%
															if (arr.contains("레이온")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="레이온" checked>레이온
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="레이온">레이온
														</label></li>
														<%
															}
														%>

														<%
															if (arr.contains("리오셀")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="리오셀" checked>리오셀
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="리오셀">리오셀
														</label></li>
														<%
															}
														%>

														<%
															if (arr.contains("아세테이트")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="아세테이트" checked>아세테이트
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="아세테이트">아세테이트
														</label></li>
														<%
															}
														%>

														<%
															if (arr.contains("아즐론")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="아즐론" checked>아즐론
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="아즐론">아즐론
														</label></li>
														<%
															}
														%>

													</ul>
												</div>
												<div id="23" class="tab-pane fade">
													<ul
														style="overflow-y: scroll; list-style: none; height: 100px;"
														class="syntheticfiber">
														<%
															if (arr.contains("나일론")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="나일론" checked>나일론
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="나일론">나일론
														</label></li>
														<%
															}
														%>

														<%
															if (arr.contains("폴리에스테르")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="폴리에스테르" checked>폴리에스테르
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="폴리에스테르">폴리에스테르
														</label></li>
														<%
															}
														%>

														<%
															if (arr.contains("올레핀")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="올레핀" checked>올레핀
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="올레핀">올레핀
														</label></li>
														<%
															}
														%>

														<%
															if (arr.contains("아크릴")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="아크릴" checked>아크릴
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="아크릴">아크릴
														</label></li>
														<%
															}
														%>

														<%
															if (arr.contains("탄성중합체")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="탄성중합체" checked>탄성중합체
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="탄성중합체">탄성중합체
														</label></li>
														<%
															}
														%>

														<%
															if (arr.contains("아라미드")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="아라미드" checked>아라미드
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="아라미드">아라미드
														</label></li>
														<%
															}
														%>

														<%
															if (arr.contains("유리")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="유리" checked>유리
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="유리">유리
														</label></li>
														<%
															}
														%>

														<%
															if (arr.contains("금속 섬유")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="금속 섬유" checked>금속
																섬유
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="금속 섬유">금속 섬유
														</label></li>
														<%
															}
														%>

														<%
															if (arr.contains("플리락트산")) {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="플리락트산" checked>플리락트산
														</label></li>
														<%
															} else {
														%>
														<li><label class="checkbox-inline"> <input
																type="checkbox" name="fiber" value="플리락트산">플리락트산
														</label></li>
														<%
															}
														%>
													</ul>
												</div>
											</div>
										</td>
									</tr>
								</table>
							</div>
							<!-- <div class="col-md-12 " style="padding: 0">
							<div id="show_fiber">
								<div class="col-md-4 " style="padding: 0" style="display:hidden">
									<h4 class="col-md-4">
										<label class="control-label">면</label>
									</h4>
									<div class="col-md-6" style="padding: 0">
										<input type="text" class="form-control" placeholder="원산지"
											name="origin">
									</div>
								</div>
							</div>
						</div> -->
						</div>
					</div>

					<h2>상세 정보</h2>
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="col-md-6">
								<h3 class="col-md-12"
									style="padding-left: 0; padding-right: 0; margin-left: 0; margin-right: 0;">피륙</h3>
								<div class="col-md-12 ">
									<h4 class="col-md-2">
										<label class="control-label">중량</label>
									</h4>
									<div class="col-md-5" style="padding-left: 0; padding-right: 0">
										<c:choose>
											<c:when test="${product.weight ne 0 }">
												<input type="number" class="form-control" name="weight"
													min="0" placeholder="중량을 입력하세요" value=${product.weight }>
											</c:when>
											<c:when test="${product.weight eq 0 }">
												<input type="number" class="form-control" name="weight"
													min="0" placeholder="중량을 입력하세요">
											</c:when>
										</c:choose>
									</div>
									<div class="col-md-2" style="padding-top: 1%">
										<h5>g/㎡</h5>
									</div>
								</div>
								<div class="col-md-12">
									<h4 class="col-md-2">
										<label class="control-label">조직</label>
									</h4>
									<div class="col-md-5" style="padding-left: 0; padding-right: 0">
										<c:choose>
											<c:when test="${product.weave ne null }">
												<input type="text" class="form-control"
													placeholder="조직을 입력하세요" name="weave"
													value=${product.weave }>
											</c:when>
											<c:when test="${product.weave eq null }">
												<input type="text" class="form-control"
													placeholder="조직을 입력하세요" name="weave">
											</c:when>
										</c:choose>
									</div>
								</div>
								<div class="col-md-12 ">
									<h4 class="col-md-2">
										<label class="control-label">밀도</label>
									</h4>
									<div class="col-md-2" style="padding-left: 0; padding-right: 0">
										<c:choose>
											<c:when test="${product.density_weight ne 0 }">
												<input type="number" class="form-control" placeholder="질량"
													min="0" name="density_weight"
													value=${product.density_weight }>
											</c:when>
											<c:when test="${product.density_weight eq 0 }">
												<input type="number" class="form-control" placeholder="질량"
													min="0" name="density_weight">
											</c:when>
										</c:choose>
									</div>
									<div class="col-md-1">*</div>
									<div class="col-md-2" style="padding-left: 0; padding-right: 0">
										<c:choose>
											<c:when test="${product.density_volume ne 0 }">
												<input type="text" class="form-control" placeholder="부피"
													min="0" name="density_volume"
													value=${product.density_volume }>
											</c:when>
											<c:when test="${product.density_volume eq 0 }">
												<input type="text" class="form-control" placeholder="부피"
													min="0" name="density_volume">
											</c:when>
										</c:choose>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<h3 class="col-md-12"
									style="padding-left: 0; padding-right: 0; margin-left: 0; margin-right: 0;">원단</h3>

								<div class="col-md-12">
									<h4 class="col-md-2">
										<label class="control-label">굵기</label>
									</h4>
									<div class="col-md-3" style="padding-left: 0; padding-right: 0">
										<c:choose>
											<c:when test="${product.thickWp ne 0 }">
												<input type="number" class="form-control"
													placeholder="경사 입력" min="0" name="thickWp"
													value=${product.thickWp }>
											</c:when>
											<c:when test="${product.thickWp eq 0 }">
												<input type="number" class="form-control"
													placeholder="경사 입력" min="0" name="thickWp">
											</c:when>
										</c:choose>
									</div>
									<div class="col-md-3" style="padding-left: 0; padding-right: 0">
										<c:choose>
											<c:when test="${product.thickWt ne 0 }">
												<input type="number" class="form-control"
													placeholder="위사 입력" min="0" name="thickWt"
													value=${product.thickWt }>
											</c:when>
											<c:when test="${product.thickWt eq 0 }">
												<input type="number" class="form-control"
													placeholder="위사 입력" min="0" name="thickWt">
											</c:when>
										</c:choose>
									</div>
									<div class="col-md-3" style="padding-left: 0; padding-right: 0">
										<select name="thickUnit" class="form-control">
											<c:choose>
												<c:when test="${product.thickUnit eq '번수' }">
													<option value="0">단위선택</option>
													<option value="번수" selected>번수</option>
													<option value="데니어">데니어</option>
												</c:when>
												<c:when test="${product.thickUnit eq '데니어' }">
													<option value="0">단위선택</option>
													<option value="번수">번수</option>
													<option value="데니어" selected>데니어</option>
												</c:when>
												<c:when test="${product.thickUnit eq '0' }">
													<option value="0">단위선택</option>
													<option value="번수">번수</option>
													<option value="데니어">데니어</option>
												</c:when>
											</c:choose>
										</select>
									</div>
								</div>

								<div class="col-md-12">
									<h4 class="col-md-2">
										<label class="control-label">길이</label>
									</h4>
									<div class="col-md-3" style="padding-left: 0; padding-right: 0">
										<select class="form-control" name="lengthWp">
											<c:choose>
												<c:when test="${product.lengthWp eq '0' }">
													<option value="0">경사</option>
													<option value="필라멘트사">필라멘트사</option>
													<option value="스테이플사">스테이플사</option>
												</c:when>
												<c:when test="${product.lengthWp eq '필라멘트사' }">
													<option value="0">경사</option>
													<option value="필라멘트사" selected>필라멘트사</option>
													<option value="스테이플사">스테이플사</option>
												</c:when>
												<c:when test="${product.lengthWp eq '스테이플사' }">
													<option value="0">경사</option>
													<option value="필라멘트사">필라멘트사</option>
													<option value="스테이플사" selected>스테이플사</option>
												</c:when>
											</c:choose>
										</select>
									</div>
									<div class="col-md-3" style="padding-left: 0; padding-right: 0">
										<select class="form-control" name="lengthWt">
											<c:choose>
												<c:when test="${product.lengthWt eq '0' }">
													<option value="0">위사</option>
													<option value="필라멘트사">필라멘트사</option>
													<option value="스테이플사">스테이플사</option>
												</c:when>
												<c:when test="${product.lengthWt eq '필라멘트사' }">
													<option value="0">위사</option>
													<option value="필라멘트사" selected>필라멘트사</option>
													<option value="스테이플사">스테이플사</option>
												</c:when>
												<c:when test="${product.lengthWt eq '스테이플사' }">
													<option value="0">위사</option>
													<option value="필라멘트사">필라멘트사</option>
													<option value="스테이플사" selected>스테이플사</option>
												</c:when>
											</c:choose>
										</select>
									</div>
								</div>

								<div class="col-md-12">
									<h4 class="col-md-2">
										<label class="control-label">꼬임</label>
									</h4>
									<div class="col-md-3" style="padding-left: 0; padding-right: 0">
										<select class="form-control" name="twistWp">
											<c:choose>
												<c:when test="${product.twistWp eq '0' }">
													<option value="0">경사</option>
													<option value="무연사">무연사</option>
													<option value="약연사">약연사</option>
													<option value="강연사">강연사</option>
												</c:when>
												<c:when test="${product.twistWp eq '무연사' }">
													<option value="0">경사</option>
													<option value="무연사" selected>무연사</option>
													<option value="약연사">약연사</option>
													<option value="강연사">강연사</option>
												</c:when>
												<c:when test="${product.twistWp eq '약연사' }">
													<option value="0">경사</option>
													<option value="무연사">무연사</option>
													<option value="약연사" selected>약연사</option>
													<option value="강연사">강연사</option>
												</c:when>
												<c:when test="${product.twistWp eq '강연사' }">
													<option value="0">경사</option>
													<option value="무연사">무연사</option>
													<option value="약연사">약연사</option>
													<option value="강연사" selected>강연사</option>
												</c:when>
											</c:choose>
										</select>
									</div>
									<div class="col-md-3" style="padding-left: 0; padding-right: 0">
										<select class="form-control" name="twistWt">
											<c:choose>
												<c:when test="${product.twistWt eq '0' }">
													<option value="0">위사</option>
													<option value="무연사">무연사</option>
													<option value="약연사">약연사</option>
													<option value="강연사">강연사</option>
												</c:when>
												<c:when test="${product.twistWt eq '무연사' }">
													<option value="0">위사</option>
													<option value="무연사" selected>무연사</option>
													<option value="약연사">약연사</option>
													<option value="강연사">강연사</option>
												</c:when>
												<c:when test="${product.twistWt eq '약연사' }">
													<option value="0">위사</option>
													<option value="무연사">무연사</option>
													<option value="약연사" selected>약연사</option>
													<option value="강연사">강연사</option>
												</c:when>
												<c:when test="${product.twistWt eq '강연사' }">
													<option value="0">위사</option>
													<option value="무연사">무연사</option>
													<option value="약연사">약연사</option>
													<option value="강연사" selected>강연사</option>
												</c:when>
											</c:choose>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<h3 class="col-md-12"
									style="padding-left: 0; padding-right: 0; margin-left: 0; margin-right: 0;">가공</h3>
								<div class="col-md-6">
									<h4 class="col-md-2">
										<label class="control-label">가공</label>
									</h4>
									<div class="col-md-5" style="padding-left: 0">
										<c:choose>
											<c:when test="${product.gagong ne null}">
												<input type="text" class="form-control" placeholder="가공"
													name="gagong" value=${product.gagong }>
											</c:when>
											<c:when test="${product.gagong eq null }">
												<input type="text" class="form-control" placeholder="가공"
													name="gagong">
											</c:when>
										</c:choose>
									</div>

								</div>

								<div class="col-md-6 ">
									<div class="col-md-12">
										<h4 class="col-md-2">
											<label class="control-label">염색</label>
										</h4>
										<div class="col-md-3" style="padding-left: 0">
											<select class="form-control" name="dye">
												<c:choose>
													<c:when test="${product.dye eq '0' }">
														<option value="0">선택</option>
														<option value="무염">무염</option>
														<option value="침염">침염</option>
														<option value="날염">날염</option>
													</c:when>
													<c:when test="${product.dye eq '무염' }">
														<option value="0">선택</option>
														<option value="무염" selected>무염</option>
														<option value="침염">침염</option>
														<option value="날염">날염</option>
													</c:when>
													<c:when test="${product.dye eq '침염' }">
														<option value="0">선택</option>
														<option value="무염">무염</option>
														<option value="침염" selected>침염</option>
														<option value="날염">날염</option>
													</c:when>
													<c:when test="${product.dye eq '날염' }">
														<option value="0">선택</option>
														<option value="무염">무염</option>
														<option value="침염">침염</option>
														<option value="날염" selected>날염</option>
													</c:when>
												</c:choose>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<h3 class="col-md-12"
									style="padding-left: 0; padding-right: 0; margin-left: 0; margin-right: 0;">심미</h3>
								<div class="col-md-6">
									<h4 class="col-md-2">
										<label class="control-label">질감</label>
									</h4>
									<div class="col-md-5" style="padding-left: 0">
										<c:choose>
											<c:when test="${product.texture eq null }">
												<input type="text" class="form-control" name="texture"
													placeholder="ex)부드러움/매우 거침">
											</c:when>
											<c:when test="${product.texture ne null }">
												<input type="text" class="form-control" name="texture"
													placeholder="ex)부드러움/매우 거침" value=${product.texture }>
											</c:when>
										</c:choose>
									</div>

								</div>
								<div class="col-md-6">
									<div class="col-md-12">
										<h4 class="col-md-2">
											<label class="control-label">비침</label>
										</h4>
										<div class="col-md-9" style="padding-left: 0">
											<h5 class="col-md-4"
												style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
												<label class="checkbox-inline" style="padding-left: 0">
													<c:choose>
														<c:when test="${product.through eq '비침 없음' }">
															<input type="radio" name="through" value="비침 없음" checked>비침 없음
														</c:when>
														<c:otherwise>
															<input type="radio" name="through" value="비침 없음">비침 없음
														</c:otherwise>
													</c:choose>
												</label>
											</h5>
											<h5 class="col-md-4"
												style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
												<label class="checkbox-inline" style="padding-left: 0">
													<c:choose>
														<c:when test="${product.through eq '약간 비침' }">
															<input type="radio" name="through" value="약간 비침" checked>약간 비침
														</c:when>
														<c:otherwise>
															<input type="radio" name="through" value="약간 비침">약간 비침
														</c:otherwise>
													</c:choose>
												</label>
											</h5>
											<h5 class="col-md-4"
												style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
												<label class="checkbox-inline" style="padding-left: 0">
													<c:choose>
														<c:when test="${product.through eq '많이 비침' }">
															<input type="radio" name="through" value="많이 비침" checked>많이 비침
														</c:when>
														<c:otherwise>
															<input type="radio" name="through" value="많이 비침">많이 비침
														</c:otherwise>
													</c:choose>
												</label>
											</h5>
										</div>
									</div>
								</div>



								<div class="col-md-6 ">
									<h4 class="col-md-2">
										<label class="control-label">광택</label>
									</h4>
									<div class="col-md-7" style="padding-left: 0">
										<h5 class="col-md-4"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<label class="checkbox-inline" style="padding-left: 0">
												<c:choose>
													<c:when test="${product.gloss eq '광택 없음' }">
														<input type="radio" name="gloss" value="광택 없음" checked>광택 없음
														</c:when>
													<c:otherwise>
														<input type="radio" name="gloss" value="광택 없음">광택 없음
														</c:otherwise>
												</c:choose>
											</label>
										</h5>
										<h5 class="col-md-4"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<label class="checkbox-inline" style="padding-left: 0">
												<c:choose>
													<c:when test="${product.gloss eq '약간 빛남' }">
														<input type="radio" name="gloss" value="약간 빛남" checked>약간 빛남
														</c:when>
													<c:otherwise>
														<input type="radio" name="gloss" value="약간 빛남">약간 빛남
														</c:otherwise>
												</c:choose>
											</label>
										</h5>
										<h5 class="col-md-4"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<label class="checkbox-inline" style="padding-left: 0">
												<c:choose>
													<c:when test="${product.gloss eq '많이 빛남' }">
														<input type="radio" name="gloss" value="많이 빛남" checked>많이 빛남
														</c:when>
													<c:otherwise>
														<input type="radio" name="gloss" value="많이 빛남">많이 빛남
														</c:otherwise>
												</c:choose>
											</label>
										</h5>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="col-md-12">
										<h4 class="col-md-2">
											<label class="control-label">신축</label>
										</h4>
										<div class="col-md-9" style="padding-left: 0">
											<h5 class="col-md-4"
												style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
												<label class="checkbox-inline" style="padding-left: 0">
													<c:choose>
														<c:when test="${product.elastic eq '신축 없음' }">
															<input type="radio" name="elastic" value="신축 없음" checked>신축 없음
														</c:when>
														<c:otherwise>
															<input type="radio" name="elastic" value="신축 없음">신축 없음
														</c:otherwise>
													</c:choose>
												</label>
											</h5>
											<h5 class="col-md-4"
												style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
												<label class="checkbox-inline" style="padding-left: 0">
													<c:choose>
														<c:when test="${product.elastic eq '약간 신축' }">
															<input type="radio" name="elastic" value="약간 신축" checked>약간 신축
														</c:when>
														<c:otherwise>
															<input type="radio" name="elastic" value="약간 신축">약간 신축
														</c:otherwise>
													</c:choose>
												</label>
											</h5>
											<h5 class="col-md-4"
												style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
												<label class="checkbox-inline" style="padding-left: 0">
													<c:choose>
														<c:when test="${product.elastic eq '많이 신축' }">
															<input type="radio" name="elastic" value="많이 신축" checked>많이 신축
														</c:when>
														<c:otherwise>
															<input type="radio" name="elastic" value="많이 신축">많이 신축
														</c:otherwise>
													</c:choose>
												</label>
											</h5>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<h4 class="col-md-1">
										<label class="control-label">유연</label>
									</h4>
									<div class="col-md-6" style="padding-left: 0">
										<h5 class="col-md-3"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<label class="checkbox-inline" style="padding-left: 0">
												<c:choose>
													<c:when test="${product.pliability eq '매우 부드러운' }">
														<input type="radio" name="pliability" value="매우 부드러운"
															checked>매우 부드러운
														</c:when>
													<c:otherwise>
														<input type="radio" name="pliability" value="매우 부드러운">매우 부드러운
														</c:otherwise>
												</c:choose>
											</label>
										</h5>
										<h5 class="col-md-2"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<label class="checkbox-inline" style="padding-left: 0">
												<c:choose>
													<c:when test="${product.pliability eq '부드러운' }">
														<input type="radio" name="pliability" value="부드러운" checked>부드러운
														</c:when>
													<c:otherwise>
														<input type="radio" name="pliability" value="부드러운">부드러운
														</c:otherwise>
												</c:choose>
											</label>
										</h5>
										<h5 class="col-md-2"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<label class="checkbox-inline" style="padding-left: 0">
												<c:choose>
													<c:when test="${product.pliability eq '적당한' }">
														<input type="radio" name="pliability" value="적당한" checked>적당한
														</c:when>
													<c:otherwise>
														<input type="radio" name="pliability" value="적당한">적당한
														</c:otherwise>
												</c:choose>
											</label>
										</h5>
										<h5 class="col-md-2"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<label class="checkbox-inline" style="padding-left: 0">
												<c:choose>
													<c:when test="${product.pliability eq '빳빳한' }">
														<input type="radio" name="pliability" value="빳빳한" checked>빳빳한
														</c:when>
													<c:otherwise>
														<input type="radio" name="pliability" value="빳빳한">빳빳한
														</c:otherwise>
												</c:choose>
											</label>
										</h5>
										<h5 class="col-md-3"
											style="margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0">
											<label class="checkbox-inline" style="padding-left: 0">
												<c:choose>
													<c:when test="${product.pliability eq '매우 빳빳한' }">
														<input type="radio" name="pliability" value="매우 빳빳한"
															checked>매우 빳빳한
														</c:when>
													<c:otherwise>
														<input type="radio" name="pliability" value="매우 빳빳한">매우 빳빳한
														</c:otherwise>
												</c:choose>
											</label>
										</h5>
									</div>

								</div>
							</div>


						</div>
					</div>
				</div>
			</div>
		</form>
	</div>


	<script>
	//폭- 하나만 선택 가능하게
	
		$(function() {
			$("#width_input").on('keydown', function() {
				$('.width_radio').each(function() {
					this.checked = false;
				})
			})
			$(".width_radio").on('click', function() {
				$('#width_input').val("");
			})
		})
	
		// 이미지 사이즈 조절
		$(window).resize(function() {
			// This will execute whenever the window is resized
	
			$(".previewImg").height($(".previewImg").width());
		});
	
		//추가 정보 펼치기
		$("#more_info").hide();
		$("#more_1").click(function() {
			console.log("more")
			$("#more_info").toggle();
			return false;
	
		});
	
		// 이미지 업로드 
		$(document).on('click', '.previewImg', function(e) {
	
			//do whatever
			e.preventDefault();
	
			$(this).next().trigger('click');
	
		});
	
		// 색상 박스
		$(document).on('click', '.colorBox', function(e) {
			//do whatever
			e.preventDefault();
			var name = $(this).next().attr("name");
			$("input[name=" + name + "]:radio").each(function() {
	
				$(this).prev().css("border", "");
			});
			$(this).css("border", "2px solid black");
			$(this).next().trigger('click');
		});
	
		$(document).on('click', '.stock_unknown', function(e) {
			var unknown = $(this).prev().val();
			var stockid = "#stock" + unknown
			if (this.checked) {
				$(stockid).val("-1");
				$(stockid).attr("disabled", true);
				console.log("sad  " + stockid);
				$("#stock1").val("-1");
				$("#stock1").attr("disabled", true);
			} else {
				$("#stock1").removeAttr("disabled");
				$("#stock1").val("");
			}
	
		});
	
	
		function readURL1(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader(); //파일을 읽기 위한 FileReader객체 생성
				reader.onload = function(e) {
					//파일 읽어들이기를 성공했을때 호출되는 이벤트 핸들러
					//이미지 Tag의 SRC속성에 읽어들인 File내용을 지정
					//(아래 코드에서 읽어들인 dataURL형식)
					console.log("a");
					$(input).siblings('.previewImg').attr('src',
						e.target.result);
					$(input).siblings('.hidden_input').val(e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			//File내용을 읽어 dataURL형식의 문자열로 저장
			}
		} //readURL()--
		//file 양식으로 이미지를 선택(값이 변경) 되었을때 처리하는 코드
		$(document).on('change', '.inputFile_1', function() {
	
			//alert(this.value); //선택한 이미지 경로 표시
			console.log('input clicked');
			readURL1(this);
		/* $('#imageUrl').val(this.value);
		 */
		});
	
		//상단바 고정
		$(function() {
			var top_pos = $('.menubar').offset().top;
			var width = $('.menubar').width();
			win = window;
			$(win).on('scroll', function() {
				var pos = $(this).scrollTop();
				$('#content').attr('value', pos);
				if (pos >= top_pos) {
					$('.menubar').addClass('fix');
					$('.fix').width(width);
				} else {
					$('.menubar').removeClass('fix');
				}
			});
	
		});
	
	
		$(document)
			.on(
				'click',
				'#submit_upload',
				function(e) {
	
					flag = 1;
					/* if (document.frm.itemName.value.length == 0) {
						alert("상품명을 써주세요");
						frm.itemName.focus();
						flag = 0;
						return false;
					} */
	
					$(".colorName").each(function() {
						if (this.value == "") {
							alert("색상명을 써주세요");
							this.focus();
							flag = 0;
							return false;
						}
					});
	
					/* if (document.frm.retail.value.length == 0) {
						alert("소매가를 입력하세요.");
						frm.retail.focus();
						flag = 0;
						return false;
					}
					if (document.frm.wholesale.value.length == 0) {
						alert("도매가를 입력하세요.");
						frm.wholesale.focus();
						flag = 0;
						return false;
					}
					if (document.frm.standard.value.length == 0) {
						alert("도매가 기준을 입력하세요.");
						frm.standard.focus();
						flag = 0;
						return false;
					}
					if (document.frm.swatch.value == 0) {
						alert("스와치 제공여부를 선택하세요.");
						flag = 0;
						return false;
					} */
					$(".stock")
						.each(
							function() {
								if (this.value == "") {
									console
										.log("재고량 : "
											+ $(this)
												.parent()
												.siblings(
													".stock_unknown")
												.is())
									alert("재고량을 써주세요");
									this.focus();
									flag = 0;
									return false;
								}
							});
					/* if (document.frm.width_radio.value.length == 0
						&& document.getElementById("width_input").value == "") {
						alert("폭을 선택하세요.");
						flag = 0;
						return false;
					}
					if (document.getElementById("width_input").value.length != 0
						&& document.frm.widthUnit.value == 0) {
						alert("폭의 단위를 선택하세요.");
						flag = 0;
						return false;
					}
					if (document.frm.thick.value.length == 0) {
						alert("두께를 선택하세요.");
						flag = 0;
						return false;
					} */
					var count = $("#add_count").val();
					for (var i = 0; i < count; i++) {
						if (!$('input[name=color' + (i + 1) + ']:radio:checked').val()) {
							alert("색상을 선택하세요.");
							flag = 0;
							return false;
						}
					}
					//이미지
					$(".requireImg")
						.each(
							function() {
								if ($(this).attr('src') == "../img/no_image.png") {
									alert("이미지를 넣어주세요");
									this.focus();
									flag = 0;
									return false;
								}
							});
	
				/* 	//폭
					if (document.frm.width_radio.value.length != 0) {
						document.getElementById("take_width").value = document.frm.width_radio.value;
					} else {
						document.getElementById("take_width").value = document.frm.width_input.value;
					} */
	
					if (flag == 1)
						submitProduct();
	
				});
		// smarteditor
		$(".previewImg").height($(".previewImg").width());
		//전역변수선언
		var editor_object = [];
	
		nhn.husky.EZCreator.createInIFrame({
			oAppRef : editor_object,
			elPlaceHolder : "smarteditor", //연결지을 textarea의 id명
			sSkinURI : "/editor/SmartEditor2Skin.html", //에디터 스킨 경로
			htParams : {
				// 툴바 사용 여부 (true:사용/ false:사용하지 않음)
				bUseToolbar : true,
				// 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
				bUseVerticalResizer : true,
				// 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
				bUseModeChanger : true,
			}
		});
	
		//전송버튼 클릭이벤트
		function submitProduct() {
			console.log("aaadsfsfsddsfs")
			//id가 smarteditor인 textarea에 에디터에서 대입\
			var input = confirm("등록하시겠습니까?");
			if (input == true) {
	
				editor_object.getById["smarteditor"].exec(
					"UPDATE_CONTENTS_FIELD", []);
	
				// 이부분에 에디터 validation 검증
	
				document.frm.action = "../SidServlet?command=after_modify_product";
				document.frm.submit();
	
			} else {
				return;
			}
			//폼 submit
			//	$("#fm").submit();
	
		}
		;
		
		
	</script>
</body>
</html>
