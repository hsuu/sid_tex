<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../include/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript"
	src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="../editor/js/HuskyEZCreator.js"
	charset="utf-8"></script>

<!--색상 스타일시트 -->
<link rel="stylesheet" type="text/css" href="../css/color.css">
<script src="http://code.jquery.com/jquery-3.1.0.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">
			<li role="presentation"><a
				href="../SidServlet?command=purchase_list">주문 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_manage_stock">재고 관리</a></li>
			<li class="active" role="presentation"><a
				href="../SidServlet?command=product_upload">상품 등록</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_manage_balance">정산 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_manage_delivery">배송 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_manage_customer">고객 관리</a></li>
		</ul>
	</div>
	<br>
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="text-center">
					<h1>
						<i class="fa fa-check"></i>
					</h1>
					<br>
					<h2 class="text-center">상품등록이 완료되었습니다.</h2>
					<div class="panel-body">
						<div class="form-group col-md-4 col-md-offset-4">
							<a href="../SidServlet?command=main"
								class="btn btn-lg btn-primary btn-block">홈으로 바로가기</a>
						</div>
						<div class="form-group col-md-4 col-md-offset-4">
							<a href="../SidServlet?command=product_upload"
								class="btn btn-lg btn-primary btn-block">상품등록 바로가기</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="../include/footer.jsp"%>
</body>
</html>