<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../include/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
		int admin = (int) session.getAttribute("admin");
		if (admin > 1) {
			response.sendRedirect("../SidServlet?command=main");
		}
	%>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">
			<li role="presentation"><a href="../admin/admin_main.jsp">홈</a></li>
			<li class="active" role="presentation"><a
				href="../SidServlet?command=list_all_user">회원 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_all_order">주문 관리</a></li>
			<li role="presentation"><a href="../admin/admin_all_balance_wait.jsp">정산
					관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_all_consult">문의 관리</a></li>
			<li role="presentation"><a href="../admin/boardWrite.jsp">공지사항
					등록</a></li>
			<li role="presentation"><a href="../admin/admin_write_faq.jsp">자주하는
					질문 등록</a></li>
		</ul>
	</div>
	<br>
	<div class="col-md-2">
	<ul class="nav nav-pills nav-stacked">
	 <li class="active" role="presentation"><a href="../SidServlet?command=list_all_user">전체</a></li>
	  <li role="presentation"><a href="../SidServlet?command=list_rep_num">사업자 등록번호 검토</a></li>
	</ul>
	</div>
	<div class="col-md-10">
		<form name="frm" method="post">
			<table class="table">
				<thead>
					<tr>
						<th class="1">이메일</th>
						<th class="2">이름</th>
						<th class="3">전화번호</th>
						<th class="4">주소목록</th>
						<th class="5">권한</th>
						<%
							if (admin == 0) {
						%>
						<th class="6">탈퇴처리</th>
						<%
							}
						%>
					</tr>
				</thead>
				<tbody>

					<c:forEach items="${list}" var="list">
						<tr>
							<td class="1">${list.email }</td>
							<td class="2">${list.name }</td>
							<td class="3">${list.phone }</td>
							<td class="4">${list.address1 }</td>
							<td class="5"><c:if test="${list.admin eq '0'}">
								슈퍼관리자
							</c:if> <c:if test="${list.admin eq '1'}">
								관리자
								</c:if> <c:if test="${list.admin eq '2'}">
								기업판매자
								</c:if> <c:if test="${list.admin eq '3'}">
								기업구매자
								</c:if> <c:if test="${list.admin eq '4'}">
								개인구매자
								</c:if></td>
							<%
								if (admin == 0) {
							%>
							<td class="6"><button type="button" class="btn btn-primary"
									onclick="secede('${list.email }');">강제탈퇴</button></td>
							<%
								}
							%>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</form>
	</div>
	<%@ include file="../include/footer.jsp"%>
</body>
<script>
	function secede(email) {
		var input = confirm("탈퇴시키겠습니까?");
		if (input == true) {
			document.frm.action = "../SidServlet?command=secede_member&email=" + email;
			document.frm.submit();
		} else {
			return;
		}
	}
	;
</script>
</html>