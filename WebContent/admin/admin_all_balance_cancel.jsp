<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../include/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css"
	type="text/css" />
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>
</head>
<body>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">
			<li role="presentation"><a href="../admin/admin_main.jsp">홈</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_all_user">회원 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_all_order">주문 관리</a></li>
			<li class="active" role="presentation"><a
				href="../admin/admin_all_balance_wait.jsp">정산 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_all_consult">문의 관리</a></li>
			<li role="presentation"><a href="../admin/boardWrite.jsp">공지사항
					등록</a></li>
			<li role="presentation"><a href="../admin/admin_write_faq.jsp">자주하는
					질문 등록</a></li>
		</ul>
	</div>
	<br>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">

			<li role="presentation"><a
				href="../admin/admin_all_balance_wait.jsp">정산 대기</a></li>
			<li class="active"><a
				href="../SidServlet?command=admin_all_balance_cancel">취소 거래 정산</a></li>
			<li role="presentation"><a
				href="../admin/admin_all_balance_complete.jsp">정산 완료</a></li>
			
		</ul>
	</div>
	<br>
	<div>
		<form id="balance" method="post"
			action="../SidServlet?command=admin_all_balance_wait">
			<div>
				조회기간 : <input type="text" id="datepicker1" name="datepicker1">
				~ <input type="text" id="datepicker2" name="datepicker2">
				<button class="btn btn-primary" type="submit">적용하기</button>
				<br>
			</div>
		</form>
	</div>
	<br>
	<div>
		<button class="btn btn-primary" onclick="updateState()">선택
			정산완료</button>
	</div>
	<div class="col-md-12">
		<form name="frm" method="post">
			<table class="table">
				<thead>
					<tr>
						<th><input type="checkbox" id="checkth"></th>
						<th class="1">상태</th>
						<th>주문일</th>
						<th class="2">취소일</th>
						<th class="3">판매자명</th>
						<th class="4">구매자이메일</th>
						<th class="5">취소금액</th>
						<th class="6">결제번호</th>
						<th>취소형태</th>
						<th>취소자</th>

					</tr>
				</thead>
				<tbody>

					<c:forEach items="${list}" var="list">
						<tr>
							<td><input type="checkbox" name="checktd"></td>
							<td class="1"><c:if test="${list.state eq 0 }">
						정산 대기
						</c:if> <c:if test="${list.state eq 1 }">정산 완료</c:if></td>
							<td class="2">${list.appldate }</td>
							<td class="3">${list.date}</td>
							<td class="4">${list.company }</td>
							<td class="5">${list.buyerEmail}</td>
							<td class="6">${list.totalCost}</td>
							<td class="7">${list.oid}</td>
							<td class="8">${list.cancelWay}</td>
							<td>${list.canceller}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</form>
	</div>
</body>
<script>
function updateState(){
	var oid=new Array();
	var date_arr=new Array();
	var flag=1;
 	$("input[name=checktd]:checked").each(function(){
 		if($(this).parent().siblings(".1").text()=="정산완료"){
 			flag=0;
 		}else{
 			
		oid.push($(this).parent().siblings(".7").text());
 		}
	})
	
	if(flag==1){
		
	$.ajax({
		url : '../SidServlet?command=admin_update_cancel_state',
		data : 'oid=' + oid,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success : function(data) {
			if (data >0) {
				alert("변경되었습니다.");
				history.go(0);
			} else {
				alert("변경안됨");
			}

		}
	}); 
	
	
	}else{
		alert("이미 정산되었습니다.")
	}
}

//달력
$.datepicker.setDefaults({
	 dateFormat: 'yy-mm-dd',
	    prevText: '이전 달',
	    nextText: '다음 달',
	    monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
	    monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
	    dayNames: ['일','월','화','수','목','금','토'],
	    dayNamesShort: ['일','월','화','수','목','금','토'],
	    dayNamesMin: ['일','월','화','수','목','금','토'],
	    showMonthAfterYear: true,
	    yearSuffix: '년'
});
$(function() {
    $("#datepicker1, #datepicker2").datepicker();
  });


function openCalculate(email){
	var pop_title = "popupOpener";
	
	var url = "../SidServlet?command=calculate&email=" + email;
	window.open(url, "_blank_1", "toolbar=no, menubar=no, scrollbars=yes, resizable=no, width=1200, height=900");
}

//테이블 체크박스 전체 선택,해제
$("#checkth").click(function() {
	if ($("#checkth").prop("checked")) {
		$("input[name=checktd]:checkbox").each(function() {
			this.checked = true;
		});
	} else {
		$("input[name=checktd]:checkbox").each(function() {
			this.checked = false;
		});
	}

});
//개별 선택시 전체선택 버튼 해제
$("input[name=checktd]:checkbox").click(function() {
	if ($("#checkth").prop("checked")) {
		$("#checkth").prop("checked", false);
	}
});

</script>
<%@ include file="../include/footer.jsp"%>
</html>