<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../include/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
		int admin = (int) session.getAttribute("admin");
		if (admin > 1) {
			response.sendRedirect("../SidServlet?command=main");
		}
	%>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">
			<li role="presentation"><a href="../admin/admin_main.jsp">홈</a></li>
			<li class="active" role="presentation"><a
				href="../SidServlet?command=list_all_user">회원 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_all_order">주문 관리</a></li>
			<li role="presentation"><a href="../admin/admin_all_balance_wait.jsp">정산
					관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_all_consult">문의 관리</a></li>
			<li role="presentation"><a href="../admin/boardWrite.jsp">공지사항
					등록</a></li>
			<li role="presentation"><a href="../admin/admin_write_faq.jsp">자주하는
					질문 등록</a></li>
		</ul>
	</div>
	<br>
	<div class="col-md-2">
	<ul class="nav nav-pills nav-stacked">
	 <li role="presentation"><a href="../SidServlet?command=list_all_user">전체</a></li>
	  <li class="active" role="presentation"><a href="../SidServlet?command=list_rep_num">사업자 등록번호 검토</a></li>
	</ul>
	</div>
	<div class="col-md-10">
		<a href="https://www.hometax.go.kr/websquare/websquare.wq?w2xPath=/ui/pp/index_pp.xml" target="_blank">확인 주소 링크</a>
		<form name="frm" method="post">
			<table class="table">
				<thead>
					<tr>
						<th class="1">이메일</th>
						<th class="2">사업자이름</th>
						<th class="3">사업자전화번호</th>
						<th class="4">사업자등록번호</th>
						<th class="5">권한</th>
						<th class="6">승인</th>
					</tr>
				</thead>
				<tbody>

					<c:forEach items="${list}" var="list">
						<tr>
							<td class="1">${list.email }</td>
							<td class="2">${list.rep_name }</td>
							<td class="3">${list.rep_phone }</td>
							<td class="4">${list.rep_num }</td>
							<td class="5"><c:if test="${list.admin eq '0'}">
								슈퍼관리자
							</c:if> <c:if test="${list.admin eq '1'}">
								관리자
								</c:if> <c:if test="${list.admin eq '2'}">
								기업판매자
								</c:if> <c:if test="${list.admin eq '3'}">
								기업구매자
								</c:if> <c:if test="${list.admin eq '4'}">
								개인구매자
								</c:if></td>
							<td class="6"><button type="button" class="btn btn-primary"
									onclick="confirmRepNum('${list.email}');">승인</button></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</form>
	</div>
	<%@ include file="../include/footer.jsp"%>
</body>
<script>
	function confirmRepNum(email) {
		var input = confirm("승인시키겠습니까?");
		if (input == true) {
			$.ajax({
				url : '../SidServlet?command=update_rep_num_flag',
				data : "email="+email,
				success : function(data) {
				
					if (data == 1) {
						alert("성공");
					}
					if (data == 0) {
						alert("실패");
					}
					location.reload();
				}
			});
		} else {
			return;
		}
	}
	;
</script>
</html>