<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../include/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<%
int admin=(int)session.getAttribute("admin");
if(admin>1){
	response.sendRedirect("../SidServlet?command=main");
}
%>
<body>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">
			<li role="presentation"><a href="../admin/admin_main.jsp">홈</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_all_user">회원 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_all_order">주문 관리</a></li>
			<li role="presentation"><a href="../admin/admin_all_balance_wait.jsp">정산
					관리</a></li>
			<li class="active" role="presentation"><a
				href="../SidServlet?command=list_all_consult">문의 관리</a></li>
			<li role="presentation"><a href="../admin/boardWrite.jsp">공지사항
					등록</a></li>
			<li role="presentation"><a href="../admin/admin_write_faq.jsp">자주하는
					질문 등록</a></li>
		</ul>
	</div>
	<br>
	<div class="col-md-12">
		<table class="table">
			<thead>
				<tr>
					<th class="1">번호</th>
					<th class="2">이메일</th>
					<th class="3">내용</th>
					<th class="4">답장</th>
					<th class="5">작성일자</th>
				</tr>
			</thead>
			<tbody>

				<c:forEach items="${list}" var="list">
					<tr>
						<td class="1">${list.consultNum }</td>
						<td class="2">${list.email }</td>
						<td class="3"><button type="button" class="btn btn-primary contentPop"
								data-container="body" data-toggle="popover"
								data-placement="left" data-content="${list.content }">내용
								확인</button></td>
						<td class="4"><c:if test="${list.reply eq '0'}">
								<button type="button" class="btn btn-primary" id="reply"
									data-email="${list.email }" data-postdate="${list.postdate}"
									data-content="${list.content}" data-num="${list.consultNum}"
									data-toggle="modal" data-target="#myModal">답장</button>
							</c:if> <c:if test="${list.reply eq '1'}">
								답장완료
								</c:if></td>
						<td class="5">${list.postdate}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">문의</h4>
				</div>
				<div class="modal-body">
					<p id="num"></p>
					<p id="email"></p>
					<hr>
					<textarea class="form-control" rows="12" id="textContent"></textarea>
					<hr>
					<p id="postdate"></p>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
					<button type="button" class="btn btn-primary" id="send">답장</button>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="../include/footer.jsp"%>
</body>
<script>
	$(function() {


		var num;
		var email;
		var content;
		var postdate;
		var replyContent;

		//popover
		$('[data-toggle="popover"]').popover();		
		
		//modal 클릭시 input value 설정
		$(document).on("click", "#reply", function() {
			$(".contentPop").popover('hide');
			email = $(this).data('email');
			num = $(this).data('num');
			content = $(this).data('content');
			postdate = $(this).data('postdate');
			replyContent = "\n\n\n\n-------------------질문-----------------------\n\n\n" + content;
			$("#email").text(email);
			$("#textContent").val(replyContent);
			$("#postdate").text(postdate);
			$("#num").text(num + "번 글");
		});

		$(document).on("click", "#send", function() {
			replyContent = $("#textContent").val();
			$.ajax({
				url : '../SidServlet?command=consult_reply',
				data : 'email=' + email + "&textContent=" + replyContent + "&consultNum=" + num,
				success : function(data) {
					if (data == "1") {
						alert("success");
						$('#myModal').modal().hide();
						location.reload();
					} else {
						alert("fail");
						$('#myModal').modal().hide();
						location.reload();
					}

				}
			});
		});
	});
</script>
</html>