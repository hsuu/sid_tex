<%@page import="com.sid.dto.PurchaseVO"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../include/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.row {
	margin-top: 40px;
	padding: 0 10px;
}

.clickable {
	cursor: pointer;
}

.panel-heading div {
	margin-top: -18px;
	font-size: 15px;
}

.panel-heading div span {
	margin-left: 5px;
}

.panel-body {
	display: none;
}

body {
	padding-left: 30px;
	padding-right: 30px;
}

table a {
	color: #FFBF00;
}

th {
	white-space: nowrap;
	text-align: center;
}

td {
	white-space: nowrap;
	text-align: center;
}
</style>
</head>
<body>
	<%
		int admin = (int) session.getAttribute("admin");
		if (admin > 1) {
			response.sendRedirect("../SidServlet?command=main");
		}
	%>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">
			<li role="presentation"><a href="../admin/admin_main.jsp">홈</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_all_user">회원 관리</a></li>
			<li class="active" role="presentation"><a
				href="../SidServlet?command=list_all_order">주문 관리</a></li>
			<li role="presentation"><a
				href="../admin/admin_all_balance_wait.jsp">정산 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_all_consult">문의 관리</a></li>
			<li role="presentation"><a href="../admin/boardWrite.jsp">공지사항
					등록</a></li>
			<li role="presentation"><a href="../admin/admin_write_faq.jsp">자주하는
					질문 등록</a></li>
		</ul>
	</div>
	<br>
	<div class="col-md-12">
		<div class="row" id="resizeDiv">
			<div class="table-responsive panel panel-default">
				<table class="table table-striped table-bordered resizeTable">
					<thead>
						<tr>
							<th class="1">주문번호</th>
							<th class="2">상품번호</th>
							<th>상태</th>
							<th class="3">상품명</th>
							<th class="4">색상명</th>
							<th class="5">수량</th>
							<th class="6">수취인명</th>
							<th class="7">수취인연락처</th>
							<th class="8">주소</th>
							<th>판매자이메일</th>
							<th>회사명</th>
							<th class="9">구매자이메일</th>
							<th class="10">구매자명</th>
							<th class="11">구매자연락처</th>
							<th class="12">결제금액</th>
							<th class="13">결제방법</th>
							<th class="14">상품가격</th>
							<th class="15">할인금액</th>
							<th class="16">주문시간</th>
						</tr>
					</thead>
					<tbody>
						<%
							ArrayList<PurchaseVO> relate = (ArrayList<PurchaseVO>) request.getAttribute("list");
							ArrayList<Integer> relateNum = (ArrayList<Integer>) request.getAttribute("relateNum");

							int first = 0;

							for (int i = 0; i < relateNum.size(); i++) {

								if (i == 0) {
									first = 0;
								} else {
									first += relateNum.get(i - 1);
								}
								for (int j = first; j < first + relateNum.get(i); j++) {

									if (j == first) {
										out.print("<tr><td class='1'>" + relate.get(j).getPurchaseId() + "</td><td class='2'>");
									}
									out.print("<div>" + relate.get(j).getProductId() + "</div>");
									if (j == first + (relateNum.get(i) - 1)) {
										out.print("</td><td>");
										switch (relate.get(j).getState()) {
											case -1 :
												out.print("입금대기");
												break;
											case 0 :
												out.print("결제완료");
												break;
											case 1 :
												out.print("배송대기");
												break;
											case 2 :
												out.print("배송중");
												break;
											case 3 :
												out.print("배송완료");
												break;
											case 4 :
												out.print("구매완료");
												break;
											case 5 :
												out.print("취소상품");
												break;
											default :
												out.print(relate.get(j).getState());
												break;
										}
										out.print("</td><td class='3'>" + relate.get(j).getItemName() + "</td><td class='4'>");
									}
								}
								for (int j = first; j < first + relateNum.get(i); j++) {
									out.print("<div>" + relate.get(j).getColorName() + "</div>");
								}

								for (int j = first; j < first + relateNum.get(i); j++) {

									if (j == first) {
										out.print("</td><td class='5'>");
									}
									out.print("<div>" + relate.get(j).getQuantity() + "</div>");
									if (j == first + (relateNum.get(i) - 1)) {
										out.print("</td><td class='6'>" + relate.get(j).getRecipientName() + "</td>" + "<td class='7'>"
												+ relate.get(j).getRecipientPhone() + "</td>" + "<td class='8'>"
												+ relate.get(j).getRecipientAddress() + "</td>" + "<td>" + relate.get(j).getSeller()
												+ "</td><td>" + relate.get(j).getCompany() + "</td><td class='9'>"
												+ relate.get(j).getBuyerEmail() + "</td>" + "<td class='10'>"
												+ relate.get(j).getBuyerName() + "</td>" + "<td class='11'>"
												+ relate.get(j).getBuyerPhone() + "</td>" + "<td class='12'>"
												+ relate.get(j).getBuyOption() + "</td>" + "<td class='13'>"
												+ relate.get(j).getPayOption() + "</td>" + "<td class='14'>" + relate.get(j).getCost()
												+ "</td>" + "<td class='15'>할인금액</td>" + "<td class='16'>"
												+ relate.get(j).getOrderDate() + "</td>" + "</tr>");
									}

								}
							}
						%>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
<script>
	$(function() {
		$(".resizeTable")
			.tablesorter({
				widgets : [ 'filter', 'resizable', 'print' ],
				widgetOptions : {
					// filter_anyMatch replaced! Instead use the filter_external option
					// Set to use a jQuery selector (or jQuery object) pointing to the
					// external filter (column specific or any match)
					filter_external : '.search',
					// add a default type search to the first name column
					filter_defaultFilter : {
						1 : '~{query}'
					},
					// include column filters
					filter_columnFilters : true,
					filter_saveFilters : true,
					filter_reset : '.reset',
					filter_liveSearch : {
						// when false, the user must press enter to blur the input to trigger the search
						3 : false,
						// the query will initiate when 5 or more characters are entered into the filter
						4 : 5,
						// no live search on the last three columns (using a header class name)
						'.last-3-columns' : false,
						// for columns that aren't defined; this will set the fallback value
						// otherwise the fallback defaults to false.
						'fallback' : true
					},

					resizable_addLastColumn : true,
					resizable_widths : [ '50px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '100px', '130px', '130px' ],
					print_title : '', // this option > caption > table id > "table"
					print_dataAttrib : 'data-name', // header attrib containing modified header name
					print_rows : 'f', // (a)ll, (v)isible, (f)iltered, or custom css selector
					print_columns : 's', // (a)ll, (v)isible or (s)elected (columnSelector widget)
					print_extraCSS : '', // add any extra css definitions for the popup window here
					//print_styleSheet : '../css/theme.blue.css', // add the url of your print stylesheet
					print_now : true, // Open the print dialog immediately if true
					// callback executed when processing completes - default setting is null
					print_callback : function(config, $table, printStyle) {
						// do something to the $table (jQuery object of table wrapped in a div)
						// or add to the printStyle string, then...
						// print the table using the following code
						$.tablesorter.printTable.printOutput(config, $table.html(), printStyle);

					}
				}
			});
	});
</script>
</html>
<%@ include file="../include/footer.jsp"%>