<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../include/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
		int admin = (int) session.getAttribute("admin");
		if (admin > 1) {
			response.sendRedirect("../SidServlet?command=main");
		}
	%>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">
			<li class="active" role="presentation"><a href="../admin/admin_main.jsp">홈</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_all_user">회원 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_all_order">주문 관리</a></li>
			<li role="presentation"><a href="../admin/admin_all_balance_wait.jsp">정산
					관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_all_consult">문의 관리</a></li>
			<li role="presentation"><a href="../admin/boardWrite.jsp">공지사항
					등록</a></li>
			<li role="presentation"><a href="../admin/admin_write_faq.jsp">자주하는
					질문 등록</a></li>
		</ul>
	</div>
	<br>
	<div class="col-md-12">
		<p>알바생들에게 고한다</p>
		<select name="export_type" id="export_type">
			<option class="export_option" value="pdf">PDF</option>
			<option class="export_option" value="xlsx">Excel</option>
			<option class="export_option" value="docx">DocX</option>
		</select>
		
		<a href="/mypage/INIStdCancel.html">결제취소</a>
	</div>
	<%@ include file="../include/footer.jsp"%>
</body>
<script>
	$(document).ready(function() {

		$(".export_type").click(function(e) {
			//alert('click');
			console.log($(this).val() + "clcik");
		});

	});
</script>
</html>