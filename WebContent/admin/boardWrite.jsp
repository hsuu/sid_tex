<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../include/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="../editor/js/HuskyEZCreator.js"
	charset="utf-8"></script>
</head>
<body>
	<%
		int admin = (int) session.getAttribute("admin");
		if (admin > 1) {
			response.sendRedirect("../SidServlet?command=main");
		}
	%>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">
			<li role="presentation"><a href="../admin/admin_main.jsp">홈</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_all_user">회원 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_all_order">주문 관리</a></li>
			<li role="presentation"><a href="../admin/admin_all_balance_wait.jsp">정산
					관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_all_consult">문의 관리</a></li>
			<li class="active" role="presentation"><a href="../admin/boardWrite.jsp">공지사항
					등록</a></li>
			<li role="presentation"><a href="../admin/admin_write_faq.jsp">자주하는
					질문 등록</a></li>
		</ul>
	</div>
	<br>
	<div class="col-md-12">
		<form name="frm" method="post">
			<input type="hidden" name="command" value="board_write">
			<div class="form-group">
				<label class="control-label">제목</label> <input type="text"
					class="form-control" style="width: 70%" name="title">
			</div>
			<div class="form-group">
				<label class="control-label">분류</label> <input type="text"
					class="form-control" style="width: 70%" name="sort">
			</div>
			<div class="form-group">

				<label class="control-label">내용</label>
				<textarea name="content" id="smarteditor" rows="10" cols="100"
					style="width: 766px; height: 412px;"></textarea>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary" id="submit_upload">등록</button>
				<button type="button" class="btn btn-primary"
					onclick='location.href="../BoardServlet?command=board_list"'>목록</button>
			</div>
		</form>
	</div>
	<%@ include file="../include/footer.jsp"%>
</body>
<script>
	// smarteditor
	$(function() {

		//전역변수선언
		var editor_object = [];

		nhn.husky.EZCreator.createInIFrame({
			oAppRef : editor_object,
			elPlaceHolder : "smarteditor", //연결지을 textarea의 id명
			sSkinURI : "/editor/SmartEditor2Skin2.html", //에디터 스킨 경로
			htParams : {
				// 툴바 사용 여부 (true:사용/ false:사용하지 않음)
				// 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
				bUseVerticalResizer : true,
				// 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
				bUseModeChanger : true,
			}
		});

		//전송버튼 클릭이벤트
		$("#submit_upload").click(function() {
			//id가 smarteditor인 textarea에 에디터에서 대입
			editor_object.getById["smarteditor"].exec("UPDATE_CONTENTS_FIELD", []);

			// 이부분에 에디터 validation 검증
			document.frm.action = "../BoardServlet?command=board_write";
			document.frm.submit();
			//폼 submit
			//	$("#fm").submit();

		})
	})
</script>
</html>