<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../include/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
		int admin = (int) session.getAttribute("admin");
		if (admin > 1) {
			response.sendRedirect("../SidServlet?command=main");
		}
	%>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">
			<li role="presentation"><a href="../admin/admin_main.jsp">홈</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_all_user">회원 관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_all_order">주문 관리</a></li>
			<li role="presentation"><a href="../admin/admin_all_balance_wait.jsp">정산
					관리</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=list_all_consult">문의 관리</a></li>
			<li role="presentation"><a href="../admin/boardWrite.jsp">공지사항
					등록</a></li>
			<li class="active" role="presentation"><a href="../admin/admin_write_faq.jsp">자주하는
					질문 등록</a></li>
		</ul>
	</div>
	<br>

	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<form name="frm" method="post" class="form-horizontal"
					action="../SidServlet?command=write_faq">

					<div class="form-group">
						<label for="question" class="col-sm-1 control-label">종류</label>
						<div class="col-sm-11">
							<button type="button" class="btn btn-primary"
								data-toggle="dropdown">
								<span id="sort_concept">회원관련</span> <span class="caret"></span>
							</button>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#product">상품 관련</a></li>
								<li><a href="#member">회원 관련</a></li>
								<li><a href="#delivery">배송 관련</a></li>
								<li><a href="#order">주문 관련</a></li>
								<li><a href="#payment">결제/취소/환불</a></li>
								<li><a href="#other">기타</a>
							</ul>
						</div>
					</div>
					<input type="hidden" name="sort" value="member" id="sort">
					<div class="form-group">
						<label for="question" class="col-sm-1 control-label">질문</label>
						<div class="col-sm-11">
							<textarea class="form-control" name="question" rows="3"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="answer" class="col-sm-1 control-label">답변</label>
						<div class="col-sm-11">
							<textarea class="form-control" name="answer" rows="3"></textarea>
						</div>
					</div>
					
					<div class="pull-right">
						<button type="submit" class="btn btn-primary">등록</button>
					</div>
					
				</form>
			</div>
		</div>
	</div>
</body>
<script>
	//검색
	$(document).ready(function(e) {
		$('.dropdown-menu').find('a').click(function(e) {
			e.preventDefault();
			var param = $(this).text();
			$('#sort_concept').text(param);
			$('#sort').val(param);
		});


	});
</script>
</html>
<%@ include file="../include/footer.jsp"%>