<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../include/header.jsp"%>
<!DOCTYPE html>
<html>
<body>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">

			<li role="presentation"><a
				href="../SidServlet?command=list_all_basket">장바구니</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=purchase_list_buyer">거래내역 </a></li>
			<li class="active" role="presentation"><a
				href="../mypage/myinfo.jsp">아이디관리</a></li>
		</ul>
	</div>
	<br>
	<div class="col-md-12">
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="text-center">
						<h1>
							<i class="fa fa-lock"></i>
						</h1>
						<h2 class="text-center">기본정보수정</h2>
						<div class="form-group">
							<%
								if (session.getAttribute("admin") != null && session.getAttribute("admin").equals(4)) {
							%>
							<a href="../SidServlet?command=myinfo"
								class="btn btn-lg btn-primary btn-block">수정 페이지로</a>
							<%
								} else {
							%>
							<a href="../SidServlet?command=myinfoB"
								class="btn btn-lg btn-primary btn-block">수정 페이지로</a>
							<%
								}
							%>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="text-center">
						<h1>
							<i class="fa fa-truck"></i>
						</h1>
						<h2 class="text-center">배송정보관리</h2>
						<div class="form-group">
							<a href="../SidServlet?command=delivery"
								class="btn btn-lg btn-primary btn-block">관리 페이지로</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="text-center">
						<h1>
							<i class="fa fa-user-times"></i>
						</h1>
						<h2 class="text-center">회원 탈퇴</h2>
						<div class="form-group">
							<a href="../mypage/memberLeave.jsp"
								class="btn btn-lg btn-danger btn-block">탈퇴 페이지로</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


</body>
</html>
<%@ include file="../include/footer.jsp"%>