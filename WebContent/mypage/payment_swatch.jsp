<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="com.sid.dto.CompanyVO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.sid.dto.BasketVO"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../include/header.jsp"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.inicis.std.util.SignatureUtil"%>
<%@page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
</head>
 <!-- <script language="javascript" type="text/javascript" src="HTTPS:/stgstdpay.inicis.com/stdjs/INIStdPay.js" charset="UTF-8"></script>
 --><script language="javascript" type="text/javascript" src="HTTPS://stdpay.inicis.com/stdjs/INIStdPay.js" charset="UTF-8"></script>
<style>
body {
	padding-left: 30px;
	padding-right: 30px;
}

th {
	white-space: nowrap;
	text-align: center;
}

td {
	white-space: nowrap;
	text-align: center;
	vertical-align: middle;
}
</style>
<body onload="INIStdPay.allowpopup();">
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">

			<li class="active" role="presentation"><a
				href="../SidServlet?command=list_all_basket">장바구니</a></li>
			<li role="presentation"><a
				href="../SidServlet?command=purchase_list_buyer">거래내역 </a></li>
			<li role="presentation"><a href="../mypage/myinfo.jsp">아이디관리</a></li>
		</ul>
	</div>
	<div class="col-md-12">
			<div class="wizard">
					<div class="wizard-inner">
						<div class="connecting-line"></div>
						<ul class="nav nav-tabs" role="tablist">

							<li role="presentation" class="disabled"><a href="#"
								 aria-controls="step1" role="tab"
								title="Step 1"> <span class="round-tab"> <i
										class="fa fa-shopping-cart"></i>
								</span>
							</a></li>

							<li role="presentation" class="active"><a href="#"
								 aria-controls="step2" role="tab"
								title="Step 2"> <span class="round-tab"> <i
										class="fa fa-credit-card"></i>
								</span>
							</a></li>
							<li role="presentation" class="disabled"><a href="#"
								 aria-controls="step3" role="tab"
								title="Step 3"> <span class="round-tab"> <i
										class="glyphicon glyphicon-ok"></i>
								</span>
							</a></li>
						</ul>
					</div>
				</div>
		

		<div class="col-md-12 purchaseList">

			<h3>결제상품 목록</h3>

			<div class="col-md-12">
				<div class="row" id="resizeDiv">
					<div class="table-responsive panel panel-default">
						<table class="table table-striped table-bordered" id="resizeTable">
							<thead>
								<tr>
									<th class="1">이미지</th>
									<th class="2">상품명</th>
									<th class="3">판매사</th>
									<th class="5">가격</th>
									<th class="6">선택 옵션</th>
									<th>배송비</th>
									<th class="7">계</th>
								</tr>
							</thead>
							<tbody>

								<%
									ArrayList<CompanyVO> company = (ArrayList<CompanyVO>) request.getAttribute("company");
									ArrayList<String> companyList = (ArrayList<String>) request.getAttribute("companyList");
							
									String itemname="(스와치)"; //상품명
									int totalprice=0; //가격
									int tempprice=0;//임시 가격 (조건부무료 체크용)
							
									ArrayList<BasketVO> relate = (ArrayList<BasketVO>) request.getAttribute("relate");
									System.out.println("norelate size : "+relate.size());
									String bIdList="";
									String costList="";
									String countList="";
									
									for (int j = 0; j < relate.size(); j++) {
										
										itemname+=relate.get(j).getItemName();
												totalprice+=200;
												tempprice=200;
												out.print("<tr>");
												out.print("<td class='1'><img src='" + relate.get(j).getMainImg()
														+ "'style='width: 100px; height: 150px'><input type='hidden' class='retail' value='"
														+ relate.get(j).getRetail() + "'><input type='hidden' class='wholesale' value='"
														+ relate.get(j).getWholesale() + "'><input type='hidden' class='standard' value='"
														+ relate.get(j).getStandard() + "'></td>");
												out.print("<td>" + relate.get(j).getItemName() + "</td>");
												out.print("<td>" + relate.get(j).getCompany() + "</td>");
												out.print("<td>소매가 :" + relate.get(j).getRetail() + "<br>도매가 :" + relate.get(j).getStandard()
														+ "↑ " + relate.get(j).getWholesale() + "</td>");
												out.print("<td class='6'>스와치 주문</td><td>");
										 	for(int k=0;k<company.size();k++){
												if(relate.get(j).getCompany().equals(company.get(k).getCompany())){
													if(company.get(k).getDelivery_costway().equals("무료")){
														out.print("무료");
													}else if(company.get(k).getDelivery_costway().equals("유료")){
														out.print(company.get(k).getDelivery_cost());
														company.get(k).setTotalCost(company.get(k).getDelivery_cost());
													}else{//조건부무료
														out.print(company.get(k).getDelivery_cost2()+"원<br>("+company.get(k).getDelivery_cost()+"원 이상 무료)");
														company.get(k).setTotalCost(company.get(k).getTotalCost()+tempprice);
													}
													
													break;
												}
											} 
										
										out.print("</td><td class='itemCost' style='background-color: #e6e6e6;'>200</td>");
										out.print("</tr>");
										
										bIdList+=(""+relate.get(j).getBasketId()+",");
										costList+=(""+tempprice+",");
										countList+=("1,");
										
									}
							
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<%
	
	JSONObject obj = new JSONObject();
		//배송가격 구하기, 저장하기
	int deliveryprice=0;
	for(int i=0;i<company.size();i++){
		if(company.get(i).getDelivery_costway().equals("조건부무료")){
			if(company.get(i).getTotalCost()>=company.get(i).getDelivery_cost()){
				obj.put(""+company.get(i).getCompany(),new Integer(0));
			}else{
				deliveryprice+=company.get(i).getDelivery_cost2();
				obj.put(""+company.get(i).getCompany(),new Integer(company.get(i).getDelivery_cost2()));
			}
		}else{
			deliveryprice+=company.get(i).getTotalCost();
			obj.put(""+company.get(i).getCompany(),new Integer(company.get(i).getTotalCost()));
		}
	}
	

		/*
			//*** 위변조 방지체크를 signature 생성 ***
		
				oid, price, timestamp 3개의 키와 값을
		
				key=value 형식으로 하여 '&'로 연결한 하여 SHA-256 Hash로 생성 된값
		
				ex) oid=INIpayTest_1432813606995&price=819000&timestamp=2012-02-01 09:19:04.004
					
		
				 * key기준 알파벳 정렬
		
				 * timestamp는 반드시 signature생성에 사용한 timestamp 값을 timestamp input에 그대로 사용하여야함
		*/

		//############################################
		// 1.전문 필드 값 설정(***가맹점 개발수정***)
		//############################################

		// 여기에 설정된 값은 Form 필드에 동일한 값으로 설정
		String mid = "sidtexsidt"; // 가맹점 ID(가맹점 수정후 고정)					

		//인증
		String signKey = "RjFPWVpKNnc0VnhYd1VFYzZTbDRHZz09"; // 가맹점에 제공된 웹 표준 사인키(가맹점 수정후 고정)
		String timestamp = SignatureUtil.getTimestamp(); // util에 의해서 자동생성
		StringBuffer buffer = new StringBuffer();
		for (int k = 0; k < 4; k++) {
			int n = (int) (Math.random() * 10);
			buffer.append(n); 
		}
		String oid = SignatureUtil.getTimestamp()+buffer.toString(); // 가맹점 주문번호(가맹점에서 직접 설정)
		String price = "" + (totalprice+deliveryprice); // 상품가격(특수기호 제외, 가맹점에서 직접 설정)

		String cardNoInterestQuota = "11-2:3:,34-5:12,14-6:12:24,12-12:36,06-9:12,01-3:4"; // 카드 무이자 여부 설정(가맹점에서 직접 설정)
		String cardQuotaBase = "2:3:4:5:6:11:12:24:36"; // 가맹점에서 사용할 할부 개월수 설정

		//###############################################
		// 2. 가맹점 확인을 위한 signKey를 해시값으로 변경 (SHA-256방식 사용)
		//###############################################
		String mKey = SignatureUtil.hash(signKey, "SHA-256");

		//###############################################
		// 2.signature 생성
		//###############################################
		Map<String, String> signParam = new HashMap<String, String>();

		signParam.put("oid", oid); // 필수
		signParam.put("timestamp", timestamp); // 필수
		signParam.put("price", price);
		// 필수
		// signature 데이터 생성 (모듈에서 자동으로 signParam을 알파벳 순으로 정렬후 NVP 방식으로 나열해 hash)
		String signature = SignatureUtil.makeSignature(signParam);

		/* 기타 */
		String siteDomain = "http://localhost:8080/mypage"; //가맹점 도메인 입력		아이맥	
		//String siteDomain = "http://localhost:10920/mypage"; //가맹점 도메인 입력		놋북
		//String siteDomain="http://www.sid-tex.com/mypage";
		// 페이지 URL에서 고정된 부분을 적는다. 
		// Ex) returnURL이 http://localhost:8080INIpayStdSample/INIStdPayReturn.jsp 라면
		// http://localhost:8080/INIpayStdSample 까지만 기입한다.
	%>
	<div class="col-md-offset-8 col-md-4">

		<div class="panel panel-primary" style="margin-right: 12px;">
			<div class="panel-heading">
				<h3 class="panel-title" style="text-align: center;">계  + 배송비 합계 = 총 결제금액</h3>
			</div>
			<div class="panel-body">
				<p id="totalCost" style="text-align: center;"><%=totalprice %> + <%=deliveryprice%> = <%=totalprice+deliveryprice%></p>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		
			
				<div class="panel panel-default">
					<div class="panel-body">
			<div class="col-md-8">
				<form name="frm" class="form-horizontal" id="SendPayForm_id" method="post">
						<input type="hidden" style="width: 100%;" name="version"
								value="1.0">
						<input type="hidden" style="width: 100%;" name="mid" value="<%=mid%>">
						<input type="hidden" style="width: 100%;" name="goodname" value="<%=itemname%>">
						<input type="hidden" style="width: 100%;" name="oid" value="<%=oid%>"> 
						<input type="hidden" style="width:100%;" value="<%=totalprice+deliveryprice%>" name="price">
						<input type="hidden" style="width: 100%;" name="currency" value="WON">
						<input type="hidden" style="width: 100%;" name="buyeremail"
								value="${sessionScope.email }">
						<input type="hidden" style="width: 100%;" name="timestamp" value="<%=timestamp%>">
						<input type="hidden" style="width: 100%;" name="signature"
								value="<%=signature%>">
						<input type="hidden" style="width: 100%;" name="returnUrl" value="<%=siteDomain%>/payment_success.jsp">
						<input type="hidden" name="mKey" value="<%=mKey%>">
						<input type="hidden" style="width: 100%;" name="gopaymethod" value="">
						<input type="hidden" style="width: 100%;" name="offerPeriod"
								value="20170101-20171231">
						<input type="hidden" style="width: 100%;" name="acceptmethod" value="CARDPOINT:HPP(1):no_receipt:va_receipt:vbanknoreg(0):below1000">
						<input type="hidden" style="width: 100%;" name="languageView"
								value="">
						<input type="hidden" style="width: 100%;" name="charset" value="">
						<input type="hidden" style="width: 100%;" name="payViewType" value="">
						<input type="hidden" style="width: 100%;" name="closeUrl" value="<%=siteDomain%>/close.jsp">
						<input type="hidden" style="width: 100%;" name="popupUrl" value="<%=siteDomain%>/popup.jsp">
						<input type="hidden" style="width: 100%;" name="quotabase" value="<%=cardQuotaBase%>">
						<input type="hidden" style="width: 100%;" name="ini_onlycardcode" value="">
						<input type="hidden" style="width: 100%;" name="ini_cardcode" value="">
						<input type="hidden" style="width: 100%;" name="ansim_quota" value="">
						<input type="hidden" style="width: 100%;" name="vbankRegNo" value="">
						<input type="hidden" style="width: 100%;" name="merchantData"
								value="">
						<input type="hidden" id="idList" name="idList" value="<%=bIdList%>">
						<input type="hidden" id="costList" name="costList" value="<%=costList%>">
						<input type="hidden" id="countList" name="countList" value="<%=countList%>">
						<input type="hidden" id="deliveryList" name="deliveryList" value=<%=obj.toJSONString()%>>
						<input type="hidden" name="moid" value="<%=oid%>">
						
							<h2 class="text-center">배송지 정보 입력</h2>
							<div class="control-group col-md-6">
								<!-- Username -->
								<label class="control-label" for="buyername">받는사람</label>
								<div class="controls ">
									<input type="text" name="buyername" placeholder=""
										class="form-control" value="${member.name}">
								</div>
							</div>
							<div class="control-group col-md-6">
								<!-- phone -->
								<label class="control-label" for="buyertel">연락처</label>
								<div class="controls">
									<input type="text" id="buyertel" name="buyertel" placeholder=""
										class="form-control" value="${member.phone}">
								</div>
							</div>
								<!-- address -->
								
							<div class="control-group col-md-6">
								<label class="control-label " for="zipnum">주소</label>
								<div class="controls">
									<input type="text" class="form-control" id="sample6_postcode" name="zipNum" placeholder="우편번호" readonly>
								</div>
							</div>
							<div class="control-group col-md-6">
							<label class="control-label" for="zipnum">&nbsp;</label>
								<div class="controls">
									<button type="button" class="btn btn-primary" onclick="sample6_execDaumPostcode()">우편번호 찾기</button>
								</div>
							</div>
							<div class="control-group col-md-6" style="padding-top:5px;">
								<input type="text" class="form-control" id="sample6_address"
									placeholder="주소" name="roadAddrPart1">
							</div>
							<div class="control-group col-md-6" style="padding-top:5px;">
								<input type="text" class="form-control" id="sample6_address2"
									placeholder="상세주소" name="addrDetail">
							</div>
							
							<div class="control-group col-md-12" style="padding-top:5px;">
								<label class="control-label" for="zipnum">&nbsp;</label>
								<input type="text" class="form-control"
									placeholder="배송메시지" name="delivery_message">
							</div>
							<div class="col-md-12" style="padding-top:20px;">
								<button type="button" onclick="checkForm()" class="btn btn-primary">결제하기</button>
							</div>
								</form>
						</div>						
						<!-- /col-md-8 -->
						<div class="col-md-4">
							<h4>배송지 변경하기</h4>
							<input type="radio" name="addressRadio1" value="newAddress" checked> 신규 배송지 <br>
							<input type="radio" name="addressRadio1" value="address1"> 배송지 1 <br>
							<input type="radio" name="addressRadio1" value="address2"> 배송지 2 <br>
							<input type="radio" name="addressRadio1" value="address3"> 배송지 3 <br>
							<input type="radio" name="addressRadio1" value="address4"> 배송지 4
						</div>
						<!-- /col-md-4 -->
						
					</div>
					<!-- /panel-body -->
				</div>
				<!-- /panel-default -->
	
	</div>

</body>
<script>
	//주소찾기
	function sample6_execDaumPostcode() {
		new daum.Postcode({
			oncomplete : function(data) {
				// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

				// 각 주소의 노출 규칙에 따라 주소를 조합한다.
				// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
				var fullAddr = ''; // 최종 주소 변수
				var extraAddr = ''; // 조합형 주소 변수

				// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
				if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
					fullAddr = data.roadAddress;

				} else { // 사용자가 지번 주소를 선택했을 경우(J)
					fullAddr = data.jibunAddress;
				}

				// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
				if (data.userSelectedType === 'R') {
					//법정동명이 있을 경우 추가한다.
					if (data.bname !== '') {
						extraAddr += data.bname;
					}
					// 건물명이 있을 경우 추가한다.
					if (data.buildingName !== '') {
						extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
					}
					// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
					fullAddr += (extraAddr !== '' ? ' (' + extraAddr + ')' : '');
				}

				// 우편번호와 주소 정보를 해당 필드에 넣는다.
				document.getElementById('sample6_postcode').value = data.zonecode; //5자리 새우편번호 사용
				document.getElementById('sample6_address').value = fullAddr;

				// 커서를 상세주소 필드로 이동한다.
				document.getElementById('sample6_address2').focus();
			}
		}).open();
	}
	//폼 체크
	function checkForm() {
		var flag = 1;
		if (document.frm.buyername.value.length == 0) {
			alert("받는 사람을 써주세요");
			frm.username.focus();
			flag = 0;
			return false;
		}
		if (document.frm.buyertel.value.length == 0) {
			alert("연락처를 써주세요");
			frm.phone.focus();
			flag = 0;
			return false;
		}
		if (document.frm.zipNum.value.length == 0) {
			alert("주소를 입력하세요");
			frm.zipNum.focus();
			flag = 0;
			return false;
		}
		if (flag == 1) {
			
			 $.ajax({
				url : '../SidServlet?command=order_swatch',
				data : $('#SendPayForm_id').serialize(),
				success : function(data) {
					if (data > 0) {
						console.log("성공");
						INIStdPay.pay('SendPayForm_id'); 
					} else {
						console.log("실패");
					}
				}
			}) 


		}
	}



	$(function() {
		//유저 정보 가져오기
		var address = [];
		var address2 = [];
		var zipnum = [];
		$.ajax({
			url : '../SidServlet?command=get_user_address',
			success : function(data) {

				var info = data.split(":");


				var temp = [];
				var temp2 = [];
				for (var i = 0; i < 4; i++) {
					temp = info[i].split(",");
					temp2 = temp[0].split("/");
					address[i] = temp2[0];
					address2[i] = temp2[1];

					zipnum[i] = temp[1];
				}


			}
		});

		$("input[name=addressRadio1]").on('click', function(e) {
			switch (this.value) {
			case "newAddress":
				$("#sample6_postcode").val("");
				$("#sample6_address").val("");
				$("#sample6_address2").val("");
				break;
			case "address1":
				$("#sample6_postcode").val(zipnum[0]);
				$("#sample6_address").val(address[0]);
				$("#sample6_address2").val(address2[0]);
				break;
			case "address2":
				$("#sample6_postcode").val(zipnum[1]);
				$("#sample6_address").val(address[1]);
				$("#sample6_address2").val(address2[1]);
				break;
			case "address3":
				$("#sample6_postcode").val(zipnum[2]);
				$("#sample6_address").val(address[2]);
				$("#sample6_address2").val(address2[2]);
				break;
			case "address4":
				$("#sample6_postcode").val(zipnum[3]);
				$("#sample6_address").val(address[3]);
				$("#sample6_address2").val(address2[3]);
				break;
			default:
				break;
			}
		})

		

	});
</script>

</html>
<%@ include file="../include/footer.jsp"%>