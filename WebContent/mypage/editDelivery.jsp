<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../include/header.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">

			<li role="presentation"><a
				href="../SidServlet?command=list_all_basket">장바구니</a></li>
			<li role="presentation"><a href="../SidServlet?command=purchase_list_buyer">결제내역
			</a></li>
			<li class="active" role="presentation"><a
				href="../mypage/myinfo.jsp">아이디관리</a></li>
		</ul>
	</div>
	<br>
	<div id="checkDiv">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="text-center">
						<h1>
							<i class="fa fa-truck"></i>
						</h1>
						<h2 class="text-center">배송 정보 관리</h2>
						<p>비밀번호 확인</p>
						<div class="panel-body">
							<div class="alert alert-danger" role="alert" id="pwdCheck">
								<span class="glyphicon glyphicon-exclamation-sign"
									aria-hidden="true"></span> <span class="sr-only">Error:</span>
								비밀번호가 다릅니다.
							</div>
							<div class="form-group">
								<input id="pwd" name="password" placeholder="password"
									class="form-control" type="password">
							</div>
							<div class="form-group">
								<input name="recover-submit" id="checkPassword"
									class="btn btn-lg btn-primary btn-block" value="확인"
									type="button">
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="myinfo">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="text-center">
						<h1>
							<i class="fa fa-edit"></i>
						</h1>
						<h2 class="text-center">배송 정보 관리</h2>
						<hr>
					</div>
					<div>
						<form method="post" name="frm" id="standardinfo">
							<div>
								<h3>배송지1</h3>
								<div class="form-group col-md-12">
									<input type="hidden" value="business" name="state">
									<div class="col-md-2" style="padding-left: 0; margin-left: 0">
										<h4>
											<label class="control-label">주소</label>
										</h4>
									</div>

									<div class="form-group col-md-10">
										<div class="col-md-6" style="margin: 0; padding: 0">
											<input type="hidden" id="confmKey" name="confmKey"> <input
												type="text" class="form-control" name="zipNum" id="zipNum"
												placeholder="우편번호" value="${list.zipnum1 }" readonly>
										</div>
										<div class="col-md-5">
											<button type="button" class="btn btn-primary"
												onclick="goPopup()">검색</button>
										</div>
									</div>
									<div class="col-md-2"></div>
									<div class="form-group col-md-10">
										<input type="text" class="form-control" name="roadAddrPart1"
											placeholder="도로명주소" id="roadAddrPart1"
											 value="${address1}">

									</div>
									<div class="col-md-2"></div>
									<div class="form-group col-md-10">
										<input type="text" class="form-control" name="addrDetail"
											id="addrDetail" placeholder="상세주소" value="${address_detail1 }">
									</div>
								</div>
							</div>
							<div>
								<h3>배송지2</h3>
								<div class="form-group col-md-12">
									<input type="hidden" value="business" name="state">
									<div class="col-md-2" style="padding-left: 0; margin-left: 0">
										<h4>
											<label class="control-label">주소</label>
										</h4>
									</div>

									<div class="form-group col-md-10">
										<div class="col-md-6" style="margin: 0; padding: 0">
											<input type="hidden" id="confmKey" name="confmKey"> <input
												type="text" class="form-control" name="zipNum2" id="zipNum2"
												placeholder="우편번호"value="${list.zipnum2 }" readonly>
										</div>
										<div class="col-md-5">
											<button type="button" class="btn btn-primary"
												onclick="goPopup2()">검색</button>
										</div>
									</div>
									<div class="col-md-2"></div>
									<div class="form-group col-md-10">
										<input type="text" class="form-control" name="roadAddrPart2"
											placeholder="도로명주소" id="roadAddrPart2" value="${address2}">

									</div>
									<div class="col-md-2"></div>
									<div class="form-group col-md-10">
										<input type="text" class="form-control" name="addrDetail2"
											id="addrDetail2" placeholder="상세주소"value="${address_detail2 }">
									</div>
								</div>
							</div>
							<div>
								<h3>배송지3</h3>
								<div class="form-group col-md-12">
									<input type="hidden" value="business" name="state">
									<div class="col-md-2" style="padding-left: 0; margin-left: 0">
										<h4>
											<label class="control-label">주소</label>
										</h4>
									</div>

									<div class="form-group col-md-10">
										<div class="col-md-6" style="margin: 0; padding: 0">
											<input type="hidden" id="confmKey" name="confmKey"> <input
												type="text" class="form-control" name="zipNum3" id="zipNum3"
												placeholder="우편번호"value="${list.zipnum3 }" readonly>
										</div>
										<div class="col-md-5">
											<button type="button" class="btn btn-primary"
												onclick="goPopup3()">검색</button>
										</div>
									</div>
									<div class="col-md-2"></div>
									<div class="form-group col-md-10">
										<input type="text" class="form-control" name="roadAddrPart3"
											placeholder="도로명주소" id="roadAddrPart3" value="${address3}" >

									</div>
									<div class="col-md-2"></div>
									<div class="form-group col-md-10">
										<input type="text" class="form-control" name="addrDetail3"
											id="addrDetail3" placeholder="상세주소" value="${address_detail3 }">
									</div> 
								</div>
							</div>
							<div>
								<h3>배송지4</h3>
								<div class="form-group col-md-12">
									<input type="hidden" value="business" name="state">
									<div class="col-md-2" style="padding-left: 0; margin-left: 0">
										<h4>
											<label class="control-label">주소</label>
										</h4>
									</div>

									<div class="form-group col-md-10">
										<div class="col-md-6" style="margin: 0; padding: 0">
											<input type="hidden" id="confmKey" name="confmKey"> <input
												type="text" class="form-control" name="zipNum4" id="zipNum4"
												placeholder="우편번호"value="${list.zipnum4 }" readonly>
										</div>
										<div class="col-md-5">
											<button type="button" class="btn btn-primary"
												onclick="goPopup4()">검색</button>
										</div>
									</div>
									<div class="col-md-2"></div>
									<div class="form-group col-md-10">
										<input type="text" class="form-control" name="roadAddrPart4"
											placeholder="도로명주소" id="roadAddrPart4" value="${address4}" >

									</div>
									<div class="col-md-2"></div>
									<div class="form-group col-md-10">
										<input type="text" class="form-control" name="addrDetail4"
											id="addrDetail4" placeholder="상세주소" value="${address_detail4 }">
									</div> 
								</div>
							</div>
							<div class="form-group">
								<button type="button" class="btn btn-primary col-md-12"
									onclick="return updateCheck();">저장</button>
								<br> <br>
							</div>
						</form>

					</div>
				</div>
			</div>
		</div>
	</div>


</body>
<script>
$(function() {
	$("#pwdCheck").hide();
	$("#myinfo").hide();
	
	$("#checkPassword").click(function(e) {

		var pwd = $("#pwd").val();

		//비밀번호 확인
		$.ajax({
			url : '../SidServlet?command=password_check',
			data : 'pwd=' + encodeURIComponent(pwd),
			success : function(data) {
				if (data == "1") {
					$("#myinfo").show();
					$("#checkDiv").hide();
				} else {
					$("#pwdCheck").show();
				}
			}
		});
	})
});
	//주소찾기
	function goPopup() {
		// í¸ì¶ë íì´ì§(jusopopup.jsp)ìì ì¤ì  ì£¼ìê²ìURL(http://www.juso.go.kr/addrlink/addrLinkUrl.do)ë¥¼ í¸ì¶íê² ë©ëë¤.
		var pop = window.open("../address/jusoPopup.jsp", "pop", "width=570,height=420, scrollbars=yes, resizable=yes");
	}
	function jusoCallBack(roadAddrPart1, addrDetail, zipNo) {
		// íìíì´ì§ìì ì£¼ììë ¥í ì ë³´ë¥¼ ë°ìì, í íì´ì§ì ì ë³´ë¥¼ ë±ë¡í©ëë¤.
		document.frm.roadAddrPart1.value = roadAddrPart1;
		document.frm.addrDetail.value = addrDetail;
		document.frm.zipNum.value = zipNo;
	}
	function goPopup2() {
		// í¸ì¶ë íì´ì§(jusopopup.jsp)ìì ì¤ì  ì£¼ìê²ìURL(http://www.juso.go.kr/addrlink/addrLinkUrl.do)ë¥¼ í¸ì¶íê² ë©ëë¤.
		var pop = window.open("../address/jusoPopup2.jsp", "pop", "width=570,height=420, scrollbars=yes, resizable=yes");
	}
	function jusoCallBack2(roadAddrPart1, addrDetail, zipNo) {
		// íìíì´ì§ìì ì£¼ììë ¥í ì ë³´ë¥¼ ë°ìì, í íì´ì§ì ì ë³´ë¥¼ ë±ë¡í©ëë¤.
		document.frm.roadAddrPart2.value = roadAddrPart1;
		document.frm.addrDetail2.value = addrDetail;
		document.frm.zipNum2.value = zipNo;
	}
	function goPopup3() {
		// í¸ì¶ë íì´ì§(jusopopup.jsp)ìì ì¤ì  ì£¼ìê²ìURL(http://www.juso.go.kr/addrlink/addrLinkUrl.do)ë¥¼ í¸ì¶íê² ë©ëë¤.
		var pop = window.open("../address/jusoPopup3.jsp", "pop", "width=570,height=420, scrollbars=yes, resizable=yes");
	}
	function jusoCallBack3(roadAddrPart1, addrDetail, zipNo) {
		// íìíì´ì§ìì ì£¼ììë ¥í ì ë³´ë¥¼ ë°ìì, í íì´ì§ì ì ë³´ë¥¼ ë±ë¡í©ëë¤.
		document.frm.roadAddrPart3.value = roadAddrPart1;
		document.frm.addrDetail3.value = addrDetail;
		document.frm.zipNum3.value = zipNo;
	}
	function goPopup4() {
		// í¸ì¶ë íì´ì§(jusopopup.jsp)ìì ì¤ì  ì£¼ìê²ìURL(http://www.juso.go.kr/addrlink/addrLinkUrl.do)ë¥¼ í¸ì¶íê² ë©ëë¤.
		var pop = window.open("../address/jusoPopup4.jsp", "pop", "width=570,height=420, scrollbars=yes, resizable=yes");
	}
	function jusoCallBack4(roadAddrPart1, addrDetail, zipNo) {
		// íìíì´ì§ìì ì£¼ììë ¥í ì ë³´ë¥¼ ë°ìì, í íì´ì§ì ì ë³´ë¥¼ ë±ë¡í©ëë¤.
		document.frm.roadAddrPart4.value = roadAddrPart1;
		document.frm.addrDetail4.value = addrDetail;
		document.frm.zipNum4.value = zipNo;
	}

	function updateCheck() {
		if (document.frm.zipNum.value=="") {
			alert("최소 1개의 배송지가 필요합니다.")
			goPopup();
			return false;
			}

		//정보 수정 ajax
		$.ajax({
			url : "../SidServlet?command=update_delivery",
			data : $("#standardinfo").serialize(),
			success : function(data) {
				if (data == "1") {
					alert("수정 완료");
					location.reload();
					//$("#resultMessage").val("fail");
					return true;
				} else {
					alert("수정 실패");
					location.reload();
					//$("#authNumConfirm").val(data);
					return true;
				}
			}
		});
		return true;
	}
	

</script>
<%@ include file="../include/footer.jsp"%>
</html>
