<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../include/header.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">

			<li role="presentation"><a
				href="../SidServlet?command=list_all_basket">장바구니</a></li>
			<li role="presentation"><a href="../SidServlet?command=purchase_list_buyer">결제내역
			</a></li>
			<li class="active" role="presentation"><a
				href="../mypage/myinfo.jsp">아이디관리</a></li>
		</ul>
	
	</div>
	<br>
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="text-center">
					<h1>
						<i class="fa fa-user-times"></i>
					</h1>
					<h2 class="text-center">회원 탈퇴</h2>

					<div class="panel-body">
						<div class="form-group">
							<p>탈퇴 사유</p>
							<textarea class="form-control" rows="7" id="content"></textarea>

						</div>
						<div class="alert alert-danger" role="alert" id="pwdCheck">
							<span class="glyphicon glyphicon-exclamation-sign"
								aria-hidden="true"></span> <span class="sr-only">Error:</span>
							비밀번호가 다릅니다.
						</div>
						<p>비밀번호</p>

						<input id="pwd" name="password" placeholder="password"
							class="form-control" type="password">
					</div>

					<div class="form-group">
						<input name="recover-submit" id="leave"
							class="btn btn-lg btn-danger btn-block" value="회원 탈퇴"
							type="button">
					
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="modal fade" id="memberLeave" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-body">
						<p>탈퇴 후에는 3개월 간 재가입이 불가능합니다. 정말 탈퇴하시겠습니까?</p>
						<p>진행중인 결제가 있을 경우에는 회원 탈퇴가 반려됩니다.</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary"
							onclick="memberLeave()">예</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">아니오</button>
					</div>
				</div>

			</div>
		</div>
</body>
<script>
	$(function() {
		$("#pwdCheck").hide();
		$("#leave").click(function(e) {

			var pwd = $("#pwd").val();
			$.ajax({
				url : '../SidServlet?command=password_check',
				data : 'pwd=' +encodeURIComponent(pwd),
				success : function(data) {
					if (data == "1") {
						$("#memberLeave").modal();
					} else {
						$("#pwdCheck").show();
					}
				}
			});
		});
		
	});
		function memberLeave(){
			$.ajax({
				url : '../SidServlet?command=leave_user',
				data : 'reason='+encodeURIComponent($("#content").val()),
				success : function(data) {
					if (data >0) {
						alert("회원탈퇴에 성공했습니다. 이용해주셔서 감사합니다.");
						location.replace("../SidServlet?command=main");
					} else {
						alert("회원탈퇴에 실패했습니다. 관리자에게 문의하세요.");
					}
				}
			});
		}

</script>
<%@ include file="../include/footer.jsp"%>
</html>