<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.sid.dto.PurchaseVO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../include/header.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<style>
th {
	text-align: center;
}

body {
	padding-left: 30px;
	padding-right: 30px;
}

th {
	text-align: center;
}

td {
	text-align: center;
}
</style>
<body>

	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">

			<li role="presentation"><a href="../SidServlet?command=list_all_basket">장바구니</a></li>
			<li class="active" role="presentation"><a href="../SidServlet?command=purchase_list_buyer">거래내역 </a></li>
			<li role="presentation"><a href="../mypage/myinfo.jsp">아이디관리</a></li>
		</ul>
	</div>
	<br>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">

			<li class="active"><a href="../SidServlet?command=purchase_list_buyer">진행중인 거래</a></li>
			<li role="presentation"><a href="../SidServlet?command=purchase_list_complete_buyer">완료된 거래</a></li>
			<li role="presentation"><a href="../SidServlet?command=purchase_list_cancel_buyer">취소된 거래</a></li>
		</ul>
	</div>
	<div class="col-md-12 col-xs-12" id="customer-orders">
		<div class="box">
			<br>
			<div class="tab-content">

						<div class="col-md-12">
							<div class="col-md-10">
								<button type="button" class="btn btn-primary" onclick="check_cancelOrder()">선택 주문 취소</button>
							</div>
							<div class="col-md-2">

								<button type="button" class="btn btn-primary" onclick="check_confirmOrder()">선택 구매 확정</button>
							</div>
						</div>
						<br> <br>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th></th>
									<th class="1">결제번호(주문일시)</th>
									<th class="2">판매사</th>
									<th>상품정보</th>
									<th class="3">선택옵션</th>
									<th class="7">가격</th>
									<th>배송비</th>
									<th class="8">주문상태</th>
									<th>총 가격</th>
								</tr>
							</thead>
							<tbody>

								<%
								ArrayList<ArrayList<PurchaseVO>> list = (ArrayList<ArrayList<PurchaseVO>>) request.getAttribute("list");
								ArrayList<PurchaseVO> childlist = null;
								JSONParser parser = new JSONParser();
								JSONObject price = (JSONObject) parser.parse((String) request.getAttribute("price"));
								JSONObject obj = (JSONObject) parser.parse((String) request.getAttribute("oidjson"));
								JSONObject tempobj = null;
									for (int i = 0; i < list.size(); i++) {
										System.out.println("list size" + list.size());
										for (int j = 0; j < list.get(i).size(); j++) {
											System.out.println("list get i size" + list.get(i).size());

											childlist = list.get(i);
											if(childlist.get(j).getState()<6){
												if(j==0){
													out.print("<tr>");
												}
													out.print("<td><input type='checkbox' class='processing'><input type='hidden' class='pid' value="+childlist.get(j).getPurchaseId()+" ><input type='hidden' class='seller' value="+childlist.get(j).getSeller()+"></td>");
												if (j == 0) {
													out.print("<td class='oidtd' rowspan=" + list.get(i).size() + ">"
															+ childlist.get(j).getOid() + "<br>(" + childlist.get(j).getOrderDate() + ")<br>");
													if(childlist.get(j).getPayFlag()==1&&childlist.get(j).getState()==-1){
														
														out.print("<button type='button' class='btn btn-primary' onclick='getAccountInfo(\""+childlist.get(j).getOid()+"\")'>입금계좌</button>");
													}
													tempobj = (JSONObject) obj.get(childlist.get(j).getOid());
													out.print("</td>");
												}
												out.print("<td class='companytd'>" + childlist.get(j).getCompany() + "</td>");
												out.print("<td><img src='" + childlist.get(j).getMainImg() + "' style='width:100px; height:150px'>&nbsp;&nbsp;"
														+ childlist.get(j).getItemName() + "</td>");
												out.print("<td>" + childlist.get(j).getPurchase_detail() + "</td>");
												out.print(
														"<td>" + childlist.get(j).getCost() + "<br>(" + childlist.get(j).getQuantity() + "마)</td>");
												out.print("<td class='delivery_cost'>");
												if(childlist.get(j).getExpress()==1){//퀵일경우
													out.print("후불(퀵)");
												}else{
													out.print(childlist.get(j).getDelivery_cost());
												}
												out.print("</td>");
												
												if(childlist.get(j).getState()==-1){
													out.print("<td class='7'>입금대기<input type='hidden' class='state' value='-1'>");
												}else if(childlist.get(j).getState()==0){
													out.print("<td class='7'>결제완료<input type='hidden' class='state' value='0'>");
												}else if(childlist.get(j).getState()==1){
													out.print("<td class='7'>배송대기<input type='hidden' class='state' value='1'>");
												}else if(childlist.get(j).getState()==2){
													out.print("<td class='7'>배송중<input type='hidden' class='state' value='2'>");
													out.print("<br><br><a tabindex='0' role='button' data-trigger='focus' data-container='body' data-toggle='popover' data-placement='right' data-content='"+
															childlist.get(j).getInvoiceNum()+"'>송장번호</a>");
												}else if(childlist.get(j).getState()==3){
													out.print("<td class='7'>배송완료<input type='hidden' class='state' value='3'>");
													out.print("<br><br><a tabindex='0' role='button' data-trigger='focus' data-container='body' data-toggle='popover' data-placement='right' data-content='"+
															childlist.get(j).getInvoiceNum()+"'>송장번호</a>");
												}else if(childlist.get(j).getState()==4){
													out.print("<td class='7'>구매확정<input type='hidden' class='state' value='4'>");
													out.print("<br><br><a tabindex='0' role='button' data-trigger='focus' data-container='body' data-toggle='popover' data-placement='right' data-content='"+
															childlist.get(j).getInvoiceNum()+"'>송장번호</a>");
												}else if(childlist.get(j).getState()==5){
													out.print("<td class='7' style='color:red'>주문취소<input type='hidden' class='state' value='5'>");
												}
														out.print("</td>");
												if (j == 0) {
	
													out.print("<td rowspan=" + list.get(i).size() + ">" + price.get(childlist.get(j).getOid()));
													if(childlist.get(j).getDelivery_message()!=null&&childlist.get(j).getDelivery_message().length()>3){
														
														out.print("<br><br><a tabindex='0' role='button' data-trigger='focus' data-container='body' data-toggle='popover' data-placement='right' data-content='"+
																childlist.get(j).getDelivery_message()+"'>"+ childlist.get(j).getDelivery_message().substring(0,3)
																+ "...펼치기</a></td>");
													}else{
														out.print("<br><br>"+
																childlist.get(j).getDelivery_message()+"</td>");
													}
													}
												out.print("<input type='hidden' id='oid' value='" + childlist.get(j).getOid() + "'>");
												out.print("</tr>");
											
											}
										}

									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="cancelModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-body">
					<p class="text-center">
						<span id="cancelMessage"></span><br>주문 취소 사유를 입력해주세요.
					</p>
					<textarea class="form-control" id="cancel_reason" rows="3" cols="3"></textarea>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" onclick="cancelOrder()">주문 취소</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="confirmModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-body">
					<p class="text-center">
						<span id="confirmMessgae"></span><br>구매 확정하시겠습니까?
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" onclick="confirmOrder()">구매 확정</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="showReasonModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-body">
					<p class="text-center">주문 취소 사유</p>
					<input type="hidden" id="seller_email"> <input type="hidden" id="purchase_id">
					<table class="table">
						<thead>
							<tr>
								<th>주문 번호</th>
								<th>사유</th>
								<th>취소 일자</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td id="pid"></td>
								<td id="reason"></td>
								<td id="cancel_date"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="showAccountModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-body">
					<p class="text-center">무통장 입금</p>
					<table class="table">
						<tr>
							<td class="title">입금 금액</td>
							<td id="price"></td>
						</tr>
						<tr>
							<td class="title">입금 계좌번호</td>
							<td id="vact_num"></td>
							<td class="title">입금 은행명</td>
							<td id="vact_bankName"></td>
						</tr>
						<tr>
							<td class="title">예금주 명</td>
							<td id="vact_name"></td>
							<td class="title">송금자 명</td>
							<td id="vact_inputName"></td>
						</tr>
						<tr>
							<td class="title">송금 일자</td>
							<td id="vact_date"></td>
							<td class="title">송금 시간</td>
							<td id="vact_time"></td>
						</tr>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>
	<!-- /.container -->

</body>
<script>
$('[data-toggle="popover"]').popover();	
	$(function() {
		var company = "";
		var tempcompany = "";
		var oid = "";
		var tempoid = "";
		$('.companytd').each(function() {
			tempoid = $(this).siblings('#oid').val();
			tempcompany = $(this).text();
			if (tempoid==oid && tempcompany==company) {
				$(this).siblings('.delivery_cost').text("");
			}
			company = tempcompany;
			oid = tempoid;
		})
	})
	function getAccountInfo(oid) {
		$.ajax({
			url : "../SidServlet?command=get_account_info",
			dataType : "json",
			data : "oid=" + oid,

			success : function(data) {
				var Ca = /\+/g;
				$("#price").text(decodeURIComponent(data.price));
				$("#vact_num").text(decodeURIComponent((data.vact_num).replace(Ca, " ")));
				$("#vact_bankName").text(decodeURIComponent((data.vact_bankName).replace(Ca, " ")));
				$("#vact_name").text(decodeURIComponent((data.vact_name).replace(Ca, " ")));
				$("#vact_inputName").text(decodeURIComponent((data.vact_inputName).replace(Ca, " ")));
				$("#vact_date").text(decodeURIComponent((data.vact_date).replace(Ca, " ")));
				$("#vact_time").text(decodeURIComponent((data.vact_time).replace(Ca, " ")));
			}
		});
		$("#showAccountModal").modal();
	}
	function show_reason(id, seller) {
		$.ajax({
			url : "../SidServlet?command=get_cancel_reason",
			dataType : "json",
			data : "purchaseId=" + id,

			success : function(data) {
				var Ca = /\+/g;
				$("#pid").text(decodeURIComponent(data.purchaseId));
				$("#reason").text(decodeURIComponent((data.reason).replace(Ca, " ")));
				$("#cancel_date").text(decodeURIComponent(data.date));

			}
		});
		$("#purchase_id").val(id);
		$("#seller_email").val(seller);
		$("#showReasonModal").modal();
	}

	function check_cancelOrder() {
		var count = 0;
		var allcount = 0;
		var flag = 0;
		//유효성 체크
		$(".processing").each(function() {
			if (this.checked) {
				allcount++;
				var state = $(this).parent().siblings(".7").find('.state').val();
				if (state ==0 || state==-1) {

					flag = 1;
					count++;
				}
			}
		});
		if (flag == 1 && allcount == count) {
			$("#cancelMessage").text(count + "개의 상품이 선택되었습니다.");
			$("#cancelModal").modal();
		} else if (flag == 0 && allcount == count) {
			alert("상품을 선택하세요.");
		} else {
			alert("주문 취소는 입금대기, 결제완료 단계에서만 할 수 있습니다.");
		}
	}

	function cancelOrder() {
		var idList = new Array();
		var emailList = new Array();
		var reason = $("#cancel_reason").val();
		var idList2 = new Array();
		var emailList2 = new Array();

		$(".processing").each(function() {
			var state = $(this).parent().siblings('.7').find('.state').val();
			if (this.checked && state == 0) {

				idList.push($(this).siblings(".pid").val());
				emailList.push($(this).siblings(".seller").val());
			/*  else if (this.checked && state == "배송대기") {
				idList2.push($(this).siblings(".pid").val());
				emailList2.push($(this).siblings(".seller").val()); */
			} else if (this.checked && state == -1) {
				idList.push($(this).siblings(".pid").val());
				emailList.push($(this).siblings(".seller").val());
			}
		});
		console.log("1 : " + idList.toString() + " 2: " + idList2.toString());
		$.ajax({
			url : "../SidServlet?command=cancel_order_buyer",
			data : "idList=" + idList.toString() + "&idList2=" + idList2.toString() + "&emailList=" + emailList.toString() + "&emailList2=" + emailList2.toString() + "&reason=" + reason + "&state=5",
			success : function(data) {
				if (data > 0) {
					console.log("success");
				} else {
					console.log("fail");
				}
				location.reload();
			}
		});


	}
	
	//선택 구매확정
	function check_confirmOrder() {
		var flag = 0;
		var count = 0;
		var allcount = 0;
		//유효성 체크
		$(".processing").each(function() {
			if (this.checked) {
				allcount++;
				var state = $(this).parent().siblings(".7").find('.state').val()
				if (state == 2 || state == 3) {
					flag = 1;
					count++;
				}
			}
		});
		if (flag == 1 && allcount == count) {
			$("#confirmMessage").text(count + "개의 상품이 선택되었습니다.");
			$("#confirmModal").modal();
		} else if (flag == 0 && allcount == count) {
			alert("상품을 선택하세요.");
		} else {
			alert("구매 확정은 배송중, 배송완료 단계에서만 할 수 있습니다.");
		}
	}
	function confirmOrder() {
		var idList = new Array();
		var emailList = new Array();
		$(".processing").each(function() {
			if (this.checked) {
				idList.push($(this).siblings(".pid").val());
				emailList.push($(this).siblings(".seller").val());
			}
		});
		$.ajax({
			url : "../SidServlet?command=request_consignment_buyer",
			data : "idList=" + idList.toString() + "&emailList=" + emailList.toString() + "&state=4",
			success : function(data) {
				if (data > 0) {
					console.log("success");
				} else {
					console.log("fail");
				}
				location.reload();
			}
		});
	}

	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			html : true,
			container : 'body'
		});
	});
</script>
<%@ include file="../include/footer.jsp"%>
</html>