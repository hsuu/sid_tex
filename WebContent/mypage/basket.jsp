<%@page import="java.util.ArrayList"%>
<%@page import="com.sid.dto.BasketVO"%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="../include/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
th {
	white-space: nowrap;
	text-align: center;
}

td {
	white-space: nowrap;
	text-align: center;
	vertical-align: middle;
}

#cost p {
	text-align: left;
}

.colorDiv {
	padding: 3px;
	margin: 0px
}
.btn-swatch{
	 background-color: #999999;
  	border-color: #8c8c8c;
}
.dropdown-menu
{
   height: 110px;
   overflow: auto;
   }
</style>
</head>
<body>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">
			<li class="active" role="presentation"><a href="../SidServlet?command=list_all_basket">장바구니</a></li>
			<li role="presentation"><a href="../SidServlet?command=purchase_list_buyer">거래내역 </a></li>
			<li role="presentation"><a href="../mypage/myinfo.jsp">아이디관리</a></li>
		</ul>
	</div>

	<div class="col-md-12">

		<div class="wizard">
			<div class="wizard-inner">
				<div class="connecting-line"></div>
				<ul class="nav nav-tabs" role="tablist">

					<li role="presentation" class="active"><a href="#" aria-controls="step1" role="tab" title="Step 1"> <span class="round-tab"> <i
								class="fa fa-shopping-cart"></i>
						</span>
					</a></li>

					<li role="presentation" class="disabled"><a href="#" aria-controls="step2" role="tab" title="Step 2"> <span class="round-tab"> <i
								class="fa fa-credit-card"></i>
						</span>
					</a></li>
					<li role="presentation" class="disabled"><a href="#" aria-controls="step3" role="tab" title="Step 3"> <span class="round-tab"> <i
								class="glyphicon glyphicon-ok"></i>
						</span>
					</a></li>
				</ul>
			</div>
		</div>
		<!-- Tab panes -->
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="home">
				<!-- tab 시작-->
				<br>
				<div class="col-md-12">
					<div class="form-group pull-right">
						<button type="button" class="btn btn-primary" id="sel_del">선택 삭제</button>
					</div>
				</div>
				<div class="col-md-12">
					<div class="row" id="resizeDiv">
						<div class="table-responsive panel panel-default">
							<table class="table table-striped table-bordered" id="resizeTable">
								<thead>
									<tr>
										<th><input type="checkbox" id="checkth"></th>
										<th class="1">이미지</th>
										<th class="2">상품명</th>
										<th class="3">판매사</th>
										<th class="4">스와치</th>
										<th>배송</th>
										<th class="5">가격</th>
										<th class="6" style="width: 300px">선택 옵션</th>
										<th class="7">계</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${norelate}" var="basket">
										<tr>
											<td class="0"><input type="checkbox" id="basketBox" name="checktd"> <input type="hidden" class="retail"
												value="${basket.retail}"> <input type="hidden" class="wholesale" value="${basket.wholesale}"> <input type="hidden"
												class="standard" value="${basket.standard}"> <input type="hidden" class="bId" value="${basket.basketId}"><input
												type="hidden" class="basketType" value="norelate"></td>
											<td class="1"><a href="../SidServlet?command=read_product&productId=${basket.productId}"><img src="${basket.mainImg}"
													style="width: 100px; height: 150px"></a></td>
											<td class="2">${basket.itemName}</td>
											<td class="3">${basket.company}</td>
											<td class="4">${basket.swatch}</td>
											<td class="del"><c:if test="${basket.delivery eq 1}">퀵</c:if> <c:if test="${basket.delivery eq 10}">택배</c:if> <c:if
													test="${basket.delivery eq 11}">퀵,택배</c:if></td>
											<td class="5" id="cost"><p>소매가 : ${basket.retail}원</p>
												<p>도매가 : ${basket.wholesale }원&nbsp; ${basket.standard}마↑</p></td>
											<td class="6">
												<div class="col-md-12">
													<label class="col-md-4 control-label">${basket.colorName}</label><input type="number" name="quantity"
														style="text-align: center; width: 30%" class="quantity" min="1" step="1" value="${basket.quantity}">
														<c:if test="${basket.state eq '판매중'}"><input type="hidden" class="productState" value="1"></c:if>
														<c:if test="${basket.state eq '판매중지'}">판매중지<input type="hidden" class="productState" value="0"></c:if>
													<%-- (재고:
														<c:choose>
															<c:when test="${basket.stock eq -1}">∞)</c:when>
															<c:otherwise>${basket.stock })</c:otherwise>
														</c:choose>) --%>
													<input type="hidden" class="colorStock" value="${basket.stock}"><input type="hidden" class="basketId" value="${basket.basketId}">
													<input type="hidden" class="productId" value="${basket.productId}">
												</div>
											</td>
											<td class="itemCost" style="background-color: #e6e6e6;">0원</td>
										</tr>
									</c:forEach>


									<%
										ArrayList<BasketVO> relate = (ArrayList<BasketVO>) request.getAttribute("relate");
										ArrayList<Integer> relateNum = (ArrayList<Integer>) request.getAttribute("relateNum");
										int first = 0;

										for (int i = 0; i < relateNum.size(); i++) {
											if (i == 0) {
												first = 0;
											} else {
												first += relateNum.get(i - 1);
											}
											for (int j = first; j < first + relateNum.get(i); j++) {
												if (j == first) {

													out.print("<tr>");
													out.print(
															"<td class='0'><input type='checkbox' id='basketBox' name='checktd'><input type='hidden' class='bId' value="
																	+ relate.get(j).getRelate()
																	+ "><input type='hidden' class='basketType' value='relate'>"
																	+ "<input type='hidden' class='retail' value='" + relate.get(j).getRetail()
																	+ "'><input type='hidden' class='wholesale' value='"
																	+ relate.get(j).getWholesale()
																	+ "'><input type='hidden' class='standard' value='"
																	+ relate.get(j).getStandard() + "'></td>");
													out.print("<td><a href='../SidServlet?command=read_product&productId="
															+ relate.get(j).getProductId() + "'><img src='" + relate.get(j).getMainImg()
															+ "'style='width: 100px; height: 150px'></a></td>");
													out.print("<td>" + relate.get(j).getItemName() + "</td>");
													out.print("<td>" + relate.get(j).getCompany() + "</td>");
													out.print("<td class='4'>" + relate.get(j).getSwatch() + "</td>");
													out.print("<td class='del'>");

													if (relate.get(j).getDelivery() == 1) {
														out.print("퀵");
													} else if (relate.get(j).getDelivery() == 10) {
														out.print("택배");
													} else {
														out.print("퀵,택배");
													}

													out.print("</td><td id='cost'><p>소매가 : " + relate.get(j).getRetail() + "원</p>" + "<p>도매가 : "
															+ relate.get(j).getWholesale() + "원   " + relate.get(j).getStandard() + "마 " + "↑ "
															+ "</p></td>");
													out.print("<td class='6'>");

													//드롭다운 생성
													for (int k = first; k < first + relateNum.get(i); k++) {
														if (k == first) {
															out.print("<div class='dropdown'>"
																	+ "<button class='btn btn-primary dropdown-toggle' type='button' data-toggle='dropdown'>색상 옵션 <span class='caret'></span></button>"
																	+ "<ul class='dropdown-menu'>");
														}
														if(relate.get(k).getState().equals("판매중")){
															
															out.print("<li><a href='#' onclick='addColor(" + relate.get(k).getBasketId() + ")'>"
																+ relate.get(k).getColorName() + "&nbsp;&nbsp;" + "(재고 :");
														}else{
															out.print("<li><a href='#' onclick='alert(\"판매 중지된 상품입니다.\")'>"
																	+ relate.get(k).getColorName() + "&nbsp;[판매 중지]&nbsp;&nbsp;" + "(재고 :");
														}
														if (relate.get(k).getStock().equals("-1")) {
															out.print("∞)");
														} else {
															out.print(relate.get(k).getStock() + ")");
														}
														out.print("</a></li>");

													}

													out.print("</ul></div>");

												}

												//아이템 리스트
												String stock = null;
												if (relate.get(j).getQuantity() != 0) {//0이 아니면
													out.print("<div class='colorDiv' id='div" + relate.get(j).getBasketId()
															+ "'><label class='col-md-4 control-label'>" + relate.get(j).getColorName()
															+ "</label><input type='number' name='quantity' class='quantity' style='text-align:center;width:30%' min='1' step='1' value='"
															+ relate.get(j).getQuantity() + "'>");
													if(relate.get(j).getState().equals("판매중")){
														out.print("<input type='hidden' class='productState' value='1'>");
													}else{
														out.print("&nbsp;[판매중지]<input type='hidden' class='productState' value='0'>");
													}
												out.print("<input type='hidden' class='colorStock'" + " value='"
																+ relate.get(j).getStock() + "'><input type='hidden' class='productId' value='"
																+ relate.get(j).getProductId() + "'><input type='hidden' class='basketId' value='"
																+ relate.get(j).getBasketId() + "'>&nbsp;");
												

													stock = relate.get(j).getStock();

												} else {
													out.print("<div class='colorDiv' id='div" + relate.get(j).getBasketId()
															+ "' style='display:none;'><label class='col-md-4 control-label'>"
															+ relate.get(j).getColorName()
															+ "</label><input type='number' name='quantity' class='quantity' style='text-align:center;width:30%' min='1' step='1' value='"
															+ relate.get(j).getQuantity() + "'><input type='hidden' class='colorStock'" + " value='"
															+ relate.get(j).getStock() + "'><input type='hidden' class='productId' value='"
															+ relate.get(j).getProductId() + "'><input type='hidden' class='basketId' value='"
															+ relate.get(j).getBasketId() + "'>&nbsp;");
													stock = relate.get(j).getStock();
												}

												out.print("&nbsp;<button type='button' class='btn btn-primary' onclick='deleteColor("
														+ relate.get(j).getBasketId() + ")'><i class='fa fa-close'></i></button></div>");

											}
											out.print("</td><td class='itemCost' style='background-color: #e6e6e6;'>0원</td>");
											out.print("</tr>");

										}
									%>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-offset-8 col-md-4">

		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title" style="text-align: center;">선택합계</h3>
			</div>
			<div class="panel-body">
				<p id="totalCost" style="text-align: center;"></p>
			</div>
		</div>
	</div>

	<div class="col-md-12">
		<div class="pull-right">
			<form method="post" name="frm">
				<input type="text" id="idList" name="idList" style="display: none" /> <input type="hidden" id="delivery_way" name="delivery_way">

				<button type="button" class="btn btn-primary" onclick="saveState()">
					적용하기 <i class="fa fa-check-square-o"></i>
				</button>

				<button type="button" class="btn btn-swatch" onclick="order_swatch_quick()">
					선택 스와치 퀵 주문하기 <i class="fa fa-chevron-right"></i>
				</button>
				<button type="button" class="btn btn-swatch" onclick="order_swatch()">
					선택 스와치 택배 주문하기 <i class="fa fa-chevron-right"></i>
				</button>
				
				<button type="button" class="btn btn-primary" onclick="orderquick()">
					선택 원단 퀵 주문하기 <i class="fa fa-chevron-right"></i>
				</button>
				<button type="button" class="btn btn-primary" onclick="orderparcel()">
					선택 원단 택배 주문하기 <i class="fa fa-chevron-right"></i>
				</button>

			

			</form>
		</div>
	</div>
</body>
<script>
	var sel = 0;
	var saveFlag = 1; //1이면 바로 주문가능, 0이면 저장후 주문

	//이니셜라이징부터 '계' 계산
	$(function() {
		$(".quantity").trigger("change");
		$("#checkth").trigger("click");
		saveFlag = 1;
	});

	function deleteColor(id) {
		$("#div" + id).css("display", "none");
		$("#div" + id).find('.quantity').val(0);
	}

	//색상  추가
	function addColor(id) {
		if ($("#div" + id).find('.quantity').val() == 0) {
			$("#div" + id).css("display", "");
			$("#div" + id).find('.quantity').val(1);
			$("#div" + id).focus();
		}
	}

	//상태 저장(적용)
	function saveState() {
		var barr = new Array();
		var qarr = new Array();
		$(".quantity").each(function() {
			//0이 아니면
			var basketId = $(this).siblings('.basketId').val();
			var quantity = this.value;
			barr.push(basketId);
			qarr.push(quantity);

		});

	

		setInterval(function() {
			$.ajax({
				url : '../SidServlet?command=update_color_option',
				data : 'basketId=' + barr.toString() + '&quantity=' + qarr.toString(),
				success : function(data) {
					if (data == 1) {
						location.reload();
					} else {
						alert("fail");
					}
				}
			});
			
		}, 1000);


		saveFlag = 1;


	}

	//원단 주문하기
		function orderquick() {
			var flag=1;
			if (saveFlag == 0) {
				alert("적용 후 주문하세요")
			} else {

				//주문할 경우
				if ($("#totalCost").text() == "0원") {
					alert("주문하실 상품을 선택하세요");
				} else {

					//체크된것들의 바스켓 아이디를 가져와서 뿌려줌
					var count = 0;
					var pIdList = new Array();
					$(".quantity").each(function() {
						if(this.value<0){
							
							flag=3;
						}
						
						var checkbox = $(this).parent().parent().siblings('.0').find('#basketBox');
						if (checkbox.is(':checked')) {
							if($(this).siblings(".productState").val()==0){
								flag=5;
							}else{
							
							if(checkbox.parent().siblings('.del').text()=='택배'){
								console.log(checkbox.parent().siblings('.del').text())
								flag=2;
							}else if(checkbox.parent().siblings('.itemCost').text()=='0'){
								flag=4;
							}else{
								var pId = $(this).siblings('.productId').val();
								pIdList.push(pId);
								count++;	
							}
							}
						}
					});
					if(flag==2){
						alert("퀵 가능 상품을 확인해주세요. ")
					}else if(flag==3){
						alert("수량 확인을 해주세요!");
					}else if(flag==4){
						alert("상품 옵션을 확인해주세요.");
					}else if(flag==5){
							alert("판매중지인 상품을 확인해주세요.");
					}else if(flag==1){
					$("#delivery_way").val("quick");
					
					$("#idList").val(pIdList.toString());
					setInterval(function() {
						document.frm.action="../SidServlet?command=to_payment";
						document.frm.submit();
					}, 1000);
					}
				}
			}
		}
	
	//원단 주문하기
	function orderparcel() {
		var flag=1;
			if (saveFlag == 0) {
				alert("적용 후 주문하세요")
			} else {

				//주문할 경우
				if ($("#totalCost").text() == "0원") {
					alert("주문하실 상품을 선택하세요");
				} else {

					//체크된것들의 바스켓 아이디를 가져와서 뿌려줌
					var count = 0;
					var pIdList = new Array();
					$(".quantity").each(function() {
							
						if(this.value<0){
							
							flag=3;
						}
						
						var checkbox = $(this).parent().parent().siblings('.0').find('#basketBox');
						if (checkbox.is(':checked')) {
							if($(this).siblings(".productState").val()==0){
								flag=5;
							}else{
							
							if(checkbox.parent().siblings('.del').text()=='퀵'){
								console.log(checkbox.parent().siblings('.del').text())
								flag=2;
							}else if(checkbox.parent().siblings('.itemCost').text()=='0'){
								flag=4;
							}else{
								var pId = $(this).siblings('.productId').val();
								pIdList.push(pId);
								count++;	
							}
							}
							
						}
						
					});
					if(flag==2){
						alert("택배 가능 상품을 확인해주세요. ")
					}else if(flag==4){
						alert("상품 옵션을 확인해주세요.")	;
					}else if(flag==3){
						alert("수량 확인을 해주세요!");
					}else if(flag==5){
						alert("판매중지인 상품을 확인해주세요.");
					}else if(flag==1){
					$("#delivery_way").val("parcel");
					
					
					$("#idList").val(pIdList.toString());
					console.log("pid:"+pIdList.toString());
				 	setInterval(function() {
						document.frm.action="../SidServlet?command=to_payment";
						document.frm.submit();
					}, 1000); 
					}
				}
			}
		}
	
	//스와치 주문하기
	function order_swatch() {
		if (saveFlag == 0) {
			alert("적용 후 주문하세요")
		} else {

			//주문할 경우
			if ($("#totalCost").text() == "0원") {
				alert("주문하실 상품을 선택하세요");
			} else {
				//체크된것들의 바스켓 아이디를 가져와서 뿌려줌
				var flag=1;
				var pIdList = new Array();
				var pId;
				$("input[name=checktd]").each(function() {
					if(this.checked){
						if($(this).parent().siblings('.4').text()=="제공"){
							
							if($(this).parent().siblings('.del').text()=='퀵'){
								flag=2;
							}

							pId=$(this).parent().siblings('.6').find('.productId').val();
							pIdList.push(pId);
							
						}else if((${sessionScope.admin}!=4)&&$(this).parent().siblings('.4').text()=="사업자에게만 제공"){
							pId=$(this).parent().siblings('.6').find('.productId').val();
							pIdList.push(pId);
							if($(this).parent().siblings('.del').text()=='퀵'){
								flag=2;
							}
						}else{
							flag=0;
						}
					}
				})
				if(flag==1){
					$("#idList").val(pIdList.toString());
					console.log("pid:"+pIdList.toString());
				  	setInterval(function() {
						document.frm.action="../SidServlet?command=to_swatch_payment";
						document.frm.submit();
					}, 1000); 
				}else if(flag==2){
					alert("택배 가능 상품을 확인해주세요.")	
				
				}else{
					alert("스와치를 미제공하는 상품이 포함되어 있습니다.");
				}
			}
		}
	}
	//스와치 주문하기
		function order_swatch_quick() {
			if (saveFlag == 0) {
				alert("적용 후 주문하세요")
			} else {

				//주문할 경우
				if ($("#totalCost").text() == "0원") {
					alert("주문하실 상품을 선택하세요");
				} else {
					//체크된것들의 바스켓 아이디를 가져와서 뿌려줌
					var flag=1;
					var pIdList = new Array();
					var pId;
					$("input[name=checktd]").each(function() {
						if(this.checked){

							if($(this).parent().siblings('.4').text()=="제공"){
								
								if($(this).parent().siblings('.del').text()=='퀵'){
									flag=2;
								}

								pId=$(this).parent().siblings('.6').find('.productId').val();
								pIdList.push(pId);
								
							}else if((${sessionScope.admin}!=4)&&$(this).parent().siblings('.4').text()=="사업자에게만 제공"){
								pId=$(this).parent().siblings('.6').find('.productId').val();
								pIdList.push(pId);
								
								if($(this).parent().siblings('.del').text()=='퀵'){
									flag=2;
								}
							}else{
								flag=0;
							}
						}
					})
					$("#delivery_way").val("quick");
					if(flag==1){
						$("#idList").val(pIdList.toString());
						console.log("pid:"+pIdList.toString());
					  	setInterval(function() {
							document.frm.action="../SidServlet?command=to_swatch_payment";
							document.frm.submit();
						}, 1000); 
					}else if(flag==2){
						alert("택배 가능 상품을 확인해주세요.")	
					
					}else{
						alert("스와치를 미제공하는 상품이 포함되어 있습니다.");
					}
				}
			}
		}


	//테이블 체크박스 전체 선택,해제
	$("#checkth").click(function() {
		sel = 0;
		if ($("#checkth").prop("checked")) {
			$("input[name=checktd]:checkbox").each(function() {
				this.checked = true;
				sel++;
			});
		} else {
			$("input[name=checktd]:checkbox").each(function() {
				this.checked = false;
			});
			sel = 0;
		}
		$("#selItems").text(sel);

		getTotalCost();

	});

	//개별 선택시 전체선택 버튼 해제
	$("input[name=checktd]:checkbox").click(function() {
		if ($("#checkth").prop("checked")) {
			$("#checkth").prop("checked", false);
		}
		if (this.checked) {
			sel++;
		} else {
			sel--;
		}
		$("#selItems").text(sel);

		getTotalCost();
	});

	function getTotalCost() {
		var totalCost = 0;
		$("input[name=checktd]:checkbox").each(function() {
			if (this.checked) {

				var itemCost = $(this).parent().siblings(".itemCost").text();

				totalCost += parseInt(itemCost);
			}
		})
		$("#totalCost").text(totalCost + "원");
	}


	//재고량
	$(document).on('change', '.quantity', function(e) {
		var totalCount = 0;
		var retail = $(this).parent().parent().siblings(".0").find(".retail").val();
		var wholesale = $(this).parent().parent().siblings(".0").find(".wholesale").val();
		var standard = $(this).parent().parent().siblings(".0").find(".standard").val();

		var colorStock = parseInt($(this).siblings('.colorStock').val());
		if (colorStock != -1) {

			if (this.value > colorStock) {
				alert("재고량 부족");
				this.value = colorStock;
			}

		}

		$(this).parent().parent().find(".quantity").each(function() {
			totalCount += parseInt(this.value);
		});

		if (totalCount >= standard) {
			cost = totalCount * wholesale;
		} else {
			cost = totalCount * retail;
		}
		$(this).parent().parent().siblings(".itemCost").text(cost);

		saveFlag = 0;
		getTotalCost();
	})

	//선택삭제
	$("#sel_del").click(function() {
		$("input[name=checktd]:checkbox").each(function() {

			if (this.checked) {
				var num = $(this).siblings(".bId").val();
				var type = $(this).siblings(".basketType").val();
				$.ajax({
					url : '../SidServlet?command=delete_basket',
					data : 'basketId=' + num + '&type=' + type,
					success : function(data) {
						if (data == "1") {
							alert("success");
							location.reload();
						} else {
							alert("fail");
						}

					}
				});
			}
		});
	});
</script>
</html>
<%@ include file="../include/footer.jsp"%>