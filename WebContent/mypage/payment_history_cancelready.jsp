<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.sid.dto.PurchaseVO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../include/header.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<style>
th {
	text-align: center;
}

body {
	padding-left: 30px;
	padding-right: 30px;
}

th {
	text-align: center;
}

td {
	text-align: center;
}
</style>
<body>

	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">

			<li role="presentation"><a href="../SidServlet?command=list_all_basket">장바구니</a></li>
			<li class="active" role="presentation"><a href="../SidServlet?command=purchase_list_buyer">거래내역 </a></li>
			<li role="presentation"><a href="../mypage/myinfo.jsp">아이디관리</a></li>
		</ul>
	</div>
	<br>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">

		<li role="presentation"><a href="../SidServlet?command=purchase_list_buyer">진행중인 거래</a></li>
			<li role="presentation"><a href="../SidServlet?command=purchase_list_complete_buyer">완료된 거래</a></li>
			<li role="presentation"><a href="../SidServlet?command=purchase_list_cancel_buyer">취소된 거래</a></li>
			<li class="active"><a href="../SidServlet?command=purchase_list_cancelready_buyer">취소 대기 중인 거래</a></li>
		</ul>
	</div>
	<div class="col-md-12 col-xs-12" id="customer-orders">
		<div class="box">
			<br>
			<div class="tab-content">

				<br> <br>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th class="1">주문번호(주문일시)</th>
							<th class="2">판매사</th>
							<th>상품정보</th>
							<th class="3">선택옵션</th>
							<th class="7">가격</th>
							<th>배송비</th>
							<th class="8">주문상태</th>
						</tr>
					</thead>
					<tbody>

						<%
							ArrayList<PurchaseVO> childlist = (ArrayList<PurchaseVO>) request.getAttribute("list");
							for (int j = 0; j < childlist.size(); j++) {
								out.print("<tr><td>" + childlist.get(j).getOid() + "<br>(" + childlist.get(j).getOrderDate()
										+ ")</td>");
								out.print("<td>" + childlist.get(j).getCompany() + "</td>");
								out.print("<td><img src='" + childlist.get(j).getMainImg() + "' style='width:100px; height:150px'>&nbsp;&nbsp;"
										+ childlist.get(j).getItemName() + "</td>");
								out.print("<td>" + childlist.get(j).getPurchase_detail() + "</td>");
								out.print("<td>" + childlist.get(j).getCost() + "<br>(" + childlist.get(j).getQuantity() + "마)</td>");
								out.print("<td class='delivery_cost'>" + childlist.get(j).getDelivery_cost() + "</td>");
								out.print("<td>취소 대기 중인 거래<br><button type='button' class='btn btn-primary' onclick='show_reason(\""+childlist.get(j).getPurchaseId()+"\",\""+childlist.get(j).getSeller()+"\")'>사유 확인</button></td>");
								out.print("</tr>");

							}
						%>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="showReasonModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-body">
					<p class="text-center">주문 취소 사유</p>
					<input type="hidden" id="seller_email"> <input type="hidden" id="purchase_id">
					<table class="table">
						<thead>
							<tr>
								<th>주문 번호</th>
								<th>사유</th>
								<th>취소 일자</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td id="pid"></td>
								<td id="reason"></td>
								<td id="cancel_date"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>

	<!-- /.container -->

</body>
<script>
	$(function() {
		var company = "";
		var tempcompany = "";
		var oid = "";
		var tempoid = "";
		$('.companytd').each(function() {
			tempoid = $(this).siblings('#oid').val();
			tempcompany = $(this).text();
			if (tempoid == oid && tempcompany == company) {
				$(this).siblings('.delivery_cost').text("");
			}
			company = tempcompany;
			oid = tempoid;
		})
	})
	
	function show_reason(id, seller) {
		$.ajax({
			url : "../SidServlet?command=get_cancel_reason",
			dataType : "json",
			data : "purchaseId=" + id,

			success : function(data) {
				var Ca = /\+/g;
				$("#pid").text(decodeURIComponent(data.purchaseId));
				$("#reason").text(decodeURIComponent((data.reason).replace(Ca, " ")));
				$("#cancel_date").text(decodeURIComponent(data.date));

			}
		});
		$("#purchase_id").val(id);
		$("#seller_email").val(seller);
		$("#showReasonModal").modal();
	}



	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({
			html : true,
			container : 'body'
		});
	});
</script>
<%@ include file="../include/footer.jsp"%>
</html>