<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../include/header.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid">
		<ul class="nav nav-pills nav-justified">

			<li role="presentation"><a
				href="../SidServlet?command=list_all_basket">장바구니</a></li>
			<li role="presentation"><a href="../SidServlet?command=purchase_list_buyer">거래내역
			</a></li>
			<li class="active" role="presentation"><a
				href="../mypage/myinfo.jsp">아이디관리</a></li>
		</ul>
	</div>
	<br>
	<div id="checkDiv">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="text-center">
						<h1>
							<i class="fa fa-lock"></i>
						</h1>
						<h2 class="text-center">기업 회원 정보 수정</h2>
						<p>비밀번호 확인</p>
						<div class="panel-body">
							<div class="alert alert-danger" role="alert" id="pwdCheck">
								<span class="glyphicon glyphicon-exclamation-sign"
									aria-hidden="true"></span> <span class="sr-only">Error:</span>
								비밀번호가 다릅니다.
							</div>
							<div class="form-group">
								<input id="pwd" name="password" placeholder="password"
									class="form-control" type="password">
							</div>
							<div class="form-group">
								<input name="recover-submit" id="checkPassword"
									class="btn btn-lg btn-primary btn-block" value="확인"
									type="button">
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="myinfo">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="text-center">
						<h1>
							<i class="fa fa-edit"></i>
						</h1>
						<h2 class="text-center">기업 회원 정보 수정</h2>
						<hr>
					</div>
					<div>
						<form method="post" name="frm" id="standardinfo">
							<div class="form-group col-md-12">
								<input type="hidden" value="business" name="state">
								<div class="col-md-4" style="padding-left: 0; margin-left: 0">
									<h4>
										<label class="control-label">사업자 등록번호</label>
									</h4>
								</div>
								<div class="col-md-8">
									<input type="text" class="form-control" name="rep_num"
										placeholder="사업자 등록번호" value="${list2.rep_num }" readonly>
								</div>
							</div>
							<div class="form-group  col-md-12">
								<div class="col-md-4" style="padding-left: 0; margin-left: 0">
									<h4>
										<label class="control-label">대표명</label>
									</h4>
								</div>
								<div class="col-md-8">
									<input type="text" class="form-control" name="rep_name"
										placeholder="대표명" value="${list2.rep_name }" readonly>
								</div>
							</div>
							<div class="form-group col-md-12">
								<div class="col-md-4" style="padding-left: 0; margin-left: 0">
									<h4>
										<label class="control-label">상호명</label>
									</h4>
								</div>
								<div class="col-md-8">
									<input type="text" class="form-control" name="company"
										placeholder="상호명" value="${list2.fullCompany }" readonly>
								</div>
							</div>
							<div class="form-group col-md-12">
								<div class="col-md-4" style="padding-left: 0; margin-left: 0">
									<h4>
										<label class="control-label">대표 연락처</label>
									</h4>
								</div>
								<div class="col-md-8">
									<input type="number" class="form-control" name="rep_phone"
										placeholder="대표연락처-숫자만" value="${list2.rep_phone }">
								</div>
							</div>
							<div class="form-group col-md-12">
								<hr>
								<div class="col-md-4" style="padding-left: 0; margin-left: 0">
									<h4>
										<label class="control-label">E-Mail</label>
									</h4>
								</div>
								<div class="col-md-8">
									<input type="text" class="form-control" name="email"
										placeholder="E-mail 입력" value="${list.email}" readonly>
									<input type="hidden" name="reemail">
								</div>
							</div>
							<div class="form-group col-md-12">
								<div class="col-md-4" style="padding-left: 0; margin-left: 0">
									<h4>
										<label class="control-label">이름</label>
									</h4>
								</div>
								<div class="col-md-8">
									<input type="text" class="form-control" name="name"
										placeholder="이름" value="${list.name }">
								</div>
							</div>
							<div class="form-group col-md-12">
								<div class="col-md-4" style="padding-left: 0; margin-left: 0">
									<h4>
										<label class="control-label">연락처</label>
									</h4>
								</div>
								<div class="col-md-8">
									<input type="number" class="form-control" name="phone"
										placeholder="연락처-숫자만" value="${list.phone }">
								</div>
							</div>

							<div class="form-group">
								<button type="button" class="btn btn-primary col-md-12"
									onclick="return updateCheck();">저장</button>
								<br>
								<br>
							</div>
						</form>

					</div>
					<hr>
					<div class="col-md-12" style="margin: 0; padding: 0">
						<form method="post" name="frm2" id="passwordinfo">
							<input type="hidden" value="pwd" name="state"> <input
								type="hidden" value="${list.email}" name="email">
							<div class="form-group ">
								<hr>
								<h2 class="text-center">비밀번호 변경</h2>
								<hr>
								<div class="col-md-6">
									<input type="password" class="form-control" name="pwd"
										id="changePwd" placeholder="비밀번호">
								</div>
								<div class="col-md-6">
									<input type="password" class="form-control" name="pwd_check"
										placeholder="비밀번호 확인"> <br> <br>
								</div>
							</div>
							<div class="col-md-12">
								<div id="pwd_ex">
									<div class="alert alert-danger" role="alert">
										<span class="glyphicon glyphicon-exclamation-sign"
											aria-hidden="true"></span> <span class="sr-only">Error:</span>
										&nbsp;비밀번호는 문자, 숫자, 특수문자의 조합으로 8~16자리로 입력해주세요.
									</div>
								</div>
								<div id="pwd_ex2">
									<div class="alert alert-success" role="alert">
										<span class="glyphicon glyphicon-exclamation-sign"
											aria-hidden="true"></span> <span class="sr-only">Error:</span>
										&nbsp;비밀번호 입력 완료
									</div>
								</div>
							</div>
							<div class="form-group">
								<button type="button" class="btn btn-primary col-md-12"
									onclick="return passwordCheck();">변경</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>


</body>
<script>
	$(function() {
		$("#pwdCheck").hide();
		$("#myinfo").hide();
		//이메일 보내기 버튼 클릭
		$("#checkPassword").click(function(e) {

			var pwd = $("#pwd").val();

			//이메일로 임시 비밀번호 보내기
			$.ajax({
				url : '../SidServlet?command=password_check',
				data : 'pwd=' + encodeURIComponent(pwd),
				success : function(data) {
					if (data == "1") {
						$("#myinfo").show();
						$("#checkDiv").hide();
					} else {
						$("#pwdCheck").show();
					}
				}
			});


		})

	});
	//주소찾기
	function goPopup() {
		// í¸ì¶ë íì´ì§(jusopopup.jsp)ìì ì¤ì  ì£¼ìê²ìURL(http://www.juso.go.kr/addrlink/addrLinkUrl.do)ë¥¼ í¸ì¶íê² ë©ëë¤.
		var pop = window.open("../address/jusoPopup.jsp", "pop", "width=570,height=420, scrollbars=yes, resizable=yes");
	}
	function jusoCallBack(roadAddrPart1, addrDetail, zipNo) {
		// íìíì´ì§ìì ì£¼ììë ¥í ì ë³´ë¥¼ ë°ìì, í íì´ì§ì ì ë³´ë¥¼ ë±ë¡í©ëë¤.
		document.frm.roadAddrPart1.value = roadAddrPart1;
		document.frm.addrDetail.value = addrDetail;
		document.frm.zipNum.value = zipNo;
	}

	function updateCheck() {
		if ($("#admin_check").val() != 4) {
			if (document.frm.rep_phone.value == "") {
				alert("대표 연락처를 입력하세요");
				frm.rep_phone.focus();
				return false;
			}
		}
		if (document.frm.name.value.length == 0) {
			alert("이름을 입력하세요.");
			frm.firstname.focus();
			return false;
		}
		if (document.frm.phone.value.length == 0) {
			alert("연락처를 입력하세요.");
			frm.phone.focus();
			return false;
		}
		if (!document.frm.phone.value.match("^[0-9]+$")) {
			alert("연락처를 제대로 입력하세요.")
			frm.phone.focus();
			return false;
		}

		//정보 수정 ajax
		$.ajax({
			url : "../SidServlet?command=update_info",
			data : $("#standardinfo").serialize(),
			success : function(data) {
				if (data == "1") {
					alert("수정 완료");
					location.reload();
					//$("#resultMessage").val("fail");
					return true;
				} else {
					alert("수정 실패");
					location.reload();
					//$("#authNumConfirm").val(data);
					return true;
				}
			}
		});
		return true;
	}
	function passwordCheck() {
		if (document.frm2.pwd.value == "") {
			alert("비밀번호는 반드시 입력해야 합니다.");
			frm2.pwd.focus();
			return false;
		}
		if (!document.frm2.pwd.value.match("^.*(?=^.{8,16}$)(?=.*\\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&+=]).*$")) {
			alert("비밀번호는 문자, 숫자, 특수문자의 조합으로 8~16자리로 입력해주세요.")
			frm2.pwd.focus();
			return false;
		}
		if (document.frm2.pwd.value != document.frm2.pwd_check.value) {
			alert("비밀번호가 일치하지 않습니다.");
			frm2.pwd.focus();
			return false;
		}

		//가입 ajax
		$.ajax({
			url : "../SidServlet?command=update_info",
			data : $("#passwordinfo").serialize(),
			success : function(data) {
				if (data == "1") {
					alert("수정 완료");
					location.reload();
					//$("#resultMessage").val("fail");
					return true;
				} else {
					alert("수정 실패");
					location.reload();
					//$("#authNumConfirm").val(data);
					return true;
				}
			}
		});
		return true;
	}
	$("#pwd_ex").hide();
	$("#pwd_ex2").hide();
	$("#changePwd").keyup(function() {
		var check = /^(?=.*[a-zA-Z])(?=.*[!@#$%^&*+=-])(?=.*[0-9]).{8,16}$/;
		if (!check.test(this.value)) {
			$("#pwd_ex").show();
			$("#pwd_ex2").hide();
		} else {
			$("#pwd_ex").hide();
			$("#pwd_ex2").show();
		}
	}
	);
</script>
<%@ include file="../include/footer.jsp"%>
</html>
