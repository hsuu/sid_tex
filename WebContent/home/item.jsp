<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ include file="../include/header.jsp"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/color.css">
<style>
#caution {
	font-size: 9pt;
	color : grey;
}

.left {
	font-weight: bold;
}

h4 {
	color: #424242;
}

.panel-body {
	border: 2px solid #A4A4A4;
}

.panel {
	border: none;
}

.col-md-4 {
	padding-top: 1.5%;
}

h2 {
	margin-top: 0;
}

.colorDiv {
	padding: 5px
}

#seller_table th, td {
	text-align: center;
}

table, td, th {
	border: 1px solid #ddd;
	text-align: left;
}

table {
	border-collapse: collapse;
	width: 100%;
}

th, td {
	padding: 15px;
}

.title {
	background-color: #585858;
	color: white;
	text-align: center;
}

.table td {
	text-align: center;
}
</style>
</head>
<body>

	<div class="col-md-12">
		<div class="col-md-6">
			<div class="form-group" style="text-align: center">
				<img src=${product.mainImg } style="max-width: 80%; width: 100%;">
			</div>
		</div>
		<div class="col-md-6" style="padding: 15px;">
			<div class="form-group row ">
				<label class="col-sm-4 control-label">상품명</label>
				<div class="col-sm-8">
					<p>${product.itemName }</p>
				</div>
				<label class="col-sm-4 control-label">색상명</label>
				<div class="col-sm-8">
					<p>${product.colorName }</p>
				</div>
				<label class="col-sm-4 control-label">판매자명</label>
				<div class="col-sm-8">
					<p>${product.company }</p>
				</div>
				<%-- <label class="col-sm-4 control-label">배송방법</label>
				<div class="col-sm-8">
					<p>
						<c:choose>
							<c:when test="${product.express ==1 }">
                           퀵
                           </c:when>
							<c:when test="${product.express ==10 }">
                           택배
                           </c:when>
							<c:when test="${product.express ==11 }">
                           퀵, 택배
                           </c:when>
						</c:choose>
					</p>
				</div> --%>
				<label class="col-sm-4 control-label">폭</label>
				<div class="col-sm-8">
					<p>${product.width }cm</p>
				</div>
				<label class="col-sm-4 control-label">두께</label>
				<div class="col-sm-8">
					<p>${product.thick }</p>
				</div>
				<label class="col-sm-4 control-label">스와치</label>
				<div class="col-sm-8">
					<p>${product.swatch }</p>
				</div>
				<label class="col-sm-4 control-label">소매가</label>
				<div class="col-sm-8">
					<p id="retail">${product.retail }원</p>
				</div>
				<label class="col-sm-4 control-label">도매가</label>
				<div class="col-sm-8">
					<p id="wholesale">${product.wholesale }원&nbsp;(${product.standard }↑)</p>
				</div>
				<form method="post" id="basket">
					<div class="col-md-12" style="margin: 0; padding: 0;">
						<!-- 색상 옵션이 있으면 드롭다운 생성 -->
						<c:choose>
							<c:when test="${relateCount > 1 }">
								<label class="col-sm-4 control-label">색상선택</label>
								<div class="dropdown col-sm-8">
									<button class="btn btn-primary dropdown-toggle" type="button"
										data-toggle="dropdown">
										색상 옵션 <span class="caret"></span>
									</button>
									<ul class="dropdown-menu">
										<c:forEach items="${relate}" var="relate">
										<c:if test="${relate.state eq '판매중'}">
											<li><a href="#" onclick="addColor(${relate.productId})">
													${relate.colorName}&nbsp;&nbsp;
													
														(재고 : 
														<c:choose>
															<c:when test="${relate.stock eq -1}">∞</c:when>
															<c:otherwise>${relate.stock }</c:otherwise>
														</c:choose>)
													
													
											</a></li>
										</c:if>
										
										<c:if test="${relate.state eq '판매중지'}">
												<li><a href="#" onclick="alert('판매중지 상품입니다.')">
													${relate.colorName}&nbsp;&nbsp;[판매중지]
													
											</a></li>
										</c:if>
										</c:forEach>
									</ul>
								</div>
							</c:when>
							<%-- 색상 옵션 없으면--%>
							<c:otherwise>
								<div class="col-md-12" style="margin: 0; padding: 0">
									<label class="col-md-4" style="font: 12px; color: grey;">
										${product.colorName} (재고 : <c:choose>
											<c:when test="${product.stock eq -1}">
											∞
										</c:when>
											<c:otherwise>
											${product.stock}
										</c:otherwise>
										</c:choose> )
									</label>
									<div class="col-md-4">
										<input type="hidden" name="pid1" value="${product.productID}">
										<input type="hidden" class="colorStock"
											value="${product.stock}"> <input type="number"
											style="width: 100px; height: 35px; text-align: center;"
											class="colorQuantity" name="quantity1" min='0' step='1'
											value='1'>
									</div>
								</div>
							</c:otherwise>
						</c:choose>
					</div>

					<!-- 색상 옵션 카운트-->
					<input type="hidden" id="color_count" name="color_count"
						value="${relateCount }">

					<!-- 드롭다운으로 생성된 색상 옵션 -->
					<div class="col-md-12" id="colorDiv"
						style="margin-top: 10px; padding: 0">
						<c:forEach items="${relate}" var="relate" varStatus="status">
							<div class="col-md-12 coldiv" id="div${relate.productId}"
								style="margin: 0; padding: 0">
								<div class="col-md-4">
									<label style="font: 12px; color: grey;">
										${relate.colorName} </label>
								</div>
								<div class="col-md-4" style="margin: 0; padding: 0">
									<div class="col-md-8">
										<input type="hidden" name="pid${status.count}"
											value="${relate.productId}"> <input type="number"
											class="colorQuantity"
											style="width: 100px; height: 35px; text-align: center;"
											name="quantity${status.count}" min='0' step='1' value='0'>
										<input type="hidden" class="colorStock"
											value="${relate.stock}">
									</div>
									<div class="col-md-2" style="padding-top: 0">
										<button type="button" class="btn btn-danger"
											onclick="hideColorDiv(${relate.productId})">X</button>
									</div>
								</div>
							</div>
						</c:forEach>
					</div>
				</form>

				<div class="col-sm-4">
					<br> <label class="control-label">가격</label>
				</div>
				<div class="col-sm-8">
					<br>
					<p id="totalCost">0원</p>
				</div>

				<hr>
			</div>
			<!-- 장바구니로 -->
			<div class="form-group">
				<div class="btn-group col-md-12">
					<div class="col-md-4"></div>
					<c:choose>
						<c:when
							test="${product.retail_only eq 1 && sessionScope.admin eq 4}">
							<div class="col-md-8">사업자고객에만 판매중인 상품입니다.</div>
						</c:when>
						<c:otherwise>
							<div class="col-md-8" style="margin: 0; padding: 0">
								<button type="button" class="btn btn-primary"
									onclick="toBasket(${product.productID})" id="toBasket">
									<span class="glyphicon glyphicon-shopping-cart"
										aria-hidden="true">&nbsp;장바구니</span>
								</button>
							</div>
							<!-- <button type="button" class="btn btn-primary dropdown-toggle"
                           data-toggle="dropdown">
                           <span class="caret"></span> <span class="sr-only">Toggle
                              Dropdown</span>
                        </button>

                        <ul class="dropdown-menu">
                           <li class="active"><a href="#">장바구니1에 담기</a></li>
                           <li><a href="#">장바구니2에 담기</a></li>
                           <li><a href="#">장바구니3에 담기</a></li>
                        </ul> -->
						</c:otherwise>
					</c:choose>
				</div>
			</div>

			<hr>
		</div>
	</div>
	<div class="col-md-12">
		<div id="section1" class="container-fluid">
			<ul class="nav nav-pills nav-justified">
				<li class="active"><a href="#" onclick="toSection('1')">색상
						옵션</a></li>
				<li><a href="#" onclick="toSection('2')">상품 정보</a></li>
				<li><a href="#" onclick="toSection('3')">상품 이미지</a></li>
				<li><a href="#" onclick="toSection('4')">배송 정보</a></li>
				<li><a href="#" onclick="toSection('5')">판매자 정보</a></li>
			</ul>
			<div class="col-md-12">
				<c:forEach items="${relate}" var="relate">
					<!-- item -->
					<div class="col-md-2 col-xs-6"
						style="line-height: 0.7em; padding: 0.5px;">
						<div class="col-md-12">
							<hr>

							<!-- 이미지 -->
							<div style="height: 240px;">
								<a
									href="../SidServlet?command=read_product&productId=${relate.productId}">
									<img src="${relate.mainImg}"
									style="max-width: 100%; width: 100%; max-height: 240px;">
								</a>
							</div>
							<!--색상명-->
							<div class="text-center">
								<p>${relate.colorName}</p>
							</div>
							<div>
								<br>
								<p class="text-center" style="font-size: 90%">
									<c:set var="color" value="${relate.color}" />
									<c:if test='${fn:contains(color, "RS00")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 0, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>

									<c:if test='${fn:contains(color, "YRS0")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 192, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "YS00")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 255, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "GYS0")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(192, 255, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "GS00")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 255, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "BGS0")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 255, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "BS00")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 0, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "PBS0")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(192, 0, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "PS00")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 0, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "RPS0")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 0, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "WGBS")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 128, 128); border: 1px solid black">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>

									<c:if test='${fn:contains(color, "RL00")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 192, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "YRL0")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 224, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "YL00")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 255, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "GYL0")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(224, 255, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "GL00")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(192, 255, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "BGL0")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(192, 255, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "BL00")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(192, 192, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "PBL0")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(224, 192, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "PL00")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 192, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "RPL0")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 192, 224)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "WGBL")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(255, 255, 255); border: 0.5px solid grey">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>

									<c:if test='${fn:contains(color, "RD00")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 0, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "YRD0")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 64, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "YD00")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 128, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "GYD0")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(64, 128, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "GD00")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 128, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "BGD0")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 64, 64)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "BD00")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 0, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "PBD0")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(64, 0, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "PD00")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 0, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "RPD0")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(128, 0, 64)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>
									<c:if test='${fn:contains(color, "WGBD")}'>
										<a href="#" class="colorBox"
											style="background-color: rgb(0, 0, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</c:if>

								</p>
							</div>

						</div>
					</div>
				</c:forEach>
			</div>
			<!-- /item -->
		</div>
		<hr>

		<div id="section2" class="container-fluid">
			<ul class="nav nav-pills nav-justified">
				<li><a href="#" onclick="toSection('1')">색상 옵션</a></li>
				<li class="active"><a href="#" onclick="toSection('2')">상품
						정보</a></li>
				<li><a href="#" onclick="toSection('3')">상품 이미지</a></li>
				<li><a href="#" onclick="toSection('4')">배송 정보</a></li>
				<li><a href="#" onclick="toSection('5')">판매자 정보</a></li>
			</ul>
			<br> <br>
			<div class="panel panel-default">
				<div class="panel-body">
					<h2>기본</h2>
					<div class="col-md-6 ">
						<h4 class="col-md-3">
							<label class="control-label">상품명</label>
						</h4>
						<div class="col-md-4">${product.itemName }</div>
					</div>
					<div class="col-md-6 ">
						<h4 class="col-md-3">
							<label class="control-label">스와치</label>
						</h4>
						<div class="col-md-4">
							<p>${product.swatch }</p>
						</div>
					</div>
					<div class="col-md-6 ">
						<h4 class="col-md-3">
							<label class="control-label">두께</label>
						</h4>
						<div class="col-md-4">${product.thick }</div>
					</div>
					<div class="col-md-6 ">
						<h4 class="col-md-3">
							<label class="control-label">소매가</label>
						</h4>
						<div class="col-md-4">${product.retail}원</div>
					</div>
					<div class="col-md-6 ">
						<h4 class="col-md-3">
							<label class="control-label">폭</label>
						</h4>
						<div class="col-md-4">${product.width}cm</div>
					</div>
					<div class="col-md-6 ">
						<h4 class="col-md-3">
							<label class="control-label">도매가</label>
						</h4>
						<div class="col-md-4">${product.wholesale}원</div>
					</div>
					<div class="col-md-6 ">
						<h4 class="col-md-3">
							<label class="control-label">색상명</label>
						</h4>
						<%-- 						<c:forEach items="${relate}" var="relate" varStatus="status">
						<div class="col-md-4">${relate.colorName }</div>
						</c:forEach> --%>
						<div class="col-md-4">${product.colorName }</div>
					</div>
					<%-- <div class="col-md-6">
						<h4 class="col-md-3">
							<label class="control-label">배송</label>
						</h4>
						<div class="col-md-4">
							<c:choose>
								<c:when test="${product.express ==1 }">
                           퀵
                           </c:when>
								<c:when test="${product.express ==10 }">
                           택배
                           </c:when>
								<c:when test="${product.express ==11 }">
                           퀵, 택배
                           </c:when>
							</c:choose>
						</div>
					</div> --%>
					<div class="col-md-6 ">
						<h4 class="col-md-3">
							<label class="control-label">색상</label>
						</h4>
						<div class="col-md-9" style="padding-top: 1.5%">
							<%
								String arr2 = ((String) request.getAttribute("color"));
								arr2 = arr2.substring(1, arr2.length() - 1);

								if (arr2.contains("RS00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(255, 0, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>

							<%
								}
								if (arr2.contains("YRS0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(255, 192, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>

							<%
								}
								if (arr2.contains("YS00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(255, 255, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("GYS0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(192, 255, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>

							<%
								}
								if (arr2.contains("GS00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(0, 255, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>

							<%
								}
								if (arr2.contains("BGS0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(0, 255, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("BS00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(0, 0, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("PBS0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(192, 0, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("PS00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(255, 0, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("RPS0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(255, 0, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("WGBS")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(128, 128, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("RL00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(255, 192, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("YRL0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(255, 224, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("YL00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(255, 255, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("GYL0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(224, 255, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("GL00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(192, 255, 192)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("BGL0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(192, 255, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("BL00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(192, 192, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("PBL0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(224, 192, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("PL00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(255, 192, 255)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("RPL0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(255, 192, 224)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("WGBL")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(255, 255, 255); border: 0.5px solid grey">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("RD00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(128, 0, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("YRD0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(128, 64, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("YD00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(128, 128, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("GYD0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(64, 128, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("GD00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(0, 128, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("BGD0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(0, 64, 64)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("BD00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(0, 0, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("PBD0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(64, 0, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("PD00")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(128, 0, 128)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("RPD0")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(128, 0, 64)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
								if (arr2.contains("WGBD")) {
							%>
							<a href="#" class="colorBox"
								style="background-color: rgb(0, 0, 0)">&nbsp;&nbsp;&nbsp;&nbsp;</a>
							<%
								}
							%>

						</div>
					</div>

				</div>
				<div class="panel panel-default" style="padding-top: 3%">
					<div class="panel-body">
						<h2>핵심</h2>
						<div class="col-md-6 ">
							<h4 class="col-md-3">
								<label class="control-label">원산지</label>
							</h4>
							<div class="col-md-4">${product.origin }</div>
						</div>
						<div class="col-md-6 ">
							<h4 class="col-md-3">
								<label class="control-label">용도</label>
							</h4>
							<div class="col-md-9" style="padding-top: 2%">
								<%
									String arr = ((String) request.getAttribute("uses"));
									arr = arr.substring(1, arr.length() - 1);

									out.println(arr);
								%>
							</div>
						</div>
						<div class="col-md-6 ">
							<h4 class="col-md-3">
								<label class="control-label">제조사</label>
							</h4>
							<div class="col-md-4">${product.manufacturer }</div>
						</div>
						<div class="col-md-6 ">
							<h4 class="col-md-3">
								<label class="control-label">패턴</label>
							</h4>
							<div class="col-md-4">${product.pattern }</div>
						</div>
						<div class="col-md-6 ">
							<h4 class="col-md-3">
								<label class="control-label">제조년월</label>
							</h4>
							<div class="col-md-4">${product.manuDate }</div>
						</div>
						<div class="col-md-6 ">
							<h4 class="col-md-3">
								<label class="control-label">명칭</label>
							</h4>
							<div class="col-md-4">${product.sortName }</div>
						</div>
						<div class="col-md-6 ">
							<h4 class="col-md-3">
								<label class="control-label">섬유</label>
							</h4>
							<div class="col-md-9" style="padding-top: 1.5%">
								<%
									String arr3 = ((String) request.getAttribute("fiber"));
									arr3 = arr3.substring(1, arr3.length() - 1);

									out.println(arr3);
								%>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default col-md-6" style="padding-left: 0">
					<div class="panel-body">
						<h2>피륙</h2>
						<div class="col-md-12">
							<h4 class="col-md-3">
								<label class="control-label">중량</label>
							</h4>
							<div class="col-md-4">
								<c:choose>
									<c:when test="${product.weight == 0 }">
									</c:when>
									<c:when test="${product.weight!= 0}">
                              ${product.weight }g/㎡
                           </c:when>
								</c:choose>
							</div>
						</div>
						<div class="col-md-12">
							<h4 class="col-md-3">
								<label class="control-label">조직</label>
							</h4>
							<div class="col-md-4">${product.weave }</div>
						</div>
						<div class="col-md-12">
							<h4 class="col-md-3">
								<label class="control-label">밀도</label>
							</h4>

							<div class="col-md-4">
								<c:choose>
									<c:when
										test="${product.density_volume == 0 && product.density_weight==0}">
									</c:when>
									<c:otherwise>
                           ${product.density_volume }   *   ${product.density_weight }
                           </c:otherwise>
								</c:choose>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default col-md-6" style="padding-right: 0">
					<div class="panel-body">
						<h2>원단</h2>
						<div class="col-md-12">
							<h4 class="col-md-3">
								<label class="control-label">굵기</label>
							</h4>
							<div class="col-md-6" style="padding-top: 2%">
								<c:choose>
									<c:when test="${product.thickWp == 0 || product.thickWt==0}">
									</c:when>
									<c:otherwise>
                           ${product.thickWp }   -   ${product.thickWt }      &nbsp;단위 : ${product.thickUnit }
                           </c:otherwise>
								</c:choose>
							</div>
						</div>
						<div class="col-md-12">
							<h4 class="col-md-3">
								<label class="control-label">길이</label>
							</h4>
							<div class="col-md-6" style="padding-top: 2%">
								<c:choose>
									<c:when
										test="${product.lengthWp == '0' || product.lengthWt=='0'}">
									</c:when>
									<c:otherwise>
                           ${product.lengthWp }   -   ${product.lengthWt }
                           </c:otherwise>
								</c:choose>
							</div>
						</div>
						<div class="col-md-12">
							<h4 class="col-md-3">
								<label class="control-label">꼬임</label>
							</h4>
							<div class="col-md-6" style="padding-top: 2%">
								<c:choose>
									<c:when
										test="${product.twistWp == '0' || product.twistWt=='0'}">
									</c:when>
									<c:otherwise>
                           ${product.twistWp }   -   ${product.twistWt }
                           </c:otherwise>
								</c:choose>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default col-md-12"
					style="padding-right: 0; padding-left: 0">
					<div class="panel-body">
						<h2>가공</h2>
						<div class="col-md-6">
							<h4 class="col-md-3">
								<label class="control-label">가공</label>
							</h4>
							<div class="col-md-4">${product.gagong }</div>
						</div>
						<div class="col-md-6">
							<h4 class="col-md-3">
								<label class="control-label">염색</label>
							</h4>
							<div class="col-md-4">
								<c:choose>
									<c:when test="${product.dye == '0'}">
									</c:when>
									<c:otherwise>
                           ${product.dye }
                           </c:otherwise>
								</c:choose>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default col-md-12"
					style="padding-right: 0; padding-left: 0">
					<div class="panel-body">
						<h2>심미</h2>
						<div class="col-md-6">
							<h4 class="col-md-3">
								<label class="control-label">질감</label>
							</h4>
							<div class="col-md-4">${product.texture }</div>
						</div>
						<div class="col-md-6">
							<h4 class="col-md-3">
								<label class="control-label">비침</label>
							</h4>
							<div class="col-md-4">${product.through }</div>
						</div>
						<div class="col-md-6">
							<h4 class="col-md-3">
								<label class="control-label">광택</label>
							</h4>
							<div class="col-md-4">${product.gloss }</div>
						</div>
						<div class="col-md-6">
							<h4 class="col-md-3">
								<label class="control-label">신축</label>
							</h4>
							<div class="col-md-4">${product.elastic }</div>
						</div>
						<div class="col-md-6">
							<h4 class="col-md-3">
								<label class="control-label">유연</label>
							</h4>
							<div class="col-md-4">${product.pliability}</div>
						</div>
					</div>
				</div>
			</div>

			<hr>
			<div id="section3" class="container-fluid text-center">
				<ul class="nav nav-pills nav-justified">
					<li><a href="#" onclick="toSection('1')">색상 옵션</a></li>
					<li><a href="#" onclick="toSection('2')">상품 정보</a></li>
					<li class="active"><a href="#" onclick="toSection('3')">상품
							이미지</a></li>
					<li><a href="#" onclick="toSection('4')">배송 정보</a></li>
					<li><a href="#" onclick="toSection('5')">판매자 정보</a></li>
				</ul>
				<br> <br>
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-md-12">${product.expl }</div>
						<%-- <div class="col-md-12">
							<br> <img src=${product.mainImg }
								style="max-width: 80%; width: 100%;"> <br>
							<c:if test="${product.subImg != '../img/null' }">
								<img src=${product.subImg } style="max-width: 80%; width: 100%;">
							</c:if>
							<br>
							<c:if test="${product.subImg2 != '../img/null' }">
								<img src=${product.subImg2 }
									style="max-width: 80%; width: 100%;">
							</c:if>
							<br>
							<c:if test="${product.subImg3 != '../img/null' }">
								<img src=${product.subImg3 }
									style="max-width: 80%; width: 100%;">
							</c:if>
							<br>
							<c:if test="${product.subImg4 != '../img/null' }">
								<img src=${product.subImg4 }
									style="max-width: 80%; width: 100%;">
							</c:if>
							<br>
							<c:if test="${product.subImg5 != '../img/null' }">
								<img src=${product.subImg5 }
									style="max-width: 80%; width: 100%;">
							</c:if>
						</div> --%>
					</div>
				</div>

				<hr>
				<div id="section4" class="container-fluid">
					<ul class="nav nav-pills nav-justified">
						<li><a href="#" onclick="toSection('1')">색상 옵션</a></li>
						<li><a href="#" onclick="toSection('2')">상품 정보</a></li>
						<li><a href="#" onclick="toSection('3')">상품 이미지</a></li>
						<li class="active"><a href="#" onclick="toSection('4')">배송
								정보</a></li>
						<li><a href="#" onclick="toSection('5')">판매자 정보</a></li>
					</ul>
					<br> <br>
					<div style="text-align: left">
						<div class="panel panel-default">
							<div class="panel-body" style="padding: 30px; padding-top: 10px">
								<h3>일반 배송 안내</h3>
								<div>
									배송 가능 방법 :
									<c:choose>
										<c:when test="${list.delivery ==1 }">
											퀵
										</c:when>
										<c:when test="${list.delivery ==10 }">
											택배
										</c:when>
										<c:when test="${list.delivery ==11 }">
											퀵, 택배
										</c:when>
									</c:choose>
								</div>
								<div>예상되는 배송 기간 : 주문 이후 ${list.delivery_period }일 이내 발송</div>
								<div>거래 관련 처리 안내 : 본 사이트에서 진행되는 거래의 책임과 배송, 교환, 환불, 민원은 직접
									관리하고 있음</div>
								<div>소비자와 사업자 사이의 분쟁처리 : 소비자분쟁해결기준(공정거래위원회 고시) 및 관계법령에 따름</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-body col-md-6"
								style="margin: 0; padding: 30px; padding-top: 10px">
								<h3>퀵</h3>
								<div>
									배송사 : ${list.deliveryCompany_quick } <br> 배송료 : 착불(거리에 따라 상이)<br> 교환배송비 : 착불(환불금액에서
									차감)<br> 반품배송비 : 착불(환불금액에서 차감)
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-body col-md-6"
								style="padding: 30px; padding-top: 10px">
								<h3>택배</h3>
								<div>
									배송사 :
									<c:choose>
										<c:when test="${list.deliveryCompany ne null }">
									${list.deliveryCompany }
									</c:when>
									</c:choose>
									<br> 배송료 :
									<c:choose>
										<c:when test="${list.delivery_costway =='무료' }">
											무료
									</c:when>
										<c:when test="${list.delivery_costway=='유료' }">
											유료(${list.delivery_cost}원)
									</c:when>
										<c:when test="${list.delivery_costway=='조건부무료' }">
											${list.delivery_cost2 }원 / (${list.delivery_cost }원 이상 구매시 무료)
									</c:when>
									</c:choose>
									<br> 교환배송비 : 왕복(${list.re_delivery_cost1*2 }원)<br>
									반품배송비 : 편도(${list.re_delivery_cost1 }원) / 최초 배송비 무료인 경우
									(${list.re_delivery_cost1*2 })원
								</div>
							</div>

						</div>
						<br> <br> <br> <br> <br> <br> <br>
						<br> <br> <br>
						<div class="panel panel-default">
							<div class="panel-body col-md-12"
								style="padding: 30px; padding-top: 10px">
								<h3>교환/반품 안내</h3>
								<div>
									보내실 곳 : (${list.returnZipnum }) ${list.returnRoadAddrPart1 } ${list.returnAddrDetail }  <br> (구매자 귀책사유로 인한 교환 및 반품 시에는 구매자가, 판매자
									귀책사유로 인한 교환 및 반품 시에는 판매자가 교환 및 반품 배송비용을 부담합니다.)<br> <br>
									- 원단은 롤에서 고객님의 주문수량만큼 컷팅되어서 배송이 되므로 고객님의 단순변심에 의한 교환 및 반품은
									불가합니다.<br> - 상품의 이미지는 모니터 사양에 따라 약간의 색상차이가 있을 수 있으며, 원단
									재가공시 약간의 색상차이가 나는 부분은 원단 가공시 나타나는 현상이므로 교환 및 반품이 불가능합니다.<br>
									- 세탁, 재단, 미싱 등으로 원단이 훼손되거나 변형된 경우는 교환 및 반품이 불가능합니다.<br> -
									상품 주문후 주문상태가 상품준비중일 경우에는 교환 및 반품이 불가능합니다.
								</div>

								<h3>교환/반품 기준</h3>
								<div>
									상품 수령 후 7일 이내에 신청하실 수 있습니다. 단, 제품이 표시광고 내용과 다르거나 불량 등 계약과 다르게
									이행된 경우는<br> 제품 수령일부터 3개월 이내, 그 사실을 안 날 또는 알 수 있었던 날로부터
									30일 이내에 교환/반품이 가능합니다.<br> <br> 단, 다음의 경우 해당하는 반품/교환은
									불가능할 수 있습니다.<br> - 소비자의 책임 있는 사유로 상품 등이 멸실 또는 훼손된 경우<br> -
									소비자의 사용 또는 소비에 의해 상품 등의 가치가 현저히 감소한 경우<br> - 시간의 경과에 의해
									재판매가 곤란할 정도로 상품 등의 가치가 현저히 감소한 경우<br> - 복제가 가능한 상품 등의 포장을
									훼손한 경우<br> - 소비자의 주문에 따라 개별적으로 생산되는 상품이 제작에 들어간 경우
								</div>
							</div>
						</div>
						<br> <br> <br> <br> <br> <br> <br> <br> <br>
						<br> <br> <br> <br> <br> <br> <br> <br> <br>
						<br> <br> <br> <br>
						<div style="padding: 30px; padding-top: 10px">
							<h3>주의사항</h3>
							<div id="caution">
								전자상거래 등에서의 소비자보호에 관한 법률에 의한 반품규정이 판매자가 지정한 반품 조건보다 우선합니다.<br>
								전자상거래 등에서의 소비자 보호에 관한 법률에 의거하여 미성년자가 물품을 구매하는 경우, 법정대리인이 동의하지
								않으면 미성년자 본인 또는 법정대리인이 구매를 취소할 수 있습니다.<br> <!-- 시드텍스에 등록된 판매상품과
								상품의 내용은 판매자가 등록한 것으로 (주)시드월드엔터프라이즈에서는 그 등록내역에 대하여 일체의 책임을 지지
								않습니다.<br> --> 시드텍스의 결제시스템을 이용하지 않고 판매자와 직접 거래하실 경우 상품을 받지 못하거나
								구매한 상품과 상이한 상품을 받는 등 피해가 발생할 수 있으니 유의하시기 바랍니다.
							</div>
						</div>
					</div>

				</div>
				<hr>
				<div id="section5" class="container-fluid">
					<ul class="nav nav-pills nav-justified">
						<li><a href="#" onclick="toSection('1')">색상 옵션</a></li>
						<li><a href="#" onclick="toSection('2')">상품 정보</a></li>
						<li><a href="#" onclick="toSection('3')">상품 이미지</a></li>
						<li><a href="#" onclick="toSection('4')">배송 정보</a></li>
						<li class="active"><a href="#" onclick="toSection('5')">판매자
								정보</a></li>
					</ul>
					<br> <br>
					<table class="table" id="seller_table">
						<tr>
							<td class="title">판매자</td>
							<td>${rep.company}</td>
							<td class="title">사업자등록번호</td>
							<td>${rep.rep_num }</td>
						</tr>
						<tr>
							<td class="title">상호명</td>
							<td>${rep.fullCompany}</td>
							<td class="title">통신판매업번호</td>
							<td>-</td>
						</tr>
						<tr>
							<td class="title">대표자</td>
							<td>${rep.rep_name }</td>
							<td class="title">대표전화</td>
							<td>${rep.rep_phone }</td>
							
						</tr>
						<tr>
							<td class="title">사업장소재지</td>
							<td colspan="3">${rep.companyAddress }</td>
						</tr>
					</table>
				</div>
				<hr>
			</div>
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<p>담기 성공</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal" onclick="reloadPage()">닫기</button>
							<a href="../SidServlet?command=list_all_basket"
								class="btn btn-primary">장바구니로 이동</a>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
		</div>
		<%@ include file="../include/footer.jsp"%>
</body>


<script>
		
				//아무것도 없는데 장바구니로 누를때 방지용
				var flag=0;
				var count=0;	
				var endCount=$("#color_count").val();
				var totalCount=0;
				var retail=parseInt(${product.retail });
				var wholesale=parseInt(${product.wholesale});
				var standard=parseInt(${product.standard});
				var cost=0;
				var stock=parseInt(${product.stock});
		$("#totalCost").text(retail+"원");
				
			function reloadPage() {
				location.reload();
			}
			
			
		
		
			$(document).on('click', '.removeColor', function(e) {
		
				var count = $("#color_count").val();
				$("#colorDiv" + count).remove();
				$("#color_count").val(parseInt($("#color_count").val()) - 1);
		
			});
		
			$(function(){
				$(".coldiv").each(function() {
					$(this).hide();
				});
				
				//연관상품이 없는 상품일 경우 바로 장바구니로 가기 가능
				if(endCount==1){
					count=1;
					flag=1;
				}
				
			});
		//색상 보이기
			function addColor(id) {
				count++;
				if(count>0){
					flag=1;
				}
				$("#div"+id).find('.colorQuantity').val(1);
				$("#div"+id).show();
				$(".colorQuantity").trigger("change");
			}
			function hideColorDiv(id){
				count--;
				if(count<=0){
					flag=0;
				}
				$("#div"+id).hide();
				$("#div"+id).find('.colorQuantity').val(0);
				$(".colorQuantity").trigger("change");
			}
			//수량 조절
			$(document).on('change', '.colorQuantity', function(e) {
				
				
				var colorStock=parseInt($(this).siblings('.colorStock').val());
				if(colorStock!=-1){
					
				if(this.value>colorStock){
						alert("재고량 부족");
						this.value=colorStock;
					}
				
				}
				totalCount=0;
				
				$(".colorQuantity").each(function() {
					totalCount+=parseInt(this.value);
				});
								
				if(totalCount>=standard){
					cost=totalCount*wholesale;
				}else{
					cost=totalCount*retail;
				}
				$("#totalCost").text(cost+"원");
				
			})
		
			function toSection(seq) {
				var offset = $("#section" + seq).offset();
				$('html, body').animate({
					scrollTop : offset.top
				}, 400);
		
			}
		
			// 장바구니 담기
			function toBasket(id) {
				if(flag==1){
					$.ajax({
						type : "POST",
						url : "../SidServlet?command=toBasket",
						data : $("#basket").serialize(),
						success : function(result) {
							if (result == 1) {
								$('#myModal').modal('show');
			
							}else if(result==-1){
								alert("로그인 하세요");
							
							} else{
								alert("색상을 선택하세요")
							}
						},
						error : function(request, status, error) {
							alert("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
						}
					});
				}else{
					alert("옵션을 선택하세요.");
				}
				
			}
		
		
			$("#selectpicker").change(function() {
				var value = $("#selectpicker option:selected").val();
				if (!(value == "색상선택")) {
					$(".color_quantity").each(function() {
						if (this.value == value) {
						}
					});
					$("#color_choice").append('<div class="color_quantity col-sm-6" >' + value + '</div><div class="col-sm-6" style="padding-left: 0"><input type="number" name="quantity"  class="quantity col-sm-9" min="1" step="1" value="1"></div>');
				}
			});
		
		
			/* 	${"#red"}.click(function(){
					var value=$("#selectpicker option:selected").val();
					$("#color_choice").append(value);
				})			
				
				${"#orange"}.click(function(){
					var value=$("#selectpicker option:selected").val();
					console.log("picker:"+value);
					$("#color_choice").append(value);
				})	 */
		</script>


</html>
