<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>SID-TEX</title>



<!-- footer css -->
<link rel="stylesheet"
	href="../css/footer-distributed-with-address-and-phones.css">

<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

<link href="http://fonts.googleapis.com/css?family=Cookie"
	rel="stylesheet" type="text/css">

<style>
body {
	padding-left: 0px;
	padding-right: 0px;
}

.first {
	color: rgb(172, 172, 172);
	font-size: 9pt;
}

.last {
	font-size: 9pt;
	color: grey;
}

.footer_h4 {
	font-size: 9pt;
	color: grey;
}

#footer {
	color: grey;
}
p{
	font-size:9pt;
	color: grey;
}
</style>
</head>
<body>
	</div>

	<div style="line-height: 50%; padding-top: 90px; padding-bottom: 50px"
		id="footer">
		<div class="text-center">
			<img src="../mark/fair2.gif" style="width: 40px; heigh: 40px">
			<img
				src='http://image.inicis.com/mkt/certmark/escrow/escrow_74x74_color.png'
				border="0" alt="클릭하시면 이니시스 결제시스템의 유효성을 확인하실 수 있습니다."
				style="cursor: hand; width: 40px; height: 40px"
				Onclick="javascript:window.open('https://mark.inicis.com/mark/escrow_popup.php?mid=sidtexsidt','mark','scrollbars=no,resizable=no,width=565,height=683')">
			<img src="../mark/cash.jpg" style="width: 40px; height: 40px;">
			 <br> <br>
			<p>(본 사이트는 구글 Chrome(크롬) 브라우저에 최적화 되어있으므로 Chrome(크롬) 브라우저의 사용을 권장합니다.)<br><br></p>
		</div>
		<div class="text-center ">

			<p class="text-center"></p>
			<h4 class="footer_h4">
				(주)시드월드엔터프라이즈 &nbsp;&nbsp;<span>|</span>&nbsp;&nbsp;사업자등록번호
				815-81-00599 &nbsp;&nbsp;<span>|</span>&nbsp;&nbsp;통신판매업신고번호 :
				2016-서울성동-01177&nbsp;&nbsp;<span>|</span>&nbsp;&nbsp; 대표이사 : 고예휘
				&nbsp;&nbsp;<span>|</span> &nbsp;&nbsp;<a
					href="http://www.ftc.go.kr/info/bizinfo/communicationList.jsp">사업자등록정보확인</a>
			</h4>
			<h4 class="footer_h4">
				주소 : 서울특별시 성동구 왕십리로 222 에이치아이티관 비234호&nbsp;&nbsp; <span>|</span>&nbsp;&nbsp;
				대표전화 : 070-8808-2222 &nbsp;&nbsp;<span>|</span>&nbsp;&nbsp; 이메일 : <a
					href="mailto:support@sid-tex.com">support@sid-tex.com</a>
			</h4>
			<br>
			<p class="last">
				Copyright &copy; <span style="font-weight: bold;">SID Corp.</span>
				All Rights Reserved.
			</p>
		</div>
	</div>

</body>
</html>