<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1000px user-scalable=yes">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>SID-TEX</title>

<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="../js/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<script
   src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/docs.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../js/ie10-viewport-bug-workaround.js"></script>
<script src="../js/colResizable-1.6.min.js"></script>
<link rel="stylesheet"
   href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

<!-- table sorter -->
<link rel="stylesheet" href="../css/theme.default.css">
<script type="text/javascript" src="../js/table/jquery.tablesorter.js"></script>
<script type="text/javascript" src="../js/table/widget-filter.js"></script>
<script type="text/javascript" src="../js/table/widget-resizable.js"></script>
<script type="text/javascript" src="../js/table/widget-stickyHeaders.js"></script>
<script type="text/javascript" src="../js/table/widget-print.js"></script>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<link href="../css/custom.css" rel="stylesheet">
</head>

<style>
@import url(http://fonts.googleapis.com/earlyaccess/hanna.css);
body {
   padding-top: 70px;
   font-family: 'Hanna', serif;
}
#alarm_badge {
	background-color: #ff9999;
}
#resizeTable,#resizeTable1{
	font-family: 'Hanna', serif;
}
</style>
<body>
   <div class="all">
      <div class="container">
         <div class="text-center">
            <nav class="navbar navbar-static-top">
               <a class="navbar-brand" href="../SidServlet?command=main"> <img
                  alt="Brand" src="../img/logo.gif" style="height: 70px;">
               </a>
               <ul class="nav navbar-nav navbar-right">
                  <%
                     if (session.getAttribute("email") == null) {
                  %>
                  <li class="active"><a href="../member/login.jsp">로그인</a></li>
                  <%
                     } else {
                  %>
                  <li><a href="../SidServlet?command=list_unread_alarm"><i class="fa fa-bell-o"></i><span class="badge" id="alarm_badge"><%if((int)session.getAttribute("alarm")>0){out.print(session.getAttribute("alarm"));}%></span></a></li>
                  <li><a href="../SidServlet?command=logout">로그아웃</a></li>
                  <%
                     }
                  %>
                  <%
                     if (session.getAttribute("admin") != null
                           && (session.getAttribute("admin").equals(0) || session.getAttribute("admin").equals(1))) {
                  %>
                  <li><a href="../admin/admin_main.jsp">관리자 페이지</a>
                  <li><a href="../SidServlet?command=purchase_list&state=0">판매자 페이지</a>
                  <li><a href="../SidServlet?command=list_all_basket">마이페이지</a></li>
                  <%
                     } else if (session.getAttribute("admin") != null
                           && (session.getAttribute("admin").equals(2))) {
                 
                  if (session.getAttribute("emailChk")!=null&&session.getAttribute("emailChk").equals(1)) {
                  %>
                  <li><a href="../member/emailAuthentication.jsp">이메일 인증하기</a></li>
                  <%
                        } else {
                  %>

				<%if(session.getAttribute("repflag")!=null&&session.getAttribute("repflag").equals(1)){ %>
						
                          <li><a href="../SidServlet?command=purchase_list&state=0">판매자 페이지</a>
                  <li><a href="../SidServlet?command=list_all_basket">마이페이지</a></li>
                  <%
				}else{
					%>
					
					 <li><a href="#" onclick="alert('사업자등록번호 검토중입니다.')">판매자 페이지</a>
                  <li><a href="../SidServlet?command=list_all_basket">마이페이지</a></li>
					<%
					
				}
                        }
            
                     } else if (session.getAttribute("admin") != null && session.getAttribute("admin").equals(4)) {
                        if (session.getAttribute("emailChk")!=null&&session.getAttribute("emailChk").equals(1)) {
                  %>
                     <li><a href="../member/emailAuthentication.jsp">이메일 인증하기</a></li>
                  <%
                        } else {
                  %>

                  <li><a href="../SidServlet?command=list_all_basket">마이페이지</a></li>
                  <%
                        }
                     } else if (session.getAttribute("admin") != null && session.getAttribute("admin").equals(3)) {
                         if (session.getAttribute("emailChk")!=null&&session.getAttribute("emailChk").equals(1)) {
                             %>
                             <li><a href="../member/emailAuthentication.jsp">이메일 인증하기</a></li>
                             <%
                                   } else {
                             %>

                             <li><a href="../SidServlet?command=list_all_basket">마이페이지</a></li>
                             <%
                                   }
                     }
                  %>
                  <li><a href="../BoardServlet?command=board_list">고객센터</a></li>
               </ul>
            </nav>
         </div>
         <%-- <div class="inner" style="padding:30px">
               <a href="../home/index.jsp"><h3 class="masthead-brand">SID-TEX</h3></a>
               <ul class="nav masthead-nav">
                  <%
                     if (session.getAttribute("email") == null) {
                  %>
                  <li class="active"><a href="../member/login.jsp">로그인</a></li>
                  <%
                     } else {
                        
                  %>
                  <%=session.getAttribute("email") %>
                  <li><a href="../SidServlet?command=logout">로그아웃</a></li>
                  <li><a href="../mypage/myinfo.jsp">마이페이지</a></li>
                  <%
                     }
                  %>
                  <%
                     if (session.getAttribute("admin")!=null&&session.getAttribute("admin").equals(0)) {
                  %>
                  <li><a href="../admin/admin_main.jsp">관리자 페이지</a>
                     <li><a href="../product/product_main.jsp">판매자 페이지</a>
                  <%
                      }else if(session.getAttribute("admin")!=null&&session.getAttribute("admin").equals(1)){
                         %>
                         
                  
                  <li><a href="../product/product_main.jsp">판매자 페이지</a>
                         <%
                      }
                   %>
                  
                  <li><a href="../mypage/customer.jsp">고객센터</a></li>
               </ul>
            
         </div> --%>

         <hr>
         </body>
         <script>
         $(function(){
        	 $.ajax({
     			url : "../SidServlet?command=check_alarm",
     			success : function(data) {
     				if (data > 0) {
     				} else {
     				}
     			}
     		});
			/* var host = location.host.toLowerCase();
			var currentAddress = location.href;
			if (host.indexOf("www")== -1)
			{
			currentAddress = currentAddress.replace("//","//www.");
			location.href = currentAddress;
			}  */
         });
         </script>
</html>