<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="../css/cover.css" rel="stylesheet">
<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="../js/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/docs.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../js/ie10-viewport-bug-workaround.js"></script>
<script src="../js/colResizable-1.6.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link href="../css/custom.css" rel="stylesheet">
<!-- table sorter -->
<link rel="stylesheet" href="../css/theme.default.css">
<script type="text/javascript" src="../js/table/jquery.tablesorter.js"></script>
<script type="text/javascript" src="../js/table/widget-filter.js"></script>
<script type="text/javascript" src="../js/table/widget-resizable.js"></script>
<script type="text/javascript" src="../js/table/widget-stickyHeaders.js"></script>
<script type="text/javascript" src="../js/table/widget-print.js"></script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>회원 관리</title>

</head>
<body>
	<div class="text-center">
		<h2>Email 중복확인</h2>
		<form method="post" action="../SidServlet?command=email_check"
			name="frm">
			<div class="form-group form-inline">
				<input type="text" class="form-control" name="email"
					placeholder="E-mail">

				<button type="submit" class="btn btn-primary"
					onclick="return check()">확인</button>
			</div>
			<br>
			<c:if test="${result==1}">
				<script type="text/javascript">
					opener.document.frm.reemail.value = "";
				</script>
			${email}는 이미 사용 중인 이메일입니다.
		</c:if>
			<c:if test="${result==-1}">
			${email}는 사용 가능한 이메일입니다.
			<input type="button" class="btn btn-primary" value="사용함"
					onclick="idok()">
			</c:if>
		</form>
	</div>

	<script type="text/javascript">
		function idok() {
			opener.frm.reemail.value = "${email}";
			opener.frm.email.value = "${email}";
	
			$(opener.document).find("#email_ex").show();
	
	
	
			self.close();
		}
		function check() {
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if (re.test(document.frm["email"].value)) {
			} else {
				alert("잘못된 이메일 형식입니다")
				return false;
			}
		}
	</script>

</body>
</html>