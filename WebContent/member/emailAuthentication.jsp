<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../include/header.jsp"%>
<!DOCTYPE html>
<html>
<style>
.form-gap {
	padding-top: 70px;
}
</style>
<body>

	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div id="before">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="text-center">
								<h1>
									<i class="fa fa-lock"></i>
								</h1>
								<h2 class="text-center">서비스를 이용하시려면 이메일 인증을 하셔야 합니다.</h2>
								<p>가입하신 이메일로 인증번호를 보내드렸습니다. 인증번호를 입력해주세요.</p>
								<div class="panel-body">
									<div id="alertAuth" style="display: none">
										<div class="alert alert-danger" role="alert">
											<span class="glyphicon glyphicon-exclamation-sign"
												aria-hidden="true"></span> <span class="sr-only">Error:</span>
											인증번호가 다릅니다
										</div>
									</div>
									<div class="form-group">
										<input type="text" class="form-control" name="emailChk"
											id="emailChk" placeholder="인증번호 입력">
									</div>

									<div class="form-group">
										<input name="recover-submit" id="authSubmit"
											class="btn btn-lg btn-primary btn-block" value="확인"
											type="button">
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="after">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="text-center">
								<h1>
									<i class="fa fa-unlock"></i>
								</h1>
								<h2 class="text-center">인증 완료</h2>
								<p>감사합니다</p>
								<div class="panel-body">
								<a href="../SidServlet?command=main" class="btn btn-lg btn-primary btn-block">메인 화면으로</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<%@ include file="../include/footer.jsp"%>
</body>
<script>
	$("#after").hide();
	$(function() {
		//이메일 보내기 버튼 클릭
		$("#authSubmit").click(function(e) {

			var chk = $("#emailChk").val();

			//이메일로 임시 비밀번호 보내기
			$.ajax({
				url : '../SidServlet?command=update_auth',
				data : 'chk=' + chk,
				success : function(data) {
					if (data == "1") {
						$("#after").show();
						$("#before").hide();
					} else {
						$("#alertAuth").show();
					}
				}
			});


		})

	});
</script>

</html>
