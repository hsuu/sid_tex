<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../include/header.jsp"%>
<!DOCTYPE html>
<html>
<body>

	<%
		Cookie[] cookies = request.getCookies();
		String SaveEmail = "";
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				String key = cookies[i].getName();
				if (key != null && key.trim().equals("SaveEmail")) {
					SaveEmail = cookies[i].getValue();
					break;
				}
			}
		}
	%>

	<div class="container">
		<div class="row vertical-offset-100">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">로그인 하세요</h3>
					</div>
					<div class="panel-body">
						<form method="post" action="../SidServlet?command=login">
							<fieldset>
								<%
									if (request.getAttribute("result") != null && request.getAttribute("result").equals("0")) {//로그인 실패시
								%>
								<div>
									<div class="alert alert-danger" role="alert">
										<span class="glyphicon glyphicon-exclamation-sign"
											aria-hidden="true"></span> <span class="sr-only">Error:</span>
										이메일 혹은 패스워드를 확인해 주세요.
									</div>
								</div>

								<%
									}
								%>
								<%
									if (request.getAttribute("result") != null && request.getAttribute("result").equals("1")) {//탈퇴한 회원일시
								%>
								<div>
									<div class="alert alert-danger" role="alert">
										<span class="glyphicon glyphicon-exclamation-sign"
											aria-hidden="true"></span> <span class="sr-only">Error:</span>
										탈퇴한 회원입니다. 고객센터에 문의해주세요.
									</div>
								</div>

								<%
									}
								%>
								<div class="form-group">
									<input class="form-control" placeholder="E-mail" name="email" value="<%=SaveEmail%>"
										type="text"/>
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="Password" name="pwd"
										type="password" value=""/>
								</div>
								<div class="checkbox">
									<label> <input name="remember" type="checkbox"
										value="on"<%if(SaveEmail.length()!=0)out.println("checked"); %>/>아이디 저장
									</label>
								</div>
								<div class="text-center">
									<button type="submit" class="btn btn-primary"
										style="width: 100%">로그인</button>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
				<div class="text-center panel panel-danger">
					<div class="panel-body">
						<a href="../member/join.jsp" type="button" class="btn btn-info"
							style="width: 100%">회원가입</a> <br> <br> <a
							href="../member/findPassword.jsp" type="button"
							class="btn btn-danger" style="width: 100%">아이디/패스워드 찾기</a>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>

	<%@ include file="../include/footer.jsp"%>