<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../include/header.jsp"%>
<!DOCTYPE html>
<html>
<style>
.form-gap {
	padding-top: 70px;
}
</style>
<body>

	<div>
		<div class="row">

			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="text-center">
							<h1>
								<i class="fa fa-user"></i>
							</h1>
							<h2 class="text-center">아이디를 잃어버리셨습니까?</h2>
							<p>고객센터에 문의해주세요</p>
							<div class="panel-body">
								<div class="form-group">
									<a href="../customer/consult_phone.jsp"
										class="btn btn-lg btn-primary btn-block">고객센터 바로가기</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-md-offset-3">
				<div id="beforeSend">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="text-center">
								<h1>
									<i class="fa fa-lock"></i>
								</h1>
								<h2 class="text-center">비밀번호를 잃어버리셨습니까?</h2>
								<p>가입하신 이메일로 임시 비밀번호를 보내드립니다.</p>
								<div class="panel-body">
									<div class="alert alert-danger" role="alert" id="emailCheck">
										<span class="glyphicon glyphicon-exclamation-sign"
											aria-hidden="true"></span> <span class="sr-only">Error:</span>
										존재하지 않는 이메일입니다.
									</div>
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon"><i
												class="glyphicon glyphicon-envelope color-blue"></i></span> <input
												id="email" name="email" placeholder="email address"
												class="form-control" type="email">
										</div>
									</div>
									<div class="form-group">
										<input name="recover-submit" id="sendPassword"
											class="btn btn-lg btn-primary btn-block" value="임시 비밀번호 받기"
											type="button">
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="afterSend">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="text-center">
								<h1>
									<i class="fa fa-unlock"></i>
								</h1>
								<h2 class="text-center">가입하신 이메일로 임시 <br>비밀번호를 보내드렸습니다.</h2>
								<p>내정보 페이지에서 비밀번호를 바꿔주세요.</p>
								<div class="panel-body">


									<div class="form-group">
										<a href="../member/login.jsp"
											class="btn btn-lg btn-primary btn-block">로그인 페이지 바로가기</a>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

</div>
</body>
<script>
	$(function() {

		$("#afterSend").hide();
		$("#emailCheck").hide();
		//이메일 보내기 버튼 클릭
		$("#sendPassword").click(function(e) {
			var email = $("#email").val();

			//이메일로 임시 비밀번호 보내기
			$.ajax({
				url : '../SidServlet?command=send_password',
				data : 'email=' + email,
				success : function(data) {
					if (data == "1") {
						$("#afterSend").show();
						$("#beforeSend").hide();
					} else if (data == "-1") {
						$("#emailCheck").show();
					} else {
						alert("error");
					}
				}
			});


		})

	});
</script>

</html>
		<%@ include file="../include/footer.jsp"%>