<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../include/header.jsp"%>
<!DOCTYPE html>
<html>
<head>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<style>
.table th, td {
	text-align: center;
}

.table td {
	font-size: 10pt;
}
</style>
</head>
<body>
	<div class="col-md-12">
	<br>
	<br>
	<div class="panel panel-default"
								style="display: center; margin: auto">
								<div class="panel-body text-center">
								
								<h1><img
                  alt="Brand" src="../img/logo.gif" style="height: 70px;">에 오신걸 환영합니다.</h1>
								<p>회원 가입하신 후 Sid-tex의 다양한 서비스를 만나보세요.</p>
								</div>
								</div>
		<div class="col-md-6">
			<h3>구매 회원</h3>
			<div class="alert alert-info col-md-12" role="alert">
				<div class="col-md-6">
					<label class="radio-inline"> <input type="radio"
						name="inlineRadioOptions" id="inlineRadio1" value="option1">
						일반
					</label>
				</div>
				<label class="radio-inline"> <input type="radio"
					name="inlineRadioOptions" id="inlineRadio2" value="option2">
					사업자
				</label>
			</div>
		</div>
		<div class="col-md-6">
			<h3>판매 회원</h3>
			<div class="alert alert-info col-md-12" role="alert">
				<div class="col-md-6">
					<label class="radio-inline"> <input type="radio"
						name="inlineRadioOptions" id="inlineRadio3" value="option3">
						사업자
					</label>
				</div>
			</div>
		</div>

		<ul class="list-inline pull-right">
			<li><button type="button" class="btn btn-primary next-step"
					onclick="return checkSort()">다음으로</button></li>
		</ul>
	</div>
</body>

<script>
function checkSort(){
	
	var check=$("input[name=inlineRadioOptions]:checked").val();
	
	switch(check){
	case "option1":
		
		location.href="join_personal_buyer.jsp";
		break;
case "option2":
		
		location.href="join_business_buyer.jsp";
		break;
case "option3":
	
	location.href="join_business_seller.jsp";
	break;
	
	}
}
</script>
</html>
<%@ include file="../include/footer.jsp"%>
